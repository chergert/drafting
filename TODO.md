# TODO

## Documents

We have the plumbing for saving files back to disk, but there is not UI yet to activate those actions.

## Preview

Currently the `DraftItem` has a way to serialize for display.
That often removes things we know can break `GtkBuilder` without the application in-process.
More work will be needed here to get things less breakable by users.

When serializing for display, if there is a type we do not know about, but have a template for (from another document) we can serialize that inline.
That way we don't need the related object loaded in-process just to display.
When the related object changes, we'll need to update the builder of the object too.

## Panels

We still need to port our panel engine from Builder/Dazzle to GTK 4.

### Properties

For many common properties, we will have overrides to provide an easier, more aesthetically pleasing experience.
For the other cases, we will need to automatically generate some amount of UI.
That UI may be a property grid similar to the Inspector.

#### Object Properties

This is where properties specific to the widget are placed.
For example, this may be the label text and attributes for a `GtkLabel`.

#### Layout

This is where layout specific properties are set.
That includes margin, expand, and alignment for widgets.

For layout managers that we discover, that also includes properties known to the layout manager.
That could include box or grid positions, constraints, and more for any other type of layouts we support.
For custom layout managers, this may not be something we can display without additional help from the layout manager.

#### Signals

The signal section will contain a list of signals grouped by class.
Clicking on a signal will cause the signal dialog to be displayed.
If it was a signal specification, a new signal is to be inserted.
If it was a signal instance, that signal will be edited.

#### Accessibility

Accessibility roles and associated information belong here.
Things like shortcuts belong here as they are a11y too.


### Project

The project panel should consist of information about the loaded project as a tree.
It might include all the objects, as a tree.
It might also include a virtual folder of CSS files.
Depending if we allow opening `.gresource.xml` files, it may show information about those too.


### Palette

The widget palette still needs implementation.
It will start as a categorized grid of widgets until we have alternative designs.
It should be possible to drag the widget from the palette to the designer.
For accessibility, double clicking or activating the palette item should insert into the current selection.


### History

This history panel is a view to display information on what has happened in the document.
It is mostly for debug purposes but could be useful in the future to jump back to where you try an experiment.


## Canvas

The canvas still lacks almost all features beyond picking.

 * Drag destination for widget palette.
 * Showing grid lines for alignment
 * Cut/Copy/Paste/Delete operations
 * Moving between pages in a stack/notebook/etc
 * It's not clear how we want to handle multiple documents yet.
   But we know we need the ability to load them so that we can serialize contents from related type when displaying w/o access to related object in-process.
 * We need a context menu for operations on a widget.
 
 
## Menuing

We would like a menu editor so we can adjust that without having to display them on the canvas.


## Objects

For objects that are not widgets, we need a way to display that to the user and allow editing using the properties panel.
We might choose to display them on the canvas.


