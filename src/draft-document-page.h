/* draft-document-page.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <drafting.h>
#include <libpanel.h>

G_BEGIN_DECLS

#define DRAFT_TYPE_DOCUMENT_PAGE (draft_document_page_get_type())

G_DECLARE_FINAL_TYPE (DraftDocumentPage, draft_document_page, DRAFT, DOCUMENT_PAGE, PanelWidget)

GtkWidget     *draft_document_page_new          (DraftDocument     *document);
DraftDocument *draft_document_page_get_document (DraftDocumentPage *self);
DraftCanvas   *draft_document_page_get_canvas   (DraftDocumentPage *self);

G_END_DECLS
