/* draft-application.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "build-ident.h"

#include <drafting.h>
#include <libpanel.h>
#include <glib/gi18n.h>

#include "draft-application.h"
#include "draft-session.h"
#include "draft-window.h"

struct _DraftApplication
{
  AdwApplication parent_instance;
  DraftSession *session;
  GSettings *settings;
};

G_DEFINE_TYPE (DraftApplication, draft_application, ADW_TYPE_APPLICATION)

static void
draft_application_activate (GApplication *app)
{
  DraftApplication *self = (DraftApplication *)app;
  DraftWindow *window;

  g_assert (DRAFT_IS_APPLICATION (self));

  window = draft_session_get_active_window (self->session);

  if (window == NULL)
    window = draft_session_create_window (self->session);

  gtk_window_present (GTK_WINDOW (window));
}

static gboolean
style_variant_to_color_scheme (GValue   *value,
                               GVariant *variant,
                               gpointer  user_data)
{
  if (g_strcmp0 (g_variant_get_string (variant, NULL), "dark") == 0)
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_DARK);
  else
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_LIGHT);

  return TRUE;
}

static void
draft_application_startup (GApplication *app)
{
  DraftApplication *self = (DraftApplication *)app;
  AdwStyleManager *style_manager;
  g_autoptr(GtkCssProvider) css = NULL;
  g_autoptr(GAction) theme = NULL;

  g_assert (DRAFT_IS_APPLICATION (app));

  self->session = draft_session_new ();
  self->settings = g_settings_new ("org.gnome.Drafting");

  theme = g_settings_create_action (self->settings, "style-variant");
  g_action_map_add_action (G_ACTION_MAP (self), theme);

  G_APPLICATION_CLASS (draft_application_parent_class)->startup (app);

  panel_init ();
  draft_init ();

  css = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css, "/css/stylesheet.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (css),
                                              GTK_STYLE_PROVIDER_PRIORITY_THEME+1);

  style_manager = adw_style_manager_get_default ();
  g_settings_bind_with_mapping (self->settings, "style-variant",
                                style_manager, "color-scheme",
                                G_SETTINGS_BIND_GET,
                                style_variant_to_color_scheme,
                                NULL, NULL, NULL);
}

static void
draft_application_shutdown (GApplication *app)
{
  DraftApplication *self = (DraftApplication *)app;

  g_assert (DRAFT_IS_APPLICATION (app));

  G_APPLICATION_CLASS (draft_application_parent_class)->shutdown (app);

  g_clear_object (&self->session);
  g_clear_object (&self->settings);

  panel_finalize ();
}

DraftApplication *
draft_application_new (void)
{
  return g_object_new (DRAFT_TYPE_APPLICATION,
                       "application-id", APP_ID,
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       "resource-base-path", "/org/gnome/drafting",
                       NULL);
}

static void
draft_application_about_action (GSimpleAction *action,
                                GVariant      *param,
                                gpointer       user_data)
{
  static const char *artists[] = {
    "Christian Hergert",
    NULL
  };
  static const char *authors[] = {
    "Christian Hergert",
    NULL
  };
  static const char *documenters[] = {
    "Christian Hergert",
    NULL
  };

  GtkAboutDialog *dialog;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (DRAFT_IS_APPLICATION (user_data));

  dialog = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                         "artists", artists,
                         "authors", authors,
                         "documenters", documenters,
                         "program-name", PACKAGE_NAME,
                         "version", DRAFT_BUILD_IDENTIFIER,
                         "title", _("About Drafting"),
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "website", "https://wiki.gnome.org/Apps/Drafting",
                         NULL);
  gtk_window_present (GTK_WINDOW (dialog));
}

static void
draft_application_window_added (GtkApplication *app,
                                GtkWindow      *window)
{
  g_assert (ADW_IS_APPLICATION (app));
  g_assert (GTK_IS_WINDOW (window));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (window), "devel");
#endif

  GTK_APPLICATION_CLASS (draft_application_parent_class)->window_added (app, window);
}

static void
draft_application_open (GApplication  *application,
                        GFile        **files,
                        gint           n_files,
                        const gchar   *hint)
{
  DraftApplication *self = (DraftApplication *)application;
  DraftWindow *window = NULL;

  g_assert (DRAFT_IS_APPLICATION (self));
  g_assert (files != NULL);
  g_assert (n_files > 0);

  window = draft_session_create_window (self->session);
  draft_window_import (window, files, n_files);
  gtk_window_present (GTK_WINDOW (window));
}

static void
draft_application_help_action (GSimpleAction *action,
                               GVariant      *param,
                               gpointer       user_data)
{
  DraftApplication *self = user_data;
  GtkWindow *window;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (DRAFT_IS_APPLICATION (user_data));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));
  gtk_show_uri (window, "help:drafting", GDK_CURRENT_TIME);
}

static GActionEntry action_entries[] = {
  { "about", draft_application_about_action },
  { "help", draft_application_help_action },
};

static void
draft_application_class_init (DraftApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);
  GtkApplicationClass *gtk_app_class = GTK_APPLICATION_CLASS (klass);

  app_class->activate = draft_application_activate;
  app_class->open = draft_application_open;
  app_class->startup = draft_application_startup;
  app_class->shutdown = draft_application_shutdown;

  gtk_app_class->window_added = draft_application_window_added;
}

static void
draft_application_init (DraftApplication *self)
{
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   action_entries,
                                   G_N_ELEMENTS (action_entries),
                                   self);
}
