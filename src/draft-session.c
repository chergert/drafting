/* draft-session.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "draft-session.h"

struct _DraftSession
{
  GObject parent_instance;
  GList *windows;
};

G_DEFINE_TYPE (DraftSession, draft_session, G_TYPE_OBJECT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_session_new:
 *
 * Create a new #DraftSession.
 *
 * Returns: (transfer full): a newly created #DraftSession
 */
DraftSession *
draft_session_new (void)
{
  return g_object_new (DRAFT_TYPE_SESSION, NULL);
}

static void
draft_session_finalize (GObject *object)
{
  DraftSession *self = (DraftSession *)object;

  G_OBJECT_CLASS (draft_session_parent_class)->finalize (object);
}

static void
draft_session_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  DraftSession *self = DRAFT_SESSION (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_session_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  DraftSession *self = DRAFT_SESSION (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_session_class_init (DraftSessionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_session_finalize;
  object_class->get_property = draft_session_get_property;
  object_class->set_property = draft_session_set_property;
}

static void
draft_session_init (DraftSession *self)
{
}

DraftWindow *
draft_session_create_window (DraftSession *self)
{
  g_return_val_if_fail (DRAFT_IS_SESSION (self), NULL);

  return draft_window_new (DRAFT_APPLICATION_INSTANCE);
}

void
draft_session_add_window (DraftSession *self,
                          DraftWindow  *window)
{
  g_return_if_fail (DRAFT_IS_SESSION (self));
  g_return_if_fail (!window || DRAFT_IS_WINDOW (window));

  if (window == NULL)
    window = draft_session_create_window (self);

  gtk_window_present (GTK_WINDOW (window));
}

DraftWindow *
draft_session_get_active_window (DraftSession *self)
{
  GList *windows;

  g_return_val_if_fail (DRAFT_IS_SESSION (self), NULL);

  windows = gtk_application_get_windows (GTK_APPLICATION (DRAFT_APPLICATION_INSTANCE));

  for (const GList *iter = windows; iter; iter = iter->next)
    {
      if (DRAFT_IS_WINDOW (iter->data))
        return iter->data;
    }

  return NULL;
}
