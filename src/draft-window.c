/* draft-window.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>
#include <libpanel.h>

#include "draft-document-page.h"
#include "draft-window.h"

struct _DraftWindow
{
  GtkApplicationWindow    parent_instance;

  DraftProject           *project;

  DraftAccessibilityPage *a11y_page;
  DraftBehaviorPage      *behavior_page;
  PanelDock              *dock;
  PanelGrid              *grid;
  DraftHistoryPage       *history_page;
  DraftLayoutPage        *layout_page;
  DraftObjectPage        *object_page;
  DraftProjectPage       *project_page;
  DraftSignalsPage       *signals_page;
  GtkMenuButton          *menu_button;
  GtkMenuButton          *themes_button;
};

G_DEFINE_TYPE (DraftWindow, draft_window, ADW_TYPE_APPLICATION_WINDOW)

enum {
  PROP_0,
  PROP_PROJECT,
  PROP_PROJECT_THEME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftWindow *
draft_window_new (DraftApplication *application)
{
  return g_object_new (DRAFT_TYPE_WINDOW,
                       "application", application,
                       NULL);
}

static void
draft_window_action_import_response_cb (DraftWindow          *self,
                                        int                   response_id,
                                        GtkFileChooserNative *chooser)
{
  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (GTK_IS_FILE_CHOOSER_NATIVE (chooser));

  if (response_id == GTK_RESPONSE_ACCEPT)
    {
      g_autoptr(GListModel) files = NULL;
      guint n_items;

      files = gtk_file_chooser_get_files (GTK_FILE_CHOOSER (chooser));
      n_items = g_list_model_get_n_items (files);

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(GFile) file = g_list_model_get_item (files, i);
          g_autofree char *uri = g_file_get_uri (file);

          draft_project_load_file_async (self->project, file, NULL, NULL, NULL);
          gtk_recent_manager_add_item (gtk_recent_manager_get_default (), uri);
        }
    }

  gtk_native_dialog_destroy (GTK_NATIVE_DIALOG (chooser));
}

static void
draft_window_action_import (GtkWidget  *widget,
                            const char *action_name,
                            GVariant   *param)
{
  DraftWindow *self = (DraftWindow *)widget;
  GtkFileChooserNative *dialog;

  g_assert (DRAFT_IS_WINDOW (self));

  dialog = gtk_file_chooser_native_new (_("Import Template"),
                                        GTK_WINDOW (self),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        _("Import"),
                                        _("Cancel"));
  gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (dialog), TRUE);
  gtk_native_dialog_set_modal (GTK_NATIVE_DIALOG (dialog), TRUE);

  g_signal_connect_object (dialog,
                           "response",
                           G_CALLBACK (draft_window_action_import_response_cb),
                           self,
                           G_CONNECT_SWAPPED);

  gtk_native_dialog_show (GTK_NATIVE_DIALOG (dialog));
}

static void
draft_window_action_undo (GtkWidget  *widget,
                          const char *action_name,
                          GVariant   *param)
{
  DraftWindow *self = (DraftWindow *)widget;
  g_assert (DRAFT_IS_WINDOW (self));
  draft_project_undo (self->project);
}

static void
draft_window_action_redo (GtkWidget  *widget,
                          const char *action_name,
                          GVariant   *param)
{
  DraftWindow *self = (DraftWindow *)widget;
  g_assert (DRAFT_IS_WINDOW (self));
  draft_project_redo (self->project);
}

static void
draft_window_notify_can_undo_cb (DraftWindow  *self,
                                 GParamSpec   *pspec,
                                 DraftProject *project)
{
  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_PROJECT (project));

  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "project.undo",
                                 draft_project_get_can_undo (project));
}

static void
draft_window_notify_can_redo_cb (DraftWindow  *self,
                                 GParamSpec   *pspec,
                                 DraftProject *project)
{
  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_PROJECT (project));

  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "project.redo",
                                 draft_project_get_can_redo (project));
}

static void
draft_window_action_new (GtkWidget  *widget,
                         const char *action_name,
                         GVariant   *param)
{
  DraftWindow *window;

  g_assert (DRAFT_IS_WINDOW (widget));

  window = draft_window_new (DRAFT_APPLICATION_INSTANCE);
  gtk_window_present (GTK_WINDOW (window));
}

static const char *
draft_window_get_project_theme (DraftWindow *self)
{
  DraftProjectContext *context;

  g_assert (DRAFT_IS_WINDOW (self));

  if (self->project != NULL &&
      (context = draft_project_get_context (self->project)))
    return draft_project_context_get_theme (context);

  return NULL;
}

static void
draft_window_set_project_theme (DraftWindow *self,
                                const char  *theme)
{
  DraftProjectContext *context;

  g_assert (DRAFT_IS_WINDOW (self));

  if (self->project == NULL ||
      !(context = draft_project_get_context (self->project)))
    return;

  draft_project_context_set_theme (context, theme);
  draft_project_clear_selection (self->project);
}

static void
notify_available_themes_cb (DraftWindow         *self,
                            GParamSpec          *pspec,
                            DraftProjectContext *context)
{
  g_autoptr(GMenu) submenu = NULL;
  const char * const *themes;

  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_PROJECT_CONTEXT (context));

  themes = draft_project_context_get_available_themes (context);
  submenu = g_menu_new ();

  if (themes != NULL)
    {
      for (guint i = 0; themes[i]; i++)
        {
          g_autoptr(GMenuItem) item = NULL;

          item = g_menu_item_new (themes[i], NULL);
          g_menu_item_set_action_and_target_value (item,
                                                   "project.theme",
                                                   g_variant_new_string (themes[i]));
          g_menu_append_item (submenu, item);
        }
    }

  gtk_menu_button_set_menu_model (self->themes_button, G_MENU_MODEL (submenu));
}

static void
notify_theme_cb (DraftWindow         *self,
                 GParamSpec          *pspec,
                 DraftProjectContext *context)
{
  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_PROJECT_CONTEXT (context));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT_THEME]);
}

static void
draft_window_document_added_cb (DraftWindow   *self,
                                DraftDocument *document,
                                DraftProject  *project)
{
  GtkWidget *page;

  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IS_PROJECT (project));

  page = draft_document_page_new (document);
  panel_grid_add (self->grid, PANEL_WIDGET (page));
  panel_widget_raise (PANEL_WIDGET (page));
}

static void
draft_window_document_removed_cb (DraftWindow   *self,
                                  DraftDocument *document,
                                  DraftProject  *project)
{
  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IS_PROJECT (project));

  /* TODO: Foreach page, if document matches, remove */
}

static void
draft_window_setup_themes_menu (DraftWindow *self)
{
  DraftProjectContext *context;

  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (!self->project || DRAFT_IS_PROJECT (self->project));

  if (self->project == NULL ||
      !(context = draft_project_get_context (self->project)))
    return;

  g_signal_connect_object (context,
                           "notify::available-themes",
                           G_CALLBACK (notify_available_themes_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (context,
                           "notify::theme",
                           G_CALLBACK (notify_theme_cb),
                           self,
                           G_CONNECT_SWAPPED);
  notify_available_themes_cb (self, NULL, context);
  g_object_bind_property (self, "project-theme",
                          self->themes_button, "label",
                          G_BINDING_SYNC_CREATE);
}

static void
draft_window_constructed (GObject *object)
{
  DraftWindow *self = (DraftWindow *)object;

  G_OBJECT_CLASS (draft_window_parent_class)->constructed (object);

  if (self->project == NULL)
    {
      g_autoptr(DraftProjectContext) context = draft_project_context_new ();
      self->project = draft_project_new ();
      draft_project_set_context (self->project, context);
    }

  g_signal_connect_object (self->project,
                           "notify::can-undo",
                           G_CALLBACK (draft_window_notify_can_undo_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->project,
                           "notify::can-redo",
                           G_CALLBACK (draft_window_notify_can_redo_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->project,
                           "document-added",
                           G_CALLBACK (draft_window_document_added_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->project,
                           "document-removed",
                           G_CALLBACK (draft_window_document_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  draft_window_setup_themes_menu (self);

  draft_accessibility_page_set_project (self->a11y_page, self->project);
  draft_behavior_page_set_project (self->behavior_page, self->project);
  draft_history_page_set_project (self->history_page, self->project);
  draft_layout_page_set_project (self->layout_page, self->project);
  draft_object_page_set_project (self->object_page, self->project);
  draft_project_page_set_project (self->project_page, self->project);
  draft_signals_page_set_project (self->signals_page, self->project);

  /* Ensure the zero column/page is created */
  panel_grid_column_get_row (panel_grid_get_column (self->grid, 0), 0);
}

static PanelFrame *
draft_window_create_frame_cb (PanelGrid   *grid,
                              DraftWindow *self)
{
  PanelFrame *frame;
  PanelFrameHeader *header;

  g_assert (PANEL_IS_GRID (grid));
  g_assert (DRAFT_IS_WINDOW (self));

  frame = PANEL_FRAME (panel_frame_new ());
  header = PANEL_FRAME_HEADER (panel_frame_header_bar_new ());
  panel_frame_set_header (frame, header);
  panel_frame_set_placeholder (frame,
                               g_object_new (ADW_TYPE_STATUS_PAGE,
                                             "title", _("Open or Create a Design"),
                                             "icon-name", "document-new-symbolic",
                                             NULL));

  return frame;
}

static void
draft_window_finalize (GObject *object)
{
  DraftWindow *self = (DraftWindow *)object;

  g_clear_object (&self->project);

  G_OBJECT_CLASS (draft_window_parent_class)->finalize (object);
}

static void
draft_window_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DraftWindow *self = DRAFT_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, draft_window_get_project (self));
      break;

    case PROP_PROJECT_THEME:
      g_value_set_string (value, draft_window_get_project_theme (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_window_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DraftWindow *self = DRAFT_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      self->project = g_value_dup_object (value);
      break;

    case PROP_PROJECT_THEME:
      draft_window_set_project_theme (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_window_class_init (DraftWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = draft_window_constructed;
  object_class->finalize = draft_window_finalize;
  object_class->get_property = draft_window_get_property;
  object_class->set_property = draft_window_set_property;

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The project for the window",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));

  properties [PROP_PROJECT_THEME] =
    g_param_spec_string ("project-theme",
                         "Project Theme",
                         "Project Theme",
                         "Default",
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/draft-window.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, a11y_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, behavior_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, dock);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, grid);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, history_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, layout_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, menu_button);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, object_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, project_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, signals_page);
  gtk_widget_class_bind_template_child (widget_class, DraftWindow, themes_button);

  gtk_widget_class_install_action (widget_class, "project.undo", NULL, draft_window_action_undo);
  gtk_widget_class_install_action (widget_class, "project.redo", NULL, draft_window_action_redo);
  gtk_widget_class_install_action (widget_class, "project.import", NULL, draft_window_action_import);
  gtk_widget_class_install_action (widget_class, "project.new", NULL, draft_window_action_new);
  gtk_widget_class_install_property_action (widget_class, "project.theme", "project-theme");

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK, "project.undo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK|GDK_SHIFT_MASK, "project.redo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_y, GDK_CONTROL_MASK, "project.redo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_o, GDK_CONTROL_MASK, "project.import", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_n, GDK_CONTROL_MASK|GDK_SHIFT_MASK, "project.new", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F1, 0, "app.help", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, 0, "win.reveal-start", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, GDK_SHIFT_MASK, "win.reveal-end", NULL);

  g_type_ensure (DRAFT_TYPE_ACCESSIBILITY_PAGE);
  g_type_ensure (DRAFT_TYPE_BEHAVIOR_PAGE);
  g_type_ensure (DRAFT_TYPE_HISTORY_PAGE);
  g_type_ensure (DRAFT_TYPE_LAYOUT_PAGE);
  g_type_ensure (DRAFT_TYPE_OBJECT_PAGE);
  g_type_ensure (DRAFT_TYPE_PROJECT_PAGE);
  g_type_ensure (DRAFT_TYPE_SIGNALS_PAGE);
}

static void
draft_window_init (DraftWindow *self)
{
  g_autoptr(GSettings) settings = g_settings_new ("org.gnome.Drafting");
  g_autoptr(GAction) style_action = g_settings_create_action (settings, "style-variant");
  g_autoptr(GPropertyAction) start_action = NULL;
  g_autoptr(GPropertyAction) end_action = NULL;
  GtkPopover *popover;
  GMenu *menu;

  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (self->grid,
                    "create-frame",
                    G_CALLBACK (draft_window_create_frame_cb),
                    self);

  menu = gtk_application_get_menu_by_id (GTK_APPLICATION (DRAFT_APPLICATION_INSTANCE),
                                         "draft-window-primary-menu");
  gtk_menu_button_set_menu_model (self->menu_button, G_MENU_MODEL (menu));

  gtk_widget_action_set_enabled (GTK_WIDGET (self), "project.undo", FALSE);
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "project.redo", FALSE);

  popover = gtk_menu_button_get_popover (self->menu_button);
  gtk_popover_menu_add_child (GTK_POPOVER_MENU (popover),
                              g_object_new (PANEL_TYPE_THEME_SELECTOR,
                                            "action-name", "app.style-variant",
                                            NULL),
                              "theme");

  start_action = g_property_action_new ("reveal-start", self->dock, "reveal-start");
  end_action = g_property_action_new ("reveal-end", self->dock, "reveal-end");

  g_action_map_add_action (G_ACTION_MAP (self), style_action);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (start_action));
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (end_action));
}

DraftProject *
draft_window_get_project (DraftWindow *self)
{
  g_return_val_if_fail (DRAFT_IS_WINDOW (self), NULL);

  return self->project;
}

static void
draft_window_import_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  DraftProject *project = (DraftProject *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree char *uri = NULL;
  DraftWindow *self;
  GPtrArray *ar;
  GFile *file;

  g_assert (DRAFT_IS_PROJECT (project));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  ar = g_task_get_task_data (task);

  g_assert (DRAFT_IS_WINDOW (self));
  g_assert (ar != NULL);
  g_assert (ar->len > 0);

  file = g_ptr_array_index (ar, ar->len - 1);

  if (!draft_project_load_file_finish (project, result, &error))
    {
      uri = g_file_get_uri (file);
      g_warning ("Failed to import \"%s\"", uri);
    }

  g_ptr_array_remove_index (ar, ar->len - 1);

  if (ar->len > 0)
    draft_project_load_file_async (self->project,
                                   g_ptr_array_index (ar, ar->len-1),
                                   NULL,
                                   draft_window_import_cb,
                                   g_ptr_array_ref (ar));
  else
    g_task_return_boolean (task, TRUE);
}

void
draft_window_import (DraftWindow  *self,
                     GFile       **files,
                     int           n_files)
{
  g_autoptr(GTask) task = NULL;
  GPtrArray *ar;

  g_return_if_fail (DRAFT_IS_WINDOW (self));
  g_return_if_fail (files != NULL);
  g_return_if_fail (G_IS_FILE (files[0]));
  g_return_if_fail (n_files > 0);

  ar = g_ptr_array_new_with_free_func (g_object_unref);

  task = g_task_new (self, NULL, NULL, NULL);
  g_task_set_source_tag (task, draft_window_import);
  g_task_set_task_data (task, ar, (GDestroyNotify)g_ptr_array_unref);

  for (int i = n_files - 1; i >= 0; i--)
    g_ptr_array_add (ar, g_object_ref (files[i]));

  draft_project_load_file_async (self->project,
                                 g_ptr_array_index (ar, ar->len-1),
                                 NULL,
                                 draft_window_import_cb,
                                 g_steal_pointer (&task));
}
