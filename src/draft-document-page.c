/* draft-document-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "draft-document-page.h"

struct _DraftDocumentPage
{
  PanelWidget parent_instance;
  DraftDocument *document;
  DraftCanvas *canvas;
};

G_DEFINE_TYPE (DraftDocumentPage, draft_document_page, PANEL_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_DOCUMENT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_document_page_new (DraftDocument *document)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (document), NULL);

  return g_object_new (DRAFT_TYPE_DOCUMENT_PAGE,
                       "document", document,
                       NULL);
}

static void
draft_document_page_set_document (DraftDocumentPage *self,
                                  DraftDocument     *document)
{
  g_assert (DRAFT_IS_DOCUMENT_PAGE (self));
  g_assert (DRAFT_IS_DOCUMENT (document));

  g_set_object (&self->document, document);
  draft_canvas_set_document (self->canvas, document);

  g_object_bind_property (G_OBJECT (document), "title", self, "title", G_BINDING_SYNC_CREATE);
}

static void
draft_document_page_dispose (GObject *object)
{
  DraftDocumentPage *self = (DraftDocumentPage *)object;

  g_clear_object (&self->document);

  G_OBJECT_CLASS (draft_document_page_parent_class)->dispose (object);
}

static void
draft_document_page_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftDocumentPage *self = DRAFT_DOCUMENT_PAGE (object);

  switch (prop_id)
    {
    case PROP_DOCUMENT:
      g_value_set_object (value, draft_document_page_get_document (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_document_page_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DraftDocumentPage *self = DRAFT_DOCUMENT_PAGE (object);

  switch (prop_id)
    {
    case PROP_DOCUMENT:
      draft_document_page_set_document (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_document_page_class_init (DraftDocumentPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_document_page_dispose;
  object_class->get_property = draft_document_page_get_property;
  object_class->set_property = draft_document_page_set_property;

  properties [PROP_DOCUMENT] =
    g_param_spec_object ("document",
                         "Document",
                         "Document",
                         DRAFT_TYPE_DOCUMENT,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/draft-document-page.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftDocumentPage, canvas);
}

static void
draft_document_page_init (DraftDocumentPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  panel_widget_set_kind (PANEL_WIDGET (self), PANEL_WIDGET_KIND_DOCUMENT);
}

DraftDocument *
draft_document_page_get_document (DraftDocumentPage *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT_PAGE (self), NULL);

  return self->document;
}

DraftCanvas *
draft_document_page_get_canvas (DraftDocumentPage *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT_PAGE (self), NULL);

  return self->canvas;
}
