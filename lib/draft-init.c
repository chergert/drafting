/* draft-init.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <gtk/gtk.h>

#include "draft-init.h"

void
draft_init (void)
{
  g_autoptr(GtkCssProvider) css = NULL;
  GdkDisplay *display = gdk_display_get_default ();

  if (display == NULL)
    return;

  css = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css, "/org/gnome/drafting/css/shared.css");
  gtk_style_context_add_provider_for_display (display,
                                              GTK_STYLE_PROVIDER (css),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION-1);
}
