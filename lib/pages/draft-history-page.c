/* draft-history-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>

#include "draft-project-private.h"
#include "draft-observer-private.h"
#include "pages/draft-history-page.h"
#include "undo/draft-transaction-private.h"

struct _DraftHistoryPage
{
  GtkWidget      parent_instance;

  DraftProject  *project;
  DraftObserver *observer;
  GListStore    *undo_store;
  GListStore    *redo_store;

  GtkBox        *box;
  GtkListBox    *undo_list;
  GtkListBox    *redo_list;
};

G_DEFINE_TYPE (DraftHistoryPage, draft_history_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_history_page_new:
 *
 * Create a new #DraftHistoryPage.
 *
 * Returns: (transfer full): a newly created #DraftHistoryPage
 */
GtkWidget *
draft_history_page_new (void)
{
  return g_object_new (DRAFT_TYPE_HISTORY_PAGE, NULL);
}

static void
draft_history_page_apply_cb (DraftHistoryPage *self,
                             DraftTransaction *transaction,
                             DraftObserver    *observer)
{
  g_assert (DRAFT_IS_HISTORY_PAGE (self));
  g_assert (DRAFT_IS_TRANSACTION (transaction));
  g_assert (DRAFT_IS_OBSERVER (observer));

  g_list_store_append (self->undo_store, transaction);

  /* If there is anything in the redo store, we just stole position 0 */
  if (g_list_model_get_n_items (G_LIST_MODEL (self->redo_store)) > 0)
    g_list_store_remove (self->redo_store, 0);
}

static void
draft_history_page_revert_cb (DraftHistoryPage *self,
                              DraftTransaction *transaction,
                              DraftObserver    *observer)
{
  g_assert (DRAFT_IS_HISTORY_PAGE (self));
  g_assert (DRAFT_IS_TRANSACTION (transaction));
  g_assert (DRAFT_IS_OBSERVER (observer));
  g_assert (g_list_model_get_n_items (G_LIST_MODEL (self->undo_store)) > 0);

  g_list_store_remove (self->undo_store,
                       g_list_model_get_n_items (G_LIST_MODEL (self->undo_store)) - 1);
  g_list_store_insert (self->redo_store, 0, transaction);
}

static GtkWidget *
create_row_func (gpointer item,
                 gpointer user_data)
{
  DraftTransaction *transaction = item;
  g_autofree char *description = NULL;

  g_assert (DRAFT_IS_TRANSACTION (transaction));
  g_assert (DRAFT_IS_HISTORY_PAGE (user_data));

  description = _draft_transaction_get_description (transaction);

  return g_object_new (GTK_TYPE_LABEL,
                       "xalign", 0.0f,
                       "label", description,
                       "margin-start", 6,
                       "margin-end", 6,
                       "ellipsize", PANGO_ELLIPSIZE_END,
                       "wrap", TRUE,
                       NULL);
}

static void
draft_history_page_dispose (GObject *object)
{
  DraftHistoryPage *self = (DraftHistoryPage *)object;

  draft_history_page_set_project (self, NULL);

  g_clear_object (&self->undo_store);
  g_clear_object (&self->redo_store);
  g_clear_pointer ((GtkWidget **)&self->box, gtk_widget_unparent);
  g_clear_object (&self->observer);

  G_OBJECT_CLASS (draft_history_page_parent_class)->dispose (object);
}

static void
draft_history_page_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  DraftHistoryPage *self = DRAFT_HISTORY_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, draft_history_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_history_page_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  DraftHistoryPage *self = DRAFT_HISTORY_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_history_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_history_page_class_init (DraftHistoryPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_history_page_dispose;
  object_class->get_property = draft_history_page_get_property;
  object_class->set_property = draft_history_page_set_property;

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The project to display history for",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-history-page.ui");
  gtk_widget_class_set_css_name (widget_class, "drafthistorypanel");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftHistoryPage, box);
  gtk_widget_class_bind_template_child (widget_class, DraftHistoryPage, redo_list);
  gtk_widget_class_bind_template_child (widget_class, DraftHistoryPage, undo_list);
}

static void
draft_history_page_init (DraftHistoryPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_list_box_set_placeholder (self->undo_list,
                                g_object_new (GTK_TYPE_LABEL,
                                              "label", _("No actions to undo"),
                                              "xalign", 0.0f,
                                              "margin-top", 12,
                                              "margin-bottom", 12,
                                              "margin-start", 6,
                                              "margin-end", 6,
                                              NULL));

  self->undo_store = g_list_store_new (DRAFT_TYPE_TRANSACTION);
  self->redo_store = g_list_store_new (DRAFT_TYPE_TRANSACTION);

  gtk_list_box_bind_model (self->undo_list,
                           G_LIST_MODEL (self->undo_store),
                           create_row_func,
                           self,
                           NULL);
  gtk_list_box_bind_model (self->redo_list,
                           G_LIST_MODEL (self->redo_store),
                           create_row_func,
                           self,
                           NULL);

  self->observer = _draft_observer_new ();
  g_signal_connect_object (self->observer,
                           "apply",
                           G_CALLBACK (draft_history_page_apply_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "revert",
                           G_CALLBACK (draft_history_page_revert_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "clear-redo-history",
                           G_CALLBACK (g_list_store_remove_all),
                           self->redo_store,
                           G_CONNECT_SWAPPED);
}

void
draft_history_page_set_project (DraftHistoryPage *self,
                                DraftProject     *project)
{
  g_return_if_fail (DRAFT_IS_HISTORY_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  if (self->project != NULL)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (self->project != NULL)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}

/**
 * draft_history_page_get_project:
 * @self: A #DraftHistoryPage
 *
 * Gets the project associated with the panel.
 *
 * Returns: (nullable) (transfer none): A #DraftProject or %NULL
 */
DraftProject *
draft_history_page_get_project (DraftHistoryPage *self)
{
  g_return_val_if_fail (DRAFT_IS_HISTORY_PAGE (self), NULL);

  return self->project;
}
