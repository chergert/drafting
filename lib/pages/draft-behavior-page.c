/* draft-behavior-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <adwaita.h>

#include "draft-observer-private.h"
#include "draft-project-private.h"
#include "pages/draft-behavior-page.h"

struct _DraftBehaviorPage
{
  GtkWidget      parent_instance;

  DraftObserver *observer;
  DraftProject  *project;

  AdwPreferencesPage *page;
};

G_DEFINE_TYPE (DraftBehaviorPage, draft_behavior_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_behavior_page_new:
 *
 * Create a new #DraftBehaviorPage.
 *
 * Returns: (transfer full): a newly created #DraftBehaviorPage
 */
GtkWidget *
draft_behavior_page_new (void)
{
  return g_object_new (DRAFT_TYPE_BEHAVIOR_PAGE, NULL);
}

static void
draft_behavior_page_dispose (GObject *object)
{
  DraftBehaviorPage *self = (DraftBehaviorPage *)object;

  draft_behavior_page_set_project (self, NULL);
  g_clear_object (&self->observer);
  g_clear_pointer ((GtkWidget **)&self->page, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_behavior_page_parent_class)->dispose (object);
}

static void
draft_behavior_page_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftBehaviorPage *self = DRAFT_BEHAVIOR_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, draft_behavior_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_behavior_page_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DraftBehaviorPage *self = DRAFT_BEHAVIOR_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_behavior_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_behavior_page_class_init (DraftBehaviorPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_behavior_page_dispose;
  object_class->get_property = draft_behavior_page_get_property;
  object_class->set_property = draft_behavior_page_set_property;

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "Project",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-behavior-page.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftBehaviorPage, page);
}

static void
draft_behavior_page_init (DraftBehaviorPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->observer = _draft_observer_new ();
}

/**
 * draft_behavior_page_get_project:
 * @self: A #DraftBehaviorPage
 *
 * Gets the project associated with the page.
 *
 * Returns: (transfer none) (nullable): A #DraftProject or %NULL
 */
DraftProject *
draft_behavior_page_get_project (DraftBehaviorPage *self)
{
  g_return_val_if_fail (DRAFT_IS_BEHAVIOR_PAGE (self), NULL);

  return self->project;
}

void
draft_behavior_page_set_project (DraftBehaviorPage *self,
                                 DraftProject      *project)
{
  g_return_if_fail (DRAFT_IS_BEHAVIOR_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  if (self->project)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (self->project)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}
