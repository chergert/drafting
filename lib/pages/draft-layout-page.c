/* draft-layout-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "pages/draft-layout-page.h"
#include "widgets/draft-alignment-grid-private.h"

struct _DraftLayoutPage
{
  GtkWidget           parent_instance;

  DraftObserver      *observer;
  DraftProject       *project;

  DraftAlignmentGrid *alignment;
  GtkWidget          *box;
};

G_DEFINE_TYPE (DraftLayoutPage, draft_layout_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_layout_page_new:
 *
 * Create a new #DraftLayoutPage.
 *
 * Returns: (transfer full): a newly created #DraftLayoutPage
 */
GtkWidget *
draft_layout_page_new (void)
{
  return g_object_new (DRAFT_TYPE_LAYOUT_PAGE, NULL);
}

static void
draft_layout_page_clear_selection_cb (DraftLayoutPage *self,
                                      DraftObserver   *observer)
{
  g_assert (DRAFT_IS_LAYOUT_PAGE (self));
  g_assert (DRAFT_IS_OBSERVER (observer));

  draft_alignment_grid_set_object (self->alignment, NULL);
}

static void
draft_layout_page_select_item_cb (DraftLayoutPage *self,
                                  DraftItem       *item,
                                  DraftObserver   *observer)
{
  g_assert (DRAFT_IS_LAYOUT_PAGE (self));
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (DRAFT_IS_OBSERVER (observer));

  if (_draft_item_is_of_type (item, "GtkWidget"))
    draft_alignment_grid_set_object (self->alignment, DRAFT_OBJECT (item));
  else
    draft_alignment_grid_set_object (self->alignment, NULL);
}

static void
draft_layout_page_dispose (GObject *object)
{
  DraftLayoutPage *self = (DraftLayoutPage *)object;

  draft_layout_page_set_project (self, NULL);
  g_clear_object (&self->observer);
  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_layout_page_parent_class)->dispose (object);
}

static void
draft_layout_page_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  DraftLayoutPage *self = DRAFT_LAYOUT_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, draft_layout_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_layout_page_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  DraftLayoutPage *self = DRAFT_LAYOUT_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_layout_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_layout_page_class_init (DraftLayoutPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_layout_page_dispose;
  object_class->get_property = draft_layout_page_get_property;
  object_class->set_property = draft_layout_page_set_property;

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "Project",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-layout-page.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftLayoutPage, alignment);
  gtk_widget_class_bind_template_child (widget_class, DraftLayoutPage, box);

  g_type_ensure (DRAFT_TYPE_ALIGNMENT_GRID);
}

static void
draft_layout_page_init (DraftLayoutPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->observer = _draft_observer_new ();
  g_signal_connect_object (self->observer,
                           "clear-selection",
                           G_CALLBACK (draft_layout_page_clear_selection_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "select-item",
                           G_CALLBACK (draft_layout_page_select_item_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

/**
 * draft_layout_page_get_project:
 * @self: A #DraftLayoutPage
 *
 * Gets the project associated with the page.
 *
 * Returns: (transfer none) (nullable): A #DraftProject or %NULL
 */
DraftProject *
draft_layout_page_get_project (DraftLayoutPage *self)
{
  g_return_val_if_fail (DRAFT_IS_LAYOUT_PAGE (self), NULL);

  return self->project;
}

void
draft_layout_page_set_project (DraftLayoutPage *self,
                               DraftProject    *project)
{
  g_return_if_fail (DRAFT_IS_LAYOUT_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  if (self->project)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (self->project)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}
