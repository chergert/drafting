/* draft-object-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "pages/draft-object-page.h"
#include "widgets/draft-entry-private.h"
#include "widgets/draft-property-grid-private.h"

struct _DraftObjectPage
{
  GtkWidget          parent_instance;

  DraftProject      *project;
  DraftObserver     *observer;
  DraftObject       *object;

  GtkWidget         *box;
  GtkLabel          *class_name;
  DraftEntry        *id_entry;
  DraftPropertyGrid *property_grid;
};

G_DEFINE_TYPE (DraftObjectPage, draft_object_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_OBJECT,
  PROP_PROJECT,
  N_PROPS
};

static void draft_object_page_set_object (DraftObjectPage *self,
                                          DraftObject     *object);

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_object_page_new (void)
{
  return g_object_new (DRAFT_TYPE_OBJECT_PAGE, NULL);
}

static void
draft_object_page_clear_selection_cb (DraftObjectPage *self,
                                      DraftObserver   *observer)
{
  g_assert (DRAFT_IS_OBJECT_PAGE (self));
  g_assert (DRAFT_IS_OBSERVER (observer));

  draft_object_page_set_object (self, NULL);
}

static void
draft_object_page_select_item_cb (DraftObjectPage *self,
                                  DraftItem       *item,
                                  DraftObserver   *observer)
{
  g_assert (DRAFT_IS_OBJECT_PAGE (self));
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (DRAFT_IS_OBSERVER (observer));

  if (DRAFT_IS_OBJECT (item))
    draft_object_page_set_object (self, DRAFT_OBJECT (item));
  else
    draft_object_page_set_object (self, NULL);
}

static void
draft_object_page_dispose (GObject *object)
{
  DraftObjectPage *self = (DraftObjectPage *)object;

  draft_object_page_set_project (self, NULL);

  g_clear_object (&self->observer);
  g_clear_object (&self->project);
  g_clear_object (&self->object);

  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_object_page_parent_class)->dispose (object);
}

static void
draft_object_page_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  DraftObjectPage *self = DRAFT_OBJECT_PAGE (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_object_page_get_object (self));
      break;

    case PROP_PROJECT:
      g_value_set_object (value, draft_object_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_object_page_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  DraftObjectPage *self = DRAFT_OBJECT_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_object_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_object_page_class_init (DraftObjectPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_object_page_dispose;
  object_class->get_property = draft_object_page_get_property;
  object_class->set_property = draft_object_page_set_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "Object",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The project to edit",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-object-page.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftObjectPage, box);
  gtk_widget_class_bind_template_child (widget_class, DraftObjectPage, class_name);
  gtk_widget_class_bind_template_child (widget_class, DraftObjectPage, id_entry);
  gtk_widget_class_bind_template_child (widget_class, DraftObjectPage, property_grid);

  g_type_ensure (DRAFT_TYPE_ENTRY);
  g_type_ensure (DRAFT_TYPE_PROPERTY_GRID);
}

static void
draft_object_page_init (DraftObjectPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->observer = _draft_observer_new ();
  g_signal_connect_object (self->observer,
                           "clear-selection",
                           G_CALLBACK (draft_object_page_clear_selection_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "select-item",
                           G_CALLBACK (draft_object_page_select_item_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

/**
 * draft_object_page_get_object:
 * @self: A #DraftObjectPage
 *
 * Gets the object being edited.
 *
 * Returns: (transfer none) (nullable): a #DraftObject or %NULL
 */
DraftObject *
draft_object_page_get_object (DraftObjectPage *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT_PAGE (self), NULL);

  return self->object;
}

static void
draft_object_page_set_object (DraftObjectPage *self,
                              DraftObject     *object)
{
  g_return_if_fail (DRAFT_IS_OBJECT_PAGE (self));
  g_return_if_fail (!object || DRAFT_IS_OBJECT (object));

  if (g_set_object (&self->object, object))
    {
      const char *class_name = NULL;

      if (object != NULL)
        class_name = draft_object_get_class_name (object);

      draft_entry_set_item_property (self->id_entry, DRAFT_ITEM (object), "id");
      gtk_label_set_label (self->class_name, class_name);
      draft_property_grid_set_object (self->property_grid, object);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
    }
}

/**
 * draft_object_page_get_project:
 * @self: a #DraftObjectPage
 *
 * Gets the #DraftObjectPage:project property.
 *
 * Returns: (transfer none) (nullable): a #DraftProject or %NULL
 */
DraftProject *
draft_object_page_get_project (DraftObjectPage *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT_PAGE (self), NULL);

  return self->project;
}

void
draft_object_page_set_project (DraftObjectPage *self,
                               DraftProject    *project)
{
  g_return_if_fail (DRAFT_IS_OBJECT_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  draft_object_page_set_object (self, NULL);

  if (self->project)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (self->project)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}
