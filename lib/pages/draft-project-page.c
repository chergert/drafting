/* draft-project-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>

#include "draft-observer-private.h"
#include "draft-project-private.h"

#include "dialogs/draft-signal-dialog-private.h"
#include "items/draft-document.h"
#include "items/draft-object.h"
#include "items/draft-item-private.h"
#include "items/draft-signal.h"
#include "pages/draft-project-page.h"
#include "util/draft-signal-group-private.h"
#include "widgets/draft-item-label-private.h"

struct _DraftProjectPage
{
  GtkWidget          parent_instance;

  DraftProject      *project;
  DraftObserver     *observer;
  GListStore        *documents;

  GtkListView       *list;
  GtkScrolledWindow *scroller;

  guint              in_selection : 1;
};

G_DEFINE_TYPE (DraftProjectPage, draft_project_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_project_page_new:
 *
 * Create a new #DraftProjectPage.
 *
 * Returns: (transfer full): a newly created #DraftProjectPage
 */
GtkWidget *
draft_project_page_new (void)
{
  return g_object_new (DRAFT_TYPE_PROJECT_PAGE, NULL);
}

static void
draft_project_page_document_added (DraftProjectPage *self,
                                   DraftDocument    *document,
                                   DraftObserver    *observer)
{
  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IS_OBSERVER (observer));

  g_list_store_append (self->documents, document);
}

static void
draft_project_page_document_removed (DraftProjectPage *self,
                                     DraftDocument    *document,
                                     DraftObserver    *observer)
{
  guint n_items;

  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IS_OBSERVER (observer));

  n_items = g_list_model_get_n_items (G_LIST_MODEL (self->documents));

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(DraftDocument) ele = g_list_model_get_item (G_LIST_MODEL (self->documents), i);

      if (ele == document)
        {
          g_list_store_remove (self->documents, i);
          break;
        }
    }
}

static void
on_selection_changed_cb (DraftProjectPage  *self,
                         guint               position,
                         guint               items_changed,
                         GtkSingleSelection *model)
{
  GtkTreeListRow *row;

  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (GTK_IS_SINGLE_SELECTION (model));

  if (self->in_selection)
    return;

  self->in_selection = TRUE;

  _draft_project_clear_selection (self->project);

  if ((row = gtk_single_selection_get_selected_item (model)))
    {
      g_autoptr(DraftItem) item = gtk_tree_list_row_get_item (row);
      g_assert (DRAFT_IS_ITEM (item));
      _draft_project_select_item (self->project, item);
    }

  self->in_selection = FALSE;
}

static void
draft_project_page_select_item (DraftProjectPage *self,
                                DraftItem        *item,
                                DraftObserver    *observer)
{
  GtkSelectionModel *selection_model;
  GQueue queue = G_QUEUE_INIT;
  GListModel *model;
  DraftItem *aux;
  guint n_items;

  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (DRAFT_IS_OBSERVER (observer));

  if (self->in_selection)
    return;

  aux = item;
  while ((aux = draft_item_get_parent (aux)) != NULL)
    g_queue_push_head (&queue, aux);

  selection_model = gtk_list_view_get_model (self->list);
  n_items = g_list_model_get_n_items (G_LIST_MODEL (selection_model));

  /* Disable autoexpand while selecting the item */
  model = gtk_single_selection_get_model (GTK_SINGLE_SELECTION (selection_model));
  gtk_tree_list_model_set_autoexpand (GTK_TREE_LIST_MODEL (model), FALSE);

  aux = g_queue_pop_head (&queue);
  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(GtkTreeListRow) row = NULL;
      DraftItem *ele;

      row = g_list_model_get_item (G_LIST_MODEL (selection_model), i);
      ele = gtk_tree_list_row_get_item (row);

      if (ele == aux)
        {
          gtk_tree_list_row_set_expanded (row, TRUE);
          aux = g_queue_pop_head (&queue);
          n_items = g_list_model_get_n_items (G_LIST_MODEL (selection_model));
        }

      if (ele == item)
        {
          self->in_selection = TRUE;
          gtk_selection_model_select_item (selection_model, i, TRUE);
          self->in_selection = FALSE;
          break;
        }
    }

  gtk_tree_list_model_set_autoexpand (GTK_TREE_LIST_MODEL (model), TRUE);

  g_queue_clear (&queue);
}

static void
draft_project_page_clear_selection (DraftProjectPage *self,
                                    DraftObserver    *observer)
{
  GtkSelectionModel *model;

  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (DRAFT_IS_OBSERVER (observer));

  if (self->in_selection)
    return;

  model = gtk_list_view_get_model (self->list);

  self->in_selection = TRUE;
  gtk_selection_model_unselect_all (model);
  self->in_selection = FALSE;
}

static void
setup_row_cb (GtkSignalListItemFactory *factory,
              GtkListItem              *list_item,
              DraftProjectPage         *self)
{
  GtkWidget *expander;
  GtkWidget *label;

  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_PROJECT_PAGE (self));

  expander = gtk_tree_expander_new ();
  gtk_list_item_set_child (list_item, expander);

  label = draft_item_label_new ();
  gtk_tree_expander_set_child (GTK_TREE_EXPANDER (expander), label);
}

static void
bind_row_cb (GtkSignalListItemFactory *factory,
             GtkListItem              *list_item,
             DraftProjectPage         *self)
{
  g_autoptr(DraftItem) item = NULL;
  GtkTreeListRow *list_row;
  GtkWidget *expander;
  GtkWidget *label;

  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_PROJECT_PAGE (self));

  list_row = gtk_list_item_get_item (list_item);
  expander = gtk_list_item_get_child (list_item);
  gtk_tree_expander_set_list_row (GTK_TREE_EXPANDER (expander), list_row);

  item = gtk_tree_list_row_get_item (list_row);
  expander = gtk_list_item_get_child (list_item);
  label = gtk_tree_expander_get_child (GTK_TREE_EXPANDER (expander));

  g_assert (DRAFT_IS_ITEM (item));

  draft_item_label_set_item (DRAFT_ITEM_LABEL (label), item);
}

static void
unbind_row_cb (GtkSignalListItemFactory *factory,
               GtkListItem              *list_item,
               DraftProjectPage         *self)
{
  GtkWidget *expander;
  GtkWidget *label;

  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_PROJECT_PAGE (self));

  expander = gtk_list_item_get_child (list_item);
  label = gtk_tree_expander_get_child (GTK_TREE_EXPANDER (expander));

  draft_item_label_set_item (DRAFT_ITEM_LABEL (label), NULL);
}

static GListModel *
draft_project_page_create_model_cb (gpointer item,
                                    gpointer user_data)
{
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (G_IS_LIST_MODEL (item));

  return g_object_ref (G_LIST_MODEL (item));
}

static void
row_activated_cb (DraftProjectPage *self,
                  guint             position,
                  GtkListView      *list)
{
  g_autoptr(GtkTreeListRow) row = NULL;
  GtkSelectionModel *selection;
  DraftItem *parent;
  DraftItem *item;
  GtkRoot *toplevel;

  g_assert (DRAFT_IS_PROJECT_PAGE (self));
  g_assert (GTK_IS_LIST_VIEW (list));

  selection = gtk_list_view_get_model (list);
  row = g_list_model_get_item (G_LIST_MODEL (selection), position);
  item = gtk_tree_list_row_get_item (row);

  toplevel = gtk_widget_get_root (GTK_WIDGET (self));
  if (!GTK_IS_WINDOW (toplevel))
    toplevel = NULL;

  parent = draft_item_get_parent (item);

  if (DRAFT_IS_SIGNAL (item) && DRAFT_IS_OBJECT (parent))
    {
      DraftSignalDialog *dialog = draft_signal_dialog_new (GTK_WINDOW (toplevel));

      draft_signal_dialog_configure (dialog,
                                     self->project,
                                     DRAFT_OBJECT (parent),
                                     DRAFT_SIGNAL (item));
      gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
      gtk_window_set_title (GTK_WINDOW (dialog), _("Edit Signal"));
      gtk_window_present (GTK_WINDOW (dialog));
    }
}

static void
draft_project_page_dispose (GObject *object)
{
  DraftProjectPage *self = (DraftProjectPage *)object;

  draft_project_page_set_project (self, NULL);

  g_clear_object (&self->observer);
  g_clear_object (&self->documents);
  g_clear_pointer ((GtkWidget **)&self->scroller, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_project_page_parent_class)->dispose (object);
}

static void
draft_project_page_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  DraftProjectPage *self = DRAFT_PROJECT_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, draft_project_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_page_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  DraftProjectPage *self = DRAFT_PROJECT_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_project_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_page_class_init (DraftProjectPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_project_page_dispose;
  object_class->get_property = draft_project_page_get_property;
  object_class->set_property = draft_project_page_set_property;

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The Drafting project to load",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_css_name (widget_class, "draftpanel");
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-project-page.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftProjectPage, list);
  gtk_widget_class_bind_template_child (widget_class, DraftProjectPage, scroller);
  gtk_widget_class_bind_template_callback (widget_class, bind_row_cb);
  gtk_widget_class_bind_template_callback (widget_class, row_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, setup_row_cb);
  gtk_widget_class_bind_template_callback (widget_class, unbind_row_cb);
}

static void
draft_project_page_init (DraftProjectPage *self)
{
  g_autoptr(GtkSingleSelection) selection = NULL;
  GtkTreeListModel *tree_model;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->documents = g_list_store_new (DRAFT_TYPE_DOCUMENT);

  self->observer = _draft_observer_new ();
  g_signal_connect_object (self->observer,
                           "document-added",
                           G_CALLBACK (draft_project_page_document_added),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "document-removed",
                           G_CALLBACK (draft_project_page_document_removed),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "clear-selection",
                           G_CALLBACK (draft_project_page_clear_selection),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "select-item",
                           G_CALLBACK (draft_project_page_select_item),
                           self,
                           G_CONNECT_SWAPPED);

  tree_model = gtk_tree_list_model_new (g_object_ref (G_LIST_MODEL (self->documents)),
                                        FALSE,
                                        TRUE,
                                        draft_project_page_create_model_cb,
                                        NULL,
                                        NULL);
  selection = gtk_single_selection_new (G_LIST_MODEL (tree_model));
  gtk_list_view_set_model (self->list, GTK_SELECTION_MODEL (selection));
  g_signal_connect_object (selection,
                           "selection-changed",
                           G_CALLBACK (on_selection_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

/**
 * draft_project_page_get_project:
 * @self: a #DraftProject
 *
 * Gets the project that is loaded in the panel.
 *
 * Returns: (transfer none) (nullable): a #DraftProject or %NULL
 */
DraftProject *
draft_project_page_get_project (DraftProjectPage *self)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT_PAGE (self), NULL);

  return self->project;
}

void
draft_project_page_set_project (DraftProjectPage *self,
                                DraftProject     *project)
{
  g_return_if_fail (DRAFT_IS_PROJECT_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  if (self->project != NULL)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (project != NULL)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}
