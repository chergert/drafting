/* draft-history-page.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_HISTORY_PAGE (draft_history_page_get_type())

DRAFT_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DraftHistoryPage, draft_history_page, DRAFT, HISTORY_PAGE, GtkWidget)

DRAFT_AVAILABLE_IN_ALL
GtkWidget    *draft_history_page_new         (void);
DRAFT_AVAILABLE_IN_ALL
DraftProject *draft_history_page_get_project (DraftHistoryPage *self);
DRAFT_AVAILABLE_IN_ALL
void          draft_history_page_set_project (DraftHistoryPage *self,
                                              DraftProject     *project);

G_END_DECLS
