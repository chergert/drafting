/* draft-signals-page.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>

#include "draft-observer-private.h"
#include "draft-project-private.h"
#include "dialogs/draft-signal-dialog-private.h"
#include "introspection/draft-type-info-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-signal.h"
#include "pages/draft-signal-row-private.h"
#include "pages/draft-signals-page.h"
#include "util/draft-utils-private.h"

struct _DraftSignalsPage
{
  GtkWidget      parent_instance;

  DraftProject  *project;
  DraftObject   *object;
  DraftObserver *observer;

  GtkListView *list_view;
  GtkWidget   *box;
};

G_DEFINE_TYPE (DraftSignalsPage, draft_signals_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_OBJECT,
  PROP_PROJECT,
  N_PROPS
};

static void draft_signals_page_set_object (DraftSignalsPage *self,
                                           DraftObject      *object);

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_signals_page_new (void)
{
  return g_object_new (DRAFT_TYPE_SIGNALS_PAGE, NULL);
}

static void
setup_row_cb (GtkSignalListItemFactory *factory,
              GtkListItem              *list_item,
              DraftSignalsPage         *self)
{
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_SIGNALS_PAGE (self));

  gtk_list_item_set_child (list_item, draft_signal_row_new ());
}


static void
bind_row_cb (GtkSignalListItemFactory *factory,
             GtkListItem              *list_item,
             DraftSignalsPage         *self)
{
  DraftSignal *item;
  GtkWidget *row;

  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_SIGNALS_PAGE (self));

  item = gtk_list_item_get_item (list_item);
  row = gtk_list_item_get_child (list_item);

  g_assert (DRAFT_IS_SIGNAL (item));
  g_assert (DRAFT_IS_SIGNAL_ROW (row));

  draft_signal_row_set_signal (DRAFT_SIGNAL_ROW (row), item);
}

static void
unbind_row_cb (GtkSignalListItemFactory *factory,
               GtkListItem              *list_item,
               DraftSignalsPage         *self)
{
  GtkWidget *row;

  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));
  g_assert (GTK_IS_LIST_ITEM (list_item));
  g_assert (DRAFT_IS_SIGNALS_PAGE (self));

  row = gtk_list_item_get_child (list_item);
  draft_signal_row_set_signal (DRAFT_SIGNAL_ROW (row), NULL);
}

static void
activate_row_cb (DraftSignalsPage *self,
                 guint             position,
                 GtkListView      *list_view)
{
  g_autoptr(DraftSignal) item = NULL;
  const char *title = _("Edit Signal");
  DraftSignalDialog *dialog;
  GtkSelectionModel *model;
  DraftProject *project;
  GtkNative *native;

  g_assert (DRAFT_IS_SIGNALS_PAGE (self));
  g_assert (GTK_IS_LIST_VIEW (list_view));

  if (self->object == NULL)
    return;

  model = gtk_list_view_get_model (list_view);
  item = g_list_model_get_item (G_LIST_MODEL (model), position);
  native = gtk_widget_get_native (GTK_WIDGET (self));
  project = draft_item_get_project (DRAFT_ITEM (self->object));

  if (draft_item_get_parent (DRAFT_ITEM (item)) == NULL)
    {
      title = _("Add Signal");
      g_clear_object (&item);
      item = draft_signal_new ();
    }

  dialog = draft_signal_dialog_new (GTK_WINDOW (native));
  draft_signal_dialog_configure (dialog, project, self->object, item);
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_title (GTK_WINDOW (dialog), title);
  gtk_window_present (GTK_WINDOW (dialog));
}

static void
draft_signals_page_clear_selection_cb (DraftSignalsPage *self,
                                       DraftObserver    *observer)
{
  g_assert (DRAFT_IS_SIGNALS_PAGE (self));
  g_assert (DRAFT_IS_OBSERVER (observer));

  draft_signals_page_set_object (self, NULL);
}

static void
draft_signals_page_select_item_cb (DraftSignalsPage *self,
                                   DraftItem        *item,
                                   DraftObserver    *observer)
{
  g_assert (DRAFT_IS_SIGNALS_PAGE (self));
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (DRAFT_IS_OBSERVER (observer));

  if (DRAFT_IS_OBJECT (item))
    draft_signals_page_set_object (self, DRAFT_OBJECT (item));
  else
    draft_signals_page_set_object (self, NULL);
}

static void
draft_signals_page_dispose (GObject *object)
{
  DraftSignalsPage *self = (DraftSignalsPage *)object;

  draft_signals_page_set_project (self, NULL);

  g_clear_object (&self->object);
  g_clear_object (&self->project);
  g_clear_object (&self->observer);
  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_signals_page_parent_class)->dispose (object);
}

static void
draft_signals_page_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  DraftSignalsPage *self = DRAFT_SIGNALS_PAGE (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_signals_page_get_object (self));
      break;

    case PROP_PROJECT:
      g_value_set_object (value, draft_signals_page_get_project (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signals_page_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  DraftSignalsPage *self = DRAFT_SIGNALS_PAGE (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      draft_signals_page_set_project (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signals_page_class_init (DraftSignalsPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_signals_page_dispose;
  object_class->get_property = draft_signals_page_get_property;
  object_class->set_property = draft_signals_page_set_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "The object to connect signals to",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The drafting project",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "draftsignalspage");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-signals-page.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftSignalsPage, box);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalsPage, list_view);
  gtk_widget_class_bind_template_callback (widget_class, setup_row_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_row_cb);
  gtk_widget_class_bind_template_callback (widget_class, unbind_row_cb);
  gtk_widget_class_bind_template_callback (widget_class, activate_row_cb);
}

static void
draft_signals_page_init (DraftSignalsPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->observer = _draft_observer_new ();
  g_signal_connect_object (self->observer,
                           "select-item",
                           G_CALLBACK (draft_signals_page_select_item_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->observer,
                           "clear-selection",
                           G_CALLBACK (draft_signals_page_clear_selection_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

DraftObject *
draft_signals_page_get_object (DraftSignalsPage *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNALS_PAGE (self), NULL);

  return self->object;
}

static void
draft_signals_page_set_object (DraftSignalsPage *self,
                               DraftObject      *object)
{
  g_autoptr(GListModel) filtered = NULL;
  g_autoptr(GListStore) store = NULL;
  g_autoptr(GListStore) all = NULL;
  g_autoptr(DraftSignal) new_signal = NULL;
  g_autoptr(GtkFlattenListModel) flatten = NULL;
  g_autoptr(GtkSingleSelection) selection = NULL;

  g_return_if_fail (DRAFT_IS_SIGNALS_PAGE (self));
  g_return_if_fail (!object || DRAFT_IS_OBJECT (object));

  if (self->object == object)
    return;

  g_set_object (&self->object, object);

  if (object != NULL)
    {
      filtered = _draft_item_type_filter_new (G_LIST_MODEL (object), DRAFT_TYPE_SIGNAL);
      store = g_list_store_new (DRAFT_TYPE_SIGNAL);
      new_signal = draft_signal_new ();
      g_list_store_append (store, new_signal);
      all = g_list_store_new (G_TYPE_LIST_MODEL);
      g_list_store_append (all, filtered);
      g_list_store_append (all, store);
      flatten = gtk_flatten_list_model_new (g_object_ref (G_LIST_MODEL (all)));
      selection = gtk_single_selection_new (g_object_ref (G_LIST_MODEL (flatten)));
    }

  gtk_list_view_set_model (self->list_view, GTK_SELECTION_MODEL (selection));
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
}

/**
 * draft_signals_page_get_project:
 * @self: a #DraftSignalsPage
 *
 * Gets the project the page is monitoring.
 *
 * Returns: (transfer none) (nullable): a #DraftProject or %NULL
 */
DraftProject *
draft_signals_page_get_project (DraftSignalsPage *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNALS_PAGE (self), NULL);

  return self->project;
}

void
draft_signals_page_set_project (DraftSignalsPage *self,
                                DraftProject     *project)
{
  g_return_if_fail (DRAFT_IS_SIGNALS_PAGE (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  draft_signals_page_set_object (self, NULL);

  if (self->project)
    _draft_project_remove_observer (self->project, self->observer);

  g_set_object (&self->project, project);

  if (self->project)
    _draft_project_add_observer (self->project, self->observer);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}
