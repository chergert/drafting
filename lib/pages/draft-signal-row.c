/* draft-signal-row.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>

#include "draft-project.h"
#include "items/draft-signal.h"
#include "pages/draft-signal-row-private.h"
#include "undo/draft-transaction.h"

struct _DraftSignalRow
{
  GtkWidget    parent_instance;

  DraftSignal *signal;

  GtkLabel    *name_label;
  GtkLabel    *handler_label;
  GtkLabel    *object_label;
  GtkStack    *stack;
};

G_DEFINE_TYPE (DraftSignalRow, draft_signal_row, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_SIGNAL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_signal_row_new (void)
{
  return g_object_new (DRAFT_TYPE_SIGNAL_ROW, NULL);
}

static void
row_delete_action (GtkWidget  *widget,
                   const char *action_name,
                   GVariant   *param)
{
  DraftSignalRow *self = (DraftSignalRow *)widget;
  DraftTransaction *transaction;
  DraftProject *project;

  g_assert (DRAFT_IS_SIGNAL_ROW (self));
  g_assert (action_name != NULL);
  g_assert (param == NULL);

  if (self->signal == NULL ||
      !(project = draft_item_get_project (DRAFT_ITEM (self->signal))) ||
      !(transaction = draft_project_begin (project)))
    return;

  draft_transaction_remove (transaction, DRAFT_ITEM (self->signal));
  draft_project_commit (project);
}

static void
draft_signal_row_dispose (GObject *object)
{
  DraftSignalRow *self = (DraftSignalRow *)object;

  g_clear_pointer ((GtkWidget **)&self->stack, gtk_widget_unparent);
  g_clear_object (&self->signal);

  G_OBJECT_CLASS (draft_signal_row_parent_class)->dispose (object);
}

static void
draft_signal_row_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftSignalRow *self = DRAFT_SIGNAL_ROW (object);

  switch (prop_id)
    {
    case PROP_SIGNAL:
      g_value_set_object (value, draft_signal_row_get_signal (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signal_row_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftSignalRow *self = DRAFT_SIGNAL_ROW (object);

  switch (prop_id)
    {
    case PROP_SIGNAL:
      draft_signal_row_set_signal (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signal_row_class_init (DraftSignalRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_signal_row_dispose;
  object_class->get_property = draft_signal_row_get_property;
  object_class->set_property = draft_signal_row_set_property;

  properties [PROP_SIGNAL] =
    g_param_spec_object ("signal",
                         "Signal",
                         "The signal to display",
                         DRAFT_TYPE_SIGNAL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-signal-row.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalRow, name_label);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalRow, handler_label);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalRow, object_label);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalRow, stack);

  gtk_widget_class_install_action (widget_class, "row.delete", NULL, row_delete_action);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Delete, 0, "row.delete", NULL);
}

static void
draft_signal_row_init (DraftSignalRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  draft_signal_row_set_signal (self, NULL);
}

DraftSignal *
draft_signal_row_get_signal (DraftSignalRow *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL_ROW (self), NULL);

  return self->signal;
}

void
draft_signal_row_set_signal (DraftSignalRow *self,
                             DraftSignal    *signal)
{
  g_return_if_fail (DRAFT_IS_SIGNAL_ROW (self));

  if (g_set_object (&self->signal, signal))
    {
      g_autoptr(GString) name_str = g_string_new (NULL);
      gboolean is_empty;
      const char *name = NULL;
      const char *handler = NULL;
      const char *object = NULL;

      is_empty = signal == NULL || draft_item_get_parent (DRAFT_ITEM (signal)) == NULL;

      if (!is_empty)
        {
          const char *detail = NULL;

          name = draft_signal_get_name (signal);
          detail = draft_signal_get_detail (signal);
          handler = draft_signal_get_handler (signal);
          object = draft_signal_get_object (signal);

          g_string_append (name_str, name);

          if (detail != NULL && detail[0] != 0)
            g_string_append_printf (name_str, "::%s", detail);

          if (draft_signal_get_swapped (signal))
            g_string_append (name_str, _(" (Swapped)"));

          if (draft_signal_get_after (signal))
            g_string_append (name_str, _(" (After)"));
        }

      if (object == NULL)
        object = _("No object selected");

      gtk_label_set_label (self->name_label, name_str->str);
      gtk_label_set_label (self->handler_label, handler);
      gtk_label_set_label (self->object_label, object);

      gtk_stack_set_visible_child_name (self->stack, is_empty ? "empty" : "signal");

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SIGNAL]);
    }
}
