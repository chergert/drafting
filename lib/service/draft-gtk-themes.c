/*
 * Copyright (c) 2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gtk/gtk.h>

#include "draft-gtk-themes.h"

static void
fill_gtk (const char *path,
          GHashTable *themes)
{
  const char *dir_entry;
  GDir *dir = g_dir_open (path, 0, NULL);

  if (!dir)
    return;

  while ((dir_entry = g_dir_read_name (dir)))
    {
      char *filename = g_build_filename (path, dir_entry, "gtk-4.0", "gtk.css", NULL);

      if (g_file_test (filename, G_FILE_TEST_IS_REGULAR) &&
          !g_hash_table_contains (themes, dir_entry))
        g_hash_table_add (themes, g_strdup (dir_entry));

      g_free (filename);
    }

  g_dir_close (dir);
}

char **
draft_gtk_list_themes (void)
{
  GArray *ar = g_array_new (TRUE, FALSE, sizeof (char *));
  g_autoptr(GHashTable) themes = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  const char * const *dirs;
  GHashTableIter iter;
  char **builtin_themes;
  char *path;
  char *name;

  /* Builtin themes */
  builtin_themes = g_resources_enumerate_children ("/org/gtk/libgtk/theme", 0, NULL);
  if (builtin_themes != NULL)
    {
      for (guint i = 0; builtin_themes[i] != NULL; i++)
        {
          if (g_str_has_suffix (builtin_themes[i], "/"))
            g_hash_table_add (themes, g_strndup (builtin_themes[i], strlen (builtin_themes[i]) - 1));
        }
    }
  g_strfreev (builtin_themes);

  path = g_build_filename (g_get_user_data_dir (), "themes", NULL);
  fill_gtk (path, themes);
  g_free (path);

  path = g_build_filename (g_get_home_dir (), ".themes", NULL);
  fill_gtk (path, themes);
  g_free (path);

  dirs = g_get_system_data_dirs ();
  for (guint i = 0; dirs[i]; i++)
    {
      path = g_build_filename (dirs[i], "themes", NULL);
      fill_gtk (path, themes);
      g_free (path);
    }

  g_hash_table_iter_init (&iter, themes);
  while (g_hash_table_iter_next (&iter, (gpointer *)&name, NULL))
    {
      g_array_append_val (ar, name);
      g_hash_table_iter_steal (&iter);
    }

  return (char **)(gpointer)g_array_free (ar, FALSE);
}
