/* draft-gtk-window.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-gtk-window-private.h"

struct _DraftGtkWindow
{
  GtkWindow parent_instance;
  GtkWidget *popover;
};

static void buildable_iface_init (GtkBuildableIface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftGtkWindow, draft_gtk_window, GTK_TYPE_WINDOW,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

enum {
  PROP_0,
  PROP_POPOVER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GtkBuildableIface *parent_buildable;

GtkWidget *
draft_gtk_window_new (void)
{
  return g_object_new (DRAFT_TYPE_GTK_WINDOW, NULL);
}

static void
draft_gtk_window_set_popover (DraftGtkWindow *self,
                              GtkPopover     *popover)
{
  g_assert (DRAFT_IS_GTK_WINDOW (self));

  if (GTK_WIDGET (popover) == self->popover)
    return;

  g_clear_pointer (&self->popover, gtk_widget_unparent);
  if ((self->popover = GTK_WIDGET (popover)))
    gtk_widget_set_parent (self->popover, GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_POPOVER]);
}

static void
draft_gtk_window_size_allocate (GtkWidget *widget,
                                int        width,
                                int        height,
                                int        baseline)
{
  DraftGtkWindow *self = (DraftGtkWindow *)widget;

  g_assert (DRAFT_IS_GTK_WINDOW (self));

  if (GTK_WIDGET_CLASS (draft_gtk_window_parent_class)->size_allocate)
    GTK_WIDGET_CLASS (draft_gtk_window_parent_class)->size_allocate (widget, width, height, baseline);

  if (self->popover)
    gtk_popover_present (GTK_POPOVER (self->popover));
}

static void
draft_gtk_window_dispose (GObject *object)
{
  DraftGtkWindow *self = (DraftGtkWindow *)object;

  g_clear_pointer (&self->popover, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_gtk_window_parent_class)->dispose (object);
}

static void
draft_gtk_window_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftGtkWindow *self = DRAFT_GTK_WINDOW (object);

  switch (prop_id)
    {
    case PROP_POPOVER:
      g_value_set_object (value, self->popover);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_gtk_window_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftGtkWindow *self = DRAFT_GTK_WINDOW (object);

  switch (prop_id)
    {
    case PROP_POPOVER:
      draft_gtk_window_set_popover (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_gtk_window_class_init (DraftGtkWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_gtk_window_dispose;
  object_class->get_property = draft_gtk_window_get_property;
  object_class->set_property = draft_gtk_window_set_property;

  widget_class->size_allocate = draft_gtk_window_size_allocate;

  properties [PROP_POPOVER] =
    g_param_spec_object ("popover", NULL, NULL,
                         GTK_TYPE_POPOVER,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_gtk_window_init (DraftGtkWindow *self)
{
  gtk_window_set_decorated (GTK_WINDOW (self), FALSE);
}

static void
draft_gtk_window_add_child (GtkBuildable *buildable,
                            GtkBuilder   *builder,
                            GObject      *object,
                            const char   *type)
{
  DraftGtkWindow *self = (DraftGtkWindow *)buildable;

  g_assert (DRAFT_IS_GTK_WINDOW (self));
  g_assert (GTK_IS_BUILDER (builder));
  g_assert (G_IS_OBJECT (object));

  if (g_strcmp0 (type, "popover") == 0)
    g_object_set (self, "popover", object, NULL);
  else
    parent_buildable->add_child (buildable, builder, object, type);
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  parent_buildable = g_type_interface_peek_parent (iface);
  iface->add_child = draft_gtk_window_add_child;
}
