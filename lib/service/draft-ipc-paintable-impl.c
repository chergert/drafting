/* draft-ipc-paintable.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <gdk/gdk.h>

#include "service/draft-ipc-paintable-impl.h"
#include "service/draft-gtk-window-private.h"

struct _DraftIpcPaintableImpl
{
  DraftIpcPaintableSkeleton  parent_instance;
  GdkPaintable              *paintable;
  GtkBuilder                *builder;
};

static void paintable_iface_init (DraftIpcPaintableIface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftIpcPaintableImpl, draft_ipc_paintable_impl, DRAFT_IPC_TYPE_PAINTABLE_SKELETON,
                         G_IMPLEMENT_INTERFACE (DRAFT_IPC_TYPE_PAINTABLE, paintable_iface_init))

DraftIpcPaintable *
draft_ipc_paintable_impl_new (GtkBuilder *builder,
                              GtkWidget  *widget)
{
  DraftIpcPaintableImpl *self;

  g_return_val_if_fail (GTK_IS_BUILDER (builder), NULL);
  g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);

  self = g_object_new (DRAFT_IPC_TYPE_PAINTABLE_IMPL, NULL);
  self->paintable = gtk_widget_paintable_new (widget);
  g_set_weak_pointer (&self->builder, builder);

  g_signal_connect_object (self->paintable,
                           "invalidate-size",
                           G_CALLBACK (draft_ipc_paintable_emit_invalidate_size),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->paintable,
                           "invalidate-contents",
                           G_CALLBACK (draft_ipc_paintable_emit_invalidate_contents),
                           self,
                           G_CONNECT_SWAPPED);

  return DRAFT_IPC_PAINTABLE (self);
}

static void
draft_ipc_paintable_dispose (GObject *object)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)object;

  g_clear_weak_pointer (&self->builder);
  g_clear_object (&self->paintable);

  G_OBJECT_CLASS (draft_ipc_paintable_impl_parent_class)->dispose (object);
}

static void
draft_ipc_paintable_impl_class_init (DraftIpcPaintableImplClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_ipc_paintable_dispose;
}

static void
draft_ipc_paintable_impl_init (DraftIpcPaintableImpl *self)
{
}

typedef struct
{
  DraftIpcPaintableImpl *self;
  GDBusMethodInvocation *invocation;
  GdkPaintable *paintable;
  GtkSnapshot *snapshot;
  double width;
  double height;
} Snapshot;

static void
snapshot_free (Snapshot *snapshot)
{
  g_clear_object (&snapshot->self);
  g_clear_object (&snapshot->invocation);
  g_clear_object (&snapshot->paintable);
  g_clear_object (&snapshot->snapshot);
  g_free (snapshot);
}

static void
snapshot_after_paint_cb (GdkFrameClock *frame_clock,
                         Snapshot      *snapshot)
{
  g_autoptr(GskRenderNode) node = NULL;
  g_autoptr(GBytes) bytes = NULL;

  g_assert (GDK_IS_FRAME_CLOCK (frame_clock));
  g_assert (snapshot != NULL);
  g_assert (GTK_IS_SNAPSHOT (snapshot->snapshot));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (snapshot->invocation));

  g_signal_handlers_disconnect_by_func (frame_clock,
                                        G_CALLBACK (snapshot_after_paint_cb),
                                        snapshot);

  gdk_paintable_snapshot (snapshot->paintable,
                          snapshot->snapshot,
                          snapshot->width,
                          snapshot->height);
  node = gtk_snapshot_free_to_node (g_steal_pointer (&snapshot->snapshot));

  if (node != NULL)
    bytes = gsk_render_node_serialize (node);

  if (bytes != NULL)
    draft_ipc_paintable_complete_snapshot (DRAFT_IPC_PAINTABLE (snapshot->self),
                                           g_steal_pointer (&snapshot->invocation),
                                           (const char *)g_bytes_get_data (bytes, NULL));
  else
    g_dbus_method_invocation_return_error (g_steal_pointer (&snapshot->invocation),
                                           G_DBUS_ERROR,
                                           G_DBUS_ERROR_FAILED,
                                           "Widget failed to snapshot");

  snapshot_free (snapshot);
}

static gboolean
draft_ipc_paintable_impl_handle_snapshot (DraftIpcPaintable     *paintable,
                                          GDBusMethodInvocation *invocation,
                                          double                 width,
                                          double                 height)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;
  g_autoptr(GtkSnapshot) snapshot = NULL;
  g_autoptr(GskRenderNode) node = NULL;
  g_autoptr(GBytes) bytes = NULL;
  GdkFrameClock *frame_clock;
  GtkWidget *widget;
  Snapshot *state;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(widget = gtk_widget_paintable_get_widget (GTK_WIDGET_PAINTABLE (self->paintable))))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Widget is not ready for snapshot");
      return TRUE;
    }

  snapshot = gtk_snapshot_new ();
  gdk_paintable_snapshot (self->paintable, snapshot, width, height);
  node = gtk_snapshot_free_to_node (g_steal_pointer (&snapshot));

  if (node != NULL)
    bytes = gsk_render_node_serialize (node);

  if (bytes == NULL)
    {
      state = g_new0 (Snapshot, 1);
      state->self = g_object_ref (self);
      state->invocation = g_steal_pointer (&invocation);
      state->paintable = g_object_ref (self->paintable);
      state->width = width;
      state->height = height;
      state->snapshot = gtk_snapshot_new ();

      frame_clock = gtk_widget_get_frame_clock (widget);
      g_signal_connect (frame_clock,
                        "after-paint",
                        G_CALLBACK (snapshot_after_paint_cb),
                        state);

      return TRUE;
    }

  draft_ipc_paintable_complete_snapshot (paintable,
                                         g_steal_pointer (&invocation),
                                         (const char *)g_bytes_get_data (bytes, NULL));

  return TRUE;
}

static gboolean
draft_ipc_paintable_impl_get_intrinsic_height (DraftIpcPaintable     *paintable,
                                               GDBusMethodInvocation *invocation)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));

  draft_ipc_paintable_complete_get_intrinsic_height (paintable,
                                                     invocation,
                                                     gdk_paintable_get_intrinsic_height (self->paintable));

  return TRUE;
}

static gboolean
draft_ipc_paintable_impl_get_intrinsic_width (DraftIpcPaintable     *paintable,
                                              GDBusMethodInvocation *invocation)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));

  draft_ipc_paintable_complete_get_intrinsic_width (paintable,
                                                    invocation,
                                                    gdk_paintable_get_intrinsic_width (self->paintable));

  return TRUE;
}

static gboolean
draft_ipc_paintable_impl_get_intrinsic_aspect_ratio (DraftIpcPaintable     *paintable,
                                                     GDBusMethodInvocation *invocation)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));

  draft_ipc_paintable_complete_get_intrinsic_aspect_ratio (paintable,
                                                           invocation,
                                                           gdk_paintable_get_intrinsic_aspect_ratio (self->paintable));

  return TRUE;
}

static void
get_widget_allocation (GtkWidget     *widget,
                       GtkAllocation *allocation)
{
  GtkStyleContext *style;
  GtkNative *native;
  GtkBorder padding;
  GtkBorder margin;
  double x, y;

  g_assert (GTK_IS_WIDGET (widget));
  g_assert (allocation != NULL);

  gtk_widget_get_allocation (widget, allocation);
  style = gtk_widget_get_style_context (widget);
  gtk_style_context_get_padding (style, &padding);
  gtk_style_context_get_margin (style, &margin);

  native = gtk_widget_get_native (widget);
  gtk_widget_translate_coordinates (widget, GTK_WIDGET (native),
                                    -(margin.left + padding.left),
                                    -(margin.top + padding.top),
                                    &x, &y);

  allocation->x = (int)x;
  allocation->y = (int)y;
}

static gboolean
draft_ipc_paintable_impl_pick (DraftIpcPaintable     *paintable,
                               GDBusMethodInvocation *invocation,
                               double                 x,
                               double                 y)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;
  GtkWidget *widget;
  GtkWidget *pick;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));

  if (!(widget = gtk_widget_paintable_get_widget (GTK_WIDGET_PAINTABLE (self->paintable))))
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Widget has been disposed");
      return TRUE;
    }

  pick = gtk_widget_pick (widget, x, y, GTK_PICK_INSENSITIVE | GTK_PICK_NON_TARGETABLE);

  if (pick != NULL)
    {
      for (; pick != NULL; pick = gtk_widget_get_parent (pick))
        {
          GtkAllocation alloc;
          const char *object_id;

          if (GTK_IS_BUILDABLE (pick))
            object_id = gtk_buildable_get_buildable_id (GTK_BUILDABLE (pick));
          else
            object_id = g_object_get_data (G_OBJECT (pick), "gtk-builder-id");

          if (object_id == NULL)
            continue;

          /* Make sure our object-id is from our .ui file and not a buildable
           * from a widget included from our .ui file.
           */
          if (gtk_builder_get_object (self->builder, object_id) != G_OBJECT (pick))
            continue;

          get_widget_allocation (GTK_WIDGET (pick), &alloc);
          draft_ipc_paintable_complete_pick (paintable,
                                             g_steal_pointer (&invocation),
                                             object_id,
                                             g_variant_new ("(iiii)",
                                                            alloc.x,
                                                            alloc.y,
                                                            alloc.width,
                                                            alloc.height));

          return TRUE;
        }
    }

  g_dbus_method_invocation_return_error (invocation,
                                         G_DBUS_ERROR,
                                         G_DBUS_ERROR_FAILED,
                                         "No widget was found");

  return TRUE;
}

static gboolean
draft_ipc_paintable_impl_get_allocation (DraftIpcPaintable     *paintable,
                                         GDBusMethodInvocation *invocation,
                                         const char            *object_id)
{
  DraftIpcPaintableImpl *self = (DraftIpcPaintableImpl *)paintable;
  GtkAllocation alloc;
  GObject *widget;

  g_assert (DRAFT_IPC_IS_PAINTABLE_IMPL (self));
  g_assert (object_id != NULL);

  if (self->builder == NULL ||
      !(widget = gtk_builder_get_object (self->builder, object_id)) ||
      !GTK_IS_WIDGET (widget))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "No such widget \"%s\"",
                                             object_id);
    }
  else
    {
      get_widget_allocation (GTK_WIDGET (widget), &alloc);
      draft_ipc_paintable_complete_get_allocation (paintable,
                                                   g_steal_pointer (&invocation),
                                                   g_variant_new ("(iiii)",
                                                                  alloc.x,
                                                                  alloc.y,
                                                                  alloc.width,
                                                                  alloc.height));
    }

  return TRUE;
}

static void
paintable_iface_init (DraftIpcPaintableIface *iface)
{
  iface->handle_snapshot = draft_ipc_paintable_impl_handle_snapshot;
  iface->handle_get_intrinsic_height = draft_ipc_paintable_impl_get_intrinsic_height;
  iface->handle_get_intrinsic_width = draft_ipc_paintable_impl_get_intrinsic_width;
  iface->handle_get_intrinsic_aspect_ratio = draft_ipc_paintable_impl_get_intrinsic_aspect_ratio;
  iface->handle_pick = draft_ipc_paintable_impl_pick;
  iface->handle_get_allocation = draft_ipc_paintable_impl_get_allocation;
}
