/* draft-gtk-widget.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-gtk-widget-private.h"

struct _DraftGtkWidget
{
  GtkWidget parent_instance;
  GtkBox *box;
};

static void buildable_iface_init (GtkBuildableIface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftGtkWidget, draft_gtk_widget, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

GtkWidget *
draft_gtk_widget_new (void)
{
  return g_object_new (DRAFT_TYPE_GTK_WIDGET, NULL);
}

static void
draft_gtk_widget_dispose (GObject *object)
{
  DraftGtkWidget *self = (DraftGtkWidget *)object;

  g_clear_pointer ((GtkWidget **)&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_gtk_widget_parent_class)->dispose (object);
}

static void
draft_gtk_widget_class_init (DraftGtkWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_gtk_widget_dispose;

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_css_name (widget_class, "draftgtkwidget");
}

static void
draft_gtk_widget_init (DraftGtkWidget *self)
{
  self->box = g_object_new (GTK_TYPE_BOX,
                            "orientation", GTK_ORIENTATION_VERTICAL,
                            NULL);
  gtk_widget_set_parent (GTK_WIDGET (self->box), GTK_WIDGET (self));
}

static void
draft_gtk_widget_add_child (GtkBuildable *buildable,
                            GtkBuilder   *builder,
                            GObject      *child,
                            const char   *type)
{
  DraftGtkWidget *self = DRAFT_GTK_WIDGET (buildable);

  if (GTK_IS_WIDGET (child))
    gtk_box_append (self->box, GTK_WIDGET (child));
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  iface->add_child = draft_gtk_widget_add_child;
}
