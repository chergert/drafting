/* draft-ipc-service.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <gtk/gtk.h>
#include <girepository.h>

#include "introspection/draft-type-info-private.h"

#include "draft-gtk-themes.h"
#include "draft-ipc-builder-impl.h"
#include "draft-ipc-service-impl.h"

struct _DraftIpcServiceImpl
{
  DraftIpcServiceSkeleton  parent_instance;
  GPtrArray               *builders;
};

static void
initialize_library (GIRepository *repository,
                    const char   *namespace)
{
  static GHashTable *initialized;
  gsize n_infos;

  g_assert (G_IS_IREPOSITORY (repository));
  g_assert (namespace != NULL);

  if (initialized == NULL)
    {
      initialized = g_hash_table_new (g_str_hash, g_str_equal);
      g_hash_table_insert (initialized, (char *)"Gtk", NULL);
    }

  if (g_hash_table_contains (initialized, namespace))
    return;

  g_hash_table_insert (initialized, (char *)g_intern_string (namespace), NULL);

  n_infos = g_irepository_get_n_infos (repository, namespace);

  for (gsize i = 0; i < n_infos; i++)
    {
      GIBaseInfo *info = g_irepository_get_info (repository, namespace, i);
      GIArgument return_value = {0};

      if (GI_IS_FUNCTION_INFO (info) &&
          g_strcmp0 (g_base_info_get_name (info), "init") == 0)
        {
          g_autoptr(GError) error = NULL;

          g_debug ("Found %s.init()", namespace);

          if (!g_function_info_invoke ((GIFunctionInfo *)info, NULL, 0, NULL, 0, &return_value, &error))
            g_warning ("Failed to call %s.init(): %s", namespace, error->message);
        }
    }
}

static void
reload_themes (DraftIpcServiceImpl *self)
{
  g_auto(GStrv) themes = NULL;

  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));

  themes = draft_gtk_list_themes ();
  draft_ipc_service_set_available_themes (DRAFT_IPC_SERVICE (self),
                                          (const char * const *)themes);
}

static gboolean
draft_ipc_service_handle_require (DraftIpcService       *service,
                                  GDBusMethodInvocation *invocation,
                                  const char            *name,
                                  const char            *version)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)service;
  g_autofree char *alt_version = NULL;
  g_autoptr(GError) error = NULL;
  GIRepository *repository;
  const char *dot;

  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (name != NULL);
  g_assert (version != NULL);

  /* Ignore all requests to (re)load GTK */
  if (strcasecmp (name, "gtk") == 0)
    {
      draft_ipc_service_complete_require (service,
                                          g_steal_pointer (&invocation));
      return TRUE;
    }

  /* Create an alternate version form which might help us
   * load the library in G-I version formats.
   */
  dot = strchr (version, '.');
  if (dot != NULL)
    alt_version = g_strndup (version, dot - version);
  else
    alt_version = g_strdup_printf ("%s.0", version);

  repository = g_irepository_get_default ();
  if (!g_irepository_require (repository, name, version, 0, &error))
    {
      g_autoptr(GError) alt_error = NULL;

      /* Try an alternate version (add .0 to the version or remove it) */
      if (g_irepository_require (repository, name, alt_version, 0, &alt_error))
        {
          g_clear_error (&error);
          goto initialize;
        }

      g_warning ("Failed to require %s version %s: %s",
                 name, version, error->message);
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "%s", error->message);
      return TRUE;
    }

initialize:
  /* Try to call Library.init() if it exists and has no parameters
   * so that we ensure we can use the library. For example, adw_init().
   */
  initialize_library (repository, name);

  draft_ipc_service_complete_require (service,
                                      g_steal_pointer (&invocation));

  /* Reload themes as the library may have provided new themes
   * (such as libadwaita-1), but possibly others if that whole
   * paradigm takes off.
   */
  reload_themes (self);

  return TRUE;
}

static gboolean
draft_ipc_service_handle_set_theme (DraftIpcService       *service,
                                    GDBusMethodInvocation *invocation,
                                    const char            *name)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)service;
  const char * const *themes;
  GtkSettings *settings;

  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (name != NULL);

  settings = gtk_settings_get_default ();
  themes = draft_ipc_service_get_available_themes (service);

  if (!g_strv_contains (themes, name))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "No such theme “%s”",
                                             name);
    }
  else
    {
      g_object_set (settings, "gtk-theme-name", name, NULL);
      draft_ipc_service_complete_set_theme (service, g_steal_pointer (&invocation));
    }

  return TRUE;
}

static gboolean
draft_ipc_service_handle_resolve (DraftIpcService       *service,
                                  GDBusMethodInvocation *invocation,
                                  const char            *name)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)service;
  g_autoptr(DraftTypeInfo) type_info = NULL;
  GVariantBuilder builder;
  GType gtype;

  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (name != NULL);

  gtype = g_type_from_name (name);

  if (gtype != G_TYPE_INVALID)
    {
      if (G_TYPE_IS_ENUM (gtype))
        type_info = (DraftTypeInfo *)draft_enum_info_new (gtype);
      else if (G_TYPE_IS_FLAGS (gtype))
        type_info = (DraftTypeInfo *)draft_flags_info_new (gtype);
      else if (G_TYPE_IS_OBJECT (gtype))
        type_info = (DraftTypeInfo *)draft_class_info_new (gtype);
    }

  if (type_info == NULL)
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Cannot locate type by name \"%s\"",
                                             name);
      return TRUE;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize (type_info, &builder);

  draft_ipc_service_complete_resolve (service,
                                      g_steal_pointer (&invocation),
                                      g_variant_builder_end (&builder));

  return TRUE;
}

static void
draft_ipc_service_builder_destroyed_cb (DraftIpcServiceImpl *self,
                                        DraftIpcBuilderImpl *builder)
{
  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));
  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (builder));

  g_ptr_array_remove (self->builders, builder);
}

static gboolean
draft_ipc_service_handle_get_builder (DraftIpcService       *service,
                                      GDBusMethodInvocation *invocation,
                                      const char            *builder_ui)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)service;
  g_autoptr(GtkBuilder) gtk_builder = NULL;
  g_autoptr(DraftIpcBuilder) builder = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree char *object_path = NULL;
  g_autofree char *guid = NULL;

  g_assert (DRAFT_IPC_IS_SERVICE_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (builder_ui != NULL);

  gtk_builder = gtk_builder_new ();

  if (!gtk_builder_add_from_string (gtk_builder, builder_ui, -1, &error))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_IO_ERROR,
                                             G_IO_ERROR_FAILED,
                                             "Failed to parse GtkBuilder XML: %s",
                                             error->message);
      return TRUE;
    }

  builder = draft_ipc_builder_impl_new (gtk_builder);
  guid = g_dbus_generate_guid ();
  object_path = g_strdup_printf ("/org/gnome/Drafting/Builder/%s", guid);

  g_signal_connect_object (builder,
                           "destroyed",
                           G_CALLBACK (draft_ipc_service_builder_destroyed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_ptr_array_add (self->builders, g_object_ref (builder));

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (builder),
                                    g_dbus_method_invocation_get_connection (invocation),
                                    object_path, NULL);

  draft_ipc_service_complete_get_builder (service,
                                          g_steal_pointer (&invocation),
                                          object_path);

  return TRUE;
}

static void
service_iface_init (DraftIpcServiceIface *iface)
{
  iface->handle_require = draft_ipc_service_handle_require;
  iface->handle_set_theme = draft_ipc_service_handle_set_theme;
  iface->handle_resolve = draft_ipc_service_handle_resolve;
  iface->handle_get_builder = draft_ipc_service_handle_get_builder;
}

G_DEFINE_TYPE_WITH_CODE (DraftIpcServiceImpl, draft_ipc_service_impl, DRAFT_IPC_TYPE_SERVICE_SKELETON,
                         G_IMPLEMENT_INTERFACE (DRAFT_IPC_TYPE_SERVICE, service_iface_init))

static void
draft_ipc_service_constructed (GObject *object)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)object;
  g_autoptr(GtkCssProvider) css_provider = NULL;
  GtkSettings *settings;
  GdkDisplay *display;

  G_OBJECT_CLASS (draft_ipc_service_impl_parent_class)->constructed (object);

  /* Set available themes for query */
  reload_themes (self);

  /* Track the current theme name */
  settings = gtk_settings_get_default ();
  g_object_bind_property (settings, "gtk-theme-name", self, "theme", G_BINDING_SYNC_CREATE);
  g_object_set (settings,
                "gtk-decoration-layout", ":close",
                NULL);

  /* Disable transition animations to ensure we get better snapshots */
  display = gdk_display_get_default ();
  css_provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (css_provider,
                                   "* { animation: none; "
                                   "    transition-property: none; "
                                   "    transition-duration: 0; }\n"
                                   "window.csd { border-radius: 0px; }\n"
                                   "popover contents { margin: 0; box-shadow: none; border-radius: 0; }\n",
                                   -1);
  gtk_style_context_add_provider_for_display (display,
                                              GTK_STYLE_PROVIDER (css_provider),
                                              G_MAXUINT);
}

static void
draft_ipc_service_finalize (GObject *object)
{
  DraftIpcServiceImpl *self = (DraftIpcServiceImpl *)object;

  g_clear_pointer (&self->builders, g_ptr_array_unref);

  G_OBJECT_CLASS (draft_ipc_service_impl_parent_class)->finalize (object);
}

static void
draft_ipc_service_impl_class_init (DraftIpcServiceImplClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = draft_ipc_service_constructed;
  object_class->finalize = draft_ipc_service_finalize;
}

static void
draft_ipc_service_impl_init (DraftIpcServiceImpl *self)
{
  self->builders = g_ptr_array_new_with_free_func (g_object_unref);
}

DraftIpcService *
draft_ipc_service_impl_new (void)
{
  return g_object_new (DRAFT_IPC_TYPE_SERVICE_IMPL, NULL);
}
