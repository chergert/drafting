/* draft-ipc-builder.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "service/draft-ipc-builder-impl.h"
#include "service/draft-ipc-paintable-impl.h"
#include "service/draft-gtk-window-private.h"
#include "util/draft-utils-private.h"

struct _DraftIpcBuilderImpl
{
  DraftIpcBuilderSkeleton  parent_instance;
  GtkBuilder              *builder;
  GHashTable              *paintables;
};

static void builder_iface_init (DraftIpcBuilderIface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftIpcBuilderImpl, draft_ipc_builder_impl, DRAFT_IPC_TYPE_BUILDER_SKELETON,
                         G_IMPLEMENT_INTERFACE (DRAFT_IPC_TYPE_BUILDER, builder_iface_init))

enum {
  DESTROYED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

DraftIpcBuilder *
draft_ipc_builder_impl_new (GtkBuilder *builder)
{
  DraftIpcBuilderImpl *self;

  g_return_val_if_fail (GTK_IS_BUILDER (builder), NULL);

  self = g_object_new (DRAFT_IPC_TYPE_BUILDER_IMPL, NULL);
  self->builder = g_object_ref (builder);

  return DRAFT_IPC_BUILDER (self);
}

static void
draft_ipc_builder_impl_dispose (GObject *object)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)object;

  g_clear_object (&self->builder);
  g_clear_pointer (&self->paintables, g_hash_table_unref);

  G_OBJECT_CLASS (draft_ipc_builder_impl_parent_class)->dispose (object);
}

static void
draft_ipc_builder_impl_class_init (DraftIpcBuilderImplClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_ipc_builder_impl_dispose;

  signals [DESTROYED] =
    g_signal_new ("destroyed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL, G_TYPE_NONE, 0);
}

static void
draft_ipc_builder_impl_init (DraftIpcBuilderImpl *self)
{
  self->paintables = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

static gboolean
draft_ipc_builder_handle_resize (DraftIpcBuilder       *builder,
                                 GDBusMethodInvocation *invocation,
                                 const char            *object_id,
                                 int                    width,
                                 int                    height)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  GObject *object;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (object_id != NULL);
  g_assert (GTK_IS_BUILDER (self->builder));

  width = CLAMP (width, -1, G_MAXINT16);
  height = CLAMP (height, -1, G_MAXINT16);

  if ((object = gtk_builder_get_object (self->builder, object_id)))
    {
      if (GTK_IS_WIDGET (object))
        {
          /* Always resize the native, not the widget that looks like
           * it is the toplevel to the user.
           */
          if (!GTK_IS_NATIVE (object))
            object = G_OBJECT (gtk_widget_get_native (GTK_WIDGET (object)));

          if (object != NULL)
            {
              if (GTK_IS_WINDOW (object))
                {
                  gtk_widget_hide (GTK_WIDGET (object));
                  gtk_window_set_default_size (GTK_WINDOW (object), width, height);
                  gtk_window_present (GTK_WINDOW (object));
                }
              else
                {
                  gtk_widget_set_size_request (GTK_WIDGET (object), width, height);
                }
            }
        }
    }

  draft_ipc_builder_complete_resize (builder, g_steal_pointer (&invocation));

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_measure (DraftIpcBuilder       *builder,
                                  GDBusMethodInvocation *invocation,
                                  const char            *object_id,
                                  guint                  orientation,
                                  int                    for_size)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  GObject *object;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (object_id != NULL);
  g_assert (GTK_IS_BUILDER (self->builder));

  if (orientation != GTK_ORIENTATION_HORIZONTAL &&
      orientation != GTK_ORIENTATION_VERTICAL)
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Invalid orientation 0x%x",
                                             orientation);
      return TRUE;
    }

  if ((object = gtk_builder_get_object (self->builder, object_id)))
    {
      if (GTK_IS_WIDGET (object))
        {
          int min, nat;

          gtk_widget_measure (GTK_WIDGET (object),
                              orientation,
                              for_size,
                              &min, &nat,
                              NULL, NULL);

          draft_ipc_builder_complete_measure (builder,
                                              g_steal_pointer (&invocation),
                                              min, nat);

          return TRUE;
        }
    }

  g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                         G_DBUS_ERROR,
                                         G_DBUS_ERROR_FAILED,
                                         "Failed to locate widget \"%s\"",
                                         object_id);

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_destroy (DraftIpcBuilder       *builder,
                                  GDBusMethodInvocation *invocation)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  GHashTableIter iter;
  DraftIpcPaintable *paintable;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  /* Unexport all paintables first */
  g_hash_table_iter_init (&iter, self->paintables);
  while (g_hash_table_iter_next (&iter, NULL, (gpointer *)&paintable))
    {
      g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (paintable));
      g_hash_table_iter_remove (&iter);
    }

  /* And now ourself */
  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (builder));

  /* Notify the peer of the response before we destroy objects to
   * potentially reduce latency in the call for large sets of objects.
   */
  draft_ipc_builder_complete_destroy (builder, g_steal_pointer (&invocation));

  /* And now actually destroy the builder objects */
  g_clear_object (&self->builder);

  /* Let the service drop it's reference */
  g_signal_emit (self, signals [DESTROYED], 0);

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_queue_draw (DraftIpcBuilder       *builder,
                                     GDBusMethodInvocation *invocation,
                                     const char            *object_id)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  GObject *object;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (object_id != NULL);

  if ((object = gtk_builder_get_object (self->builder, object_id)))
    {
      if (GTK_IS_WIDGET (object))
        gtk_widget_queue_draw (GTK_WIDGET (object));
    }

  draft_ipc_builder_complete_queue_draw (builder, g_steal_pointer (&invocation));

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_get_paintable (DraftIpcBuilder       *builder,
                                        GDBusMethodInvocation *invocation,
                                        const char            *object_id)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (object_id != NULL);

  if (!g_hash_table_contains (self->paintables, object_id))
    {
      GObject *object = gtk_builder_get_object (self->builder, object_id);
      GtkWidget *renderable = NULL;

      /* Try to get a containing native if it exists */
      if (GTK_IS_WIDGET (object) && !GTK_IS_NATIVE (object))
        renderable = GTK_WIDGET (gtk_widget_get_native (GTK_WIDGET (object)));

      /* Fallback to just snapshotting the widget directly, even if that means
       * some things won't look correct. We *should* have been given a native
       * by the UI process though.
       */
      if (renderable == NULL && GTK_IS_WIDGET (object))
        renderable = GTK_WIDGET (object);

      if (renderable != NULL)
        {
          g_autoptr(DraftIpcPaintable) paintable = draft_ipc_paintable_impl_new (self->builder, renderable);
          g_autofree char *object_path = NULL;

          object_path = g_strdup_printf ("%s/%s",
                                         g_dbus_interface_skeleton_get_object_path (G_DBUS_INTERFACE_SKELETON (self)),
                                         object_id);

          g_hash_table_insert (self->paintables,
                               g_strdup (object_id),
                               g_object_ref (paintable));

          g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (paintable),
                                            g_dbus_method_invocation_get_connection (invocation),
                                            object_path,
                                            NULL);

          if (GTK_IS_NATIVE (renderable))
            {
              if (GTK_IS_WINDOW (renderable))
                gtk_window_present (GTK_WINDOW (renderable));
              else if (GTK_IS_POPOVER (renderable))
                gtk_popover_popup (GTK_POPOVER (renderable));

              /* Always clear focus widget so that we don't show anything
               * rendered with "focus" rings.
               */
              if (GTK_IS_WINDOW (renderable))
                gtk_window_set_focus (GTK_WINDOW (renderable), NULL);
            }


          draft_ipc_builder_complete_get_paintable (builder,
                                                    g_steal_pointer (&invocation),
                                                    object_path);

          return TRUE;
        }
    }
  else
    {
      DraftIpcPaintable *paintable = g_hash_table_lookup (self->paintables, object_id);

      draft_ipc_builder_complete_get_paintable (builder,
                                                g_steal_pointer (&invocation),
                                                g_dbus_interface_skeleton_get_object_path (G_DBUS_INTERFACE_SKELETON (paintable)));

    }

  g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                         G_DBUS_ERROR,
                                         G_DBUS_ERROR_FAILED,
                                         "Failed to locate widget \"%s\"",
                                         object_id);

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_get_display_objects (DraftIpcBuilder       *builder,
                                              GDBusMethodInvocation *invocation)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  g_autoptr(GArray) object_ids = NULL;
  GSList *objects;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  object_ids = g_array_new (TRUE, FALSE, sizeof (char *));
  objects = gtk_builder_get_objects (self->builder);

  for (const GSList *iter = objects; iter; iter = iter->next)
    {
      GObject *object = iter->data;

      if (GTK_IS_WIDGET (object))
        {
          GtkWidget *parent;
          const char *object_id;

          if (GTK_IS_BUILDABLE (object))
            object_id = gtk_buildable_get_buildable_id (GTK_BUILDABLE (object));
          else
            object_id = g_object_get_data (object, "gtk-builder-id");

          if (object_id == NULL)
            continue;

          /* Skip our synthesized toplevels */
          if (g_str_has_prefix (object_id, "__drafting_") &&
              g_str_has_suffix (object_id, "_parent"))
            continue;

          if (GTK_IS_NATIVE (object))
            {
              g_array_append_val (object_ids, object_id);
              continue;
            }

          if ((parent = gtk_widget_get_parent (GTK_WIDGET (object))) &&
              DRAFT_IS_GTK_WINDOW (parent))
            {
              g_array_append_val (object_ids, object_id);
              continue;
            }
        }
    }

  draft_ipc_builder_complete_get_display_objects (builder,
                                                  g_steal_pointer (&invocation),
                                                  (const char * const *)(gpointer)object_ids->data);

  g_slist_free (objects);

  return TRUE;
}

static gboolean
load_from_variant (GtkBuilder *builder,
                   GObject    *object,
                   GParamSpec *pspec,
                   GVariant   *variant)
{
  GValue value = G_VALUE_INIT;
  g_autoptr(GVariant) unwrapped = NULL;

  g_assert (GTK_IS_BUILDER (builder));
  g_assert (G_IS_OBJECT (object));
  g_assert (pspec != NULL);
  g_assert (variant != NULL);
  g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT));

  /* We have a value wrapped in a variant here, so get the child
   * value which is what we really want.
   */
  if (g_variant_n_children (variant) != 1 ||
      !(unwrapped = g_variant_get_child_value (variant, 0)))
    return FALSE;

  g_value_init (&value, pspec->value_type);
  if (!_draft_value_decode (&value, unwrapped, builder))
    return FALSE;

  g_object_set_property (object, pspec->name, &value);
  g_value_unset (&value);

  return TRUE;
}

static gboolean
draft_ipc_builder_handle_set_item_property (DraftIpcBuilder       *builder,
                                            GDBusMethodInvocation *invocation,
                                            const char            *object_id,
                                            const char            *name,
                                            GVariant              *value)
{
  DraftIpcBuilderImpl *self = (DraftIpcBuilderImpl *)builder;
  GParamSpec *pspec;
  GObject *object;

  g_assert (DRAFT_IPC_IS_BUILDER_IMPL (self));
  g_assert (object_id != NULL);
  g_assert (name != NULL);
  g_assert (value != NULL);

  if (!(object = gtk_builder_get_object (self->builder, object_id)) ||
      !(pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (object), name)) ||
      !load_from_variant (self->builder, object, pspec, value))
    g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                           G_DBUS_ERROR,
                                           G_DBUS_ERROR_FAILED,
                                           "Cannot apply property %s to object %s",
                                           name, object_id);
  else
    draft_ipc_builder_complete_set_item_property (builder, g_steal_pointer (&invocation));

  return TRUE;
}

static void
builder_iface_init (DraftIpcBuilderIface *iface)
{
  iface->handle_resize = draft_ipc_builder_handle_resize;
  iface->handle_measure = draft_ipc_builder_handle_measure;
  iface->handle_destroy = draft_ipc_builder_handle_destroy;
  iface->handle_queue_draw = draft_ipc_builder_handle_queue_draw;
  iface->handle_get_paintable = draft_ipc_builder_handle_get_paintable;
  iface->handle_get_display_objects = draft_ipc_builder_handle_get_display_objects;
  iface->handle_set_item_property = draft_ipc_builder_handle_set_item_property;
}
