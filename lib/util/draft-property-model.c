/* draft-property-model.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-type-cache-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-property.h"
#include "util/draft-property-model-private.h"

struct _DraftPropertyModel
{
  GObject      parent_instance;

  DraftObject *object;
  GPtrArray   *items;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftPropertyModel, draft_property_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

enum {
  PROP_0,
  PROP_OBJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftPropertyModel *
draft_property_model_new (DraftObject *object)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT (object), NULL);

  return g_object_new (DRAFT_TYPE_PROPERTY_MODEL,
                       "object", object,
                       NULL);
}

static int
sort_items (gconstpointer a,
            gconstpointer b)
{
  DraftProperty **ap = (DraftProperty **)a;
  DraftProperty **bp = (DraftProperty **)b;

  return g_strcmp0 (draft_property_get_name (*ap),
                    draft_property_get_name (*bp));
}

static void
draft_property_model_constructed (GObject *object)
{
  DraftPropertyModel *self = (DraftPropertyModel *)object;
  DraftClassInfo *type_info;

  G_OBJECT_CLASS (draft_property_model_parent_class)->constructed (object);

  if (self->object == NULL)
    return;

  type_info = (DraftClassInfo *)DRAFT_ITEM (self->object)->type_info;
  if (!DRAFT_TYPE_INFO_IS_CLASS ((DraftTypeInfo *)type_info))
    return;

  if (type_info->properties == NULL || type_info->properties->len == 0)
    return;

  for (guint i = 0; i < type_info->properties->len; i++)
    {
      DraftPropertyInfo *info = g_ptr_array_index (type_info->properties, i);

      if ((info->flags & G_PARAM_WRITABLE) != 0)
        {
          DraftProperty *found;

          if ((found = draft_object_find_property (self->object, info->name)))
            {
              g_ptr_array_add (self->items, g_object_ref (found));
            }
          else
            {
              g_autoptr(DraftProperty) prop = draft_property_new ();

              DRAFT_ITEM (prop)->type_info = draft_type_info_ref (type_info);
              draft_property_set_name (prop, info->name);
              _draft_item_insert_after (DRAFT_ITEM (prop), DRAFT_ITEM (self->object), NULL);
              g_ptr_array_add (self->items, g_steal_pointer (&prop));
            }
        }
    }

  g_ptr_array_sort (self->items, sort_items);
}

static void
draft_property_model_dispose (GObject *object)
{
  DraftPropertyModel *self = (DraftPropertyModel *)object;

  g_clear_object (&self->object);

  G_OBJECT_CLASS (draft_property_model_parent_class)->dispose (object);
}

static void
draft_property_model_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  DraftPropertyModel *self = DRAFT_PROPERTY_MODEL (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_property_model_get_object (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_model_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  DraftPropertyModel *self = DRAFT_PROPERTY_MODEL (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      self->object = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_model_class_init (DraftPropertyModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = draft_property_model_constructed;
  object_class->dispose = draft_property_model_dispose;
  object_class->get_property = draft_property_model_get_property;
  object_class->set_property = draft_property_model_set_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "Object",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_property_model_init (DraftPropertyModel *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
}

static gpointer
draft_property_model_get_item (GListModel *model,
                               guint       position)
{
  DraftPropertyModel *self = (DraftPropertyModel *)model;

  g_assert (DRAFT_IS_PROPERTY_MODEL (self));

  if (position < self->items->len)
    return g_object_ref (g_ptr_array_index (self->items, position));

  return NULL;
}

static guint
draft_property_model_get_n_items (GListModel *model)
{
  return DRAFT_PROPERTY_MODEL (model)->items->len;
}

static GType
draft_property_model_get_item_type (GListModel *model)
{
  return DRAFT_TYPE_PROPERTY;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item = draft_property_model_get_item;
  iface->get_n_items = draft_property_model_get_n_items;
  iface->get_item_type = draft_property_model_get_item_type;
}

DraftObject *
draft_property_model_get_object (DraftPropertyModel *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY_MODEL (self), NULL);

  return self->object;
}
