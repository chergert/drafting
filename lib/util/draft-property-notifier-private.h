/* draft-property-notifier-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_PROPERTY_NOTIFIER (draft_property_notifier_get_type())

G_DECLARE_FINAL_TYPE (DraftPropertyNotifier, draft_property_notifier, DRAFT, PROPERTY_NOTIFIER, GObject)

DraftPropertyNotifier *draft_property_notifier_new         (void);
DraftItem             *draft_property_notifier_get_item    (DraftPropertyNotifier  *self);
void                   draft_property_notifier_set_item    (DraftPropertyNotifier  *self,
                                                            DraftItem              *item);
gboolean               draft_property_notifier_get_enum    (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            GType                   enum_type,
                                                            guint                  *value);
gboolean               draft_property_notifier_get_string  (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            const char            **string);
gboolean               draft_property_notifier_get_boolean (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            gboolean               *value);
gboolean               draft_property_notifier_get_int     (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            int                    *value);
void                   draft_property_notifier_set_boolean (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            gboolean                value,
                                                            DraftTransaction       *transaction);
void                   draft_property_notifier_set_enum    (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            GType                   type,
                                                            guint                   value,
                                                            DraftTransaction       *transaction);
void                   draft_property_notifier_set_string  (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            const char             *string,
                                                            DraftTransaction       *transaction);
void                   draft_property_notifier_set_int     (DraftPropertyNotifier  *self,
                                                            const char             *property_name,
                                                            int                     value,
                                                            DraftTransaction       *transaction);

G_END_DECLS
