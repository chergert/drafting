/* draft-utils.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/* GTK - The GIMP Toolkit
 * Copyright (C) 1998-2002 James Henstridge <james@daa.com.au>
 * Copyright (C) 2006-2007 Async Open Source,
 *                         Johan Dahlin <jdahlin@async.com.br>,
 *                         Henrique Romano <henrique@async.com.br>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#define _GNU_SOURCE

#include <gtk/gtk.h>

#ifdef G_OS_UNIX
# include <sys/mman.h>
# include <sys/syscall.h>
#endif

#include "util/draft-utils-private.h"

gboolean
_draft_flags_from_string (GType         type,
                          GFlagsValue  *aliases,
                          const char   *string,
                          guint        *flags_value,
                          GError      **error)
{
  GFlagsClass *fclass;
  char *endptr, *prevptr;
  guint i, j, k, value;
  char *flagstr;
  GFlagsValue *fv;
  const char *flag;
  gunichar ch;
  gboolean eos, ret;

  g_return_val_if_fail (G_TYPE_IS_FLAGS (type), FALSE);
  g_return_val_if_fail (string != 0, FALSE);

  ret = TRUE;

  endptr = NULL;
  errno = 0;
  value = g_ascii_strtoull (string, &endptr, 0);
  if (errno == 0 && endptr != string) /* parsed a number */
    *flags_value = value;
  else
    {
      fclass = g_type_class_ref (type);

      flagstr = g_strdup (string);
      for (value = i = j = 0; ; i++)
        {

          eos = flagstr[i] == '\0';

          if (!eos && flagstr[i] != '|')
            continue;

          flag = &flagstr[j];
          endptr = &flagstr[i];

          if (!eos)
            {
              flagstr[i++] = '\0';
              j = i;
            }

          /* trim spaces */
          for (;;)
            {
              ch = g_utf8_get_char (flag);
              if (!g_unichar_isspace (ch))
                break;
              flag = g_utf8_next_char (flag);
            }

          while (endptr > flag)
            {
              prevptr = g_utf8_prev_char (endptr);
              ch = g_utf8_get_char (prevptr);
              if (!g_unichar_isspace (ch))
                break;
              endptr = prevptr;
            }

          if (endptr > flag)
            {
              *endptr = '\0';

              fv = NULL;

              if (aliases)
                {
                  for (k = 0; aliases[k].value_nick; k++)
                    {
                      if (g_ascii_strcasecmp (aliases[k].value_nick, flag) == 0)
                        {
                          fv = &aliases[k];
                          break;
                        }
                    }
                }

              if (!fv)
                fv = g_flags_get_value_by_name (fclass, flag);

              if (!fv)
                fv = g_flags_get_value_by_nick (fclass, flag);

              if (fv)
                value |= fv->value;
              else
                {
                  g_set_error (error,
                               GTK_BUILDER_ERROR,
                               GTK_BUILDER_ERROR_INVALID_VALUE,
                               "Unknown flag: '%s'",
                               flag);
                  ret = FALSE;
                  break;
                }
            }

          if (eos)
            {
              *flags_value = value;
              break;
            }
        }

      g_free (flagstr);

      g_type_class_unref (fclass);
    }

  return ret;
}

char *
_draft_flags_to_string (GType type,
                        guint value)
{
  GFlagsClass *flags_class;
  GFlagsValue *val;
  GString *str;

  g_return_val_if_fail (G_TYPE_IS_FLAGS (type), NULL);

  if (value == 0)
    return NULL;

  if (!(flags_class = g_type_class_ref (type)))
    return NULL;

  if (flags_class == NULL)
    return NULL;

  str = g_string_new (NULL);

  while ((str->len == 0 || value != 0) &&
         (val = g_flags_get_first_value (flags_class, value)))
    {
      if (str->len > 0)
        g_string_append_c (str, '|');
      g_string_append (str, val->value_nick);
      value &= ~val->value;
    }

  if (value)
    {
      if (str->len > 0)
        g_string_append_c (str, '|');
      g_string_append_printf (str, "0x%x", value);
    }

  return g_string_free (str, FALSE);
}

gboolean
_draft_boolean_from_string (const char  *string,
                            gboolean    *value,
                            GError     **error)
{
  if (string[0] == '\0')
    goto error;
  else if (string[1] == '\0')
    {
      char c;

      c = string[0];
      if (c == '1' ||
          c == 'y' || c == 't' ||
          c == 'Y' || c == 'T')
        *value = TRUE;
      else if (c == '0' ||
               c == 'n' || c == 'f' ||
               c == 'N' || c == 'F')
        *value = FALSE;
      else
        goto error;
    }
  else
    {
      if (g_ascii_strcasecmp (string, "true") == 0 ||
          g_ascii_strcasecmp (string, "yes") == 0)
        *value = TRUE;
      else if (g_ascii_strcasecmp (string, "false") == 0 ||
               g_ascii_strcasecmp (string, "no") == 0)
        *value = FALSE;
      else
        goto error;
    }

  return TRUE;

error:
  g_set_error (error,
               GTK_BUILDER_ERROR,
               GTK_BUILDER_ERROR_INVALID_VALUE,
               "Could not parse boolean '%s'",
               string);
  return FALSE;
}

static gboolean
filter_by_type (gpointer item,
                gpointer user_data)
{
  GType type = GPOINTER_TO_SIZE (user_data);
  return g_type_is_a (G_OBJECT_TYPE (item), type);
}

GListModel *
_draft_item_type_filter_new (GListModel *model,
                             GType       type)
{
  g_return_val_if_fail (G_IS_LIST_MODEL (model), NULL);
  g_return_val_if_fail (g_type_is_a (type, G_TYPE_OBJECT), NULL);

  return G_LIST_MODEL (gtk_filter_list_model_new (g_object_ref (model),
                                                  GTK_FILTER (gtk_custom_filter_new (filter_by_type,
                                                                                     GSIZE_TO_POINTER (type),
                                                                                     NULL))));
}

static const char *
get_object_id (GObject *object)
{
  if (GTK_IS_BUILDABLE (object))
    return gtk_buildable_get_buildable_id (GTK_BUILDABLE (object));
  else
    return g_object_get_data (object, "gtk-builder-id");
}

GVariant *
_draft_value_encode (const GValue *value)
{
  g_return_val_if_fail (value != NULL, NULL);
  g_return_val_if_fail (G_VALUE_TYPE (value), NULL);

  if (G_VALUE_HOLDS_STRING (value))
    {
      const char *str = g_value_get_string (value);
      if (str != NULL)
        return g_variant_new_string (str);
      return g_variant_new_array (G_VARIANT_TYPE_STRING, NULL, 0);
    }
  else if (G_VALUE_HOLDS_INT (value))
    return g_variant_new_int32 (g_value_get_int (value));
  else if (G_VALUE_HOLDS_UINT (value) ||
           G_VALUE_HOLDS_ENUM (value) ||
           G_VALUE_HOLDS_FLAGS (value))
    return g_variant_new_uint32 (g_value_get_uint (value));
  else if (G_VALUE_HOLDS_INT64 (value))
    return g_variant_new_int64 (g_value_get_int64 (value));
  else if (G_VALUE_HOLDS_UINT64 (value))
    return g_variant_new_uint64 (g_value_get_uint64 (value));
  else if (G_VALUE_HOLDS_FLOAT (value))
    return g_variant_new_double (g_value_get_float (value));
  else if (G_VALUE_HOLDS_DOUBLE (value))
    return g_variant_new_double (g_value_get_double (value));
  else if (G_VALUE_HOLDS_CHAR (value))
    return g_variant_new_uint32 (g_value_get_schar (value));
  else if (G_VALUE_HOLDS_UCHAR (value))
    return g_variant_new_uint32 (g_value_get_uchar (value));
  else if (G_VALUE_HOLDS_LONG (value))
    return g_variant_new_int64 (g_value_get_int64 (value));
  else if (G_VALUE_HOLDS_ULONG (value))
    return g_variant_new_uint64 (g_value_get_uint64 (value));
  else if (G_VALUE_HOLDS_GTYPE (value))
    return g_variant_new_string (g_type_name (g_value_get_gtype (value)) ?: "");
  else if (G_VALUE_HOLDS_BOOLEAN (value))
    return g_variant_new_boolean (g_value_get_boolean (value));
  else if (G_VALUE_HOLDS_VARIANT (value))
    return g_variant_new_maybe (G_VARIANT_TYPE_VARIANT, g_value_get_variant (value));
  else if (G_VALUE_HOLDS (value, G_TYPE_STRV))
    {
      const char * const *val = g_value_get_boxed (value);

      if (val != NULL)
        return g_variant_new_strv (val, -1);
      else
        return g_variant_new_maybe (G_VARIANT_TYPE_STRING_ARRAY, NULL);
    }
  else if (G_VALUE_HOLDS_OBJECT (value))
    {
      const char *object_id = get_object_id (g_value_get_object (value));
      return g_variant_new_maybe (G_VARIANT_TYPE_STRING,
                                  object_id ? g_variant_new_string (object_id) : NULL);
    }

  return NULL;
}

gboolean
_draft_value_from_variant (GValue     *value,
                           GVariant   *variant,
                           const char *type_name)
{
  g_return_val_if_fail (value != NULL, FALSE);
  g_return_val_if_fail (variant != NULL, FALSE);

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING))
    {
      g_value_init (value, G_TYPE_STRING);
      g_value_set_string (value, g_variant_get_string (variant, NULL));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING_ARRAY))
    {
      /* A common D-Bus convention is to use an empty string[] to
       * represent a NULL string since strings can't be null by
       * D-Bus protocol.
       */
      if (g_strcmp0 (type_name, "gchararray") == 0)
        {
          g_value_init (value, G_TYPE_STRING);
        }
      else
        {
          g_value_init (value, G_TYPE_STRV);
          g_value_take_boxed (value, g_variant_dup_strv (variant, NULL));
        }
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT32))
    {
      g_value_init (value, G_TYPE_INT);
      g_value_set_int (value, g_variant_get_int32 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT32))
    {
      g_value_init (value, G_TYPE_UINT);
      g_value_set_uint (value, g_variant_get_uint32 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64))
    {
      g_value_init (value, G_TYPE_INT64);
      g_value_set_int64 (value, g_variant_get_int64 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT64))
    {
      g_value_init (value, G_TYPE_UINT64);
      g_value_set_uint64 (value, g_variant_get_uint64 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DOUBLE))
    {
      g_value_init (value, G_TYPE_DOUBLE);
      g_value_set_double (value, g_variant_get_double (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BOOLEAN))
    {
      g_value_init (value, G_TYPE_BOOLEAN);
      g_value_set_boolean (value, g_variant_get_boolean (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT))
    {
      g_value_init (value, G_TYPE_VARIANT);
      g_value_take_variant (value, g_variant_get_variant (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("ms")))
    {
      g_autoptr(GVariant) child = g_variant_get_maybe (variant);

      g_value_init (value, G_TYPE_STRING);

      if (child != NULL)
        g_value_set_string (value, g_variant_get_string (child, NULL));
      else
        g_value_set_string (value, NULL);
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("mas")))
    {
      g_autoptr(GVariant) child = g_variant_get_maybe (variant);

      g_value_init (value, G_TYPE_STRV);

      if (child != NULL)
        g_value_take_boxed (value, g_variant_dup_strv (child, NULL));
      else
        g_value_set_boxed (value, NULL);
    }
  else
    return FALSE;

  return TRUE;
}

gboolean
_draft_value_decode (GValue     *value,
                     GVariant   *variant,
                     GtkBuilder *builder)
{
  GValue tmp = G_VALUE_INIT;

  g_return_val_if_fail (G_VALUE_TYPE (value), FALSE);
  g_return_val_if_fail (variant != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_BUILDER (builder), FALSE);

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING))
    {
      g_value_init (&tmp, G_TYPE_STRING);
      g_value_set_string (&tmp, g_variant_get_string (variant, NULL));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING_ARRAY))
    {
      g_value_init (&tmp, G_TYPE_STRV);
      g_value_take_boxed (&tmp, g_variant_dup_strv (variant, NULL));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT32))
    {
      g_value_init (&tmp, G_TYPE_INT);
      g_value_set_int (&tmp, g_variant_get_int32 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT32))
    {
      g_value_init (&tmp, G_TYPE_UINT);
      g_value_set_uint (&tmp, g_variant_get_uint32 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64))
    {
      g_value_init (&tmp, G_TYPE_INT64);
      g_value_set_int64 (&tmp, g_variant_get_int64 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT64))
    {
      g_value_init (&tmp, G_TYPE_UINT64);
      g_value_set_uint64 (&tmp, g_variant_get_uint64 (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DOUBLE))
    {
      g_value_init (&tmp, G_TYPE_DOUBLE);
      g_value_set_double (&tmp, g_variant_get_double (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BOOLEAN))
    {
      g_value_init (&tmp, G_TYPE_BOOLEAN);
      g_value_set_boolean (&tmp, g_variant_get_boolean (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT))
    {
      g_value_init (&tmp, G_TYPE_VARIANT);
      g_value_take_variant (&tmp, g_variant_get_variant (variant));
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("ms")))
    {
      g_autoptr(GVariant) child = g_variant_get_maybe (variant);

      g_value_init (&tmp, G_TYPE_STRING);

      if (child != NULL)
        g_value_set_string (value, g_variant_get_string (child, NULL));
      else
        g_value_set_string (value, NULL);
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("mas")))
    {
      g_autoptr(GVariant) child = g_variant_get_maybe (variant);

      g_value_init (&tmp, G_TYPE_STRV);

      if (child != NULL)
        g_value_take_boxed (value, g_variant_dup_strv (child, NULL));
      else
        g_value_set_boxed (value, NULL);
    }

  if (!G_VALUE_TYPE (&tmp))
    return FALSE;

  if (G_VALUE_HOLDS_STRING (&tmp) && G_VALUE_HOLDS_OBJECT (value))
    {
      const char *object_id = g_value_get_string (&tmp);

      if (object_id != NULL)
        g_value_set_object (value, gtk_builder_get_object (builder, object_id));
      else
        g_value_set_object (value, NULL);

      return TRUE;
    }

  if (G_VALUE_HOLDS (value, G_VALUE_TYPE (&tmp)))
    {
      g_value_copy (&tmp, value);
      g_value_unset (&tmp);
      return TRUE;
    }

  if (g_value_transform (&tmp, value))
    {
      g_value_unset (&tmp);
      return TRUE;
    }

  g_value_unset (&tmp);

  return FALSE;
}

gboolean
_draft_binding_boolean_to_string (GBinding     *binding,
                                  const GValue *from_value,
                                  GValue       *to_value,
                                  gpointer      user_data)
{
  if (g_value_get_boolean (from_value))
    g_value_set_static_string (to_value, "true");
  else
    g_value_set_static_string (to_value, "false");
  return TRUE;
}

gboolean
_draft_binding_string_to_boolean (GBinding     *binding,
                                  const GValue *from_value,
                                  GValue       *to_value,
                                  gpointer      user_data)
{
  const char *str = g_value_get_string (from_value);
  gboolean b;

  if (str == NULL || !_draft_boolean_from_string (str, &b, NULL))
    b = FALSE;

  g_value_set_boolean (to_value, b);

  return TRUE;
}

char *
_draft_int_to_string (int value)
{
  return g_strdup_printf ("%d", value);
}

gboolean
_draft_int_from_string (const char *string,
                        int        *value)
{
  gint64 val;

  if (string == NULL)
    return FALSE;

  val = g_ascii_strtoll (string, NULL, 10);

  if (val < G_MININT || val > G_MAXINT)
    return FALSE;

  *value = (int)val;

  return TRUE;
}

char *
_draft_uint_to_string (guint value)
{
  return g_strdup_printf ("%u", value);
}

gboolean
_draft_uint_from_string (const char *string,
                         guint      *value)
{
  gint64 val;

  if (string == NULL)
    return FALSE;

  val = g_ascii_strtoll (string, NULL, 10);

  if (val < 0 || val > G_MAXUINT)
    return FALSE;

  *value = (guint)val;

  return TRUE;
}

char *
_draft_int64_to_string (gint64 value)
{
  return g_strdup_printf ("%"G_GINT64_FORMAT, value);
}

gboolean
_draft_int64_from_string (const char *string,
                          gint64     *value)
{
  char *endptr = NULL;
  gint64 val;

  if (string == NULL)
    return FALSE;

  val = g_ascii_strtoll (string, &endptr, 10);

  if ((val == G_MININT64 || val == G_MAXINT64) && errno == ERANGE)
    return FALSE;

  if (endptr == string)
    return FALSE;

  *value = val;

  return TRUE;
}

char *
_draft_uint64_to_string (guint64 value)
{
  return g_strdup_printf ("%"G_GUINT64_FORMAT, value);
}

gboolean
_draft_uint64_from_string (const char *string,
                           guint64    *value)
{
  char *endptr = NULL;
  guint64 val;

  if (string == NULL)
    return FALSE;

  val = g_ascii_strtoull (string, &endptr, 10);
  if (val == 0 && endptr == string)
    return FALSE;

  if (val == G_MAXUINT64 && errno == ERANGE)
    return FALSE;

  *value = val;

  return TRUE;
}

#ifndef __NR_memfd_create
static const char *
get_tmpdir (void)
{
  const char *tmpdir = NULL;

  if (tmpdir == NULL || *tmpdir == '\0')
    tmpdir = getenv ("TMPDIR");

#ifdef P_tmpdir
  if (tmpdir == NULL || *tmpdir == '\0')
    tmpdir = P_tmpdir;
#endif

  if (tmpdir == NULL || *tmpdir == '\0')
    tmpdir = "/tmp";

  return tmpdir;
}
#endif  /* !__NR_memfd_create */

/**
 * draft_memfd_create:
 * @name: (nullable): A descriptive name for the memfd or %NULL
 *
 * Creates a new memfd using the memfd_create syscall if available.
 * Otherwise, a tmpfile is used. Currently, no attempt is made to
 * ensure the tmpfile is on a tmpfs backed mount.
 *
 * Returns: An fd if successful; otherwise -1 and errno is set.
 */
int
_draft_memfd_create (const char *name)
{
#ifdef __NR_memfd_create
#ifndef MFD_CLOEXEC
# define MFD_CLOEXEC 1
#endif
  if (name == NULL)
    name = "[draft]";
  return syscall (__NR_memfd_create, name, MFD_CLOEXEC);
#else
  int fd;
  int flags;
  const char *tmpdir;
  char *template = NULL;
  size_t template_len = 0;

  /*
   * TODO: It would be nice to ensure tmpfs
   *
   * It is not strictly required that the preferred temporary directory
   * will be mounted as tmpfs. We should look through the mounts and ensure
   * that the tmpfile we open is on tmpfs so that we get anonymous backed
   * pages after unlinking.
   */

  tmpdir = get_tmpdir ();
  template_len = strlen (tmpdir) + 1 + strlen ("draft-XXXXXX") + 1;
  template = g_malloc0 (template_len);
  if (template == NULL)
    {
      errno = ENOMEM;
      return -1;
    }

  snprintf (template, template_len, "%s/draft-XXXXXX", tmpdir);

#ifdef __APPLE__
  flags = 0;
#else
  flags = O_BINARY | O_CLOEXEC;
#endif

  fd = TEMP_FAILURE_RETRY (mkostemp (template, flags));
  if (fd < 0)
    {
      free (template);
      return -1;
    }

  unlink (template);
  free (template);

  return fd;
#endif
}
