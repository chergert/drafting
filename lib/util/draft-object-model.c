/* draft-object-model.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-object.h"
#include "util/draft-object-model-private.h"

struct _DraftObjectModel
{
  GObject    parent_instance;
  GPtrArray *items;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftObjectModel, draft_object_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static void
draft_object_model_populate (DraftObjectModel *self,
                             DraftItem        *item)
{
  g_assert (DRAFT_IS_OBJECT_MODEL (self));
  g_assert (DRAFT_IS_ITEM (item));

  if (DRAFT_IS_OBJECT (item))
    g_ptr_array_add (self->items, g_object_ref (item));

  for (DraftItem *child = draft_item_get_first_child (item);
       child != NULL;
       child = draft_item_get_next_sibling (child))
    draft_object_model_populate (self, child);
}

DraftObjectModel *
draft_object_model_new (DraftItem *root)
{
  DraftObjectModel *self;

  g_return_val_if_fail (DRAFT_IS_ITEM (root), NULL);

  self = g_object_new (DRAFT_TYPE_OBJECT_MODEL, NULL);
  draft_object_model_populate (self, root);

  return self;
}

static void
draft_object_model_finalize (GObject *object)
{
  DraftObjectModel *self = (DraftObjectModel *)object;

  g_clear_pointer (&self->items, g_ptr_array_unref);

  G_OBJECT_CLASS (draft_object_model_parent_class)->finalize (object);
}

static void
draft_object_model_class_init (DraftObjectModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_object_model_finalize;
}

static void
draft_object_model_init (DraftObjectModel *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
}

static GType
draft_object_model_get_item_type (GListModel *model)
{
  return DRAFT_TYPE_OBJECT;
}

static guint
draft_object_model_get_n_items (GListModel *model)
{
  return DRAFT_OBJECT_MODEL (model)->items->len;
}

static gpointer
draft_object_model_get_item (GListModel *model,
                             guint       position)
{
  DraftObjectModel *self = (DraftObjectModel *)model;

  if (position < self->items->len)
    return g_object_ref (g_ptr_array_index (self->items, position));

  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = draft_object_model_get_item_type;
  iface->get_n_items = draft_object_model_get_n_items;
  iface->get_item = draft_object_model_get_item;
}

int
draft_object_model_find (DraftObjectModel *self,
                         const char       *id)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT_MODEL (self), -1);

  if (id == NULL || *id == 0)
    return -1;

  for (guint i = 0; i < self->items->len; i++)
    {
      DraftObject *item = g_ptr_array_index (self->items, i);

      if (draft_object_get_is_template (item))
        {
          if (g_strcmp0 (id, draft_object_get_class_name (item)) == 0)
            return i;
        }

      if (g_strcmp0 (id, draft_item_get_id (DRAFT_ITEM (item))) == 0)
        return i;
    }

  return -1;
}
