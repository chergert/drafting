/* draft-signal-group.h
 *
 * Copyright (C) 2015 Christian Hergert <christian@hergert.me>
 * Copyright (C) 2015 Garrett Regier <garrettregier@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRAFT_SIGNAL_GROUP_H
#define DRAFT_SIGNAL_GROUP_H

#include <glib-object.h>

G_BEGIN_DECLS

#define DRAFT_TYPE_SIGNAL_GROUP (draft_signal_group_get_type())

G_DECLARE_FINAL_TYPE (DraftSignalGroup, draft_signal_group, DRAFT, SIGNAL_GROUP, GObject)

DraftSignalGroup *draft_signal_group_new             (GType             target_type);
void              draft_signal_group_set_target      (DraftSignalGroup *self,
                                                      gpointer          target);
gpointer          draft_signal_group_get_target      (DraftSignalGroup *self);
void              draft_signal_group_block           (DraftSignalGroup *self);
void              draft_signal_group_unblock         (DraftSignalGroup *self);
void              draft_signal_group_connect_object  (DraftSignalGroup *self,
                                                      const gchar      *detailed_signal,
                                                      GCallback         c_handler,
                                                      gpointer          object,
                                                      GConnectFlags     flags);
void              draft_signal_group_connect_data    (DraftSignalGroup *self,
                                                      const gchar      *detailed_signal,
                                                      GCallback         c_handler,
                                                      gpointer          data,
                                                      GClosureNotify    notify,
                                                      GConnectFlags     flags);
void              draft_signal_group_connect         (DraftSignalGroup *self,
                                                      const gchar      *detailed_signal,
                                                      GCallback         c_handler,
                                                      gpointer          data);
void              draft_signal_group_connect_after   (DraftSignalGroup *self,
                                                      const gchar      *detailed_signal,
                                                      GCallback         c_handler,
                                                      gpointer          data);
void              draft_signal_group_connect_swapped (DraftSignalGroup *self,
                                                      const gchar      *detailed_signal,
                                                      GCallback         c_handler,
                                                      gpointer          data);

G_END_DECLS

#endif /* DRAFT_SIGNAL_GROUP_H */
