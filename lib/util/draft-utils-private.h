/* draft-utils-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

int         _draft_memfd_create              (const char    *name);
gboolean    _draft_flags_from_string         (GType          type,
                                              GFlagsValue   *aliases,
                                              const char    *string,
                                              guint         *flags_value,
                                              GError       **error);
char       *_draft_flags_to_string           (GType          type,
                                              guint          value);
gboolean    _draft_boolean_from_string       (const char    *str,
                                              gboolean      *value,
                                              GError       **error);
GListModel *_draft_item_type_filter_new      (GListModel    *model,
                                              GType          type);
GVariant   *_draft_value_encode              (const GValue  *value);
gboolean    _draft_value_decode              (GValue        *value,
                                              GVariant      *variant,
                                              GtkBuilder    *builder);
gboolean    _draft_value_from_variant        (GValue        *value,
                                              GVariant      *variant,
                                              const char    *type_name);
gboolean    _draft_binding_boolean_to_string (GBinding      *binding,
                                              const GValue  *from_value,
                                              GValue        *to_value,
                                              gpointer       user_data);
gboolean    _draft_binding_string_to_boolean (GBinding      *binding,
                                              const GValue  *from_value,
                                              GValue        *to_value,
                                              gpointer       user_data);
char       *_draft_int_to_string             (int            value);
gboolean    _draft_int_from_string           (const char    *string,
                                              int           *value);
char       *_draft_uint_to_string            (guint          value);
gboolean    _draft_uint_from_string          (const char    *string,
                                              guint         *value);
char       *_draft_int64_to_string           (gint64         value);
gboolean    _draft_int64_from_string         (const char    *string,
                                              gint64        *value);
char       *_draft_uint64_to_string          (guint64        value);
gboolean    _draft_uint64_from_string        (const char    *string,
                                              guint64       *value);

G_END_DECLS
