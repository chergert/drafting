/* draft-property-notifier.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-type-info-private.h"
#include "items/draft-item-private.h"
#include "items/draft-property.h"
#include "undo/draft-transaction.h"
#include "util/draft-property-notifier-private.h"
#include "util/draft-signal-group-private.h"
#include "util/draft-utils-private.h"

struct _DraftPropertyNotifier
{
  GObject           parent_instance;
  DraftItem        *item;
  DraftSignalGroup *item_signals;
};

G_DEFINE_TYPE (DraftPropertyNotifier, draft_property_notifier, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ITEM,
  N_PROPS
};

enum {
  CHANGED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
draft_property_notifier_child_notify_cb (DraftPropertyNotifier *self,
                                         GParamSpec            *pspec,
                                         DraftProperty         *property)
{
  g_assert (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_assert (DRAFT_IS_PROPERTY (property));

  g_signal_emit (self,
                 signals [CHANGED],
                 0,
                 draft_property_get_name (DRAFT_PROPERTY (property)));
}

static void
draft_property_notifier_add_child_cb (DraftPropertyNotifier *self,
                                      DraftItem             *child,
                                      DraftItem             *parent)
{
  g_assert (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_assert (DRAFT_IS_ITEM (child));
  g_assert (DRAFT_IS_ITEM (parent));

  if (!DRAFT_IS_PROPERTY (child))
    return;

  g_signal_connect_object (child,
                           "notify::value",
                           G_CALLBACK (draft_property_notifier_child_notify_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_emit (self,
                 signals [CHANGED],
                 0,
                 draft_property_get_name (DRAFT_PROPERTY (child)));
}

static void
draft_property_notifier_remove_child_cb (DraftPropertyNotifier *self,
                                         DraftItem             *child,
                                         DraftItem             *parent)
{
  g_assert (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_assert (DRAFT_IS_ITEM (child));
  g_assert (DRAFT_IS_ITEM (parent));

  if (!DRAFT_IS_PROPERTY (child))
    return;

  g_signal_handlers_disconnect_by_func (child,
                                        G_CALLBACK (draft_property_notifier_child_notify_cb),
                                        self);

  g_signal_emit (self,
                 signals [CHANGED],
                 0,
                 draft_property_get_name (DRAFT_PROPERTY (child)));
}

static void
draft_property_notifier_dispose (GObject *object)
{
  DraftPropertyNotifier *self = (DraftPropertyNotifier *)object;

  draft_property_notifier_set_item (self, NULL);

  g_clear_object (&self->item);
  g_clear_object (&self->item_signals);

  G_OBJECT_CLASS (draft_property_notifier_parent_class)->dispose (object);
}

static void
draft_property_notifier_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  DraftPropertyNotifier *self = DRAFT_PROPERTY_NOTIFIER (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, draft_property_notifier_get_item (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_notifier_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  DraftPropertyNotifier *self = DRAFT_PROPERTY_NOTIFIER (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      draft_property_notifier_set_item (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_notifier_class_init (DraftPropertyNotifierClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_property_notifier_dispose;
  object_class->get_property = draft_property_notifier_get_property;
  object_class->set_property = draft_property_notifier_set_property;

  properties [PROP_ITEM] =
    g_param_spec_object ("item",
                         "Item",
                         "The item to notify about child property changes",
                         DRAFT_TYPE_ITEM,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [CHANGED] =
    g_signal_new ("changed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);
}

static void
draft_property_notifier_init (DraftPropertyNotifier *self)
{
  self->item_signals = draft_signal_group_new (DRAFT_TYPE_ITEM);
  draft_signal_group_connect_object (self->item_signals,
                                     "add-child",
                                     G_CALLBACK (draft_property_notifier_add_child_cb),
                                     self,
                                     G_CONNECT_SWAPPED);
  draft_signal_group_connect_object (self->item_signals,
                                     "remove-child",
                                     G_CALLBACK (draft_property_notifier_remove_child_cb),
                                     self,
                                     G_CONNECT_SWAPPED | G_CONNECT_AFTER);
}

/**
 * draft_property_notifier_get_item:
 *
 * Returns: (transfer none) (nullable): a #DrafItem or %NULL
 */
DraftItem *
draft_property_notifier_get_item (DraftPropertyNotifier *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self), NULL);

  return self->item;
}

void
draft_property_notifier_set_item (DraftPropertyNotifier *self,
                                  DraftItem             *item)
{
  g_return_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_return_if_fail (!item || DRAFT_IS_ITEM (item));

  if (self->item == item)
    return;

  if (self->item != NULL)
    {
      for (DraftItem *child = draft_item_get_first_child (self->item);
           child != NULL;
           child = draft_item_get_next_sibling (child))
        draft_property_notifier_remove_child_cb (self, child, self->item);
    }

  g_set_object (&self->item, item);
  draft_signal_group_set_target (self->item_signals, item);

  if (self->item != NULL)
    {
      for (DraftItem *child = draft_item_get_first_child (self->item);
           child != NULL;
           child = draft_item_get_next_sibling (child))
        draft_property_notifier_add_child_cb (self, child, self->item);
    }
}

DraftPropertyNotifier *
draft_property_notifier_new (void)
{
  return g_object_new (DRAFT_TYPE_PROPERTY_NOTIFIER, NULL);
}

static DraftProperty *
find_property (DraftItem  *parent,
               const char *property_name)
{
  g_return_val_if_fail (DRAFT_IS_ITEM (parent), NULL);
  g_return_val_if_fail (property_name != NULL, NULL);

  for (DraftItem *child = draft_item_get_first_child (parent);
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (!DRAFT_IS_PROPERTY (child))
        continue;

      if (g_strcmp0 (draft_property_get_name (DRAFT_PROPERTY (child)), property_name) == 0)
        return DRAFT_PROPERTY (child);
    }

  return NULL;
}

gboolean
draft_property_notifier_get_enum (DraftPropertyNotifier *self,
                                  const char            *property_name,
                                  GType                  enum_type,
                                  guint                 *value)
{
  g_autoptr(GEnumClass) enum_class = NULL;
  const char *str = NULL;
  GEnumValue *enum_value;

  g_return_val_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self), FALSE);
  g_return_val_if_fail (property_name != NULL, FALSE);
  g_return_val_if_fail (G_TYPE_IS_ENUM (enum_type), FALSE);
  g_return_val_if_fail (value != NULL, FALSE);

  if (!draft_property_notifier_get_string (self, property_name, &str) || str == NULL)
    goto failure;

  if (!(enum_class = g_type_class_ref (enum_type)))
    goto failure;

  if (!(enum_value = g_enum_get_value_by_nick (enum_class, str)) &&
      !(enum_value = g_enum_get_value_by_name (enum_class, str)))
    goto failure;

  *value = enum_value->value;
  return TRUE;

failure:
  *value = 0;
  return FALSE;
}

gboolean
draft_property_notifier_get_string (DraftPropertyNotifier  *self,
                                    const char             *property_name,
                                    const char            **string)
{
  DraftProperty *property;

  g_return_val_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self), FALSE);
  g_return_val_if_fail (property_name != NULL, FALSE);
  g_return_val_if_fail (string != NULL, FALSE);

  if (self->item == NULL)
    {
      *string = NULL;
      return FALSE;
    }
  else if ((property = find_property (self->item, property_name)))
    {
      *string = draft_property_get_value (property);
      return TRUE;
    }
  else
    {
      *string = NULL;
      return FALSE;
    }
}

gboolean
draft_property_notifier_get_boolean (DraftPropertyNotifier *self,
                                     const char            *property_name,
                                     gboolean              *value)
{
  const char *str;

  g_return_val_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self), FALSE);
  g_return_val_if_fail (property_name != NULL, FALSE);
  g_return_val_if_fail (value != NULL, FALSE);

  return draft_property_notifier_get_string (self, property_name, &str) &&
         str != NULL &&
         _draft_boolean_from_string (str, value, NULL);
}

gboolean
draft_property_notifier_get_int (DraftPropertyNotifier *self,
                                 const char            *property_name,
                                 int                   *value)
{
  const char *str;

  g_return_val_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self), FALSE);
  g_return_val_if_fail (property_name != NULL, FALSE);
  g_return_val_if_fail (value != NULL, FALSE);

  if (!draft_property_notifier_get_string (self, property_name, &str))
    return FALSE;

  if (str == NULL)
    return FALSE;

  for (const char *c = str; *c; c++)
    {
      if ((*c == '-' || *c == '+') && c == str)
        continue;

      if (g_ascii_isdigit (*c))
        continue;

      return FALSE;
    }

  *value = atoi (str);

  return TRUE;
}

void
draft_property_notifier_set_string (DraftPropertyNotifier *self,
                                    const char            *property_name,
                                    const char            *string,
                                    DraftTransaction      *transaction)
{
  DraftProperty *property;

  g_return_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (string != NULL);
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));
  g_return_if_fail (DRAFT_IS_ITEM (self->item));

  g_debug ("Setting property “%s” to “%s”\n",
           property_name, string);

  property = find_property (self->item, property_name);

  if (string == NULL)
    {
      if (property != NULL)
        draft_transaction_remove (transaction, DRAFT_ITEM (property));
    }
  else if (property == NULL)
    {
      g_autoptr(DraftProperty) insert = draft_property_new ();

      draft_property_set_name (insert, property_name);
      draft_property_set_value (insert, string);
      draft_transaction_insert_after (transaction, DRAFT_ITEM (insert), self->item, NULL);
    }
  else
    {
      draft_transaction_set_item (transaction, DRAFT_ITEM (property),
                                  "value", string,
                                  NULL);
    }
}

static const DraftPropertyInfo *
get_property_info (DraftItem  *item,
                   const char *property_name)
{
  if (item == NULL)
    return NULL;

  if (DRAFT_TYPE_INFO_IS_CLASS (item->type_info))
    {
      const DraftClassInfo *info = (const DraftClassInfo *)item->type_info;

      if (info->properties == NULL)
        return NULL;

      for (guint i = 0; i < info->properties->len; i++)
        {
          const DraftPropertyInfo *prop = g_ptr_array_index (info->properties, i);

          if (g_strcmp0 (prop->name, property_name) == 0)
            return prop;
        }
    }

  return NULL;
}

void
draft_property_notifier_set_boolean (DraftPropertyNotifier *self,
                                     const char            *property_name,
                                     gboolean               value,
                                     DraftTransaction      *transaction)
{
  const DraftPropertyInfo *info;
  DraftProperty *property;

  g_return_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  property = find_property (self->item, property_name);

  if ((info = get_property_info (self->item, property_name)))
    {
      if (G_VALUE_HOLDS_BOOLEAN (&info->default_value) &&
          g_value_get_boolean (&info->default_value) == value)
        {
          /* We're trying to set it to the default value. Just
           * remove the property if it exists.
           */
          if (property != NULL)
            draft_transaction_remove (transaction, DRAFT_ITEM (property));
          return;
        }
    }

  draft_property_notifier_set_string (self,
                                      property_name,
                                      value ? "true" : "false",
                                      transaction);
}

void
draft_property_notifier_set_enum (DraftPropertyNotifier *self,
                                  const char            *property_name,
                                  GType                  type,
                                  guint                  value,
                                  DraftTransaction      *transaction)
{
  g_autoptr(GEnumClass) enum_class = NULL;
  const DraftPropertyInfo *info;
  DraftProperty *property;
  GEnumValue *enum_value;

  g_return_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (G_TYPE_IS_ENUM (type));
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  if (!(enum_class = g_type_class_ref (type)))
    g_return_if_reached ();

  if (!(enum_value = g_enum_get_value (enum_class, value)))
    g_return_if_reached ();

  property = find_property (self->item, property_name);

  if ((info = get_property_info (self->item, property_name)))
    {
      if ((G_VALUE_HOLDS_UINT (&info->default_value) &&
           g_value_get_uint (&info->default_value) == value) ||
          (G_VALUE_HOLDS_ENUM (&info->default_value) &&
           g_value_get_enum (&info->default_value) == value))
        {
          /* We are trying to set to the default value. Either
           * delete the property or do nothing.
           */
          if (property != NULL)
            draft_transaction_remove (transaction, DRAFT_ITEM (property));
          return;
        }
    }

  draft_property_notifier_set_string (self,
                                      property_name,
                                      enum_value->value_nick,
                                      transaction);
}

void
draft_property_notifier_set_int (DraftPropertyNotifier *self,
                                 const char            *property_name,
                                 int                    value,
                                 DraftTransaction      *transaction)
{
  const DraftPropertyInfo *info;
  DraftProperty *property;
  g_autofree char *string = NULL;

  g_return_if_fail (DRAFT_IS_PROPERTY_NOTIFIER (self));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  g_debug ("Setting property %s to %d", property_name, value);

  property = find_property (self->item, property_name);

  if ((info = get_property_info (self->item, property_name)))
    {
      if (G_VALUE_HOLDS_INT (&info->default_value) &&
          g_value_get_int (&info->default_value) == value)
        {
          if (property != NULL)
            draft_transaction_remove (transaction, DRAFT_ITEM (property));
          return;
        }
    }

  string = g_strdup_printf ("%d", value);
  draft_property_notifier_set_string (self, property_name, string, transaction);
}
