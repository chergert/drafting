# Drafting Design

The goal of libdrafting is to provide all the components necessary to write
a designer as part of another GTK application. Generally speaking, that focus
is for GNOME Builder, but other tools could embed this too.

Given how development works in containers now, an effort is made to break the
part that loads application libraries and other possibly brittle components
in a subprocess. That subprocess can run within the execution environment of
a container. Not only does this allow for the designer application and target
application run in different containers at the same time, but also ensure that
we are robust in the case of crashes.

This is done by passing information across an IPC to the peer process. It
returns a serialized render node tree which is rendered in-process.

## Document Formats

Currently, GTK 4 uses an XML format. In case we want to experiment with new
document formats, this is abstracted int a `DraftDocumentFormat`. This object
is pinned to a `DraftDocument` the first time it is saved or loaded. Doing so
allows the format to save private information which could be used to provide
non-destructive editing.

## Projects

A `DraftProject` contains one or more `DraftDocument`s. This is to allow
possibly cross-referencing documents so that you may render the contents
of one template within another.

## Documents

The document is a type that is shared between document formats. The document
format can opt to implement subclasses of nodes so they can attach additional
information to aid in non-destructive editing.

## Undo and Redo

To simplify Undo and Redo, all mutations to the tree must be done through
the use of `draft_project_begin()` to create a transaction "snapshot".

That snapshot provides mutation APIs for the tree.

You can then rollback or commit the transaction using `draft_project_commit()`
or `draft_project_rollback()`.

## Document Items

A document is a tree of `DraftItem`. The `DraftDocument` itself is the root
`DraftItem`.

These items can represent many things. We use a `DraftItem` instead of
directly loading the objects of those types so that the host application (UI
process) never needs to work with objects specific to the destination
application. Otherwise, we risk mixing in things that could modify application
state from code loaded by UI files.

Common patterns of sub-objects can be detected by various components in
Drafting. For example, Layout properties have a well formed definition of
`DraftItem` which can be found and tweaked even thought the layout itself
has not been loaded in process.

## Panels

The panels are implemented in a way that allows embedding into applications.
Those applications should bind an active  project to the `:project` property
if they'd like to reuse the widgets when switching documents.

## Type Resolver

An informant process will run within the build container (or host system if
no builder container is used) which can extract information from GIR typelibs.
This is used to resolve types from the UI document which are most likely to
match what the user wanted.

Some type resolution may happen in process for things like GTK 4.

## Rendering

The rendering process will also be performed largerly out of process (likely
by the informant process). It will take a ui description, or some other form
of buildin the object hierarchy, and return a serialized description of the
render nodes.

These render nodes can then be rendered in process by the UI application. Some
effort will be required to ensure that only render nodes we know about are used
which is the one place we risk incompatibility between GTK versions on the host
and the informant process (possibly bundled with GTK 4 in the container).

Some object types will be rendered in process. For example, creating menus does
not make sense in the informant because there is nothing to display. For those,
we can create an in-host-process menu widget editor.

## Selecting Graphical Elements

We need to be able to select graphical elements in the interface so that the
user may alter the properties, add/remove/alter placement within the hierarchy,
and such. This breaks down a bit when you receive a tree of render nodes from
the informant as you don't know what nodes to select.

We can insert debug nodes into the serialized render node tree which give us
backpointers to the objects we care about. Following the backpointer will
then let us know what object/properties/etc are available. For the backpoiner,
we can use dynamically generated object-ids for objects without ids and then
resolve them from a hashtable.

