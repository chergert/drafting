/* draft-observer.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-context.h"

#include "items/draft-document.h"
#include "items/draft-item.h"
#include "undo/draft-transaction.h"

G_DEFINE_TYPE (DraftObserver, draft_observer, G_TYPE_OBJECT)

enum {
  ADD_DISPLAY,
  APPLY,
  CLEAR_REDO_HISTORY,
  CLEAR_SELECTION,
  DOCUMENT_ADDED,
  DOCUMENT_REMOVED,
  REMOVE_DISPLAY,
  REVERT,
  SELECT_ITEM,
  SET_CONTEXT,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

DraftObserver *
_draft_observer_new (void)
{
  return g_object_new (DRAFT_TYPE_OBSERVER, NULL);
}

static void
draft_observer_class_init (DraftObserverClass *klass)
{
  signals [ADD_DISPLAY] =
    g_signal_new ("add-display",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, add_display),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_ITEM);

  signals [REMOVE_DISPLAY] =
    g_signal_new ("remove-display",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, remove_display),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_ITEM);

  signals [SELECT_ITEM] =
    g_signal_new ("select-item",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, select_item),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_ITEM);

  signals [CLEAR_REDO_HISTORY] =
    g_signal_new ("clear-redo-history",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, clear_redo_history),
                  NULL, NULL, NULL, G_TYPE_NONE, 0);

  signals [CLEAR_SELECTION] =
    g_signal_new ("clear-selection",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, clear_selection),
                  NULL, NULL, NULL, G_TYPE_NONE, 0);

  signals [DOCUMENT_ADDED] =
    g_signal_new ("document-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, document_added),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_DOCUMENT);

  signals [DOCUMENT_REMOVED] =
    g_signal_new ("document-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, document_removed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_DOCUMENT);

  signals [APPLY] =
    g_signal_new ("apply",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, apply),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_TRANSACTION);

  signals [REVERT] =
    g_signal_new ("revert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, revert),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_TRANSACTION);

  signals [SET_CONTEXT] =
    g_signal_new ("set-context",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftObserverClass, set_context),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_PROJECT_CONTEXT);
}

static void
draft_observer_init (DraftObserver *self)
{
}

void
_draft_observer_add_display (DraftObserver *self,
                             DraftItem     *item)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));

  g_signal_emit (self, signals [ADD_DISPLAY], 0, item);
}

void
_draft_observer_remove_display (DraftObserver *self,
                                DraftItem     *item)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));

  g_signal_emit (self, signals [REMOVE_DISPLAY], 0, item);
}

void
_draft_observer_clear_selection (DraftObserver *self)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));

  g_signal_emit (self, signals [CLEAR_SELECTION], 0);
}

void
_draft_observer_select_item (DraftObserver *self,
                             DraftItem     *item)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));

  g_signal_emit (self, signals [SELECT_ITEM], 0, item);
}

void
_draft_observer_document_added (DraftObserver *self,
                                DraftDocument *document)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_DOCUMENT (document));

  g_signal_emit (self, signals [DOCUMENT_ADDED], 0, document);
}

void
_draft_observer_document_removed (DraftObserver *self,
                                  DraftDocument *document)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_DOCUMENT (document));

  g_signal_emit (self, signals [DOCUMENT_REMOVED], 0, document);
}

void
_draft_observer_apply (DraftObserver    *self,
                       DraftTransaction *transaction)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  g_signal_emit (self, signals [APPLY], 0, transaction);
}

void
_draft_observer_revert (DraftObserver    *self,
                        DraftTransaction *transaction)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  g_signal_emit (self, signals [REVERT], 0, transaction);
}

void
_draft_observer_set_context (DraftObserver       *self,
                             DraftProjectContext *context)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));
  g_return_if_fail (!context || DRAFT_IS_PROJECT_CONTEXT (context));

  g_signal_emit (self, signals [SET_CONTEXT], 0, context);
}

void
_draft_observer_clear_redo_history (DraftObserver *self)
{
  g_return_if_fail (DRAFT_IS_OBSERVER (self));

  g_signal_emit (self, signals [CLEAR_REDO_HISTORY], 0);
}
