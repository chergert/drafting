/* draft-property-grid.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-type-cache-private.h"
#include "items/draft-object.h"
#include "items/draft-property.h"
#include "widgets/draft-property-cell.h"
#include "widgets/draft-property-grid-private.h"
#include "util/draft-property-model-private.h"

struct _DraftPropertyGrid
{
  GtkWidget      parent_instance;

  DraftObject   *object;

  GtkWidget     *box;
  GtkColumnView *column_view;
};

G_DEFINE_TYPE (DraftPropertyGrid, draft_property_grid, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_OBJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_property_grid_new (void)
{
  return g_object_new (DRAFT_TYPE_PROPERTY_GRID, NULL);
}

static void
setup_name_cb (GtkSignalListItemFactory *factory,
               GtkListItem              *list_item)
{
  GtkWidget *label;

  label = gtk_label_new (NULL);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_list_item_set_child (list_item, label);
  gtk_widget_add_css_class (label, "cell");
}

static void
bind_name_cb (GtkSignalListItemFactory *factory,
              GtkListItem              *list_item)
{
  GObject *item;
  GtkWidget *label;

  item = gtk_list_item_get_item (list_item);
  label = gtk_list_item_get_child (list_item);

  gtk_label_set_label (GTK_LABEL (label),
                       draft_property_get_name (DRAFT_PROPERTY (item)));
}

static void
setup_value_cb (GtkSignalListItemFactory *factory,
                GtkListItem              *list_item)
{
  GtkWidget *cell;

  cell = draft_property_cell_new (NULL);
  gtk_list_item_set_child (list_item, cell);
}

static void
bind_value_cb (GtkSignalListItemFactory *factory,
               GtkListItem              *list_item)
{
  DraftProperty *property;
  GtkWidget *cell;

  property = gtk_list_item_get_item (list_item);
  cell = gtk_list_item_get_child (list_item);

  draft_property_cell_bind (DRAFT_PROPERTY_CELL (cell), property);
}

static void
unbind_value_cb (GtkSignalListItemFactory *factory,
                 GtkListItem              *list_item)
{
  GtkWidget *cell = gtk_list_item_get_child (list_item);
  draft_property_cell_bind (DRAFT_PROPERTY_CELL (cell), NULL);
}

static void
setup_defined_at_cb (GtkSignalListItemFactory *factory,
                     GtkListItem              *list_item)
{
  GtkWidget *label;

  label = gtk_label_new (NULL);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_list_item_set_child (list_item, label);
  gtk_widget_add_css_class (label, "cell");
}

static void
bind_defined_at_cb (GtkSignalListItemFactory *factory,
                    GtkListItem              *list_item)
{
  GObject *item;
  GtkWidget *label;

  item = gtk_list_item_get_item (list_item);
  label = gtk_list_item_get_child (list_item);

  gtk_label_set_label (GTK_LABEL (label),
                       draft_property_get_defined_at (DRAFT_PROPERTY (item)));
}

static void
draft_property_grid_dispose (GObject *object)
{
  DraftPropertyGrid *self = (DraftPropertyGrid *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_property_grid_parent_class)->dispose (object);
}

static void
draft_property_grid_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftPropertyGrid *self = DRAFT_PROPERTY_GRID (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_property_grid_get_object (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_grid_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DraftPropertyGrid *self = DRAFT_PROPERTY_GRID (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      draft_property_grid_set_object (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_grid_class_init (DraftPropertyGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_property_grid_dispose;
  object_class->get_property = draft_property_grid_get_property;
  object_class->set_property = draft_property_grid_set_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "The object for which to edit properties",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-property-grid.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftPropertyGrid, box);
  gtk_widget_class_bind_template_child (widget_class, DraftPropertyGrid, column_view);
  gtk_widget_class_bind_template_callback (widget_class, setup_name_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_name_cb);
  gtk_widget_class_bind_template_callback (widget_class, setup_value_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_value_cb);
  gtk_widget_class_bind_template_callback (widget_class, unbind_value_cb);
  gtk_widget_class_bind_template_callback (widget_class, setup_defined_at_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_defined_at_cb);
}

static void
draft_property_grid_init (DraftPropertyGrid *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

/**
 * draft_property_grid_get_object:
 * @self: a #DraftPropertyGrid
 *
 * Gets the object associated with the grid.
 *
 * Returns: (transfer none) (nullable): A #DraftObject or %NULL
 */
DraftObject *
draft_property_grid_get_object (DraftPropertyGrid *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY_GRID (self), NULL);

  return self->object;
}

void
draft_property_grid_set_object (DraftPropertyGrid *self,
                                DraftObject       *object)
{
  g_return_if_fail (DRAFT_IS_PROPERTY_GRID (self));
  g_return_if_fail (!object || DRAFT_IS_OBJECT (object));

  if (g_set_object (&self->object, object))
    {
      if (object == NULL)
        {
          gtk_column_view_set_model (self->column_view, NULL);
        }
      else
        {
          g_autoptr(GtkSingleSelection) single = gtk_single_selection_new (G_LIST_MODEL (draft_property_model_new (object)));
          gtk_column_view_set_model (self->column_view, GTK_SELECTION_MODEL (single));
        }

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
    }
}
