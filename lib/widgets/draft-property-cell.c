/* draft-property-cell.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-project.h"

#include "introspection/draft-type-info-private.h"
#include "items/draft-property-private.h"
#include "widgets/draft-property-cell.h"
#include "undo/draft-transaction.h"
#include "util/draft-utils-private.h"

struct _DraftPropertyCell
{
  GtkWidget parent_instance;
  DraftProperty *property;
  GtkWidget *child;
};

G_DEFINE_TYPE (DraftPropertyCell, draft_property_cell, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PROPERTY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_property_cell_new (DraftProperty *property)
{
  g_return_val_if_fail (!property || DRAFT_IS_PROPERTY (property), NULL);

  return g_object_new (DRAFT_TYPE_PROPERTY_CELL,
                       "property", property,
                       NULL);
}

static void
draft_property_cell_set_value (DraftPropertyCell *self,
                               const char        *value)
{
  DraftTransaction *txn;
  DraftProject *project;

  g_assert (DRAFT_IS_PROPERTY_CELL (self));

  project = draft_item_get_project (DRAFT_ITEM (self->property));
  txn = draft_project_begin (project);
  draft_transaction_set_item (txn, DRAFT_ITEM (self->property), "value", value, NULL);
  draft_project_commit (project);
}

static void
on_cell_toggled_cb (DraftPropertyCell *self,
                    GtkCheckButton    *button)
{
  gboolean active;
  gboolean value;

  g_assert (DRAFT_IS_PROPERTY_CELL (self));
  g_assert (GTK_IS_CHECK_BUTTON (button));

  active = gtk_check_button_get_active (button);

  if (draft_property_get_value_boolean (self->property, &value))
    {
      if (active == value)
        return;
    }

  draft_property_cell_set_value (self, active ? "true" : "false");
}

static GtkWidget *
create_widget (DraftPropertyCell *self,
               DraftProperty     *property)
{
  const DraftPropertyInfo *info;
  GtkWidget *child = NULL;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (property), NULL);

  if (!(info = _draft_property_get_info (property)))
    goto failure;

  /* TODO:
   *
   *  - If there is a binding, we have to create a way to cancel
   *    or edit the binding.
   *  - We need to handle object properties, creating bindings,
   *    strings, value ranges, etc.
   */

  if (g_strcmp0 (info->type_name, "gboolean") == 0)
    {
      child = gtk_check_button_new ();
      gtk_widget_set_halign (child, GTK_ALIGN_CENTER);
      g_object_bind_property_full (property, "value", child, "active",
                                   G_BINDING_SYNC_CREATE,
                                   _draft_binding_string_to_boolean,
                                   _draft_binding_boolean_to_string,
                                   NULL, NULL);
      g_signal_connect_object (child,
                               "toggled",
                               G_CALLBACK (on_cell_toggled_cb),
                               self,
                               G_CONNECT_SWAPPED);
    }
  else if (g_strcmp0 (info->type_name, "gint") == 0)
    {
      GtkAdjustment *adj = draft_property_get_adjustment (property);
      child = gtk_spin_button_new (adj, 1, 0);
      gtk_widget_set_halign (child, GTK_ALIGN_END);
    }

failure:
  if (child == NULL)
    child = gtk_label_new (NULL);

  return child;
}

void
draft_property_cell_bind (DraftPropertyCell *self,
                          DraftProperty     *property)
{
  g_return_if_fail (DRAFT_IS_PROPERTY_CELL (self));
  g_return_if_fail (!property || DRAFT_IS_PROPERTY (property));

  if (self->property == property)
    return;

  if (self->property)
    {
      g_clear_object (&self->property);
      g_clear_pointer (&self->child, gtk_widget_unparent);
    }

  if (property)
    {
      self->property = g_object_ref (property);
      self->child = create_widget (self, property);
      gtk_widget_set_parent (GTK_WIDGET (self->child), GTK_WIDGET (self));
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROPERTY]);
}

static void
draft_property_cell_dispose (GObject *object)
{
  DraftPropertyCell *self = (DraftPropertyCell *)object;

  draft_property_cell_bind (self, NULL);

  G_OBJECT_CLASS (draft_property_cell_parent_class)->dispose (object);
}

static void
draft_property_cell_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftPropertyCell *self = DRAFT_PROPERTY_CELL (object);

  switch (prop_id)
    {
    case PROP_PROPERTY:
      g_value_set_object (value, self->property);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_cell_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DraftPropertyCell *self = DRAFT_PROPERTY_CELL (object);

  switch (prop_id)
    {
    case PROP_PROPERTY:
      draft_property_cell_bind (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_cell_class_init (DraftPropertyCellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_property_cell_dispose;
  object_class->get_property = draft_property_cell_get_property;
  object_class->set_property = draft_property_cell_set_property;

  properties [PROP_PROPERTY] =
    g_param_spec_object ("property",
                         "Property",
                         "Property",
                         DRAFT_TYPE_PROPERTY,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
draft_property_cell_init (DraftPropertyCell *self)
{
  gtk_widget_add_css_class (GTK_WIDGET (self), "cell");
}
