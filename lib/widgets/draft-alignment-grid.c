/* draft-alignment-grid.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-project.h"
#include "items/draft-object.h"
#include "widgets/draft-alignment-grid-private.h"
#include "undo/draft-transaction.h"
#include "util/draft-property-notifier-private.h"

struct _DraftAlignmentGrid
{
  GtkWidget              parent_instance;

  DraftObject           *object;
  DraftPropertyNotifier *notifier;

  GtkAdjustment         *margin_top_adj;
  GtkAdjustment         *margin_end_adj;
  GtkAdjustment         *margin_start_adj;
  GtkAdjustment         *margin_bottom_adj;

  GtkWidget             *grid;
  GtkEntry              *margin_start;
  GtkEntry              *margin_end;
  GtkEntry              *margin_top;
  GtkEntry              *margin_bottom;
  GtkButton             *halign_start;
  GtkButton             *halign_end;
  GtkButton             *valign_top;
  GtkButton             *valign_bottom;
  GtkToggleButton       *vexpand_top;
  GtkToggleButton       *hexpand_start;
};

G_DEFINE_TYPE (DraftAlignmentGrid, draft_alignment_grid, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_OBJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void draft_alignment_grid_block   (DraftAlignmentGrid *self);
static void draft_alignment_grid_unblock (DraftAlignmentGrid *self);

static void
draft_alignment_grid_set_object_halign (DraftAlignmentGrid *self,
                                        GtkAlign            align)
{
  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));

  switch (align)
    {
    case GTK_ALIGN_BASELINE: /* Ignore, but do something */
    case GTK_ALIGN_FILL:
      gtk_button_set_label (self->halign_start, "⇤");
      gtk_button_set_label (self->halign_end, "⇥");
      break;

    case GTK_ALIGN_START:
      gtk_button_set_label (self->halign_start, "⇤");
      gtk_button_set_label (self->halign_end, "❙");
      break;

    case GTK_ALIGN_CENTER:
      gtk_button_set_label (self->halign_start, "❙");
      gtk_button_set_label (self->halign_end, "❙");
      break;

    case GTK_ALIGN_END:
      gtk_button_set_label (self->halign_start, "❙");
      gtk_button_set_label (self->halign_end, "⇥");
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
draft_alignment_grid_set_object_valign (DraftAlignmentGrid *self,
                                        GtkAlign            align)
{
  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));

  switch (align)
    {
    case GTK_ALIGN_FILL:
      gtk_button_set_label (self->valign_top, "⤒");
      gtk_button_set_label (self->valign_bottom, "⤓");
      break;

    case GTK_ALIGN_START:
      gtk_button_set_label (self->valign_top, "⤒");
      gtk_button_set_label (self->valign_bottom, "―");
      break;

    case GTK_ALIGN_CENTER:
      gtk_button_set_label (self->valign_top, "―");
      gtk_button_set_label (self->valign_bottom, "―");
      break;

    case GTK_ALIGN_END:
      gtk_button_set_label (self->valign_top, "―");
      gtk_button_set_label (self->valign_bottom, "⤓");
      break;

    case GTK_ALIGN_BASELINE:
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
draft_alignment_grid_property_changed_cb (DraftAlignmentGrid    *self,
                                          const char            *property_name,
                                          DraftPropertyNotifier *notifier)
{
  GtkAlign align;

  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));
  g_assert (property_name != NULL);
  g_assert (DRAFT_IS_PROPERTY_NOTIFIER (notifier));

  draft_alignment_grid_block (self);

  if (g_strcmp0 (property_name, "halign") == 0 ||
      g_strcmp0 (property_name, "valign") == 0)
    {
      if (!draft_property_notifier_get_enum (self->notifier, property_name, GTK_TYPE_ALIGN, &align))
        align = GTK_ALIGN_FILL;

      if (g_strcmp0 (property_name, "halign") == 0)
        draft_alignment_grid_set_object_halign (self, align);
      else
        draft_alignment_grid_set_object_valign (self, align);
    }
  else if (g_strcmp0 (property_name, "hexpand") == 0 ||
           g_strcmp0 (property_name, "vexpand") == 0)
    {
      gboolean value;

      if (!draft_property_notifier_get_boolean (self->notifier, property_name, &value))
        value = FALSE;

      if (g_strcmp0 (property_name, "hexpand") == 0)
        gtk_toggle_button_set_active (self->hexpand_start, value);
      else
        gtk_toggle_button_set_active (self->vexpand_top, value);
    }
  else if (g_strcmp0 (property_name, "margin-start") == 0 ||
           g_strcmp0 (property_name, "margin-end") == 0 ||
           g_strcmp0 (property_name, "margin-top") == 0 ||
           g_strcmp0 (property_name, "margin-bottom") == 0)
    {
      int value;

      if (!draft_property_notifier_get_int (self->notifier, property_name, &value))
        value = 0;

      if (g_strcmp0 (property_name, "margin-start") == 0)
        gtk_adjustment_set_value (self->margin_start_adj, value);
      else if (g_strcmp0 (property_name, "margin-end") == 0)
        gtk_adjustment_set_value (self->margin_end_adj, value);
      else if (g_strcmp0 (property_name, "margin-top") == 0)
        gtk_adjustment_set_value (self->margin_top_adj, value);
      else if (g_strcmp0 (property_name, "margin-bottom") == 0)
        gtk_adjustment_set_value (self->margin_bottom_adj, value);
    }

  draft_alignment_grid_unblock (self);
}

static GtkAlign
cycle_next (GtkAlign align)
{
  switch (align)
    {
    case GTK_ALIGN_FILL:
      return GTK_ALIGN_START;
    case GTK_ALIGN_START:
      return GTK_ALIGN_CENTER;
    case GTK_ALIGN_CENTER:
      return GTK_ALIGN_END;
    case GTK_ALIGN_END:
      return GTK_ALIGN_BASELINE;
    case GTK_ALIGN_BASELINE:
      return GTK_ALIGN_FILL;
    default:
      g_assert_not_reached ();
    }
}

static void
draft_alignment_grid_cycle_align_cb (DraftAlignmentGrid *self,
                                     GtkButton          *button)
{
  DraftTransaction *transaction;
  DraftProject *project;
  GtkAlign align;

  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));
  g_assert (GTK_IS_BUTTON (button));

  if (self->object == NULL ||
      !(project = draft_item_get_project (DRAFT_ITEM (self->object))))
    return;

  draft_alignment_grid_block (self);
  transaction = draft_project_begin (project);

  if (button == self->valign_top || button == self->valign_bottom)
    {
      if (!draft_property_notifier_get_enum (self->notifier, "valign", GTK_TYPE_ALIGN, &align))
        align = GTK_ALIGN_FILL;
      align = cycle_next (align);
      draft_property_notifier_set_enum (self->notifier, "valign", GTK_TYPE_ALIGN, align, transaction);
    }
  else
    {
      if (!draft_property_notifier_get_enum (self->notifier, "halign", GTK_TYPE_ALIGN, &align))
        align = GTK_ALIGN_FILL;
      align = cycle_next (align);
      if (align == GTK_ALIGN_BASELINE)
        align = cycle_next (align);
      draft_property_notifier_set_enum (self->notifier, "halign", GTK_TYPE_ALIGN, align, transaction);
    }

  draft_project_commit (project);
  draft_alignment_grid_unblock (self);
}

static void
draft_alignment_grid_adjustment_changed_cb (DraftAlignmentGrid *self,
                                            GtkAdjustment      *adjustment)
{
  DraftTransaction *transaction;
  DraftProject *project;
  const char *property_name;
  double value;

  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));
  g_assert (GTK_IS_ADJUSTMENT (adjustment));

  if (self->object == NULL ||
      !(project = draft_item_get_project (DRAFT_ITEM (self->object))))
    return;

  draft_alignment_grid_block (self);
  transaction = draft_project_begin (project);

  if (adjustment == self->margin_top_adj)
    property_name = "margin-top";
  else if (adjustment == self->margin_end_adj)
    property_name = "margin-end";
  else if (adjustment == self->margin_start_adj)
    property_name = "margin-start";
  else
    property_name = "margin-bottom";

  value = gtk_adjustment_get_value (adjustment);
  draft_property_notifier_set_int (self->notifier, property_name, (int)value, transaction);

  draft_project_commit (project);
  draft_alignment_grid_unblock (self);
}

static void
draft_alignment_grid_expand_changed_cb (DraftAlignmentGrid *self,
                                        GtkToggleButton    *button)
{
  DraftTransaction *transaction;
  DraftProject *project;
  const char *name;

  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));
  g_assert (GTK_IS_TOGGLE_BUTTON (button));

  if (!(name = gtk_widget_get_name (GTK_WIDGET (button))) ||
      self->object == NULL ||
      !(project = draft_item_get_project (DRAFT_ITEM (self->object))))
    return;

  g_assert (g_strcmp0 (name, "hexpand") == 0 ||
            g_strcmp0 (name, "vexpand") == 0);

  draft_alignment_grid_block (self);
  transaction = draft_project_begin (project);

  draft_property_notifier_set_boolean (self->notifier,
                                       name,
                                       gtk_toggle_button_get_active (button),
                                       transaction);

  draft_project_commit (project);
  draft_alignment_grid_unblock (self);
}

static void
draft_alignment_grid_dispose (GObject *object)
{
  DraftAlignmentGrid *self = (DraftAlignmentGrid *)object;

  g_clear_object (&self->notifier);
  g_clear_object (&self->object);
  g_clear_pointer (&self->grid, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_alignment_grid_parent_class)->dispose (object);
}

static void
draft_alignment_grid_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  DraftAlignmentGrid *self = DRAFT_ALIGNMENT_GRID (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_alignment_grid_get_object (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_alignment_grid_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  DraftAlignmentGrid *self = DRAFT_ALIGNMENT_GRID (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      draft_alignment_grid_set_object (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_alignment_grid_class_init (DraftAlignmentGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_alignment_grid_dispose;
  object_class->get_property = draft_alignment_grid_get_property;
  object_class->set_property = draft_alignment_grid_set_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "Object",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-alignment-grid.ui");
  gtk_widget_class_set_css_name (widget_class, "draftalignmentgrid");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, grid);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_start);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_end);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_top);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_bottom);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, valign_top);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, valign_bottom);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, halign_start);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, halign_end);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, hexpand_start);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, vexpand_top);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_top_adj);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_end_adj);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_start_adj);
  gtk_widget_class_bind_template_child (widget_class, DraftAlignmentGrid, margin_bottom_adj);
  gtk_widget_class_bind_template_callback (widget_class, draft_alignment_grid_cycle_align_cb);
  gtk_widget_class_bind_template_callback (widget_class, draft_alignment_grid_adjustment_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, draft_alignment_grid_expand_changed_cb);
}

static void
draft_alignment_grid_init (DraftAlignmentGrid *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  draft_alignment_grid_set_object_halign (self, GTK_ALIGN_FILL);
  draft_alignment_grid_set_object_valign (self, GTK_ALIGN_FILL);

  gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);

  self->notifier = draft_property_notifier_new ();

  g_signal_connect_object (self->notifier,
                           "changed",
                           G_CALLBACK (draft_alignment_grid_property_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

static void
draft_alignment_grid_block (DraftAlignmentGrid *self)
{
  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));

#define BLOCK_HANDLER(o,f) g_signal_handlers_block_by_func ((o), G_CALLBACK (f), self);
  BLOCK_HANDLER(self->margin_top_adj, draft_alignment_grid_adjustment_changed_cb);
  BLOCK_HANDLER(self->margin_start_adj, draft_alignment_grid_adjustment_changed_cb);
  BLOCK_HANDLER(self->margin_end_adj, draft_alignment_grid_adjustment_changed_cb);
  BLOCK_HANDLER(self->margin_bottom_adj, draft_alignment_grid_adjustment_changed_cb);
  BLOCK_HANDLER(self->halign_start, draft_alignment_grid_cycle_align_cb);
  BLOCK_HANDLER(self->halign_end, draft_alignment_grid_cycle_align_cb);
  BLOCK_HANDLER(self->valign_top, draft_alignment_grid_cycle_align_cb);
  BLOCK_HANDLER(self->valign_bottom, draft_alignment_grid_cycle_align_cb);
  BLOCK_HANDLER(self->hexpand_start, draft_alignment_grid_expand_changed_cb);
  BLOCK_HANDLER(self->vexpand_top, draft_alignment_grid_expand_changed_cb);
#undef BLOCK_HANDLER
}

static void
draft_alignment_grid_unblock (DraftAlignmentGrid *self)
{
  g_assert (DRAFT_IS_ALIGNMENT_GRID (self));

#define UNBLOCK_HANDLER(o,f) g_signal_handlers_unblock_by_func ((o), G_CALLBACK (f), self);
  UNBLOCK_HANDLER(self->margin_top_adj, draft_alignment_grid_adjustment_changed_cb);
  UNBLOCK_HANDLER(self->margin_start_adj, draft_alignment_grid_adjustment_changed_cb);
  UNBLOCK_HANDLER(self->margin_end_adj, draft_alignment_grid_adjustment_changed_cb);
  UNBLOCK_HANDLER(self->margin_bottom_adj, draft_alignment_grid_adjustment_changed_cb);
  UNBLOCK_HANDLER(self->halign_start, draft_alignment_grid_cycle_align_cb);
  UNBLOCK_HANDLER(self->halign_end, draft_alignment_grid_cycle_align_cb);
  UNBLOCK_HANDLER(self->valign_top, draft_alignment_grid_cycle_align_cb);
  UNBLOCK_HANDLER(self->valign_bottom, draft_alignment_grid_cycle_align_cb);
  UNBLOCK_HANDLER(self->hexpand_start, draft_alignment_grid_expand_changed_cb);
  UNBLOCK_HANDLER(self->vexpand_top, draft_alignment_grid_expand_changed_cb);
#undef UNBLOCK_HANDLER
}

GtkWidget *
draft_alignment_grid_new (void)
{
  return g_object_new (DRAFT_TYPE_ALIGNMENT_GRID, NULL);
}

/**
 * draft_alignment_grid_get_object:
 *
 * Returns: (transfer none) (nullable): a #DraftObject or %NULL
 */
DraftObject *
draft_alignment_grid_get_object (DraftAlignmentGrid *self)
{
  g_return_val_if_fail (DRAFT_IS_ALIGNMENT_GRID (self), NULL);

  return self->object;
}

void
draft_alignment_grid_set_object (DraftAlignmentGrid *self,
                                 DraftObject        *object)
{
  g_return_if_fail (DRAFT_IS_ALIGNMENT_GRID (self));

  if (g_set_object (&self->object, object))
    {
      draft_property_notifier_set_item (self->notifier, DRAFT_ITEM (object));

      if (object != NULL)
        {
          draft_alignment_grid_property_changed_cb (self, "halign", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "valign", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "hexpand", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "vexpand", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "margin-start", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "margin-end", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "margin-top", self->notifier);
          draft_alignment_grid_property_changed_cb (self, "margin-bottom", self->notifier);
        }

      gtk_widget_set_sensitive (GTK_WIDGET (self), object != NULL);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
    }
}
