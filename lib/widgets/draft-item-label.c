/* draft-item-label.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item.h"
#include "widgets/draft-item-label-private.h"

struct _DraftItemLabel
{
  GtkWidget  parent_instance;

  DraftItem *item;

  GtkLabel  *label;
  GtkWidget *box;
};

G_DEFINE_TYPE (DraftItemLabel, draft_item_label, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_ITEM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_item_label_new (void)
{
  return g_object_new (DRAFT_TYPE_ITEM_LABEL, NULL);
}

static void
draft_item_label_dispose (GObject *object)
{
  DraftItemLabel *self = (DraftItemLabel *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);
  g_clear_object (&self->item);

  G_OBJECT_CLASS (draft_item_label_parent_class)->dispose (object);
}

static void
draft_item_label_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftItemLabel *self = DRAFT_ITEM_LABEL (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, draft_item_label_get_item (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_item_label_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftItemLabel *self = DRAFT_ITEM_LABEL (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      draft_item_label_set_item (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_item_label_class_init (DraftItemLabelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_item_label_dispose;
  object_class->get_property = draft_item_label_get_property;
  object_class->set_property = draft_item_label_set_property;

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-item-label.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftItemLabel, box);
  gtk_widget_class_bind_template_child (widget_class, DraftItemLabel, label);

  properties [PROP_ITEM] =
    g_param_spec_object ("item",
                         "Item",
                         "The item to display",
                         DRAFT_TYPE_ITEM,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_item_label_init (DraftItemLabel *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

DraftItem *
draft_item_label_get_item (DraftItemLabel *self)
{
  g_return_val_if_fail (DRAFT_IS_ITEM_LABEL (self), NULL);

  return self->item;
}

void
draft_item_label_set_item (DraftItemLabel *self,
                           DraftItem      *item)
{
  g_return_if_fail (DRAFT_IS_ITEM_LABEL (self));
  g_return_if_fail (!item || DRAFT_IS_ITEM (item));

  if (g_set_object (&self->item, item))
    {
      if (item)
        g_object_bind_property (item, "display-name", self->label, "label", G_BINDING_SYNC_CREATE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ITEM]);
    }
}
