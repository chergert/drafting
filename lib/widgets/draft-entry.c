/* draft-entry.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-project.h"
#include "items/draft-item-private.h"
#include "widgets/draft-entry-private.h"
#include "undo/draft-transaction.h"

typedef enum
{
  DRAFT_ENTRY_MODE_ITEM_PROPERTY = 1,
} DraftEntryMode;

struct _DraftEntry
{
  GtkEntry        parent_instance;

  DraftEntryMode  mode;

  DraftItem      *item;
  char           *property;
  gulong          property_notify_handler;
};

G_DEFINE_TYPE (DraftEntry, draft_entry, GTK_TYPE_ENTRY)

GtkWidget *
draft_entry_new (void)
{
  return g_object_new (DRAFT_TYPE_ENTRY, NULL);
}

static void
draft_entry_apply_item_property (DraftEntry *self)
{
  GValue value = G_VALUE_INIT;
  DraftTransaction *transaction;
  DraftProject *project;
  const char *text;

  g_assert (DRAFT_IS_ENTRY (self));

  if (self->item == NULL || self->property == NULL)
    return;

  if (!(project = draft_item_get_project (self->item)) ||
      !(transaction = draft_project_begin (project)))
    return;

  text = gtk_editable_get_text (GTK_EDITABLE (self));
  if (text != NULL && text[0] == 0)
    text = NULL;

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, text);
  draft_transaction_set_item_property (transaction, self->item, self->property, &value);
  g_value_unset (&value);

  draft_project_commit (project);
}

static void
draft_entry_apply (DraftEntry *self)
{
  g_assert (DRAFT_IS_ENTRY (self));

  if (self->mode == DRAFT_ENTRY_MODE_ITEM_PROPERTY)
    draft_entry_apply_item_property (self);
}

static void
draft_entry_leave_cb (DraftEntry         *self,
                      GtkEventController *controller)
{
  g_assert (DRAFT_IS_ENTRY (self));
  g_assert (GTK_IS_EVENT_CONTROLLER_FOCUS (controller));

  draft_entry_apply (self);
}

static void
draft_entry_activate (GtkEntry *entry)
{
  DraftEntry *self = (DraftEntry *)entry;

  g_assert (DRAFT_IS_ENTRY (self));

  draft_entry_apply (self);
}

static void
draft_entry_notify_property_cb (DraftEntry *self,
                                GParamSpec *pspec,
                                DraftItem  *item)
{
  GValue value = G_VALUE_INIT;

  g_assert (DRAFT_IS_ENTRY (self));
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (self->property != NULL);

  g_value_init (&value, G_TYPE_STRING);
  g_object_get_property (G_OBJECT (item), self->property, &value);
  if (g_value_get_string (&value))
    g_object_set_property (G_OBJECT (self), "text", &value);
  else
    gtk_editable_set_text (GTK_EDITABLE (self), "");
  g_value_unset (&value);
}

static void
draft_entry_dispose (GObject *object)
{
  DraftEntry *self = (DraftEntry *)object;

  draft_entry_clear (self);

  G_OBJECT_CLASS (draft_entry_parent_class)->dispose (object);
}

static void
draft_entry_class_init (DraftEntryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkEntryClass *entry_class = GTK_ENTRY_CLASS (klass);

  object_class->dispose = draft_entry_dispose;

  entry_class->activate = draft_entry_activate;
}

static void
draft_entry_init (DraftEntry *self)
{
  GtkEventController *controller;

  gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);

  /* Disable to avoid colliding with changing objects */
  gtk_editable_set_enable_undo (GTK_EDITABLE (self), FALSE);

  controller = GTK_EVENT_CONTROLLER (gtk_event_controller_focus_new ());
  g_signal_connect_object (controller,
                           "leave",
                           G_CALLBACK (draft_entry_leave_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);
}

void
draft_entry_clear (DraftEntry *self)
{
  g_return_if_fail (DRAFT_IS_ENTRY (self));

  gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);

  g_clear_signal_handler (&self->property_notify_handler, self->item);
  g_clear_object (&self->item);
  g_clear_pointer (&self->property, g_free);
  self->mode = 0;

  gtk_editable_set_text (GTK_EDITABLE (self), "");
}

void
draft_entry_set_item_property (DraftEntry *self,
                               DraftItem  *item,
                               const char *property)
{
  g_autofree char *notify = NULL;

  g_return_if_fail (DRAFT_IS_ENTRY (self));
  g_return_if_fail (!item || DRAFT_IS_ITEM (item));
  g_return_if_fail (property != NULL || item == NULL);

  draft_entry_clear (self);

  if (item == NULL)
    return;

  self->mode = DRAFT_ENTRY_MODE_ITEM_PROPERTY;
  self->property = g_strdup (property);
  g_set_object (&self->item, item);
  gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);

  notify = g_strdup_printf ("notify::%s", property);
  self->property_notify_handler =
    g_signal_connect_object (self->item,
                             notify,
                             G_CALLBACK (draft_entry_notify_property_cb),
                             self,
                             G_CONNECT_SWAPPED);

  draft_entry_notify_property_cb (self, NULL, self->item);
}
