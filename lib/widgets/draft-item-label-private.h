/* draft-item-label-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_ITEM_LABEL (draft_item_label_get_type())

G_DECLARE_FINAL_TYPE (DraftItemLabel, draft_item_label, DRAFT, ITEM_LABEL, GtkWidget)

GtkWidget *draft_item_label_new      (void);
DraftItem *draft_item_label_get_item (DraftItemLabel *self);
void       draft_item_label_set_item (DraftItemLabel *self,
                                      DraftItem      *item);

G_END_DECLS
