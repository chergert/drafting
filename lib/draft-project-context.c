/* draft-project-context.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <glib-unix.h>
#include <unistd.h>

#include "draft-project-context-private.h"
#include "undo/draft-transaction-private.h"

typedef struct
{
  char **typelib_dirs;
  char **command_prefix;
  char **available_themes;
  char *current_theme;
  GDBusConnection *service_connection;
  DraftIpcService *service;
} DraftProjectContextPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (DraftProjectContext, draft_project_context, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_AVAILABLE_THEMES,
  PROP_COMMAND_PREFIX,
  PROP_TYPELIB_DIRS,
  PROP_SERVICE_CONNECTION,
  PROP_THEME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * draft_project_context_new:
 *
 * Create a new #DraftProjectContext.
 *
 * When integrating Drafting into an IDE, this object provides the ability to
 * run the `libdrafting-service` process in other contexts such as a Flatpak
 * SDK or even on a remove device.
 *
 * Returns: (transfer full): a newly created #DraftProjectContext
 */
DraftProjectContext *
draft_project_context_new (void)
{
  return g_object_new (DRAFT_TYPE_PROJECT_CONTEXT, NULL);
}

static void
draft_project_context_notify_available_themes_cb (DraftProjectContext *self,
                                                  GParamSpec          *pspec,
                                                  DraftIpcService     *service)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);
  const char * const *themes;

  g_assert (DRAFT_IS_PROJECT_CONTEXT (self));
  g_assert (DRAFT_IPC_IS_SERVICE (service));

  themes = draft_ipc_service_get_available_themes (service);

  if (themes == (const char * const *)priv->available_themes)
    return;

  if (themes == NULL ||
      priv->available_themes == NULL ||
      !g_strv_equal (themes, (const char * const *)priv->available_themes))
    {
      g_strfreev (priv->available_themes);
      priv->available_themes = g_strdupv ((char **)themes);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_AVAILABLE_THEMES]);
    }
}

static void
draft_project_context_notify_theme_cb (DraftProjectContext *self,
                                       GParamSpec          *pspec,
                                       DraftIpcService     *service)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);
  const char *theme;

  g_assert (DRAFT_IS_PROJECT_CONTEXT (self));
  g_assert (DRAFT_IPC_IS_SERVICE (service));

  theme = draft_ipc_service_get_theme (service);
  if (g_strcmp0 (theme, priv->current_theme) == 0)
    return;

  g_free (priv->current_theme);
  priv->current_theme = g_strdup (theme);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_THEME]);
}

static void
draft_project_context_dispose (GObject *object)
{
  DraftProjectContext *self = (DraftProjectContext *)object;
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_clear_pointer (&priv->typelib_dirs, g_free);
  g_clear_pointer (&priv->command_prefix, g_free);
  g_clear_pointer (&priv->current_theme, g_free);
  g_clear_pointer (&priv->available_themes, g_free);
  g_clear_object (&priv->service_connection);
  g_clear_object (&priv->service);

  G_OBJECT_CLASS (draft_project_context_parent_class)->dispose (object);
}

static void
draft_project_context_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  DraftProjectContext *self = DRAFT_PROJECT_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_AVAILABLE_THEMES:
      g_value_set_boxed (value, draft_project_context_get_available_themes (self));
      break;

    case PROP_TYPELIB_DIRS:
      g_value_set_boxed (value, draft_project_context_get_typelib_dirs (self));
      break;

    case PROP_COMMAND_PREFIX:
      g_value_set_boxed (value, draft_project_context_get_command_prefix (self));
      break;

    case PROP_SERVICE_CONNECTION:
      g_value_set_object (value, draft_project_context_get_service_connection (self));
      break;

    case PROP_THEME:
      g_value_set_string (value, draft_project_context_get_theme (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_context_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  DraftProjectContext *self = DRAFT_PROJECT_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_TYPELIB_DIRS:
      draft_project_context_set_typelib_dirs (self, g_value_get_boxed (value));
      break;

    case PROP_COMMAND_PREFIX:
      draft_project_context_set_command_prefix (self, g_value_get_boxed (value));
      break;

    case PROP_SERVICE_CONNECTION:
      draft_project_context_set_service_connection (self, g_value_get_object (value));
      break;

    case PROP_THEME:
      draft_project_context_set_theme (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_context_class_init (DraftProjectContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_project_context_dispose;
  object_class->get_property = draft_project_context_get_property;
  object_class->set_property = draft_project_context_set_property;

  /**
   * DraftProjectContext:available-themes:
   *
   * The "available-themes" property contains the names of the themes
   * that are found within the project context.
   *
   * This can be used to set the #DraftProjectContext:current-theme
   * property to test alternate themes in the designer.
   */
  properties [PROP_AVAILABLE_THEMES] =
    g_param_spec_boxed ("available-themes",
                        "Available Themes",
                        "Available themes within the project context",
                        G_TYPE_STRV,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * DraftProjectContext:theme:
   *
   * The "theme" property contains the theme for the project context.
   * This is used to render the content in the project context with
   * a theme other than the default GTK theme.
   */
  properties [PROP_THEME] =
    g_param_spec_string ("theme",
                         "Theme",
                         "The current theme for the project context",
                         "Default",
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * DraftProjectContext:typelib-dirs:
   *
   * The "typelib-dirs" property contains an array of paths to search for
   * GObject Introspection `*.typelib` files.
   *
   * These are resolved from the perspective of the `libdrafting-service`
   * worker which may be running on a separate container or device.
   */
  properties [PROP_TYPELIB_DIRS] =
    g_param_spec_boxed ("typelib-dirs",
                        "Typelib Dirs",
                        "An array of paths to search for GObject Introspection typelibs",
                        G_TYPE_STRV,
                        (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  /**
   * DraftProjectContext:command-prefix:
   *
   * The "command-prefix" property contains an array of arguments to prepend
   * when executing the `libdrafting-service` process. This can be used to
   * execute a service within another container such as from a Flatpak SDK
   * or Toolbox container.
   */
  properties [PROP_COMMAND_PREFIX] =
    g_param_spec_boxed ("command-prefix",
                        "Command Prefix",
                        "An array of arguments to prepend when executing the service worker",
                        G_TYPE_STRV,
                        (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  /**
   * DraftProjectContext:service-connection:
   *
   * The "service-connection" property contains the D-Bus connection to use
   * when communicating with the `libdrafting-service` process. This is useful
   * if you need to run `libdrafting-service` on another machine or situation
   * where #DraftProjectContext:command-prefix is not flexible enough to
   * execute the `libdrafting-service` process.
   */
  properties [PROP_SERVICE_CONNECTION] =
    g_param_spec_object ("service-connection",
                         "Service Connection",
                         "The D-Bus Connection to use when communicating with a service peer",
                         G_TYPE_DBUS_CONNECTION,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_project_context_init (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);
  static const char *default_themes[] = {
    "Default",
    "Default-dark",
    "Default-hc",
    "Default-hc-dark",
    NULL
  };

  priv->current_theme = g_strdup ("Default");
  priv->available_themes = g_strdupv ((char **)default_themes);
}

/**
 * draft_project_context_get_command_prefix:
 * @self: a #DraftProjectContext
 *
 * Gets a string array containing the command prefix to prepend to the argument
 * list when executing the `libdrafting-service` process.
 *
 * Returns: (transfer none) (nullable): a string array containing the command
 *   prefix, or %NULL.
 */
const char * const *
draft_project_context_get_command_prefix (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  return (const char * const *)priv->command_prefix;
}

/**
 * draft_project_context_set_command_prefix:
 * @self: a #DraftProjectContext
 * @command_prefix: (nullable): an array of command arguments to prepend,
 *   or %NULL
 *
 * Sets the #DraftProjectContext:command-prefix property.
 *
 * Use this to execute a `libdrafting-service` found in a Flatpak SDK
 * container or Toolbox.
 */
void
draft_project_context_set_command_prefix (DraftProjectContext *self,
                                          const char * const  *command_prefix)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_if_fail (DRAFT_IS_PROJECT_CONTEXT (self));

  if (!g_strv_equal (command_prefix, (const char * const *)priv->command_prefix))
    {
      g_strfreev (priv->command_prefix);
      priv->command_prefix = g_strdupv ((char **)command_prefix);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COMMAND_PREFIX]);
    }
}

/**
 * draft_project_context_get_typelib_dirs:
 * @self: a #DraftProjectContext
 *
 * Gets the #DraftProjectContext:typelib-dirs property.
 *
 * This is an array of directories to search for `*.typelib`s from the
 * perspective of the `libdrafting-service` process.
 *
 * Returns: (transfer none) (nullable): an array of typelib search
 *   directories, or %NULL.
 */
const char * const *
draft_project_context_get_typelib_dirs (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  return (const char * const *)priv->typelib_dirs;
}

/**
 * draft_project_context_set_typelib_dirs:
 * @self: a #DraftProjectContext
 * @typelib_dirs: (nullable): an array of typelib directories, or %NULL
 *
 * Sets the #DraftProjectContext:typelib-dirs property.
 *
 * Use this to set the `libdrafting-service` process to search for
 * `*typelib`s in alternate directories.
 *
 * The directories are processed from the filesystem namespace of the
 * service process.
 *
 * This can be useful to point `libdrafting-service` at your projects
 * build or installation directory. IDEs are encouraged to do this
 * automatically for the user based on project settings.
 */
void
draft_project_context_set_typelib_dirs (DraftProjectContext *self,
                                        const char * const  *typelib_dirs)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_if_fail (DRAFT_IS_PROJECT_CONTEXT (self));

  if (!g_strv_equal (typelib_dirs, (const char * const *)priv->typelib_dirs))
    {
      g_strfreev (priv->typelib_dirs);
      priv->typelib_dirs = g_strdupv ((char **)typelib_dirs);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TYPELIB_DIRS]);
    }
}

/**
 * draft_project_context_get_service_connection:
 * @self: a #DraftProjectContext
 *
 * Gets the #DraftProjectContext:service-connection property.
 *
 * This property is useful for situations where you need to run the
 * `libdrafting-service` on another container or device for which setting
 * #DraftProjectContext:command-prefix is insufficient.
 *
 * Returns: (transfer none) (nullable): a #GDBusConnection or %NULL
 */
GDBusConnection *
draft_project_context_get_service_connection (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  return priv->service_connection;
}

/**
 * draft_project_context_set_service_connection:
 * @self: a #DraftProjectContext
 * @service_connection: (nullable): a #GDBusConnection or %NULL
 *
 * Sets the #DraftProjectContext:service-connection property.
 *
 * This property can be used to set a #GDBusConnection to use to communicate
 * with a `libdrafting-service` which is running in another container or even
 * device.
 */
void
draft_project_context_set_service_connection (DraftProjectContext *self,
                                              GDBusConnection     *service_connection)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_if_fail (DRAFT_IS_PROJECT_CONTEXT (self));
  g_return_if_fail (!service_connection || G_IS_DBUS_CONNECTION (service_connection));

  if (g_set_object (&priv->service_connection, service_connection))
    {
      g_clear_object (&priv->service);

      if (service_connection != NULL)
        {
          priv->service = draft_ipc_service_proxy_new_sync (service_connection,
                                                            G_DBUS_PROXY_FLAGS_NONE,
                                                            NULL,
                                                            "/org/gnome/Drafting/Service",
                                                            NULL, NULL);
          if (priv->service)
            {
              g_signal_connect_object (priv->service,
                                       "notify::available-themes",
                                       G_CALLBACK (draft_project_context_notify_available_themes_cb),
                                       self,
                                       G_CONNECT_SWAPPED);
              g_signal_connect_object (priv->service,
                                       "notify::theme",
                                       G_CALLBACK (draft_project_context_notify_theme_cb),
                                       self,
                                       G_CONNECT_SWAPPED);
              draft_project_context_notify_available_themes_cb (self, NULL, priv->service);
              draft_project_context_notify_theme_cb (self, NULL, priv->service);
            }
        }

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE_CONNECTION]);
    }
}

static void
draft_project_context_wait_check_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  GSubprocess *subprocess = (GSubprocess *)object;
  g_autoptr(DraftProjectContext) self = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_SUBPROCESS (subprocess));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (DRAFT_IS_PROJECT_CONTEXT (self));

  if (!g_subprocess_wait_check_finish (subprocess, result, &error))
    g_warning ("Service subprocess failed: %s", error->message);
  else
    g_message ("Service subprocess exited with code %d",
               g_subprocess_get_exit_status (subprocess));

  /* TODO: Respawn? */
}

static void
draft_project_context_spawn (DraftProjectContext  *self,
                             GError              **error)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);
  static const char *service_argv[] = { "libdrafting-service", "--read-fd=3", "--write-fd=4", NULL };
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GInputStream) input_stream = NULL;
  g_autoptr(GOutputStream) output_stream = NULL;
  g_autoptr(GIOStream) io_stream = NULL;
  g_autoptr(GDBusConnection) service_connection = NULL;
  g_autofree char *typelib_dirs = NULL;
  g_autofree char *gdk_scale = NULL;
  g_autoptr(GArray) argv = NULL;
  struct {
    int read;
    int write;
  } pipe_fds[2] = {{-1, -1}, {-1, -1}};

  g_assert (DRAFT_IS_PROJECT_CONTEXT (self));

  if (pipe ((int *)&pipe_fds[0]) != 0)
    {
      int errsv = errno;
      g_set_error_literal (error,
                           G_FILE_ERROR,
                           g_file_error_from_errno (errsv),
                           g_strerror (errsv));
      goto handle_error;
    }

  if (pipe ((int *)&pipe_fds[1]) != 0)
    {
      int errsv = errno;
      g_set_error_literal (error,
                           G_FILE_ERROR,
                           g_file_error_from_errno (errsv),
                           g_strerror (errsv));
      goto handle_error;
    }

  g_assert (pipe_fds[0].read >= 0);
  g_assert (pipe_fds[0].write >= 0);
  g_assert (pipe_fds[1].read >= 0);
  g_assert (pipe_fds[1].write >= 0);

  if (!g_unix_set_fd_nonblocking (pipe_fds[0].read, TRUE, error) ||
      !g_unix_set_fd_nonblocking (pipe_fds[0].write, TRUE, error) ||
      !g_unix_set_fd_nonblocking (pipe_fds[1].read, TRUE, error) ||
      !g_unix_set_fd_nonblocking (pipe_fds[1].write, TRUE, error))
    goto handle_error;

  input_stream = g_unix_input_stream_new (pipe_fds[0].read, TRUE);
  pipe_fds[0].read = -1;

  output_stream = g_unix_output_stream_new (pipe_fds[1].write, TRUE);
  pipe_fds[1].write = -1;

  io_stream = g_simple_io_stream_new (input_stream, output_stream);
  service_connection = g_dbus_connection_new_sync (io_stream, NULL,
                                                   G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING,
                                                   NULL, NULL, NULL);
  g_dbus_connection_set_exit_on_close (service_connection, FALSE);

  if (priv->typelib_dirs != NULL)
    typelib_dirs = g_strjoinv (":", priv->typelib_dirs);

  argv = g_array_new (TRUE, FALSE, sizeof (char *));
  if (priv->command_prefix != NULL && priv->command_prefix[0] != NULL)
    g_array_append_vals (argv, priv->command_prefix, g_strv_length (priv->command_prefix));
  g_array_append_vals (argv, service_argv, g_strv_length ((char **)service_argv));

  {
    GdkDisplay *display = gdk_display_get_default ();
    GListModel *monitors = gdk_display_get_monitors (display);
    guint n_items = g_list_model_get_n_items (monitors);
    int scale = 1;

    for (guint i = 0; i < n_items; i++)
      {
        g_autoptr(GdkMonitor) monitor = g_list_model_get_item (monitors, i);
        int monitor_scale = gdk_monitor_get_scale_factor (monitor);

        if (monitor_scale > scale)
          scale = monitor_scale;
      }

    gdk_scale = g_strdup_printf ("--gdk-scale=%d", scale);
    g_array_append_val (argv, gdk_scale);
  }

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_NONE);
  g_subprocess_launcher_set_environ (launcher, NULL);

  g_subprocess_launcher_take_fd (launcher, pipe_fds[1].read, 3);
  pipe_fds[1].read = -1;

  g_subprocess_launcher_take_fd (launcher, pipe_fds[0].write, 4);
  pipe_fds[0].write = -1;

  if (typelib_dirs != NULL && typelib_dirs[0] != 0)
    g_subprocess_launcher_setenv (launcher, "GI_TYPELIB_PATH", typelib_dirs, TRUE);

  g_subprocess_launcher_setenv (launcher, "G_SLICE", "always-malloc", TRUE);

  if (!(subprocess = g_subprocess_launcher_spawnv (launcher,
                                                   (const char * const *)(gpointer)argv->data,
                                                   error)))
    goto handle_error;

  g_subprocess_wait_check_async (subprocess,
                                 NULL,
                                 draft_project_context_wait_check_cb,
                                 g_object_ref (self));

  g_dbus_connection_start_message_processing (service_connection);
  draft_project_context_set_service_connection (self, service_connection);

handle_error:
  if (pipe_fds[0].read != -1)  close (pipe_fds[0].read);
  if (pipe_fds[0].write != -1) close (pipe_fds[0].write);
  if (pipe_fds[1].read != -1)  close (pipe_fds[1].read);
  if (pipe_fds[1].write != -1) close (pipe_fds[1].write);
}

/*
 * draft_project_context_get_service:
 *
 * Gets the #DraftIpcService D-Bus proxy to communicate with the
 * `libdrafting-service`.
 *
 * If it has not yet been executed, the `libdrafting-service` will be spawned
 * before returning from this function.
 *
 * Returns: (transfer none) (nullable): an #DraftIpcService; otherwise %NULL and
 *   @error is set.
 */
DraftIpcService *
_draft_project_context_get_service (DraftProjectContext  *self,
                                    GError              **error)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  if (priv->service == NULL)
    draft_project_context_spawn (self, error);

  return priv->service;
}

/**
 * draft_project_context_get_available_themes:
 * @self: a #DraftProjectContext
 *
 * Gets the list of available themes in the project.
 *
 * Returns: (transfer none) (nullable): a string array of theme names or %NULL
 */
const char * const *
draft_project_context_get_available_themes (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  return (const char * const *)priv->available_themes;
}

const char *
draft_project_context_get_theme (DraftProjectContext *self)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_PROJECT_CONTEXT (self), NULL);

  return priv->current_theme;
}

void
draft_project_context_set_theme (DraftProjectContext *self,
                                 const char          *theme)
{
  DraftProjectContextPrivate *priv = draft_project_context_get_instance_private (self);

  g_return_if_fail (DRAFT_IS_PROJECT_CONTEXT (self));

  if (g_strcmp0 (theme, priv->current_theme) != 0)
    {
      g_free (priv->current_theme);
      priv->current_theme = g_strdup (theme);

      if (priv->service)
        draft_ipc_service_call_set_theme (priv->service, theme, NULL, NULL, NULL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_THEME]);
    }
}
