/* draft-item.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-project.h"
#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "items/draft-property.h"
#include "undo/draft-transaction.h"

typedef struct
{
  DraftDocument *document;
  DraftItem *parent;
  char *id;
  char *internal_id;
  GQueue children;
  GList link;
  guint counter;
} DraftItemPrivate;

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (DraftItem, draft_item, G_TYPE_OBJECT,
                                  G_ADD_PRIVATE (DraftItem)
                                  G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

enum {
  PROP_0,
  PROP_ID,
  PROP_DISPLAY_NAME,
  PROP_DOCUMENT,
  PROP_PARENT,
  N_PROPS
};

enum {
  ADD_CHILD,
  REMOVE_CHILD,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];
static guint item_counter;

static void
draft_item_real_add_child (DraftItem *self,
                           DraftItem *child)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftItemPrivate *child_priv = draft_item_get_instance_private (child);

  g_list_model_items_changed (G_LIST_MODEL (self),
                              g_list_position (priv->children.head,
                                               &child_priv->link),
                              0, 1);
}

static void
draft_item_real_remove_child (DraftItem *self,
                              DraftItem *child)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftItemPrivate *child_priv = draft_item_get_instance_private (child);

  g_queue_unlink (&priv->children, &child_priv->link);
  child_priv->parent = NULL;
}

void
_draft_item_unparent (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftItemPrivate *parent_priv;
  DraftItem *parent;
  guint position;

  g_return_if_fail (DRAFT_IS_ITEM (self));
  g_return_if_fail (priv->parent == NULL || DRAFT_IS_ITEM (priv->parent));

  if (priv->parent == NULL)
    return;

  parent = priv->parent;
  parent_priv = draft_item_get_instance_private (parent);

  position = g_list_position (parent_priv->children.head, &priv->link);

  g_signal_emit (parent, signals [REMOVE_CHILD], 0, self);

  g_assert (priv->parent == NULL);

  g_list_model_items_changed (G_LIST_MODEL (parent), position, 1, 0);

  g_object_unref (self);
}

static char *
draft_item_real_get_display_name (DraftItem *self)
{
  return g_strdup (G_OBJECT_TYPE_NAME (self));
}

static void
draft_item_dispose (GObject *object)
{
  DraftItem *self = (DraftItem *)object;
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_clear_pointer (&self->type_info, draft_type_info_unref);
  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->internal_id, g_free);
  priv->document = NULL;

  while (priv->children.head != NULL)
    {
      DraftItem *child = g_queue_peek_head (&priv->children);
      _draft_item_unparent (child);
    }

  g_assert (priv->children.length == 0);
  g_assert (priv->children.head == NULL);
  g_assert (priv->children.tail == NULL);

  _draft_item_unparent (self);

  G_OBJECT_CLASS (draft_item_parent_class)->dispose (object);
}

static void
draft_item_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  DraftItem *self = DRAFT_ITEM (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, draft_item_get_id (self));
      break;

    case PROP_DISPLAY_NAME:
      g_value_take_string (value, draft_item_get_display_name (self));
      break;

    case PROP_DOCUMENT:
      g_value_set_object (value, draft_item_get_document (self));
      break;

    case PROP_PARENT:
      g_value_set_object (value, draft_item_get_parent (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_item_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  DraftItem *self = DRAFT_ITEM (object);

  switch (prop_id)
    {
    case PROP_ID:
      draft_item_set_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_item_class_init (DraftItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_item_dispose;
  object_class->get_property = draft_item_get_property;
  object_class->set_property = draft_item_set_property;

  klass->add_child = draft_item_real_add_child;
  klass->remove_child = draft_item_real_remove_child;
  klass->get_display_name = draft_item_real_get_display_name;

  /**
   * DraftItem:id:
   *
   * The "id" of the item if any.
   */
  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The identifier for the object",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * DraftItem:display-name:
   *
   * The "display-name" is the user-visible name within the interface shown
   * with the item.
   */
  properties [PROP_DISPLAY_NAME] =
    g_param_spec_string ("display-name",
                         "Display Name",
                         "Display Name",
                         NULL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * DraftItem:document:
   *
   * The "document" property contains the document the item has been added
   * to, otherwise %NULL.
   */
  properties [PROP_DOCUMENT] =
    g_param_spec_object ("document",
                         "Document",
                         "The document the item belongs to",
                         DRAFT_TYPE_DOCUMENT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * DraftItem:parent:
   *
   * The "parent" property contains the #DraftItem which contains this item
   * as a child.
   */
  properties [PROP_PARENT] =
    g_param_spec_object ("parent",
                         "Parent",
                         "The parent item",
                         DRAFT_TYPE_ITEM,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [ADD_CHILD] =
    g_signal_new ("add-child",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftItemClass, add_child),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_ITEM);

  signals [REMOVE_CHILD] =
    g_signal_new ("remove-child",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DraftItemClass, remove_child),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_ITEM);
}

static void
draft_item_init (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  priv->link.data = self;
  priv->counter = ++item_counter;
  priv->internal_id = g_strdup_printf ("__drafting_%u", priv->counter);
}

const char *
draft_item_get_id (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return priv->id;
}

const char *
_draft_item_get_internal_id (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return priv->id ? priv->id : priv->internal_id;
}

static gchar *
to_underscores (const gchar *input)
{
  gunichar last = 0;
  gunichar c;
  gunichar n;
  GString *str;

  str = g_string_new (NULL);

  for (; *input; input = g_utf8_next_char (input))
    {
      c = g_utf8_get_char (input);
      n = g_utf8_get_char (g_utf8_next_char (input));

      if (last)
        {
          if ((g_unichar_islower (last) && g_unichar_isupper (c)) ||
              (g_unichar_isupper (c) && g_unichar_islower (n)))
            g_string_append_c (str, '_');
        }

      if ((c == ' ') || (c == '-'))
        c = '_';

      g_string_append_unichar (str, g_unichar_tolower (c));

      last = c;
    }

  return g_string_free (str, FALSE);
}

static char *
get_short_name (DraftTypeInfo *type_info)
{
  DraftClassInfo *class_info = (DraftClassInfo *)type_info;
  const char *name;

  if (type_info == NULL || !DRAFT_TYPE_INFO_IS_CLASS (type_info))
    return g_strdup ("object");

  name = class_info->name;
  if (name == NULL || name[0] == 0)
    return g_strdup ("object");

  if (g_str_has_prefix (name, "Gtk"))
    name += strlen ("Gtk");

  return to_underscores (name);
}

const char *
_draft_item_generate_id (DraftItem        *self,
                         DraftTransaction *transaction)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftDocument *document;
  guint count = 1;

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);
  g_return_val_if_fail (DRAFT_IS_TRANSACTION (transaction), NULL);

  if (priv->id != NULL)
    return priv->id;

  if (!(document = draft_item_get_document (self)))
    return NULL;

  for (;;)
    {
      g_autofree char *short_name = get_short_name (self->type_info);
      g_autofree char *id = g_strdup_printf ("%s%u", short_name, count++);
      DraftItem *found = _draft_document_find_by_id (document, id);

      if (found == NULL)
        {
          draft_transaction_set_item (transaction, self,
                                      "id", id,
                                      NULL);
          return priv->id;
        }
    }

  g_assert_not_reached ();
}

void
draft_item_set_id (DraftItem  *self,
                   const char *id)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_if_fail (DRAFT_IS_ITEM (self));

  if (id != NULL && id[0] == 0)
    id = NULL;

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DISPLAY_NAME]);
    }
}

/**
 * draft_item_get_document:
 * @self: a #DraftItem
 *
 * Gets the document the item belongs to, or %NULL if it has not yet
 * been added to a document.
 *
 * Returns: (transfer none) (nullable): a #DraftDocument or %NULL
 */
DraftDocument *
draft_item_get_document (DraftItem *self)
{
  DraftItem *parent;

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  if (DRAFT_IS_DOCUMENT (self))
    return DRAFT_DOCUMENT (self);

  for (parent = self;
       parent != NULL && draft_item_get_parent (parent) != NULL;
       parent = draft_item_get_parent (parent)) { /* Do Nothing */ }

  if (DRAFT_IS_DOCUMENT (parent))
    return DRAFT_DOCUMENT (parent);

  return NULL;
}

/**
 * draft_item_get_first_child:
 * @self: a #DraftItem
 *
 * Gets the first child of @self.
 *
 * Returns: (transfer none) (nullable): a #DraftItem or %NULL
 */
DraftItem *
draft_item_get_first_child (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return g_queue_peek_head (&priv->children);
}

/**
 * draft_item_get_last_child:
 * @self: a #DraftItem
 *
 * Gets the last child of @self.
 *
 * Returns: (transfer none) (nullable): a #DraftItem or %NULL
 */
DraftItem *
draft_item_get_last_child (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return g_queue_peek_tail (&priv->children);
}

/**
 * draft_item_get_previous_sibling:
 * @self: a #DraftItem
 *
 * Gets the previous sibling within the parent.
 *
 * Returns: (transfer none) (nullable): A #DraftItem or %NULL
 */
DraftItem *
draft_item_get_previous_sibling (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  if (priv->link.prev != NULL)
    return priv->link.prev->data;

  return NULL;
}

/**
 * draft_item_get_next_sibling:
 * @self: a #DraftItem
 *
 * Gets the next sibling within the parent.
 *
 * Returns: (transfer none) (nullable): A #DraftItem or %NULL
 */
DraftItem *
draft_item_get_next_sibling (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  if (priv->link.next != NULL)
    return priv->link.next->data;

  return NULL;
}

/*
 * _draft_item_insert_after:
 * @self: (transfer full): a #DraftItem
 * @parent: the #DraftItem to add @self to
 * @previous_sibling: (nullable): a #DraftItem or %NULL to append @self
 *
 * Adds @self to the children of @parent, immediately after @previous_sibling.
 *
 * If @previous_sibling is %NULL, then @self is appended.
 */
void
_draft_item_insert_after (DraftItem *self,
                          DraftItem *parent,
                          DraftItem *previous_sibling)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftItemPrivate *parent_priv = draft_item_get_instance_private (parent);
  DraftItemPrivate *previous_priv = draft_item_get_instance_private (previous_sibling);

  g_return_if_fail (DRAFT_IS_ITEM (self));
  g_return_if_fail (DRAFT_IS_ITEM (parent));
  g_return_if_fail (!previous_sibling || DRAFT_IS_ITEM (previous_sibling));
  g_return_if_fail (!previous_sibling || draft_item_get_parent (previous_sibling) == parent);
  g_return_if_fail (priv->link.data == self);
  g_return_if_fail (parent_priv->link.data == parent);
  g_return_if_fail (priv->parent == NULL);

  priv->parent = parent;

  if (previous_sibling != NULL)
    g_queue_insert_after_link (&parent_priv->children, &previous_priv->link, &priv->link);
  else
    g_queue_push_tail_link (&parent_priv->children, &priv->link);

  g_signal_emit (parent, signals [ADD_CHILD], 0, self);
}

/*
 * _draft_item_insert_before:
 * @self: (transfer full): a #DraftItem
 * @parent: the #DraftItem to add @self to
 * @next_sibling: (nullable): a #DraftItem or %NULL to append @self
 *
 * Adds @self to the children of @parent, immediately before @next_sibling.
 *
 * If @previous_sibling is %NULL, then @self is prepended.
 */
void
_draft_item_insert_before (DraftItem *self,
                           DraftItem *parent,
                           DraftItem *next_sibling)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);
  DraftItemPrivate *parent_priv = draft_item_get_instance_private (parent);
  DraftItemPrivate *next_priv = draft_item_get_instance_private (next_sibling);

  g_return_if_fail (DRAFT_IS_ITEM (self));
  g_return_if_fail (DRAFT_IS_ITEM (parent));
  g_return_if_fail (!next_sibling || DRAFT_IS_ITEM (next_sibling));
  g_return_if_fail (!next_sibling || draft_item_get_parent (next_sibling) == parent);
  g_return_if_fail (priv->link.data == self);
  g_return_if_fail (parent_priv->link.data == parent);
  g_return_if_fail (priv->parent == NULL);

  priv->parent = parent;

  if (next_sibling != NULL)
    g_queue_insert_before_link (&parent_priv->children, &next_priv->link, &priv->link);
  else
    g_queue_push_head_link (&parent_priv->children, &priv->link);

  g_signal_emit (parent, signals [ADD_CHILD], 0, self);
}

void
_draft_item_load_from_xml (DraftItem            *self,
                           GMarkupParseContext  *context,
                           DraftXmlParser       *state,
                           const char           *element_name,
                           const char          **attribute_names,
                           const char          **attribute_values,
                           GError              **error)
{
  g_return_if_fail (DRAFT_IS_ITEM (self));
  g_return_if_fail (context != NULL);
  g_return_if_fail (state != NULL);
  g_return_if_fail (attribute_names != NULL);
  g_return_if_fail (attribute_values != NULL);
  g_return_if_fail (error != NULL && *error == NULL);

  DRAFT_ITEM_GET_CLASS (self)->load_from_xml (self,
                                              context,
                                              state,
                                              element_name,
                                              attribute_names,
                                              attribute_values,
                                              error);
}

void
_draft_item_save_to_xml (DraftItem    *self,
                         GString      *str,
                         DraftProject *project,
                         gboolean      for_display)
{
  g_return_if_fail (DRAFT_IS_ITEM (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));
  g_return_if_fail (str != NULL);

  if (DRAFT_ITEM_GET_CLASS (self)->save_to_xml)
    DRAFT_ITEM_GET_CLASS (self)->save_to_xml (self, str, project, for_display);
}

/**
 * draft_item_get_parent:
 * @self: a #DraftItem
 *
 * Gets the parent item of @self.
 *
 * Returns: (transfer none) (nullable): a #DraftItem or %NULL
 */
DraftItem *
draft_item_get_parent (DraftItem *self)
{
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return priv->parent;
}

/**
 * draft_item_get_display_name:
 * @self: a #DraftItem
 *
 * Gets the display name for the item.
 *
 * Returns: (transfer full): a string describing the item
 */
char *
draft_item_get_display_name (DraftItem *self)
{
  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  return DRAFT_ITEM_GET_CLASS (self)->get_display_name (self);
}

static GType
draft_item_get_item_type (GListModel *model)
{
  return DRAFT_TYPE_ITEM;
}

static guint
draft_item_get_n_items (GListModel *model)
{
  DraftItem *self = DRAFT_ITEM (model);
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  return priv->children.length;
}

static gpointer
draft_item_get_item (GListModel *model,
                     guint       position)
{
  DraftItem *self = DRAFT_ITEM (model);
  DraftItemPrivate *priv = draft_item_get_instance_private (self);

  if (position < priv->children.length)
    return g_object_ref (g_queue_peek_nth (&priv->children, position));
  else
    return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = draft_item_get_n_items;
  iface->get_item = draft_item_get_item;
  iface->get_item_type = draft_item_get_item_type;
}

/**
 * draft_item_get_project:
 * @self: a #DraftItem
 *
 * Gets the project containing the item.
 *
 * Returns: (transfer none) (nullable): a #DraftItem
 */
DraftProject *
draft_item_get_project (DraftItem *self)
{
  DraftDocument *document;

  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  if ((document = draft_item_get_document (self)))
    return draft_document_get_project (document);

  return NULL;
}

/**
 * draft_item_is_ancestor:
 * @self: a #DraftItem
 * @ancestor: a #DraftItem
 *
 * Calculates the depth between @self and @ancestor if @ancestor
 * is an ancestor to @self. Otherwise zero is returned.
 *
 * Returns: the depth of @self inside of @ancestor; otherwise zero.
 */
int
draft_item_is_ancestor (DraftItem *self,
                        DraftItem *ancestor)
{
  int depth = 0;

  g_return_val_if_fail (DRAFT_IS_ITEM (self), FALSE);
  g_return_val_if_fail (DRAFT_IS_ITEM (ancestor), FALSE);

  for (DraftItem *item = draft_item_get_parent (self);
       item != NULL;
       item = draft_item_get_parent (item))
    {
      depth++;

      if (item == ancestor)
        return depth;
    }

  return 0;
}

gpointer
_draft_item_find_property (DraftItem  *self,
                           const char *property_name)
{
  g_return_val_if_fail (DRAFT_IS_ITEM (self), NULL);

  for (DraftItem *child = draft_item_get_first_child (self);
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (DRAFT_IS_PROPERTY (child) &&
          g_strcmp0 (property_name, draft_property_get_name (DRAFT_PROPERTY (child))) == 0)
        return child;
    }

  return NULL;
}
