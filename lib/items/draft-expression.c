/* draft-expression.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-enums.h"

#include "items/draft-expression.h"
#include "items/draft-item-private.h"

struct _DraftExpression
{
  DraftItem parent_instance;
  char *name;
  char *function;
  char *type_name;
  GString *value;
  DraftExpressionKind kind;
};

typedef struct _DraftExpressionClass
{
  DraftItemClass parent_class;
} DraftExpressionClass;

G_DEFINE_TYPE (DraftExpression, draft_expression, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_KIND,
  PROP_TYPE_NAME,
  PROP_FUNCTION,
  PROP_NAME,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftExpression *
draft_expression_new (DraftExpressionKind kind)
{
  g_return_val_if_fail (kind == DRAFT_EXPRESSION_KIND_LOOKUP ||
                        kind == DRAFT_EXPRESSION_KIND_CLOSURE ||
                        kind == DRAFT_EXPRESSION_KIND_CONSTANT,
                        NULL);

  return g_object_new (DRAFT_TYPE_EXPRESSION,
                       "kind", kind,
                       NULL);
}

static void
draft_expression_start_element (GMarkupParseContext  *context,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                gpointer              user_data,
                                GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "lookup") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_LOOKUP));
  else if (g_strcmp0 (element_name, "closure") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_LOOKUP));
  else if (g_strcmp0 (element_name, "constant") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_CONSTANT));
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_expression_end_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftExpression *self;

  if (g_strcmp0 (element_name, "lookup") == 0 ||
      g_strcmp0 (element_name, "constant") == 0 ||
      g_strcmp0 (element_name, "closure") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_EXPRESSION);
  else
    DRAFT_XML_PARSER_ERROR ();

  /* Clear value text if we have a subobject */
  self = draft_xml_parser_current (state, DRAFT_TYPE_EXPRESSION);
  if (draft_item_get_first_child (DRAFT_ITEM (self)) != NULL)
    draft_expression_set_value (self, NULL);
}

static void
draft_expression_text (GMarkupParseContext  *context,
                       const char           *text,
                       gsize                 text_len,
                       gpointer              user_data,
                       GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftExpression *self = draft_xml_parser_current (state, DRAFT_TYPE_EXPRESSION);

  if (draft_item_get_first_child (DRAFT_ITEM (self)) != NULL)
    return;

  if (self->value == NULL)
    self->value = g_string_new (NULL);

  g_string_append_len (self->value, text, text_len);
}

static void
draft_expression_load_from_xml (DraftItem            *item,
                                GMarkupParseContext  *context,
                                DraftXmlParser       *state,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                GError              **error)
{
  static const GMarkupParser expression_parser = {
    draft_expression_start_element,
    draft_expression_end_element,
    draft_expression_text,
  };
  DraftExpression *self = DRAFT_EXPRESSION (item);
  const char *type_name = NULL;
  const char *name = NULL;
  const char *function = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "type", &type_name,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "function", &function,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_expression_set_name (self, name);
  draft_expression_set_type_name (self, type_name);
  draft_expression_set_function (self, function);

  g_markup_parse_context_push (context, &expression_parser, state);
}

static const char *
get_element_name (DraftExpression *self)
{
  switch (self->kind)
    {
    case DRAFT_EXPRESSION_KIND_CLOSURE:
      return "closure";

    case DRAFT_EXPRESSION_KIND_CONSTANT:
      return "constant";

    case DRAFT_EXPRESSION_KIND_LOOKUP:
      return "lookup";

    default:
      g_assert_not_reached ();
      return NULL;
    }
}

static void
draft_expression_save_to_xml (DraftItem    *item,
                              GString      *string,
                              DraftProject *project,
                              gboolean      for_display)
{
  DraftExpression *self = DRAFT_EXPRESSION (item);
  const char *element_name = get_element_name (self);
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_begin_open_element (string, element_name);
  draft_xml_writer_add_attributes_va (string,
                                      "name", self->name,
                                      "type", self->type_name,
                                      "function", self->function,
                                      NULL);
  draft_xml_writer_end_open_element (string, child != NULL);

  if (child == NULL)
    return;

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, element_name);
}

static void
draft_expression_finalize (GObject *object)
{
  DraftExpression *self = (DraftExpression *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->type_name, g_free);
  g_clear_pointer (&self->function, g_free);

  if (self->value != NULL)
    g_string_free (g_steal_pointer (&self->value), TRUE);

  G_OBJECT_CLASS (draft_expression_parent_class)->finalize (object);
}

static void
draft_expression_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftExpression *self = DRAFT_EXPRESSION (object);

  switch (prop_id)
    {
    case PROP_KIND:
      g_value_set_enum (value, draft_expression_get_kind (self));
      break;

    case PROP_VALUE:
      g_value_set_string (value, draft_expression_get_value (self));
      break;

    case PROP_FUNCTION:
      g_value_set_string (value, draft_expression_get_function (self));
      break;

    case PROP_TYPE_NAME:
      g_value_set_string (value, draft_expression_get_type_name (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, draft_expression_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_expression_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftExpression *self = DRAFT_EXPRESSION (object);

  switch (prop_id)
    {
    case PROP_KIND:
      self->kind = g_value_get_enum (value);
      break;

    case PROP_VALUE:
      draft_expression_set_value (self, g_value_get_string (value));
      break;

    case PROP_FUNCTION:
      draft_expression_set_function (self, g_value_get_string (value));
      break;

    case PROP_TYPE_NAME:
      draft_expression_set_type_name (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      draft_expression_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_expression_class_init (DraftExpressionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_expression_finalize;
  object_class->get_property = draft_expression_get_property;
  object_class->set_property = draft_expression_set_property;

  item_class->load_from_xml = draft_expression_load_from_xml;
  item_class->save_to_xml = draft_expression_save_to_xml;

  properties [PROP_KIND] =
    g_param_spec_enum ("kind",
                       "Kind",
                       "Kind",
                       DRAFT_TYPE_EXPRESSION_KIND,
                       DRAFT_EXPRESSION_KIND_CONSTANT,
                       (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "Name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_TYPE_NAME] =
    g_param_spec_string ("type-name",
                         "Type Name",
                         "Type Name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_FUNCTION] =
    g_param_spec_string ("function",
                         "Function",
                         "Function",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "Value",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_expression_init (DraftExpression *self)
{
  self->kind = DRAFT_EXPRESSION_KIND_CONSTANT;
}

DraftExpressionKind
draft_expression_get_kind (DraftExpression *self)
{
  g_return_val_if_fail (DRAFT_IS_EXPRESSION (self), 0);

  return self->kind;
}

const char *
draft_expression_get_name (DraftExpression *self)
{
  g_return_val_if_fail (DRAFT_IS_EXPRESSION (self), NULL);

  return self->name;
}

void
draft_expression_set_name (DraftExpression *self,
                           const char      *name)
{
  g_return_if_fail (DRAFT_IS_EXPRESSION (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const char *
draft_expression_get_type_name (DraftExpression *self)
{
  g_return_val_if_fail (DRAFT_IS_EXPRESSION (self), NULL);

  return self->type_name;
}

void
draft_expression_set_type_name (DraftExpression *self,
                                const char      *type_name)
{
  g_return_if_fail (DRAFT_IS_EXPRESSION (self));

  if (g_strcmp0 (type_name, self->type_name) != 0)
    {
      g_free (self->type_name);
      self->type_name = g_strdup (type_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TYPE_NAME]);
    }
}

const char *
draft_expression_get_function (DraftExpression *self)
{
  g_return_val_if_fail (DRAFT_IS_EXPRESSION (self), NULL);

  return self->function;
}

void
draft_expression_set_function (DraftExpression *self,
                               const char      *function)
{
  g_return_if_fail (DRAFT_IS_EXPRESSION (self));

  if (g_strcmp0 (function, self->function) != 0)
    {
      g_free (self->function);
      self->function = g_strdup (function);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FUNCTION]);
    }
}

const char *
draft_expression_get_value (DraftExpression *self)
{
  g_return_val_if_fail (DRAFT_IS_EXPRESSION (self), NULL);

  return self->value ? self->value->str : NULL;
}

void
draft_expression_set_value (DraftExpression *self,
                            const char      *value)
{
  g_return_if_fail (DRAFT_IS_EXPRESSION (self));

  if (self->value == NULL)
    {
      if (value == NULL)
        return;

      self->value = g_string_new (NULL);
    }

  if (g_strcmp0 (value, self->value->str) != 0)
    {
      g_string_truncate (self->value, 0);
      if (value == NULL)
        g_string_free (g_steal_pointer (&self->value), TRUE);
      else
        g_string_append (self->value, value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}
