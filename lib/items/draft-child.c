/* draft-child.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-child.h"
#include "items/draft-object.h"
#include "items/draft-placeholder.h"

struct _DraftChild
{
  DraftItem parent_instance;
  char *type_name;
};

typedef struct _DraftChildClass
{
  DraftItemClass parent_class;
} DraftChildClass;

G_DEFINE_TYPE (DraftChild, draft_child, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_TYPE_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
draft_child_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  DraftChild *self = DRAFT_CHILD (object);

  switch (prop_id)
    {
    case PROP_TYPE_NAME:
      g_value_set_string (value, draft_child_get_type_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_child_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  DraftChild *self = DRAFT_CHILD (object);

  switch (prop_id)
    {
    case PROP_TYPE_NAME:
      draft_child_set_type_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

DraftChild *
draft_child_new (void)
{
  return g_object_new (DRAFT_TYPE_CHILD, NULL);
}

static void
draft_child_start_element (GMarkupParseContext  *context,
                           const char           *element_name,
                           const char          **attribute_names,
                           const char          **attribute_values,
                           gpointer              user_data,
                           GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_PUSH (draft_object_new (FALSE));
  else if (g_strcmp0 (element_name, "placeholder") == 0)
    DRAFT_XML_PARSER_PUSH (draft_placeholder_new ());
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_child_end_element (GMarkupParseContext  *context,
                         const char           *element_name,
                         gpointer              user_data,
                         GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_OBJECT);
  else if (g_strcmp0 (element_name, "placeholder") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_PLACEHOLDER);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_child_load_from_xml (DraftItem            *item,
                           GMarkupParseContext  *context,
                           DraftXmlParser       *state,
                           const char           *element_name,
                           const char          **attribute_names,
                           const char          **attribute_values,
                           GError              **error)
{
  static const GMarkupParser child_parser = {
    draft_child_start_element,
    draft_child_end_element,
  };
  DraftChild *self = (DraftChild *)item;
  const char *type = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "type", &type,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_child_set_type_name (self, type);

  g_markup_parse_context_push (context, &child_parser, state);
}

static void
draft_child_save_to_xml (DraftItem    *item,
                         GString      *string,
                         DraftProject *project,
                         gboolean      for_display)
{
  DraftChild *self = DRAFT_CHILD (item);
  DraftItem *child = draft_item_get_first_child (item);

  if (child == NULL)
    return;

  draft_xml_writer_begin_open_element (string, "child");
  draft_xml_writer_add_attribute (string, "type", self->type_name);
  draft_xml_writer_end_open_element (string, TRUE);

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, "child");
}

static void
draft_child_finalize (GObject *object)
{
  DraftChild *self = (DraftChild *)object;

  g_clear_pointer (&self->type_name, g_free);

  G_OBJECT_CLASS (draft_child_parent_class)->finalize (object);
}

static void
draft_child_class_init (DraftChildClass *klass)
{
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_child_finalize;
  object_class->get_property = draft_child_get_property;
  object_class->set_property = draft_child_set_property;

  item_class->load_from_xml = draft_child_load_from_xml;
  item_class->save_to_xml = draft_child_save_to_xml;

  properties [PROP_TYPE_NAME] =
    g_param_spec_string ("type-name",
                         "Type Name",
                         "Type Name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_child_init (DraftChild *self)
{
}

const char *
draft_child_get_type_name (DraftChild *self)
{
  g_return_val_if_fail (DRAFT_IS_CHILD (self), NULL);

  return self->type_name;
}

void
draft_child_set_type_name (DraftChild *self,
                           const char *type_name)
{
  g_return_if_fail (DRAFT_IS_CHILD (self));

  if (g_strcmp0 (self->type_name, type_name) != 0)
    {
      g_free (self->type_name);
      self->type_name = g_strdup (type_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TYPE_NAME]);
    }
}
