/* draft-menu.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-menu.h"
#include "items/draft-menu-item.h"
#include "items/draft-menu-link.h"

struct _DraftMenu
{
  DraftItem parent_instance;
  char *translation_domain;
};

typedef struct _DraftMenuClass
{
  DraftItemClass parent_class;
} DraftMenuClass;

G_DEFINE_TYPE (DraftMenu, draft_menu, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_TRANSLATION_DOMAIN,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftMenu *
draft_menu_new (void)
{
  return g_object_new (DRAFT_TYPE_MENU, NULL);
}

static void
draft_menu_start_element (GMarkupParseContext  *context,
                          const char           *element_name,
                          const char          **attribute_names,
                          const char          **attribute_values,
                          gpointer              user_data,
                          GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "link") == 0)
    {
      const char *name = NULL;

      if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                        G_MARKUP_COLLECT_STRING, "name", &name,
                                        G_MARKUP_COLLECT_INVALID))
        return;

      DRAFT_XML_PARSER_PUSH (draft_menu_link_new (name));
    }
  else if (g_strcmp0 (element_name, "item") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_item_new ());
  else if (g_strcmp0 (element_name, "submenu") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_link_new ("submenu"));
  else if (g_strcmp0 (element_name, "section") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_link_new ("section"));
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_end_element (GMarkupParseContext  *context,
                        const char           *element_name,
                        gpointer              user_data,
                        GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "link") == 0 ||
      g_strcmp0 (element_name, "section") == 0 ||
      g_strcmp0 (element_name, "submenu") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU_LINK);
  else if (g_strcmp0 (element_name, "item") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU_ITEM);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_load_from_xml (DraftItem            *item,
                          GMarkupParseContext  *context,
                          DraftXmlParser       *state,
                          const char           *element_name,
                          const char          **attribute_names,
                          const char          **attribute_values,
                          GError              **error)
{
  static const GMarkupParser menu_parser = {
    draft_menu_start_element,
    draft_menu_end_element,
  };
  DraftMenu *self = DRAFT_MENU (item);
  const char *domain = NULL;
  const char *id = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "id", &id,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "domain", &domain,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_item_set_id (item, id);
  draft_menu_set_translation_domain (self, domain);

  g_markup_parse_context_push (context, &menu_parser, state);
}

static void
draft_menu_save_to_xml (DraftItem    *item,
                        GString      *string,
                        DraftProject *project,
                        gboolean      for_display)
{
  DraftMenu *self = DRAFT_MENU (item);
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_begin_open_element (string, "menu");
  draft_xml_writer_add_attributes_va (string,
                                      "id", draft_item_get_id (item),
                                      "domain", self->translation_domain,
                                      NULL);
  draft_xml_writer_end_open_element (string, TRUE);

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, "menu");
}

static void
draft_menu_finalize (GObject *object)
{
  DraftMenu *self = (DraftMenu *)object;

  g_clear_pointer (&self->translation_domain, g_free);

  G_OBJECT_CLASS (draft_menu_parent_class)->finalize (object);
}

static void
draft_menu_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  DraftMenu *self = DRAFT_MENU (object);

  switch (prop_id)
    {
    case PROP_TRANSLATION_DOMAIN:
      g_value_set_string (value, draft_menu_get_translation_domain (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  DraftMenu *self = DRAFT_MENU (object);

  switch (prop_id)
    {
    case PROP_TRANSLATION_DOMAIN:
      draft_menu_set_translation_domain (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_class_init (DraftMenuClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_menu_finalize;
  object_class->get_property = draft_menu_get_property;
  object_class->set_property = draft_menu_set_property;

  item_class->load_from_xml = draft_menu_load_from_xml;
  item_class->save_to_xml = draft_menu_save_to_xml;

  properties [PROP_TRANSLATION_DOMAIN] =
    g_param_spec_string ("translation-domain",
                         "Translation Domain",
                         "The translation domain for the menu",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_menu_init (DraftMenu *self)
{
}

const char *
draft_menu_get_translation_domain (DraftMenu *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU (self), NULL);

  return self->translation_domain;
}

void
draft_menu_set_translation_domain (DraftMenu  *self,
                                   const char *translation_domain)
{
  g_return_if_fail (DRAFT_IS_MENU (self));

  if (g_strcmp0 (translation_domain, self->translation_domain) != 0)
    {
      g_free (self->translation_domain);
      self->translation_domain = g_strdup (translation_domain);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TRANSLATION_DOMAIN]);
    }
}
