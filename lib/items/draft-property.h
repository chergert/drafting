/* draft-property.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_PROPERTY    (draft_property_get_type ())
#define DRAFT_PROPERTY(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_PROPERTY, DraftProperty))
#define DRAFT_IS_PROPERTY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_PROPERTY))

DRAFT_AVAILABLE_IN_ALL
GType          draft_property_get_type          (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftProperty *draft_property_new               (void);
DRAFT_AVAILABLE_IN_ALL
GBindingFlags  draft_property_get_bind_flags    (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_property_get_bind_property (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_property_get_bind_source   (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_property_get_name          (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_property_get_value         (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_property_get_gvalue        (DraftProperty *self,
                                                 GValue        *gvalue);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_property_get_value_boolean (DraftProperty *self,
                                                 gboolean      *value);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_property_get_translatable  (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_bind_flags    (DraftProperty *self,
                                                 GBindingFlags  bind_flags);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_bind_property (DraftProperty *self,
                                                 const char    *bind_property);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_bind_source   (DraftProperty *self,
                                                 const char    *bind_source);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_name          (DraftProperty *self,
                                                 const char    *name);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_value         (DraftProperty *self,
                                                 const char    *value);
DRAFT_AVAILABLE_IN_ALL
void           draft_property_set_translatable  (DraftProperty *self,
                                                 gboolean       translatable);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_property_get_defined_at    (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
GtkAdjustment *draft_property_get_adjustment    (DraftProperty *self);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_property_is_default_value  (DraftProperty *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftProperty, g_object_unref)

G_END_DECLS
