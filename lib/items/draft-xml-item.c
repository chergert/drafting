/* draft-xml-item.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-xml-item.h"

struct _DraftXmlItem
{
  DraftItem parent_instance;
  char *element_name;
  char **attribute_names;
  char **attribute_values;
  GString *value;
};

typedef struct _DraftXmlItemClass
{
  DraftItemClass parent_class;
} DraftXmlItemClass;

G_DEFINE_TYPE (DraftXmlItem, draft_xml_item, DRAFT_TYPE_ITEM)

DraftXmlItem *
draft_xml_item_new (void)
{
  return g_object_new (DRAFT_TYPE_XML_ITEM, NULL);
}

static void
draft_xml_item_start_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_PUSH (draft_xml_item_new ());
}

static void
draft_xml_item_end_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_POP (DRAFT_TYPE_XML_ITEM);
}

static void
draft_xml_item_text (GMarkupParseContext  *context,
                     const char           *text,
                     gsize                 text_len,
                     gpointer              user_data,
                     GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftXmlItem *self = draft_xml_parser_current (state, DRAFT_TYPE_XML_ITEM);
  g_autofree char *stripped = g_strstrip (g_strndup (text, text_len));

  if (stripped == NULL || stripped[0] == 0)
    return;

  if (self->value == NULL)
    self->value = g_string_new (stripped);
  else
    g_string_append_len (self->value, stripped, -1);
}

static void
draft_xml_item_load_from_xml (DraftItem            *item,
                              GMarkupParseContext  *context,
                              DraftXmlParser       *state,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              GError              **error)
{
  static const GMarkupParser xml_item_parser = {
    draft_xml_item_start_element,
    draft_xml_item_end_element,
    draft_xml_item_text,
  };
  DraftXmlItem *self = draft_xml_parser_current (state, DRAFT_TYPE_XML_ITEM);

  g_strfreev (self->attribute_names);
  g_strfreev (self->attribute_values);
  g_free (self->element_name);

  self->element_name = g_strdup (element_name);
  self->attribute_names = g_strdupv ((char **)attribute_names);
  self->attribute_values = g_strdupv ((char **)attribute_values);

  g_markup_parse_context_push (context, &xml_item_parser, state);
}

static void
draft_xml_item_save_to_xml (DraftItem    *item,
                            GString      *str,
                            DraftProject *project,
                            gboolean      for_display)
{
  DraftXmlItem *self = (DraftXmlItem *)item;
  DraftItem *child;

  g_assert (DRAFT_IS_XML_ITEM (self));
  g_assert (str != NULL);

  child = draft_item_get_first_child (item);

  draft_xml_writer_begin_open_element (str, self->element_name);
  draft_xml_writer_add_attributes (str,
                                   (const char * const *)self->attribute_names,
                                   (const char * const *)self->attribute_values);

  if (child == NULL && (self->value == NULL  || self->value->len == 0))
    {
      draft_xml_writer_end_open_element (str, FALSE);
      return;
    }

  draft_xml_writer_end_open_element (str, TRUE);

  if (self->value != NULL)
    draft_xml_writer_write_gstring_escaped (str, self->value);

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, str, project, for_display);

  draft_xml_writer_close_element (str, self->element_name);
}

static char *
draft_xml_item_get_display_name (DraftItem *item)
{
  if (draft_item_get_first_child (item))
    return g_strdup_printf ("<%s>", DRAFT_XML_ITEM (item)->element_name);
  else
    return g_strdup_printf ("<%s/>", DRAFT_XML_ITEM (item)->element_name);
}

static void
draft_xml_item_finalize (GObject *object)
{
  DraftXmlItem *self = (DraftXmlItem *)object;

  g_clear_pointer (&self->element_name, g_free);
  g_clear_pointer (&self->attribute_names, g_strfreev);
  g_clear_pointer (&self->attribute_values, g_strfreev);

  if (self->value != NULL)
    g_string_free (g_steal_pointer (&self->value), TRUE);

  G_OBJECT_CLASS (draft_xml_item_parent_class)->finalize (object);
}

static void
draft_xml_item_class_init (DraftXmlItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_xml_item_finalize;

  item_class->load_from_xml = draft_xml_item_load_from_xml;
  item_class->save_to_xml = draft_xml_item_save_to_xml;
  item_class->get_display_name = draft_xml_item_get_display_name;
}

static void
draft_xml_item_init (DraftXmlItem *self)
{
}

const char *
draft_xml_item_get_element_name (DraftXmlItem *self)
{
  g_return_val_if_fail (DRAFT_IS_XML_ITEM (self), NULL);

  return self->element_name;
}

const char * const *
draft_xml_item_get_attribute_names (DraftXmlItem *self)
{
  g_return_val_if_fail (DRAFT_IS_XML_ITEM (self), NULL);

  return (const char * const *)self->attribute_names;
}

const char *
draft_xml_item_get_attribute_value (DraftXmlItem *self,
                                    const char   *attribute_name)
{
  g_return_val_if_fail (DRAFT_IS_XML_ITEM (self), NULL);

  if (self->attribute_names == NULL)
    return NULL;

  for (guint i = 0; self->attribute_names[i]; i++)
    {
      if (g_strcmp0 (attribute_name, self->attribute_names[i]) == 0)
        return self->attribute_values[i];
    }

  return NULL;
}
