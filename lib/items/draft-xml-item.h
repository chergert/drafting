/* draft-xml-item.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_XML_ITEM    (draft_xml_item_get_type ())
#define DRAFT_XML_ITEM(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_XML_ITEM, DraftXmlItem))
#define DRAFT_IS_XML_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_XML_ITEM))

DRAFT_AVAILABLE_IN_ALL
GType               draft_xml_item_get_type            (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftXmlItem       *draft_xml_item_new                 (void);
DRAFT_AVAILABLE_IN_ALL
const char         *draft_xml_item_get_element_name    (DraftXmlItem *self);
DRAFT_AVAILABLE_IN_ALL
const char * const *draft_xml_item_get_attribute_names (DraftXmlItem *self);
DRAFT_AVAILABLE_IN_ALL
const char         *draft_xml_item_get_attribute_value (DraftXmlItem *self,
                                                        const char   *attribute_name);
DRAFT_AVAILABLE_IN_ALL
void                draft_xml_item_set_attribute_value (DraftXmlItem *self,
                                                        const char   *attribute_name,
                                                        const char   *attribute_value);
DRAFT_AVAILABLE_IN_ALL
const char         *draft_xml_item_get_value           (DraftXmlItem *self);
DRAFT_AVAILABLE_IN_ALL
void                draft_xml_item_set_value           (DraftXmlItem *self,
                                                        const char   *value);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftXmlItem, g_object_unref)

G_END_DECLS
