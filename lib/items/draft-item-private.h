/* draft-item-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types-private.h"
#include "items/draft-item.h"
#include "introspection/draft-type-info-private.h"

G_BEGIN_DECLS

#define DRAFT_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DRAFT_TYPE_ITEM, DraftItemClass))
#define DRAFT_IS_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DRAFT_TYPE_ITEM))
#define DRAFT_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DRAFT_TYPE_ITEM, DraftItemClass))

typedef struct
{
  DraftDocumentFormat *format;
  DraftDocument *document;
  DraftItem *current;
  DraftItem *replacement;
} DraftXmlParser;

typedef struct _DraftItem
{
  GObject parent_class;
  DraftTypeInfo *type_info;
} DraftItem;

typedef struct _DraftItemClass
{
  GObjectClass parent_class;

  char *(*get_display_name) (DraftItem            *self);
  void  (*load_from_xml)    (DraftItem            *self,
                             GMarkupParseContext  *context,
                             DraftXmlParser       *state,
                             const char           *element_name,
                             const char          **attribute_names,
                             const char          **attribute_values,
                             GError              **error);
  void  (*save_to_xml)      (DraftItem            *self,
                             GString              *str,
                             DraftProject         *project,
                             gboolean              for_display);
  void  (*add_child)        (DraftItem            *self,
                             DraftItem            *child);
  void  (*remove_child)     (DraftItem            *self,
                             DraftItem            *child);
} DraftItemClass;

void        _draft_item_insert_after    (DraftItem            *self,
                                         DraftItem            *parent,
                                         DraftItem            *previous_sibling);
void        _draft_item_insert_before   (DraftItem            *self,
                                         DraftItem            *parent,
                                         DraftItem            *next_sibling);
void        _draft_item_unparent        (DraftItem            *self);
void        _draft_item_load_from_xml   (DraftItem            *self,
                                         GMarkupParseContext  *context,
                                         DraftXmlParser       *state,
                                         const char           *element_name,
                                         const char          **attribute_names,
                                         const char          **attribute_values,
                                         GError              **error);
void        _draft_item_save_to_xml     (DraftItem            *self,
                                         GString              *str,
                                         DraftProject         *project,
                                         gboolean              for_display);
const char *_draft_item_get_internal_id (DraftItem            *self);
const char *_draft_item_generate_id     (DraftItem            *self,
                                         DraftTransaction     *transaction);
gpointer    _draft_item_find_property   (DraftItem            *self,
                                         const char           *property_name);

static inline void
_draft_item_annotate (DraftItem     *item,
                      DraftTypeInfo *type_info)
{
  g_clear_pointer (&item->type_info, draft_type_info_unref);
  if (type_info)
    item->type_info = draft_type_info_ref (type_info);
}

static inline gboolean
_draft_item_is_of_type (DraftItem  *item,
                        const char *type_name)
{
  if (item->type_info == NULL)
    return FALSE;

  return draft_type_info_is_of_type (item->type_info, type_name);
}

static inline gpointer
draft_xml_parser_current (DraftXmlParser *state,
                          GType           expected_type)
{
  g_assert (state != NULL);
  g_assert (state->current != NULL);
  g_assert (G_TYPE_CHECK_INSTANCE_TYPE (state->current, expected_type));

  return state->current;
}

static inline DraftItem *
draft_xml_parser_push (DraftXmlParser       *state,
                       gpointer              item,
                       GMarkupParseContext  *context,
                       const char           *element_name,
                       const char          **attribute_names,
                       const char          **attribute_values,
                       GError              **error)

{
  g_autoptr(DraftItem) stolen = NULL;

  g_assert (state != NULL);
  g_assert (state->document != NULL);
  g_assert (DRAFT_IS_ITEM (state->document));
  g_assert (item != NULL);
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (state->current == NULL || DRAFT_IS_ITEM (state->current));

  /* If we can find an "id" attribute, set that for the item since
   * that is generally what we want in all subclasses.
   */
  for (guint i = 0; attribute_names[i]; i++)
    {
      if (g_strcmp0 (attribute_names[i], "id") == 0)
        {
          draft_item_set_id (item, attribute_values[i]);
          break;
        }
    }

  if (state->current != NULL)
    _draft_item_insert_after (item,
                              state->current,
                              draft_item_get_last_child (state->current));
  else
    stolen = item;

  state->current = item;

  _draft_item_load_from_xml (item,
                             context,
                             state,
                             element_name,
                             attribute_names,
                             attribute_values,
                             error);

  return item;
}

static inline void
draft_xml_parser_pop (DraftXmlParser      *state,
                      GMarkupParseContext *context,
                      GType                expected_type)
{
  g_assert (state != NULL);
  g_assert (state->current == NULL || DRAFT_IS_ITEM (state->current));
  g_assert (state->current == NULL || G_TYPE_CHECK_INSTANCE_TYPE (state->current, expected_type));

  if (state->current != NULL)
    state->current = draft_item_get_parent (state->current);
  g_markup_parse_context_pop (context);
}

G_GNUC_PRINTF (3, 4)
static inline void
draft_xml_parser_error (DraftXmlParser  *state,
                        GError         **error,
                        const char      *message,
                        ...)
{
  va_list args;

  g_assert (error != NULL && *error == NULL);

  va_start (args, message);
  *error = g_error_new_valist (G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE, message, args);
  va_end (args);
}

#define DRAFT_XML_PARSER_PUSH(item) \
  draft_xml_parser_push(state, item, context, element_name, \
                        attribute_names, attribute_values, \
                        error)
#define DRAFT_XML_PARSER_ERROR() \
  draft_xml_parser_error(state, error, \
                         "Unexpected element type %s", \
                         element_name)
#define DRAFT_XML_PARSER_POP(TYPE) \
  draft_xml_parser_pop(state, context, TYPE)

static inline void
draft_xml_writer_open_element (GString    *string,
                               const char *element_name)
{
  g_string_append_printf (string, "<%s>", element_name);
}

static inline void
draft_xml_writer_begin_open_element (GString    *string,
                                     const char *element_name)
{
  g_string_append_printf (string, "<%s", element_name);
}

static inline void
draft_xml_writer_add_attribute (GString    *string,
                                const char *attribute_name,
                                const char *attribute_value)
{
  if (attribute_value != NULL)
    {
      g_autofree char *escaped = g_markup_escape_text (attribute_value, -1);

      g_string_append_c (string, ' ');
      g_string_append (string, attribute_name);
      g_string_append (string, "=\"");
      g_string_append (string, escaped);
      g_string_append_c (string, '"');
    }
}

static inline void
draft_xml_writer_add_attributes (GString            *string,
                                 const char * const *attribute_names,
                                 const char * const *attribute_values)
{
  for (guint i = 0; attribute_names[i]; i++)
    draft_xml_writer_add_attribute(string, attribute_names[i], attribute_values[i]);
}

static inline void
draft_xml_writer_end_open_element (GString  *string,
                                   gboolean  has_children)
{
  if (has_children)
    g_string_append_c (string, '>');
  else
    g_string_append (string, "/>");
}

static inline void
draft_xml_writer_close_element (GString    *string,
                                const char *element_name)
{
  g_string_append_printf (string, "</%s>", element_name);
}

static inline void
draft_xml_writer_add_attributes_va (GString    *string,
                                    const char *first_attribute,
                                    ...)
{
  va_list args;

  va_start (args, first_attribute);
  while (first_attribute)
    {
      const char *value = va_arg (args, const char *);
      if (value)
        draft_xml_writer_add_attribute (string, first_attribute, value);
      first_attribute = va_arg (args, const char *);
    }
  va_end (args);
}

static inline void
draft_xml_writer_write_escaped (GString    *string,
                                const char *str)
{
  char *escaped;

  if (str == NULL || *str == 0)
    return;

  escaped = g_markup_escape_text (str, -1);
  g_string_append (string, escaped);
  g_free (escaped);
}

static inline void
draft_xml_writer_write_gstring_escaped (GString *string,
                                        GString *to_write)
{
  if (to_write)
    draft_xml_writer_write_escaped (string, to_write->str);
}

G_END_DECLS
