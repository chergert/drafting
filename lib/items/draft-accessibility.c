/* draft-accessibility.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-accessibility.h"
#include "items/draft-property.h"

struct _DraftAccessibility
{
  DraftItem parent_instance;
};

typedef struct _DraftAccessibilityClass
{
  DraftItemClass parent_class;
} DraftAccessibilityClass;

G_DEFINE_TYPE (DraftAccessibility, draft_accessibility, DRAFT_TYPE_ITEM)

DraftAccessibility *
draft_accessibility_new (void)
{
  return g_object_new (DRAFT_TYPE_ACCESSIBILITY, NULL);
}

static void
draft_accessibility_start_element (GMarkupParseContext  *context,
                                   const char           *element_name,
                                   const char          **attribute_names,
                                   const char          **attribute_values,
                                   gpointer              user_data,
                                   GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_PUSH (draft_property_new ());
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_accessibility_end_element (GMarkupParseContext  *context,
                                 const char           *element_name,
                                 gpointer              user_data,
                                 GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_PROPERTY);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_accessibility_load_from_xml (DraftItem            *item,
                                   GMarkupParseContext  *context,
                                   DraftXmlParser       *state,
                                   const char           *element_name,
                                   const char          **attribute_names,
                                   const char          **attribute_values,
                                   GError              **error)
{
  static const GMarkupParser accessibility_parser = {
    draft_accessibility_start_element,
    draft_accessibility_end_element,
  };

  g_markup_parse_context_push (context, &accessibility_parser, state);
}

static void
draft_accessibility_save_to_xml (DraftItem    *item,
                                 GString      *string,
                                 DraftProject *project,
                                 gboolean      for_display)
{
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_open_element (string, "accessibility");
  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);
  draft_xml_writer_close_element (string, "accessibility");
}

static void
draft_accessibility_class_init (DraftAccessibilityClass *klass)
{
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  item_class->load_from_xml = draft_accessibility_load_from_xml;
  item_class->save_to_xml = draft_accessibility_save_to_xml;
}

static void
draft_accessibility_init (DraftAccessibility *self)
{
}
