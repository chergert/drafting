/* draft-tree-columns.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-tree-column.h"
#include "items/draft-tree-columns.h"

struct _DraftTreeColumns
{
  DraftItem parent_instance;
};

typedef struct _DraftTreeColumnsClass
{
  DraftItemClass parent_class;
} DraftTreeColumnsClass;

G_DEFINE_TYPE (DraftTreeColumns, draft_tree_columns, DRAFT_TYPE_ITEM)

DraftTreeColumns *
draft_tree_columns_new (void)
{
  return g_object_new (DRAFT_TYPE_TREE_COLUMNS, NULL);
}

static void
draft_tree_columns_start_element (GMarkupParseContext  *context,
                                  const char           *element_name,
                                  const char          **attribute_names,
                                  const char          **attribute_values,
                                  gpointer              user_data,
                                  GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "column") == 0)
    DRAFT_XML_PARSER_PUSH (draft_tree_column_new ());
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_tree_columns_end_element (GMarkupParseContext  *context,
                                const char           *element_name,
                                gpointer              user_data,
                                GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "column") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_TREE_COLUMN);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_tree_columns_load_from_xml (DraftItem            *columns,
                                  GMarkupParseContext  *context,
                                  DraftXmlParser       *state,
                                  const char           *element_name,
                                  const char          **attribute_names,
                                  const char          **attribute_values,
                                  GError              **error)
{
  static const GMarkupParser tree_columns_parser = {
    draft_tree_columns_start_element,
    draft_tree_columns_end_element,
  };

  g_markup_parse_context_push (context, &tree_columns_parser, state);
}

static void
draft_tree_columns_save_to_xml (DraftItem    *item,
                                GString      *string,
                                DraftProject *project,
                                gboolean      for_display)
{
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_open_element (string, "columns");
  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);
  draft_xml_writer_close_element (string, "columns");
}

static void
draft_tree_columns_class_init (DraftTreeColumnsClass *klass)
{
  DraftItemClass *columns_class = DRAFT_ITEM_CLASS (klass);

  columns_class->load_from_xml = draft_tree_columns_load_from_xml;
  columns_class->save_to_xml = draft_tree_columns_save_to_xml;
}

static void
draft_tree_columns_init (DraftTreeColumns *self)
{
}
