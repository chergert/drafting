/* draft-expression.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_EXPRESSION    (draft_expression_get_type ())
#define DRAFT_EXPRESSION(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_EXPRESSION, DraftExpression))
#define DRAFT_IS_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_EXPRESSION))

DRAFT_AVAILABLE_IN_ALL
GType                draft_expression_get_type      (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftExpression     *draft_expression_new           (DraftExpressionKind  kind);
DRAFT_AVAILABLE_IN_ALL
DraftExpressionKind  draft_expression_get_kind      (DraftExpression     *self);
DRAFT_AVAILABLE_IN_ALL
const char          *draft_expression_get_name      (DraftExpression     *self);
DRAFT_AVAILABLE_IN_ALL
const char          *draft_expression_get_type_name (DraftExpression     *self);
DRAFT_AVAILABLE_IN_ALL
const char          *draft_expression_get_function  (DraftExpression     *self);
DRAFT_AVAILABLE_IN_ALL
const char          *draft_expression_get_value     (DraftExpression     *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_expression_set_name      (DraftExpression     *self,
                                                     const char          *name);
DRAFT_AVAILABLE_IN_ALL
void                 draft_expression_set_type_name (DraftExpression     *self,
                                                     const char          *type_name);
DRAFT_AVAILABLE_IN_ALL
void                 draft_expression_set_function  (DraftExpression     *self,
                                                     const char          *function);
DRAFT_AVAILABLE_IN_ALL
void                 draft_expression_set_value     (DraftExpression     *self,
                                                     const char          *value);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftExpression, g_object_unref)

G_END_DECLS
