/* draft-style-name.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-style-name.h"

struct _DraftStyleName
{
  DraftItem parent_instance;
  char *name;
};

typedef struct _DraftStyleNameClass
{
  DraftItemClass parent_class;
} DraftStyleNameClass;

G_DEFINE_TYPE (DraftStyleName, draft_style_name, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftStyleName *
draft_style_name_new (const char *name)
{
  return g_object_new (DRAFT_TYPE_STYLE_NAME,
                       "name", name,
                       NULL);
}

static void
draft_style_name_start_element (GMarkupParseContext  *context,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                gpointer              user_data,
                                GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_style_name_end_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_style_name_load_from_xml (DraftItem            *item,
                                GMarkupParseContext  *context,
                                DraftXmlParser       *state,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                GError              **error)
{
  static const GMarkupParser style_name_parser = {
    draft_style_name_start_element,
    draft_style_name_end_element,
  };
  DraftStyleName *self = DRAFT_STYLE_NAME (item);
  const char *name = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "name", &name,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_style_name_set_name (self, name);

  g_markup_parse_context_push (context, &style_name_parser, state);
}

static void
draft_style_name_save_to_xml (DraftItem    *item,
                              GString      *string,
                              DraftProject *project,
                              gboolean      for_display)
{
  DraftStyleName *self = DRAFT_STYLE_NAME (item);

  draft_xml_writer_begin_open_element (string, "class");
  draft_xml_writer_add_attribute (string, "name", self->name);
  draft_xml_writer_end_open_element (string, FALSE);
}

static void
draft_style_name_finalize (GObject *object)
{
  DraftStyleName *self = (DraftStyleName *)object;

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (draft_style_name_parent_class)->finalize (object);
}

static void
draft_style_name_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftStyleName *self = DRAFT_STYLE_NAME (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, draft_style_name_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_style_name_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftStyleName *self = DRAFT_STYLE_NAME (object);

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_style_name_class_init (DraftStyleNameClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_style_name_finalize;
  object_class->get_property = draft_style_name_get_property;
  object_class->set_property = draft_style_name_set_property;

  item_class->load_from_xml = draft_style_name_load_from_xml;
  item_class->save_to_xml = draft_style_name_save_to_xml;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the CSS class",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_style_name_init (DraftStyleName *self)
{
}

const char *
draft_style_name_get_name (DraftStyleName *self)
{
  g_return_val_if_fail (DRAFT_IS_STYLE_NAME (self), NULL);

  return self->name;
}

void
draft_style_name_set_name (DraftStyleName *self,
                           const char     *name)
{
  g_return_if_fail (DRAFT_IS_STYLE_NAME (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}
