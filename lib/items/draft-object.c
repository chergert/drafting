/* draft-object.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-binding.h"
#include "items/draft-document.h"
#include "items/draft-item-private.h"
#include "items/draft-property.h"
#include "items/draft-object.h"
#include "items/draft-signal.h"
#include "items/draft-xml-item.h"
#include "undo/draft-transaction.h"
#include "util/draft-utils-private.h"

struct _DraftObject
{
  DraftItem parent_instance;
  char *class_name;
  char *parent_name;
  char *type_func;
  guint is_template : 1;
};

typedef struct _DraftObjectClass
{
  DraftItemClass parent_class;
} DraftObjectClass;

G_DEFINE_TYPE (DraftObject, draft_object, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_CLASS_NAME,
  PROP_IS_TEMPLATE,
  PROP_PARENT_NAME,
  PROP_TYPE_FUNC,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftObject *
draft_object_new (gboolean is_template)
{
  return g_object_new (DRAFT_TYPE_OBJECT,
                       "is-template", is_template,
                       NULL);
}

static void
draft_object_start_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_PUSH (draft_property_new ());
  else if (g_strcmp0 (element_name, "signal") == 0)
    DRAFT_XML_PARSER_PUSH (draft_signal_new ());
  else if (g_strcmp0 (element_name, "binding") == 0)
    DRAFT_XML_PARSER_PUSH (draft_binding_new ());
  else
    DRAFT_XML_PARSER_PUSH (draft_xml_item_new ());
}

static void
draft_object_end_element (GMarkupParseContext  *context,
                          const char           *element_name,
                          gpointer              user_data,
                          GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_PROPERTY);
  else if (g_strcmp0 (element_name, "signal") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_SIGNAL);
  else if (g_strcmp0 (element_name, "binding") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_BINDING);
  else
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_XML_ITEM);
}

static void
draft_object_load_from_xml (DraftItem            *item,
                            GMarkupParseContext  *context,
                            DraftXmlParser       *state,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            GError              **error)
{
  static const GMarkupParser object_parser = {
    draft_object_start_element,
    draft_object_end_element,
  };
  DraftObject *self = DRAFT_OBJECT (item);
  const char *class_name = NULL;
  const char *parent_name = NULL;
  const char *id = NULL;
  const char *type_func = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "class", &class_name,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "type-func", &type_func,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "id", &id,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "parent", &parent_name,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_item_set_id (item, id);
  draft_object_set_class_name (self, class_name);
  draft_object_set_parent_name (self, parent_name);
  draft_object_set_type_func (self, type_func);

  g_markup_parse_context_push (context, &object_parser, state);
}

static DraftItem *
apply_overrides (DraftObject *self)
{
  DraftItem *parent = NULL;

  g_assert (DRAFT_IS_OBJECT (self));

#define OVERRIDE_MISSING_PROPERTY(Type, Property, Value)                             \
  if (_draft_item_is_of_type (DRAFT_ITEM (self), Type))                              \
    {                                                                                \
      DraftProperty *prop = _draft_item_find_property (DRAFT_ITEM (self), Property); \
                                                                                     \
      if (prop == NULL)                                                              \
        {                                                                            \
          if (parent == NULL)                                                        \
            parent = DRAFT_ITEM (draft_object_new (FALSE));                          \
                                                                                     \
          prop = draft_property_new ();                                              \
          draft_property_set_name (prop, Property);                                  \
          draft_property_set_value (prop, Value);                                    \
                                                                                     \
          _draft_item_insert_after (DRAFT_ITEM (prop), parent, NULL);                \
        }                                                                            \
    }
#define OVERRIDE_PROPERTY(Type, Property, Value)                     \
  if (_draft_item_is_of_type (DRAFT_ITEM (self), Type))              \
    {                                                                \
      DraftProperty *prop;                                           \
                                                                     \
      if (parent == NULL)                                            \
        parent = DRAFT_ITEM (draft_object_new (FALSE));              \
                                                                     \
      prop = draft_property_new ();                                  \
      draft_property_set_name (prop, Property);                      \
      draft_property_set_value (prop, Value);                        \
                                                                     \
      _draft_item_insert_after (DRAFT_ITEM (prop), parent, NULL);    \
    }
# include "draft-overrides.defs"
#undef OVERRIDE_PROPERTY
#undef OVERRIDE_MISSING_PROPERTY

  return parent;
}

static void
draft_object_save_to_xml (DraftItem    *item,
                          GString      *string,
                          DraftProject *project,
                          gboolean      for_display)
{
  DraftObject *self = DRAFT_OBJECT (item);
  const char *element_name = self->is_template ? "template" : "object";
  DraftItem *child = draft_item_get_first_child (item);
  const char *class_name = self->class_name;
  const char *parent_name = self->parent_name;
  const char *id = for_display ? _draft_item_get_internal_id (item) : draft_item_get_id (item);
  g_autoptr(DraftItem) overrides = apply_overrides (self);
  gboolean needs_parent_close = FALSE;
  gboolean has_child = child != NULL || overrides != NULL;

  if (draft_object_is_toplevel (self) &&
      _draft_item_is_of_type (item, "GtkWidget") &&
      (!_draft_item_is_of_type (item, "GtkNative") || _draft_item_is_of_type (item, "GtkPopover")))
    {
      g_autofree char *parent_id = NULL;

      /* This is a widget, but not one that we will be able to
       * snapshot (as it's not a toplevel). Instead, we'll need
       * to parent it inside a toplevel helper and snapshot that
       * instead.
       */
      parent_id = g_strdup_printf ("%s_parent", _draft_item_get_internal_id (item));
      draft_xml_writer_begin_open_element (string, "object");
      draft_xml_writer_add_attributes_va (string,
                                          "id", parent_id,
                                          "class", "DraftGtkWindow",
                                          NULL);
      draft_xml_writer_end_open_element (string, TRUE);

      draft_xml_writer_begin_open_element (string, "child");
      if (_draft_item_is_of_type (item, "GtkPopover"))
        draft_xml_writer_add_attribute (string, "type", "popover");
      draft_xml_writer_end_open_element (string, TRUE);

      needs_parent_close = TRUE;
    }

  /* If we have a <template> and we're previewing the document, then we need
   * to instead translate this to an <object> so it can be created inline.
   * Additionally, we won't have the widgets class available to us, so we try
   * to instead create an instance of the parent type.
   *
   * In the future, we may even want to resolve the parent type through another
   * document (in case the parent type has a .ui file we have access to).
   *
   * Furthermore, we'll need to convert a "GtkWidget" parent type to use our
   * internal helper "DraftGtkWidget".
   */
  if (for_display && self->is_template)
    {
      element_name = "object";
      if (parent_name != NULL)
        class_name = parent_name;
      parent_name = NULL;

      if (g_strcmp0 (class_name, "GtkWidget") == 0)
        class_name = "DraftGtkWidget";
    }

  draft_xml_writer_begin_open_element (string, element_name);
  draft_xml_writer_add_attributes_va (string,
                                      "id", id,
                                      "class", class_name,
                                      "parent", parent_name,
                                      "type-func", self->type_func,
                                      NULL);
  draft_xml_writer_end_open_element (string, has_child);

  if (has_child)
    {
      for (; child; child = draft_item_get_next_sibling (child))
        _draft_item_save_to_xml (child, string, project, for_display);

      if (overrides)
        {
          for (child = draft_item_get_first_child (overrides);
               child != NULL;
               child = draft_item_get_next_sibling (child))
            _draft_item_save_to_xml (child, string, project, for_display);
        }

      draft_xml_writer_close_element (string, element_name);
    }

  if (needs_parent_close)
    {
      draft_xml_writer_close_element (string, "child");
      draft_xml_writer_close_element (string, "object");
    }
}

static char *
draft_object_get_display_name (DraftItem *item)
{
  DraftObject *self = DRAFT_OBJECT (item);
  const char *id = draft_item_get_id (item);

  if (id && self->class_name)
    return g_strdup_printf ("%s : %s", self->class_name, id);

  if (self->class_name)
    return g_strdup (self->class_name);

  if (id)
    return g_strdup (id);

  return DRAFT_ITEM_CLASS (draft_object_parent_class)->get_display_name (item);
}

static void
draft_object_finalize (GObject *object)
{
  DraftObject *self = (DraftObject *)object;

  g_clear_pointer (&self->class_name, g_free);
  g_clear_pointer (&self->parent_name, g_free);
  g_clear_pointer (&self->type_func, g_free);

  G_OBJECT_CLASS (draft_object_parent_class)->finalize (object);
}

static void
draft_object_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DraftObject *self = DRAFT_OBJECT (object);

  switch (prop_id)
    {
    case PROP_CLASS_NAME:
      g_value_set_string (value, draft_object_get_class_name (self));
      break;

    case PROP_IS_TEMPLATE:
      g_value_set_boolean (value, draft_object_get_is_template (self));
      break;

    case PROP_PARENT_NAME:
      g_value_set_string (value, draft_object_get_parent_name (self));
      break;

    case PROP_TYPE_FUNC:
      g_value_set_string (value, draft_object_get_type_func (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_object_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DraftObject *self = DRAFT_OBJECT (object);

  switch (prop_id)
    {
    case PROP_CLASS_NAME:
      draft_object_set_class_name (self, g_value_get_string (value));
      break;

    case PROP_IS_TEMPLATE:
      self->is_template = g_value_get_boolean (value);
      break;

    case PROP_PARENT_NAME:
      draft_object_set_parent_name (self, g_value_get_string (value));
      break;

    case PROP_TYPE_FUNC:
      draft_object_set_type_func (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_object_class_init (DraftObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_object_finalize;
  object_class->get_property = draft_object_get_property;
  object_class->set_property = draft_object_set_property;

  item_class->get_display_name = draft_object_get_display_name;
  item_class->load_from_xml = draft_object_load_from_xml;
  item_class->save_to_xml = draft_object_save_to_xml;

  properties [PROP_IS_TEMPLATE] =
    g_param_spec_boolean ("is-template",
                          "Is Template",
                          "If the object is a template",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CLASS_NAME] =
    g_param_spec_string ("class-name",
                         "Class Name",
                         "The name of the object class",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_PARENT_NAME] =
    g_param_spec_string ("parent-name",
                         "Parent Name",
                         "The name of the parent class",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_TYPE_FUNC] =
    g_param_spec_string ("type-func",
                         "Type Func",
                         "The function to call to get the GType",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_object_init (DraftObject *self)
{
}

const char *
draft_object_get_class_name (DraftObject *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT (self), NULL);

  return self->class_name;
}

void
draft_object_set_class_name (DraftObject *self,
                             const char  *class_name)
{
  g_return_if_fail (DRAFT_IS_OBJECT (self));

  if (g_strcmp0 (class_name, self->class_name) != 0)
    {
      g_free (self->class_name);
      self->class_name = g_strdup (class_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CLASS_NAME]);
      g_object_notify (G_OBJECT (self), "display-name");
    }
}

const char *
draft_object_get_parent_name (DraftObject *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT (self), NULL);

  return self->parent_name;
}

void
draft_object_set_parent_name (DraftObject *self,
                              const char  *parent_name)
{
  g_return_if_fail (DRAFT_IS_OBJECT (self));

  if (g_strcmp0 (parent_name, self->parent_name) != 0)
    {
      g_free (self->parent_name);
      self->parent_name = g_strdup (parent_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PARENT_NAME]);
    }
}

gboolean
draft_object_get_is_template (DraftObject *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT (self), FALSE);

  return self->is_template;
}

const char *
draft_object_get_type_func (DraftObject *self)
{
  g_return_val_if_fail (DRAFT_IS_OBJECT (self), NULL);

  return self->type_func;
}

void
draft_object_set_type_func (DraftObject *self,
                            const char  *type_func)
{
  g_return_if_fail (DRAFT_IS_OBJECT (self));

  if (g_strcmp0 (type_func, self->type_func) != 0)
    {
      g_free (self->type_func);
      self->type_func = g_strdup (type_func);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TYPE_FUNC]);
    }
}

gboolean
draft_object_is_toplevel (DraftObject *self)
{
  DraftItem *parent;

  g_return_val_if_fail (DRAFT_IS_OBJECT (self), FALSE);

  parent = draft_item_get_parent (DRAFT_ITEM (self));

  return parent == NULL || DRAFT_IS_DOCUMENT (parent);
}

/**
 * draft_object_find_property:
 * @self: a #DraftObject
 * @name: the name of the property
 *
 * Locates an existing property named @name within the object as
 * defined the template or user.
 *
 * If no property has been regisered, this function returns %NULL. That
 * doesn't mean the property doesn't exist, just that the user has not
 * set the property either in the template they loaded, or manually.
 *
 * It is also possible to get a property back from this method which
 * the user has not yet modified.
 *
 * Returns: (transfer none) (nullable): a #DraftProperty or %NULL
 */
DraftProperty *
draft_object_find_property (DraftObject *self,
                            const char  *name)
{
  DraftItem *child;

  g_return_val_if_fail (DRAFT_IS_OBJECT (self), NULL);
  g_return_val_if_fail (name != NULL, NULL);

  for (child = draft_item_get_first_child (DRAFT_ITEM (self));
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (DRAFT_IS_PROPERTY (child))
        {
          if (g_strcmp0 (name, draft_property_get_name (DRAFT_PROPERTY (child))) == 0)
            return DRAFT_PROPERTY (child);
        }
    }

  return NULL;
}
