/* draft-signal.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_SIGNAL    (draft_signal_get_type ())
#define DRAFT_SIGNAL(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_SIGNAL, DraftSignal))
#define DRAFT_IS_SIGNAL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_SIGNAL))

DRAFT_AVAILABLE_IN_ALL
GType        draft_signal_get_type    (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftSignal *draft_signal_new         (void);
DRAFT_AVAILABLE_IN_ALL
const char  *draft_signal_get_name    (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
const char  *draft_signal_get_detail   (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
const char  *draft_signal_get_handler (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
const char  *draft_signal_get_object  (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
gboolean     draft_signal_get_swapped (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
gboolean     draft_signal_get_after   (DraftSignal *self);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_name    (DraftSignal *self,
                                       const char  *name);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_detail  (DraftSignal *self,
                                       const char  *detail);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_handler (DraftSignal *self,
                                       const char  *handler);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_object  (DraftSignal *self,
                                       const char  *object);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_swapped (DraftSignal *self,
                                       gboolean     swapped);
DRAFT_AVAILABLE_IN_ALL
void         draft_signal_set_after   (DraftSignal *self,
                                       gboolean     swapped);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftSignal, g_object_unref)

G_END_DECLS
