/* draft-tree-column.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-tree-column.h"

struct _DraftTreeColumn
{
  DraftItem parent_instance;
  char *type_name;
};

typedef struct _DraftTreeColumnClass
{
  DraftItemClass parent_class;
} DraftTreeColumnClass;

G_DEFINE_TYPE (DraftTreeColumn, draft_tree_column, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_TYPE_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftTreeColumn *
draft_tree_column_new (void)
{
  return g_object_new (DRAFT_TYPE_TREE_COLUMN, NULL);
}

static void
draft_tree_column_start_element (GMarkupParseContext  *context,
                                 const char           *element_name,
                                 const char          **attribute_names,
                                 const char          **attribute_values,
                                 gpointer              user_data,
                                 GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_tree_column_end_element (GMarkupParseContext  *context,
                               const char           *element_name,
                               gpointer              user_data,
                               GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_tree_column_load_from_xml (DraftItem            *item,
                                 GMarkupParseContext  *context,
                                 DraftXmlParser       *state,
                                 const char           *element_name,
                                 const char          **attribute_names,
                                 const char          **attribute_values,
                                 GError              **error)
{
  static const GMarkupParser tree_column_parser = {
    draft_tree_column_start_element,
    draft_tree_column_end_element,
  };
  DraftTreeColumn *self = DRAFT_TREE_COLUMN (item);
  const char *type_name = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "type", &type_name,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_tree_column_set_type_name (self, type_name);

  g_markup_parse_context_push (context, &tree_column_parser, state);
}

static void
draft_tree_column_save_to_xml (DraftItem    *item,
                               GString      *string,
                               DraftProject *project,
                               gboolean      for_display)
{
  DraftTreeColumn *self = DRAFT_TREE_COLUMN (item);

  draft_xml_writer_begin_open_element (string, "column");
  draft_xml_writer_add_attribute (string, "type", self->type_name);
  draft_xml_writer_end_open_element (string, FALSE);
}

static void
draft_tree_column_finalize (GObject *object)
{
  DraftTreeColumn *self = (DraftTreeColumn *)object;

  g_clear_pointer (&self->type_name, g_free);

  G_OBJECT_CLASS (draft_tree_column_parent_class)->finalize (object);
}

static void
draft_tree_column_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  DraftTreeColumn *self = DRAFT_TREE_COLUMN (object);

  switch (prop_id)
    {
    case PROP_TYPE_NAME:
      g_value_set_string (value, draft_tree_column_get_type_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_tree_column_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  DraftTreeColumn *self = DRAFT_TREE_COLUMN (object);

  switch (prop_id)
    {
    case PROP_TYPE_NAME:
      draft_tree_column_set_type_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_tree_column_class_init (DraftTreeColumnClass *klass)
{
  DraftItemClass *column_class = DRAFT_ITEM_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_tree_column_finalize;
  object_class->get_property = draft_tree_column_get_property;
  object_class->set_property = draft_tree_column_set_property;

  column_class->load_from_xml = draft_tree_column_load_from_xml;
  column_class->save_to_xml = draft_tree_column_save_to_xml;

  properties [PROP_TYPE_NAME] =
    g_param_spec_string ("type-name",
                         "Type Name",
                         "The type name of the column",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_tree_column_init (DraftTreeColumn *self)
{
}

const char *
draft_tree_column_get_type_name (DraftTreeColumn *self)
{
  g_return_val_if_fail (DRAFT_IS_TREE_COLUMN (self), NULL);

  return self->type_name;
}

void
draft_tree_column_set_type_name (DraftTreeColumn *self,
                                 const char      *type_name)
{
  g_return_if_fail (DRAFT_IS_TREE_COLUMN (self));

  if (g_strcmp0 (self->type_name, type_name) != 0)
    {
      g_free (self->type_name);
      self->type_name = g_strdup (type_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TYPE_NAME]);
    }
}
