/* draft-object.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_OBJECT    (draft_object_get_type ())
#define DRAFT_OBJECT(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_OBJECT, DraftObject))
#define DRAFT_IS_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_OBJECT))

DRAFT_AVAILABLE_IN_ALL
GType          draft_object_get_type        (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftObject   *draft_object_new             (gboolean     is_template);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_object_get_is_template (DraftObject *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_object_get_class_name  (DraftObject *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_object_get_parent_name (DraftObject *self);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_object_get_type_func   (DraftObject *self);
DRAFT_AVAILABLE_IN_ALL
void           draft_object_set_class_name  (DraftObject *self,
                                             const char  *class_name);
DRAFT_AVAILABLE_IN_ALL
void           draft_object_set_parent_name (DraftObject *self,
                                             const char  *parent_name);
DRAFT_AVAILABLE_IN_ALL
void           draft_object_set_type_func   (DraftObject *self,
                                             const char  *type_func);
DRAFT_AVAILABLE_IN_ALL
gboolean       draft_object_is_toplevel     (DraftObject *self);
DRAFT_AVAILABLE_IN_ALL
DraftProperty *draft_object_find_property   (DraftObject *self,
                                             const char  *name);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftObject, g_object_unref)

G_END_DECLS
