/* draft-menu-link.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_MENU_LINK    (draft_menu_link_get_type ())
#define DRAFT_MENU_LINK(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_MENU_LINK, DraftMenuLink))
#define DRAFT_IS_MENU_LINK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_MENU_LINK))

DRAFT_AVAILABLE_IN_ALL
GType          draft_menu_link_get_type (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftMenuLink *draft_menu_link_new      (const char    *name);
DRAFT_AVAILABLE_IN_ALL
const char    *draft_menu_link_get_name (DraftMenuLink *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftMenuLink, g_object_unref)

G_END_DECLS
