/* draft-binding.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-binding.h"
#include "items/draft-expression.h"
#include "items/draft-item-private.h"

struct _DraftBinding
{
  DraftItem parent_instance;
  char *name;
};

typedef struct _DraftBindingClass
{
  DraftItemClass parent_class;
} DraftBindingClass;

G_DEFINE_TYPE (DraftBinding, draft_binding, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftBinding *
draft_binding_new (void)
{
  return g_object_new (DRAFT_TYPE_BINDING, NULL);
}

static void
draft_binding_start_element (GMarkupParseContext  *context,
                             const char           *element_name,
                             const char          **attribute_names,
                             const char          **attribute_values,
                             gpointer              user_data,
                             GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "lookup") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_LOOKUP));
  else if (g_strcmp0 (element_name, "constant") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_CONSTANT));
  else if (g_strcmp0 (element_name, "closure") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_CLOSURE));
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_binding_end_element (GMarkupParseContext  *context,
                           const char           *element_name,
                           gpointer              user_data,
                           GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "lookup") == 0 ||
      g_strcmp0 (element_name, "closure") == 0 ||
      g_strcmp0 (element_name, "constant") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_EXPRESSION);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_binding_load_from_xml (DraftItem            *item,
                             GMarkupParseContext  *context,
                             DraftXmlParser       *state,
                             const char           *element_name,
                             const char          **attribute_names,
                             const char          **attribute_values,
                             GError              **error)
{
  static const GMarkupParser binding_parser = {
    draft_binding_start_element,
    draft_binding_end_element,
  };
  DraftBinding *self = DRAFT_BINDING (item);
  const char *name = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "name", &name,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_binding_set_name (self, name);

  g_markup_parse_context_push (context, &binding_parser, state);
}

static void
draft_binding_save_to_xml (DraftItem    *item,
                           GString      *string,
                           DraftProject *project,
                           gboolean      for_display)
{
  DraftBinding *self = DRAFT_BINDING (item);
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_begin_open_element (string, "binding");
  draft_xml_writer_add_attribute (string, "name", self->name);
  draft_xml_writer_end_open_element (string, TRUE);

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, "binding");
}

static void
draft_binding_finalize (GObject *object)
{
  DraftBinding *self = (DraftBinding *)object;

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (draft_binding_parent_class)->finalize (object);
}

static void
draft_binding_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  DraftBinding *self = DRAFT_BINDING (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, draft_binding_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_binding_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  DraftBinding *self = DRAFT_BINDING (object);

  switch (prop_id)
    {
    case PROP_NAME:
      draft_binding_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_binding_class_init (DraftBindingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_binding_finalize;
  object_class->get_property = draft_binding_get_property;
  object_class->set_property = draft_binding_set_property;

  item_class->load_from_xml = draft_binding_load_from_xml;
  item_class->save_to_xml = draft_binding_save_to_xml;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the property for binding",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_binding_init (DraftBinding *self)
{
}

const char *
draft_binding_get_name (DraftBinding *self)
{
  g_return_val_if_fail (DRAFT_IS_BINDING (self), NULL);

  return self->name;
}

void
draft_binding_set_name (DraftBinding *self,
                        const char   *name)
{
  g_return_if_fail (DRAFT_IS_BINDING (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}
