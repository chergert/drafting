/* draft-signal.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-signal.h"

struct _DraftSignal
{
  DraftItem parent_instance;
  char *name;
  char *detail;
  char *handler;
  char *object;
  guint after : 1;
  guint swapped : 1;
};

typedef struct _DraftSignalClass
{
  DraftItemClass parent_class;
} DraftSignalClass;

G_DEFINE_TYPE (DraftSignal, draft_signal, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_AFTER,
  PROP_DETAIL,
  PROP_HANDLER,
  PROP_NAME,
  PROP_OBJECT,
  PROP_SWAPPED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftSignal *
draft_signal_new (void)
{
  return g_object_new (DRAFT_TYPE_SIGNAL, NULL);
}

static void
draft_signal_start_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_signal_end_element (GMarkupParseContext  *context,
                          const char           *element_name,
                          gpointer              user_data,
                          GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_signal_load_from_xml (DraftItem            *item,
                            GMarkupParseContext  *context,
                            DraftXmlParser       *state,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            GError              **error)
{
  static const GMarkupParser signal_parser = {
    draft_signal_start_element,
    draft_signal_end_element,
  };
  DraftSignal *self = DRAFT_SIGNAL (item);
  g_autofree char *split_name = NULL;
  g_autofree char *split_detail = NULL;
  const char *name = NULL;
  const char *object = NULL;
  const char *handler = NULL;
  const char *split;
  gboolean after = FALSE;
  gboolean swapped = FALSE;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING , "name", &name,
                                    G_MARKUP_COLLECT_STRING , "handler", &handler,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "object", &object,
                                    G_MARKUP_COLLECT_BOOLEAN | G_MARKUP_COLLECT_OPTIONAL, "swapped", &swapped,
                                    G_MARKUP_COLLECT_BOOLEAN | G_MARKUP_COLLECT_OPTIONAL, "after", &after,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  if (name && (split = strstr (name, "::")))
    {
      name = split_name = g_strndup (name, split - name);
      split_detail = g_strdup (split + 2);
    }

  draft_signal_set_name (self, name);
  draft_signal_set_detail (self, split_detail);
  draft_signal_set_handler (self, handler);
  draft_signal_set_object (self, object);
  draft_signal_set_after (self, after);
  draft_signal_set_swapped (self, swapped);

  g_markup_parse_context_push (context, &signal_parser, state);
}

static void
draft_signal_save_to_xml (DraftItem    *item,
                          GString      *string,
                          DraftProject *project,
                          gboolean      for_display)
{
  /* We ignore <signal> when displaying the document because we dont
   * know if the handlers will be registered nor if it would even be
   * safe to do so.
   */
  if (!for_display)
    {
      DraftSignal *self = DRAFT_SIGNAL (item);
      g_autofree char *name = NULL;

      if (self->detail != NULL && *self->detail != 0)
        name = g_strdup_printf ("%s::%s", self->name, self->detail);
      else
        name = g_strdup (self->name);

      draft_xml_writer_begin_open_element (string, "signal");
      draft_xml_writer_add_attributes_va (string,
                                          "name", name,
                                          "handler", self->handler,
                                          "object", self->object,
                                          "swapped", self->swapped ? "true" : NULL,
                                          "after", self->after ? "true" : NULL,
                                          NULL);
      draft_xml_writer_end_open_element (string, FALSE);
    }
}

static void
draft_signal_finalize (GObject *object)
{
  DraftSignal *self = (DraftSignal *)object;

  g_clear_pointer (&self->detail, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->handler, g_free);
  g_clear_pointer (&self->object, g_free);

  G_OBJECT_CLASS (draft_signal_parent_class)->finalize (object);
}

static void
draft_signal_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DraftSignal *self = DRAFT_SIGNAL (object);

  switch (prop_id)
    {
    case PROP_DETAIL:
      g_value_set_string (value, draft_signal_get_detail (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, draft_signal_get_name (self));
      break;

    case PROP_HANDLER:
      g_value_set_string (value, draft_signal_get_handler (self));
      break;

    case PROP_OBJECT:
      g_value_set_string (value, draft_signal_get_object (self));
      break;

    case PROP_AFTER:
      g_value_set_boolean (value, draft_signal_get_after (self));
      break;

    case PROP_SWAPPED:
      g_value_set_boolean (value, draft_signal_get_swapped (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signal_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DraftSignal *self = DRAFT_SIGNAL (object);

  switch (prop_id)
    {
    case PROP_DETAIL:
      draft_signal_set_detail (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      draft_signal_set_name (self, g_value_get_string (value));
      break;

    case PROP_HANDLER:
      draft_signal_set_handler (self, g_value_get_string (value));
      break;

    case PROP_OBJECT:
      draft_signal_set_object (self, g_value_get_string (value));
      break;

    case PROP_SWAPPED:
      draft_signal_set_swapped (self, g_value_get_boolean (value));
      break;

    case PROP_AFTER:
      draft_signal_set_after (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_signal_class_init (DraftSignalClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_signal_finalize;
  object_class->get_property = draft_signal_get_property;
  object_class->set_property = draft_signal_set_property;

  item_class->load_from_xml = draft_signal_load_from_xml;
  item_class->save_to_xml = draft_signal_save_to_xml;

  properties [PROP_DETAIL] =
    g_param_spec_string ("detail",
                         "Detail",
                         "The GQuark detail for the signal, as a string",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the signal to connect to",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_HANDLER] =
    g_param_spec_string ("handler",
                         "Handler",
                         "The handler for the signal",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_OBJECT] =
    g_param_spec_string ("object",
                         "Object",
                         "The object to pass as data for the signal",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_SWAPPED] =
    g_param_spec_boolean ("swapped",
                          "Swapped",
                          "If self and user_data parameters should be swapped",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_AFTER] =
    g_param_spec_boolean ("after",
                          "After",
                          "If the handler should be called after the default handler",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_signal_init (DraftSignal *self)
{
}

const char *
draft_signal_get_name (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), NULL);

  return self->name;
}

void
draft_signal_set_name (DraftSignal *self,
                       const char  *name)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const char *
draft_signal_get_detail (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), NULL);

  return self->detail;
}

void
draft_signal_set_detail (DraftSignal *self,
                         const char  *detail)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  if (g_strcmp0 (detail, self->detail) != 0)
    {
      g_free (self->detail);
      self->detail = g_strdup (detail);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DETAIL]);
    }
}

const char *
draft_signal_get_handler (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), NULL);

  return self->handler;
}

void
draft_signal_set_handler (DraftSignal *self,
                          const char  *handler)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  if (g_strcmp0 (handler, self->handler) != 0)
    {
      g_free (self->handler);
      self->handler = g_strdup (handler);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_HANDLER]);
    }
}

const char *
draft_signal_get_object (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), NULL);

  return self->object;
}

void
draft_signal_set_object (DraftSignal *self,
                         const char  *object)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  if (g_strcmp0 (object, self->object) != 0)
    {
      g_free (self->object);
      self->object = g_strdup (object);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OBJECT]);
    }
}

gboolean
draft_signal_get_after (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), FALSE);

  return self->after;
}

void
draft_signal_set_after (DraftSignal *self,
                        gboolean     after)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  after = !!after;

  if (after != self->after)
    {
      self->after = after;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_AFTER]);
    }
}

gboolean
draft_signal_get_swapped (DraftSignal *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL (self), FALSE);

  return self->swapped;
}

void
draft_signal_set_swapped (DraftSignal *self,
                          gboolean     swapped)
{
  g_return_if_fail (DRAFT_IS_SIGNAL (self));

  swapped = !!swapped;

  if (swapped != self->swapped)
    {
      self->swapped = swapped;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SWAPPED]);
    }
}
