/* draft-dependency.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "items/draft-item.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_DEPENDENCY    (draft_dependency_get_type ())
#define DRAFT_DEPENDENCY(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_DEPENDENCY, DraftDependency))
#define DRAFT_IS_DEPENDENCY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_DEPENDENCY))

DRAFT_AVAILABLE_IN_ALL
GType       draft_dependency_get_type               (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
DraftDependency  *draft_dependency_new                    (void);
DRAFT_AVAILABLE_IN_ALL
const char *draft_dependency_get_library (DraftDependency  *self);
DRAFT_AVAILABLE_IN_ALL
void        draft_dependency_set_library (DraftDependency  *self,
                                               const char *library);
DRAFT_AVAILABLE_IN_ALL
const char *draft_dependency_get_version (DraftDependency  *self);
DRAFT_AVAILABLE_IN_ALL
void        draft_dependency_set_version (DraftDependency  *self,
                                               const char *version);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftDependency, g_object_unref)

G_END_DECLS
