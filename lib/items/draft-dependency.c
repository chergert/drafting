/* draft-dependency.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-dependency.h"

struct _DraftDependency
{
  DraftItem parent_instance;
  char *library;
  char *version;
};

typedef struct _DraftDependencyClass
{
  DraftItemClass parent_class;
} DraftDependencyClass;

G_DEFINE_TYPE (DraftDependency, draft_dependency, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_LIBRARY,
  PROP_VERSION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftDependency *
draft_dependency_new (void)
{
  return g_object_new (DRAFT_TYPE_DEPENDENCY, NULL);
}

static void
draft_dependency_start_element (GMarkupParseContext  *context,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                gpointer              user_data,
                                GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_dependency_end_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_dependency_load_from_xml (DraftItem            *item,
                                GMarkupParseContext  *context,
                                DraftXmlParser       *state,
                                const char           *element_name,
                                const char          **attribute_names,
                                const char          **attribute_values,
                                GError              **error)
{
  static const GMarkupParser dependency_parser = {
    draft_dependency_start_element,
    draft_dependency_end_element,
  };
  DraftDependency *self = DRAFT_DEPENDENCY (item);
  const char *library = NULL;
  const char *version = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "lib", &library,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_dependency_set_library (self, library);
  draft_dependency_set_version (self, version);

  g_markup_parse_context_push (context, &dependency_parser, state);
}

static void
draft_dependency_save_to_xml (DraftItem    *item,
                              GString      *string,
                              DraftProject *project,
                              gboolean      for_display)
{
  DraftDependency *self = DRAFT_DEPENDENCY (item);

  /* We ignore <requires/> for display because we dont
   * want to have it parsed by GTK in the build container.
   */
  if (for_display)
    return;

  draft_xml_writer_begin_open_element (string, "requires");
  draft_xml_writer_add_attributes_va (string,
                                      "lib", self->library,
                                      "version", self->version,
                                      NULL);
  draft_xml_writer_end_open_element (string, FALSE);
}

static void
draft_dependency_finalize (GObject *object)
{
  DraftDependency *self = (DraftDependency *)object;

  g_clear_pointer (&self->library, g_free);
  g_clear_pointer (&self->version, g_free);

  G_OBJECT_CLASS (draft_dependency_parent_class)->finalize (object);
}

static void
draft_dependency_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  DraftDependency *self = DRAFT_DEPENDENCY (object);

  switch (prop_id)
    {
    case PROP_LIBRARY:
      g_value_set_string (value, draft_dependency_get_library (self));
      break;

    case PROP_VERSION:
      g_value_set_string (value, draft_dependency_get_version (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_dependency_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  DraftDependency *self = DRAFT_DEPENDENCY (object);

  switch (prop_id)
    {
    case PROP_LIBRARY:
      draft_dependency_set_library (self, g_value_get_string (value));
      break;

    case PROP_VERSION:
      draft_dependency_set_version (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_dependency_class_init (DraftDependencyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_dependency_finalize;
  object_class->get_property = draft_dependency_get_property;
  object_class->set_property = draft_dependency_set_property;

  item_class->load_from_xml = draft_dependency_load_from_xml;
  item_class->save_to_xml = draft_dependency_save_to_xml;

  properties [PROP_LIBRARY] =
    g_param_spec_string ("library",
                         "Library",
                         "The library to load",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "Version",
                         "The version of the library to load",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_dependency_init (DraftDependency *self)
{
}

const char *
draft_dependency_get_library (DraftDependency *self)
{
  g_return_val_if_fail (DRAFT_IS_DEPENDENCY (self), NULL);

  return self->library;
}

void
draft_dependency_set_library (DraftDependency *self,
                              const char      *library)
{
  g_return_if_fail (DRAFT_IS_DEPENDENCY (self));

  if (g_strcmp0 (library, self->library) != 0)
    {
      g_free (self->library);
      self->library = g_strdup (library);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LIBRARY]);
    }
}

const char *
draft_dependency_get_version (DraftDependency *self)
{
  g_return_val_if_fail (DRAFT_IS_DEPENDENCY (self), NULL);

  return self->version;
}

void
draft_dependency_set_version (DraftDependency *self,
                              const char      *version)
{
  g_return_if_fail (DRAFT_IS_DEPENDENCY (self));

  if (g_strcmp0 (version, self->version) != 0)
    {
      g_free (self->version);
      self->version = g_strdup (version);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VERSION]);
    }
}
