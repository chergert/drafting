/* draft-property.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-expression.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-property-private.h"
#include "util/draft-utils-private.h"

struct _DraftProperty
{
  DraftItem parent_instance;
  char *bind_source;
  char *bind_property;
  char *name;
  GtkAdjustment *adjustment;
  GString *value;
  GBindingFlags bind_flags;
  guint translatable : 1;
};

typedef struct _DraftPropertyClass
{
  DraftItemClass parent_class;
} DraftPropertyClass;

G_DEFINE_TYPE (DraftProperty, draft_property, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_BIND_SOURCE,
  PROP_BIND_PROPERTY,
  PROP_BIND_FLAGS,
  PROP_NAME,
  PROP_VALUE,
  PROP_TRANSLATABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftProperty *
draft_property_new (void)
{
  return g_object_new (DRAFT_TYPE_PROPERTY, NULL);
}

static void
draft_property_start_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_PUSH (draft_object_new (FALSE));
  else if (g_strcmp0 (element_name, "lookup") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_LOOKUP));
  else if (g_strcmp0 (element_name, "constant") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_CONSTANT));
  else if (g_strcmp0 (element_name, "closure") == 0)
    DRAFT_XML_PARSER_PUSH (draft_expression_new (DRAFT_EXPRESSION_KIND_CLOSURE));
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_property_end_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftProperty *self;

  if (g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_OBJECT);
  else if (g_strcmp0 (element_name, "lookup") == 0 ||
           g_strcmp0 (element_name, "closure") == 0 ||
           g_strcmp0 (element_name, "constant") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_EXPRESSION);
  else
    DRAFT_XML_PARSER_ERROR ();

  /* Clear value text if we have a subobject */
  self = draft_xml_parser_current (state, DRAFT_TYPE_PROPERTY);
  if (draft_item_get_first_child (DRAFT_ITEM (self)) != NULL)
    draft_property_set_value (self, NULL);
}

static void
draft_property_text (GMarkupParseContext  *context,
                     const char           *text,
                     gsize                 text_len,
                     gpointer              user_data,
                     GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftProperty *self = draft_xml_parser_current (state, DRAFT_TYPE_PROPERTY);

  if (draft_item_get_first_child (DRAFT_ITEM (self)) != NULL)
    return;

  if (self->value == NULL)
    self->value = g_string_new (NULL);

  g_string_append_len (self->value, text, text_len);
}

static void
draft_property_load_from_xml (DraftItem            *item,
                              GMarkupParseContext  *context,
                              DraftXmlParser       *state,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              GError              **error)
{
  static const GMarkupParser property_parser = {
    draft_property_start_element,
    draft_property_end_element,
    draft_property_text,
  };
  DraftProperty *self = DRAFT_PROPERTY (item);
  const char *name = NULL;
  const char *bind_property = NULL;
  const char *bind_source = NULL;
  const char *bind_flags_str = NULL;
  gboolean translatable = FALSE;
  guint bind_flags = 0;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "name", &name,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "bind-source", &bind_source,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "bind-property", &bind_property,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "bind-flags", &bind_flags_str,
                                    G_MARKUP_COLLECT_BOOLEAN | G_MARKUP_COLLECT_OPTIONAL, "translatable", &translatable,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  if (bind_flags_str)
    {
      if (!_draft_flags_from_string (G_TYPE_BINDING_FLAGS, NULL, bind_flags_str, &bind_flags, error))
        return;
    }

  draft_property_set_name (self, name);
  draft_property_set_translatable (self, translatable);
  draft_property_set_bind_source (self, bind_source);
  draft_property_set_bind_property (self, bind_property);
  draft_property_set_bind_flags (self, bind_flags);

  g_markup_parse_context_push (context, &property_parser, state);
}

static void
draft_property_save_to_xml (DraftItem    *item,
                            GString      *string,
                            DraftProject *project,
                            gboolean      for_display)
{
  DraftProperty *self = DRAFT_PROPERTY (item);
  g_autofree char *flags = _draft_flags_to_string (G_TYPE_BINDING_FLAGS, self->bind_flags);
  DraftItem *child = draft_item_get_first_child (item);

  /* If the child is a DraftExpression, then it is likely we cannot
   * service the expression without the type to load. The safest
   * thing to do is to avoid loading it altogether since it could
   * cause the GtkBuilder to fail to load.
   */
  if (for_display && DRAFT_IS_EXPRESSION (child))
    return;

  /* Don't save default values */
  if (draft_property_is_default_value (self))
    return;

  draft_xml_writer_begin_open_element (string, "property");
  draft_xml_writer_add_attributes_va (string,
                                      "name", self->name,
                                      "bind-source", self->bind_source,
                                      "bind-property", self->bind_property,
                                      "bind-flags", flags,
                                      "translatable", self->translatable ? "yes" : NULL,
                                      NULL);
  draft_xml_writer_end_open_element (string, TRUE);
  draft_xml_writer_write_gstring_escaped (string, self->value);
  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);
  draft_xml_writer_close_element (string, "property");
}

static void
draft_property_finalize (GObject *object)
{
  DraftProperty *self = (DraftProperty *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->adjustment);

  if (self->value != NULL)
    g_string_free (g_steal_pointer (&self->value), TRUE);

  G_OBJECT_CLASS (draft_property_parent_class)->finalize (object);
}

static void
draft_property_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  DraftProperty *self = DRAFT_PROPERTY (object);

  switch (prop_id)
    {
    case PROP_VALUE:
      g_value_set_string (value, draft_property_get_value (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, draft_property_get_name (self));
      break;

    case PROP_TRANSLATABLE:
      g_value_set_boolean (value, draft_property_get_translatable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  DraftProperty *self = DRAFT_PROPERTY (object);

  switch (prop_id)
    {
    case PROP_VALUE:
      draft_property_set_value (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      draft_property_set_name (self, g_value_get_string (value));
      break;

    case PROP_TRANSLATABLE:
      draft_property_set_translatable (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_property_class_init (DraftPropertyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_property_finalize;
  object_class->get_property = draft_property_get_property;
  object_class->set_property = draft_property_set_property;

  item_class->load_from_xml = draft_property_load_from_xml;
  item_class->save_to_xml = draft_property_save_to_xml;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the attribute",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The value for the attribute",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_TRANSLATABLE] =
    g_param_spec_boolean("translatable",
                         "Translatable",
                         "If the attribute is translatable",
                         FALSE,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_BIND_FLAGS] =
    g_param_spec_flags ("bind-flags",
                        "Bind Flags",
                        "Bind Flags",
                        G_TYPE_BINDING_FLAGS,
                        0,
                        (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_BIND_PROPERTY] =
    g_param_spec_string ("bind-property",
                         "Bind Property",
                         "Bind Property",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_BIND_SOURCE] =
    g_param_spec_string ("bind-source",
                         "Bind Source",
                         "Bind Source",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_property_init (DraftProperty *self)
{
}

const char *
draft_property_get_name (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  return self->name;
}

const char *
draft_property_get_value (DraftProperty *self)
{
  const DraftPropertyInfo *info;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  if (self->value != NULL)
    return self->value->str;

  if ((info = _draft_property_get_info (self)))
    {
      if (G_VALUE_HOLDS_BOOLEAN (&info->default_value))
        {
          if (g_value_get_boolean (&info->default_value))
            return "true";
          else
            return "false";
        }
      else if (G_VALUE_HOLDS_STRING (&info->default_value))
        {
          return g_value_get_string (&info->default_value);
        }
    }

  return NULL;
}

gboolean
draft_property_get_translatable (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), FALSE);

  return self->translatable;
}

void
draft_property_set_name (DraftProperty *self,
                         const char    *name)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

void
draft_property_set_value (DraftProperty *self,
                          const char    *value)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  if (g_strcmp0 (value, draft_property_get_value (self)) == 0)
    return;

  if (self->value == NULL)
    {
      if (value == NULL)
        return;

      self->value = g_string_new (NULL);
    }

  if (g_strcmp0 (value, self->value->str) != 0)
    {
      g_string_truncate (self->value, 0);
      if (value == NULL)
        g_string_free (g_steal_pointer (&self->value), TRUE);
      else
        g_string_append (self->value, value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}

void
draft_property_set_translatable (DraftProperty *self,
                                 gboolean       translatable)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  translatable = !!translatable;

  if (translatable != self->translatable)
    {
      self->translatable = translatable;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TRANSLATABLE]);
    }
}

GBindingFlags
draft_property_get_bind_flags (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), 0);

  return self->bind_flags;
}

const char *
draft_property_get_bind_source (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  return self->bind_source;
}

const char *
draft_property_get_bind_property (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  return self->bind_property;
}

void
draft_property_set_bind_flags (DraftProperty *self,
                               GBindingFlags  bind_flags)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  if (bind_flags != self->bind_flags)
    {
      self->bind_flags = bind_flags;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BIND_FLAGS]);
    }
}

void
draft_property_set_bind_property (DraftProperty *self,
                                  const char    *bind_property)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  if (g_strcmp0 (bind_property, self->bind_property) != 0)
    {
      g_free (self->bind_property);
      self->bind_property = g_strdup (bind_property);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BIND_PROPERTY]);
    }
}

void
draft_property_set_bind_source (DraftProperty *self,
                                const char    *bind_source)
{
  g_return_if_fail (DRAFT_IS_PROPERTY (self));

  if (g_strcmp0 (bind_source, self->bind_source) != 0)
    {
      g_free (self->bind_source);
      self->bind_source = g_strdup (bind_source);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BIND_SOURCE]);
    }
}

const DraftPropertyInfo *
_draft_property_get_info (DraftProperty *self)
{
  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  for (DraftItem *item = DRAFT_ITEM (self);
       item != NULL;
       item = draft_item_get_parent (item))
    {
      if (DRAFT_TYPE_INFO_IS_CLASS (item->type_info))
        {
          const DraftClassInfo *info = (const DraftClassInfo *)item->type_info;

          if (info->properties != NULL)
            {
              for (guint i = 0; i < info->properties->len; i++)
                {
                  const DraftPropertyInfo *prop = g_ptr_array_index (info->properties, i);

                  if (g_strcmp0 (prop->name, self->name) == 0)
                    return prop;
                }
            }

          break;
        }
    }

  return NULL;
}

const char *
draft_property_get_defined_at (DraftProperty *self)
{
  const DraftPropertyInfo *info;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  if ((info = _draft_property_get_info (self)))
    return info->class_name;

  return NULL;
}

/**
 * draft_property_get_adjustment:
 * @self: a #DraftProperty
 *
 * Gets the adjustment for the property, or %NULL if one cannot
 * be created based on the information gathered on the property.
 *
 * Returns: (nullable) (transfer none): a #GtkAdjustment or %NULL
 */
GtkAdjustment *
draft_property_get_adjustment (DraftProperty *self)
{
  const DraftPropertyInfo *info;
  double upper = 0;
  double lower = 0;
  double default_value = 0;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  if (self->adjustment != NULL)
    return self->adjustment;

  if (!(info = _draft_property_get_info (self)))
    return NULL;

  if (g_strcmp0 (info->type_name, "gint") == 0)
    {
      upper = g_value_get_int (&info->max_value);
      lower = g_value_get_int (&info->min_value);
      default_value = g_value_get_int (&info->default_value);
      self->adjustment = gtk_adjustment_new (default_value, lower, upper, 1, 10, 10);
      /* TODO: Sync with property value */
    }

  if (self->adjustment)
    g_object_ref_sink (self->adjustment);

  return self->adjustment;
}

gboolean
draft_property_get_gvalue (DraftProperty *self,
                           GValue        *gvalue)
{
  const DraftPropertyInfo *info;
  const char *value;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), FALSE);
  g_return_val_if_fail (gvalue != NULL, FALSE);
  g_return_val_if_fail (!G_IS_VALUE (gvalue), FALSE);

  if (!(info = _draft_property_get_info (self)))
    return FALSE;

  if (!(value = draft_property_get_value (self)))
    {
      if (G_IS_VALUE (&info->default_value))
        {
          g_value_copy (&info->default_value, gvalue);
          return TRUE;
        }
    }

  if (g_strcmp0 (info->type_name, "gboolean") == 0)
    {
      gboolean b;

      if (_draft_boolean_from_string (value, &b, NULL))
        {
          g_value_init (gvalue, G_TYPE_BOOLEAN);
          g_value_set_boolean (gvalue, b);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gint") == 0)
    {
      int v;

      if (_draft_int_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_INT);
          g_value_set_int (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "guint") == 0)
    {
      guint v;

      if (_draft_uint_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_UINT);
          g_value_set_uint (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "glong") == 0)
    {
      gint64 v;

      if (_draft_int64_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_LONG);
          /* TODO: possible truncation */
          g_value_set_long (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gulong") == 0)
    {
      guint64 v;

      if (_draft_uint64_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_ULONG);
          g_value_set_ulong (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gint64") == 0)
    {
      gint64 v;

      if (_draft_int64_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_INT64);
          g_value_set_int64 (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "guint64") == 0)
    {
      guint64 v;

      if (_draft_uint64_from_string (value, &v))
        {
          g_value_init (gvalue, G_TYPE_UINT64);
          g_value_set_uint64 (gvalue, v);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gchar") == 0)
    {
      g_value_init (gvalue, G_TYPE_CHAR);
      g_value_set_schar (gvalue, value[0]);
      return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gchararray") == 0)
    {
      g_value_init (gvalue, G_TYPE_STRING);
      g_value_set_string (gvalue, value);
      return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gdouble") == 0)
    {
      double d = g_ascii_strtod (value, NULL);

      if (d != HUGE_VAL && errno != ERANGE)
        {
          g_value_init (gvalue, G_TYPE_DOUBLE);
          g_value_set_double (gvalue, d);
          return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gfloat") == 0)
    {
      double d = g_ascii_strtod (value, NULL);

      if (d != HUGE_VAL && errno != ERANGE)
        {
          g_value_init (gvalue, G_TYPE_FLOAT);
          g_value_set_float (gvalue, d);
          return TRUE;
        }
    }

  return FALSE;
}

gboolean
draft_property_is_default_value (DraftProperty *self)
{
  const DraftPropertyInfo *info;
  const char *value;
  DraftItem *child;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), FALSE);

  if (!(info = _draft_property_get_info (self)))
    return FALSE;

  if ((child = draft_item_get_first_child (DRAFT_ITEM (self))))
    {
      for (; child; child = draft_item_get_next_sibling (child))
        {
          if (DRAFT_IS_OBJECT (child))
            return FALSE;
        }
    }

  if (!(value = draft_property_get_value (self)))
    return TRUE;

  if (g_strcmp0 (info->type_name, "gboolean") == 0)
    {
      gboolean b;

      if (_draft_boolean_from_string (value, &b, NULL) &&
          G_VALUE_HOLDS_BOOLEAN (&info->default_value) &&
          g_value_get_boolean (&info->default_value) == b)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gint") == 0)
    {
      int v;

      if (_draft_int_from_string (value, &v) &&
          G_VALUE_HOLDS_INT (&info->default_value) &&
          g_value_get_int (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "guint") == 0)
    {
      guint v;

      if (_draft_uint_from_string (value, &v) &&
          G_VALUE_HOLDS_UINT (&info->default_value) &&
          g_value_get_uint (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "glong") == 0)
    {
      gint64 v;

      if (_draft_int64_from_string (value, &v) &&
          G_VALUE_HOLDS_LONG (&info->default_value) &&
          g_value_get_long (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gulong") == 0)
    {
      guint64 v;

      if (_draft_uint64_from_string (value, &v) &&
          G_VALUE_HOLDS_ULONG (&info->default_value) &&
          g_value_get_ulong (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gint64") == 0)
    {
      gint64 v;

      if (_draft_int64_from_string (value, &v) &&
          G_VALUE_HOLDS_INT64 (&info->default_value) &&
          g_value_get_int64 (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "guint64") == 0)
    {
      guint64 v;

      if (_draft_uint64_from_string (value, &v) &&
          G_VALUE_HOLDS_UINT64 (&info->default_value) &&
          g_value_get_uint64 (&info->default_value) == v)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gchar") == 0)
    {
      if (G_VALUE_HOLDS_CHAR (&info->default_value) &&
          g_value_get_schar (&info->default_value) == value[0])
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gchararray") == 0)
    {
      if (G_VALUE_HOLDS_STRING (&info->default_value) &&
          g_strcmp0 (value, g_value_get_string (&info->default_value)) == 0)
        return TRUE;
    }
  else if (g_strcmp0 (info->type_name, "gdouble") == 0)
    {
      double d = g_ascii_strtod (value, NULL);

      if (d != HUGE_VAL && errno != ERANGE)
        {
          if (G_VALUE_HOLDS_DOUBLE (&info->default_value) &&
              g_value_get_double (&info->default_value) == d)
            return TRUE;
        }
    }
  else if (g_strcmp0 (info->type_name, "gfloat") == 0)
    {
      double d = g_ascii_strtod (value, NULL);

      if (d != HUGE_VAL && errno != ERANGE)
        {
          if (G_VALUE_HOLDS_FLOAT (&info->default_value) &&
              g_value_get_float (&info->default_value) == (float)d)
            return TRUE;
        }
    }

  return FALSE;
}

gboolean
draft_property_get_value_boolean (DraftProperty *self,
                                  gboolean      *value)
{
  const char *str;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), FALSE);

  if ((str = draft_property_get_value (self)))
    return _draft_boolean_from_string (str, value, NULL);

  return FALSE;
}

GVariant *
_draft_property_to_variant (DraftProperty *self)
{
  const DraftPropertyInfo *info;
  g_auto(GValue) value = G_VALUE_INIT;
  GVariant *ret;

  g_return_val_if_fail (DRAFT_IS_PROPERTY (self), NULL);

  if (!(info = _draft_property_get_info (self)))
    return NULL;

  if (!draft_property_get_gvalue (self, &value))
    return NULL;

  if ((ret = _draft_value_encode (&value)))
    return g_variant_take_ref (ret);

  return NULL;
}
