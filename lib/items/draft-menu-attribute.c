/* draft-menu-attribute.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-menu-attribute.h"

struct _DraftMenuAttribute
{
  DraftItem parent_instance;
  char *name;
  char *value;
  char *value_type;
  gboolean translatable;
};

typedef struct _DraftMenuAttributeClass
{
  DraftItemClass parent_class;
} DraftMenuAttributeClass;

G_DEFINE_TYPE (DraftMenuAttribute, draft_menu_attribute, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_NAME,
  PROP_VALUE,
  PROP_VALUE_TYPE,
  PROP_TRANSLATABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftMenuAttribute *
draft_menu_attribute_new (void)
{
  return g_object_new (DRAFT_TYPE_MENU_ATTRIBUTE, NULL);
}

static void
draft_menu_attribute_start_element (GMarkupParseContext  *context,
                                    const char           *element_name,
                                    const char          **attribute_names,
                                    const char          **attribute_values,
                                    gpointer              user_data,
                                    GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_attribute_end_element (GMarkupParseContext  *context,
                                  const char           *element_name,
                                  gpointer              user_data,
                                  GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_attribute_text (GMarkupParseContext  *context,
                           const char           *text,
                           gsize                 text_len,
                           gpointer              user_data,
                           GError              **error)
{
  DraftXmlParser *state = user_data;
  DraftMenuAttribute *self = draft_xml_parser_current (state, DRAFT_TYPE_MENU_ATTRIBUTE);

  if (self->value == NULL)
    {
      self->value = g_strndup (text, text_len);
    }
  else
    {
      char *old = self->value;
      self->value = g_strdup_printf ("%s%s", old, text);
      g_free (old);
    }
}

static void
draft_menu_attribute_load_from_xml (DraftItem            *item,
                                    GMarkupParseContext  *context,
                                    DraftXmlParser       *state,
                                    const char           *element_name,
                                    const char          **attribute_names,
                                    const char          **attribute_values,
                                    GError              **error)
{
  static const GMarkupParser menu_attribute_parser = {
    draft_menu_attribute_start_element,
    draft_menu_attribute_end_element,
    draft_menu_attribute_text,
  };
  DraftMenuAttribute *self = DRAFT_MENU_ATTRIBUTE (item);
  const char *name = NULL;
  const char *type = NULL;
  gboolean translatable = FALSE;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING, "name", &name,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "type", &type,
                                    G_MARKUP_COLLECT_BOOLEAN | G_MARKUP_COLLECT_OPTIONAL, "translatable", &translatable,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_menu_attribute_set_name (self, name);
  draft_menu_attribute_set_translatable (self, translatable);
  draft_menu_attribute_set_value_type (self, type);

  g_markup_parse_context_push (context, &menu_attribute_parser, state);
}

static void
draft_menu_attribute_save_to_xml (DraftItem    *item,
                                  GString      *string,
                                  DraftProject *project,
                                  gboolean      for_display)
{
  DraftMenuAttribute *self = DRAFT_MENU_ATTRIBUTE (item);

  draft_xml_writer_begin_open_element (string, "attribute");
  draft_xml_writer_add_attributes_va (string,
                                      "name", self->name,
                                      "translatable", self->translatable ? "yes" : NULL,
                                      "type", self->value_type,
                                      NULL);

  if (self->value == NULL || self->value[0] == 0)
    {
      draft_xml_writer_end_open_element (string, FALSE);
      return;
    }

  draft_xml_writer_end_open_element (string, TRUE);
  draft_xml_writer_write_escaped (string, self->value);
  draft_xml_writer_close_element (string, "attribute");
}

static void
draft_menu_attribute_finalize (GObject *object)
{
  DraftMenuAttribute *self = (DraftMenuAttribute *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->value, g_free);

  G_OBJECT_CLASS (draft_menu_attribute_parent_class)->finalize (object);
}

static void
draft_menu_attribute_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  DraftMenuAttribute *self = DRAFT_MENU_ATTRIBUTE (object);

  switch (prop_id)
    {
    case PROP_VALUE:
      g_value_set_string (value, draft_menu_attribute_get_value (self));
      break;

    case PROP_VALUE_TYPE:
      g_value_set_string (value, draft_menu_attribute_get_value_type (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, draft_menu_attribute_get_name (self));
      break;

    case PROP_TRANSLATABLE:
      g_value_set_boolean (value, draft_menu_attribute_get_translatable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_attribute_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  DraftMenuAttribute *self = DRAFT_MENU_ATTRIBUTE (object);

  switch (prop_id)
    {
    case PROP_VALUE:
      draft_menu_attribute_set_value (self, g_value_get_string (value));
      break;

    case PROP_VALUE_TYPE:
      draft_menu_attribute_set_value_type (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      draft_menu_attribute_set_name (self, g_value_get_string (value));
      break;

    case PROP_TRANSLATABLE:
      draft_menu_attribute_set_translatable (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_attribute_class_init (DraftMenuAttributeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_menu_attribute_finalize;
  object_class->get_property = draft_menu_attribute_get_property;
  object_class->set_property = draft_menu_attribute_set_property;

  item_class->load_from_xml = draft_menu_attribute_load_from_xml;
  item_class->save_to_xml = draft_menu_attribute_save_to_xml;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the attribute",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The value for the attribute",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE_TYPE] =
    g_param_spec_string ("value-type",
                         "Value Type",
                         "The value-type for the attribute",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_TRANSLATABLE] =
    g_param_spec_boolean("translatable",
                         "Translatable",
                         "If the attribute is translatable",
                         FALSE,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_menu_attribute_init (DraftMenuAttribute *self)
{
}

const char *
draft_menu_attribute_get_name (DraftMenuAttribute *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self), NULL);

  return self->name;
}

const char *
draft_menu_attribute_get_value (DraftMenuAttribute *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self), NULL);

  return self->value;
}

const char *
draft_menu_attribute_get_value_type (DraftMenuAttribute *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self), NULL);

  return self->value_type;
}

gboolean
draft_menu_attribute_get_translatable (DraftMenuAttribute *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self), FALSE);

  return self->translatable;
}

void
draft_menu_attribute_set_name (DraftMenuAttribute *self,
                               const char         *name)
{
  g_return_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

void
draft_menu_attribute_set_value (DraftMenuAttribute *self,
                                const char         *value)
{
  g_return_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self));

  if (g_strcmp0 (value, self->value) != 0)
    {
      g_free (self->value);
      self->value = g_strdup (value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}

void
draft_menu_attribute_set_value_type (DraftMenuAttribute *self,
                                     const char         *value_type)
{
  g_return_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self));

  if (g_strcmp0 (value_type, self->value_type) != 0)
    {
      g_free (self->value_type);
      self->value_type = g_strdup (value_type);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE_TYPE]);
    }
}

void
draft_menu_attribute_set_translatable (DraftMenuAttribute *self,
                                       gboolean            translatable)
{
  g_return_if_fail (DRAFT_IS_MENU_ATTRIBUTE (self));

  translatable = !!translatable;

  if (translatable != self->translatable)
    {
      self->translatable = translatable;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TRANSLATABLE]);
    }
}
