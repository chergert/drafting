/* draft-item.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_ITEM    (draft_item_get_type ())
#define DRAFT_ITEM(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRAFT_TYPE_ITEM, DraftItem))
#define DRAFT_IS_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRAFT_TYPE_ITEM))

DRAFT_AVAILABLE_IN_ALL
GType          draft_item_get_type             (void) G_GNUC_CONST;
DRAFT_AVAILABLE_IN_ALL
const char    *draft_item_get_id               (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
void           draft_item_set_id               (DraftItem  *self,
                                                const char *id);
DRAFT_AVAILABLE_IN_ALL
char          *draft_item_get_display_name     (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftDocument *draft_item_get_document         (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftItem     *draft_item_get_parent           (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftItem     *draft_item_get_first_child      (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftItem     *draft_item_get_last_child       (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftItem     *draft_item_get_next_sibling     (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftItem     *draft_item_get_previous_sibling (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
DraftProject  *draft_item_get_project          (DraftItem  *self);
DRAFT_AVAILABLE_IN_ALL
int            draft_item_is_ancestor          (DraftItem  *self,
                                                DraftItem  *ancestor);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftItem, g_object_unref)

G_END_DECLS
