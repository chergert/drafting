/* draft-menu-link.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-menu-item.h"
#include "items/draft-menu-link.h"
#include "items/draft-menu-attribute.h"

struct _DraftMenuLink
{
  DraftItem parent_instance;
  char *name;
};

typedef struct _DraftMenuLinkClass
{
  DraftItemClass parent_class;
} DraftMenuLinkClass;

G_DEFINE_TYPE (DraftMenuLink, draft_menu_link, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftMenuLink *
draft_menu_link_new (const char *name)
{
  g_return_val_if_fail (name != NULL, NULL);

  return g_object_new (DRAFT_TYPE_MENU_LINK,
                       "name", name,
                       NULL);
}

static void
draft_menu_link_start_element (GMarkupParseContext  *context,
                               const char           *element_name,
                               const char          **attribute_names,
                               const char          **attribute_values,
                               gpointer              user_data,
                               GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "link") == 0)
    {
      const char *name = NULL;

      if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                        G_MARKUP_COLLECT_STRING, "name", &name,
                                        G_MARKUP_COLLECT_INVALID))
        return;

      DRAFT_XML_PARSER_PUSH (draft_menu_link_new (name));
    }
  else if (g_strcmp0 (element_name, "item") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_item_new ());
  else if (g_strcmp0 (element_name, "submenu") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_link_new ("submenu"));
  else if (g_strcmp0 (element_name, "section") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_link_new ("section"));
  else if (g_strcmp0 (element_name, "attribute") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_attribute_new ());
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_link_end_element (GMarkupParseContext  *context,
                             const char           *element_name,
                             gpointer              user_data,
                             GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "link") == 0 ||
      g_strcmp0 (element_name, "section") == 0 ||
      g_strcmp0 (element_name, "submenu") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU_LINK);
  else if (g_strcmp0 (element_name, "item") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU_ITEM);
  else if (g_strcmp0 (element_name, "attribute") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU_ATTRIBUTE);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_menu_link_load_from_xml (DraftItem            *item,
                               GMarkupParseContext  *context,
                               DraftXmlParser       *state,
                               const char           *element_name,
                               const char          **attribute_names,
                               const char          **attribute_values,
                               GError              **error)
{
  static const GMarkupParser menu_link_parser = {
    draft_menu_link_start_element,
    draft_menu_link_end_element,
  };

  g_markup_parse_context_push (context, &menu_link_parser, state);
}

static void
draft_menu_link_save_to_xml (DraftItem    *item,
                             GString      *string,
                             DraftProject *project,
                             gboolean      for_display)
{
  DraftMenuLink *self = DRAFT_MENU_LINK (item);
  DraftItem *child = draft_item_get_first_child (item);
  gboolean is_special;

  is_special = g_strcmp0 (self->name, "section") == 0 ||
               g_strcmp0 (self->name, "submenu") == 0;

  draft_xml_writer_begin_open_element (string, is_special ? self->name : "link");
  if (!is_special)
    draft_xml_writer_add_attribute (string, "name", self->name);
  draft_xml_writer_add_attribute (string, "id", draft_item_get_id (item));
  draft_xml_writer_end_open_element (string, child != NULL);

  if (child == NULL)
    return;

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, is_special ? self->name : "link");
}

static void
draft_menu_link_finalize (GObject *object)
{
  DraftMenuLink *self = (DraftMenuLink *)object;

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (draft_menu_link_parent_class)->finalize (object);
}

static void
draft_menu_link_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  DraftMenuLink *self = DRAFT_MENU_LINK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, draft_menu_link_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_link_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  DraftMenuLink *self = DRAFT_MENU_LINK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_menu_link_class_init (DraftMenuLinkClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_menu_link_finalize;
  object_class->get_property = draft_menu_link_get_property;
  object_class->set_property = draft_menu_link_set_property;

  item_class->load_from_xml = draft_menu_link_load_from_xml;
  item_class->save_to_xml = draft_menu_link_save_to_xml;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the link such as 'section' or 'submenu'",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_menu_link_init (DraftMenuLink *self)
{
}

const char *
draft_menu_link_get_name (DraftMenuLink *self)
{
  g_return_val_if_fail (DRAFT_IS_MENU_LINK (self), NULL);

  return self->name;
}
