/* draft-layout.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-layout.h"
#include "items/draft-property.h"
#include "items/draft-xml-item.h"

struct _DraftLayout
{
  DraftItem parent_instance;
};

typedef struct _DraftLayoutClass
{
  DraftItemClass parent_class;
} DraftLayoutClass;

G_DEFINE_TYPE (DraftLayout, draft_layout, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftLayout *
draft_layout_new (void)
{
  return g_object_new (DRAFT_TYPE_LAYOUT, NULL);
}

static void
draft_layout_start_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_PUSH (draft_property_new ());
  else
    DRAFT_XML_PARSER_PUSH (draft_xml_item_new ());
}

static void
draft_layout_end_element (GMarkupParseContext  *context,
                          const char           *element_name,
                          gpointer              user_data,
                          GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "property") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_PROPERTY);
  else
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_XML_ITEM);
}

static void
draft_layout_load_from_xml (DraftItem            *item,
                            GMarkupParseContext  *context,
                            DraftXmlParser       *state,
                            const char           *element_name,
                            const char          **attribute_names,
                            const char          **attribute_values,
                            GError              **error)
{
  static const GMarkupParser layout_parser = {
    draft_layout_start_element,
    draft_layout_end_element,
  };

  g_markup_parse_context_push (context, &layout_parser, state);
}

static void
draft_layout_save_to_xml (DraftItem    *item,
                          GString      *string,
                          DraftProject *project,
                          gboolean      for_display)
{
  DraftLayout *self = DRAFT_LAYOUT (item);
  DraftItem *child = draft_item_get_first_child (item);

  draft_xml_writer_begin_open_element (string, "layout");
  draft_xml_writer_end_open_element (string, TRUE);

  for (; child; child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, "layout");
}

static void
draft_layout_finalize (GObject *object)
{
  DraftLayout *self = (DraftLayout *)object;

  G_OBJECT_CLASS (draft_layout_parent_class)->finalize (object);
}

static void
draft_layout_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DraftLayout *self = DRAFT_LAYOUT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_layout_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DraftLayout *self = DRAFT_LAYOUT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_layout_class_init (DraftLayoutClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_layout_finalize;
  object_class->get_property = draft_layout_get_property;
  object_class->set_property = draft_layout_set_property;

  item_class->load_from_xml = draft_layout_load_from_xml;
  item_class->save_to_xml = draft_layout_save_to_xml;
}

static void
draft_layout_init (DraftLayout *self)
{
}
