/* draft-document.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-private.h"

#include "items/draft-dependency.h"
#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "items/draft-menu.h"
#include "items/draft-object.h"
#include "items/draft-xml-item.h"

typedef struct _DraftDocumentClass
{
  DraftItemClass parent_class;
} DraftDocumentClass;

struct _DraftDocument
{
  DraftItem     parent_instance;
  DraftProject *project;
  GFile        *file;
  char         *translation_domain;
  char         *title;
};

G_DEFINE_TYPE (DraftDocument, draft_document, DRAFT_TYPE_ITEM)

enum {
  PROP_0,
  PROP_FILE,
  PROP_TITLE,
  PROP_PROJECT,
  PROP_TRANSLATION_DOMAIN,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftDocument *
_draft_document_new (DraftProject *project)
{
  g_return_val_if_fail (!project || DRAFT_IS_PROJECT (project), NULL);

  return g_object_new (DRAFT_TYPE_DOCUMENT,
                       "project", project,
                       NULL);
}

static void
draft_document_start_element (GMarkupParseContext  *context,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              gpointer              user_data,
                              GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "menu") == 0)
    DRAFT_XML_PARSER_PUSH (draft_menu_new ());
  else if (g_strcmp0 (element_name, "template") == 0)
    DRAFT_XML_PARSER_PUSH (draft_object_new (TRUE));
  else if (g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_PUSH (draft_object_new (FALSE));
  else if (g_strcmp0 (element_name, "requires") == 0)
    DRAFT_XML_PARSER_PUSH (draft_dependency_new ());
  else
    DRAFT_XML_PARSER_PUSH (draft_xml_item_new ());
}

static void
draft_document_end_element (GMarkupParseContext  *context,
                            const char           *element_name,
                            gpointer              user_data,
                            GError              **error)
{
  DraftXmlParser *state = user_data;

  if (g_strcmp0 (element_name, "menu") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_MENU);
  else if (g_strcmp0 (element_name, "template") == 0 ||
           g_strcmp0 (element_name, "object") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_OBJECT);
  else if (g_strcmp0 (element_name, "requires") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_DEPENDENCY);
  else
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_XML_ITEM);
}

static void
draft_document_load_from_xml (DraftItem            *item,
                              GMarkupParseContext  *context,
                              DraftXmlParser       *state,
                              const char           *element_name,
                              const char          **attribute_names,
                              const char          **attribute_values,
                              GError              **error)
{
  static const GMarkupParser document_parser = {
    draft_document_start_element,
    draft_document_end_element,
  };
  DraftDocument *self = DRAFT_DOCUMENT (item);
  const char *domain = NULL;

  if (!g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                    G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "domain", &domain,
                                    G_MARKUP_COLLECT_INVALID))
    return;

  draft_document_set_translation_domain (self, domain);

  g_markup_parse_context_push (context, &document_parser, state);
}

static void
draft_document_save_to_xml (DraftItem    *item,
                            GString      *string,
                            DraftProject *project,
                            gboolean      for_display)
{
  DraftDocument *self = DRAFT_DOCUMENT (item);

  draft_xml_writer_begin_open_element (string, "interface");
  if (self->translation_domain)
    {
      /* Translations are available when previewing */
      if (!for_display)
        draft_xml_writer_add_attribute (string, "domain", self->translation_domain);
    }
  draft_xml_writer_end_open_element (string, TRUE);

  for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (item));
       child != NULL;
       child = draft_item_get_next_sibling (child))
    _draft_item_save_to_xml (child, string, project, for_display);

  draft_xml_writer_close_element (string, "interface");
}

static void
draft_document_add_child (DraftItem *item,
                          DraftItem *child)
{
  DraftDocument *self = (DraftDocument *)item;

  g_assert (DRAFT_IS_DOCUMENT (self));
  g_assert (DRAFT_IS_ITEM (child));

  if (_draft_project_can_display (child))
    {
      DRAFT_OBSERVERS_FOREACH (self->project, {
        _draft_observer_add_display (observer, child);
      });
    }
}

static void
draft_document_remove_child (DraftItem *item,
                             DraftItem *child)
{
  DraftDocument *self = (DraftDocument *)item;

  g_assert (DRAFT_IS_DOCUMENT (self));
  g_assert (DRAFT_IS_ITEM (child));

  if (_draft_project_can_display (child))
    {
      DRAFT_OBSERVERS_FOREACH (self->project, {
        _draft_observer_remove_display (observer, child);
      });
    }

  DRAFT_ITEM_CLASS (draft_document_parent_class)->remove_child (item, child);
}

static void
draft_document_finalize (GObject *object)
{
  DraftDocument *self = (DraftDocument *)object;

  g_clear_weak_pointer (&self->project);
  g_clear_object (&self->file);
  g_clear_pointer (&self->translation_domain, g_free);
  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (draft_document_parent_class)->finalize (object);
}

static void
draft_document_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  DraftDocument *self = DRAFT_DOCUMENT (object);

  switch (prop_id)
    {
    case PROP_FILE:
      g_value_set_object (value, draft_document_get_file (self));
      break;

    case PROP_PROJECT:
      g_value_set_object (value, draft_document_get_project (self));
      break;

    case PROP_TRANSLATION_DOMAIN:
      g_value_set_string (value, draft_document_get_translation_domain (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, draft_document_get_title (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_document_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  DraftDocument *self = DRAFT_DOCUMENT (object);

  switch (prop_id)
    {
    case PROP_FILE:
      _draft_document_set_file (self, g_value_get_object (value));
      break;

    case PROP_PROJECT:
      _draft_document_set_project (self, g_value_get_object (value));
      break;

    case PROP_TRANSLATION_DOMAIN:
      draft_document_set_translation_domain (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_document_class_init (DraftDocumentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  object_class->finalize = draft_document_finalize;
  object_class->get_property = draft_document_get_property;
  object_class->set_property = draft_document_set_property;

  item_class->load_from_xml = draft_document_load_from_xml;
  item_class->save_to_xml = draft_document_save_to_xml;
  item_class->add_child = draft_document_add_child;
  item_class->remove_child = draft_document_remove_child;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the document",
                         NULL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_FILE] =
    g_param_spec_object ("file",
                         "File",
                         "The file for the document in storage",
                         G_TYPE_FILE,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "The project owning the document",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));

  properties [PROP_TRANSLATION_DOMAIN] =
    g_param_spec_string ("translation-domain",
                         "Translation Domain",
                         "The translation domain for the interface",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_document_init (DraftDocument *self)
{
}

const char *
draft_document_get_title (DraftDocument *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (self), NULL);

  if (self->title == NULL && self->file != NULL)
    self->title = g_file_get_basename (self->file);

  return self->title;
}

/**
 * draft_document_get_file:
 * @self: a #DraftDocument
 *
 * Gets the file that is backing the document.
 *
 * Returns: (transfer none) (nullable): a #GFile or %NULL
 */
GFile *
draft_document_get_file (DraftDocument *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (self), NULL);

  return self->file;
}

void
_draft_document_set_file (DraftDocument *self,
                          GFile         *file)
{
  g_return_if_fail (DRAFT_IS_DOCUMENT (self));
  g_return_if_fail (!file || G_IS_FILE (file));

  if (g_set_object (&self->file, file))
    {
      g_clear_pointer (&self->title, g_free);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILE]);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
}

/**
 * draft_document_get_project:
 * @self: a #DraftDocument
 *
 * Gets the project owning the document.
 *
 * Returns: (transfer none) (nullable): A #DraftProject or %NULL
 */
DraftProject *
draft_document_get_project (DraftDocument *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (self), NULL);

  return self->project;
}

void
_draft_document_set_project (DraftDocument *self,
                             DraftProject  *project)
{
  g_return_if_fail (DRAFT_IS_DOCUMENT (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (g_set_weak_pointer (&self->project, project))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PROJECT]);
}

const char *
draft_document_get_translation_domain (DraftDocument *self)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (self), NULL);

  return self->translation_domain;
}

void
draft_document_set_translation_domain (DraftDocument *self,
                                       const char    *translation_domain)
{
  g_return_if_fail (DRAFT_IS_DOCUMENT (self));

  if (g_strcmp0 (translation_domain, self->translation_domain) != 0)
    {
      g_free (self->translation_domain);
      self->translation_domain = g_strdup (translation_domain);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TRANSLATION_DOMAIN]);
    }
}

static DraftItem *
find_by_id_recurse (DraftItem  *item,
                    const char *object_id)
{
  if (g_strcmp0 (object_id, _draft_item_get_internal_id (item)) == 0)
    return item;

  for (DraftItem *child = draft_item_get_first_child (item);
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      DraftItem *ret = find_by_id_recurse (child, object_id);

      if (ret != NULL)
        return ret;
    }

  return NULL;
}

DraftItem *
_draft_document_find_by_id  (DraftDocument *self,
                             const char    *object_id)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (self), NULL);
  g_return_val_if_fail (object_id != NULL, NULL);

  return find_by_id_recurse (DRAFT_ITEM (self), object_id);
}
