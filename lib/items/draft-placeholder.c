/* draft-placeholder.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-item-private.h"
#include "items/draft-placeholder.h"

struct _DraftPlaceholder
{
  DraftItem parent_instance;
};

typedef struct _DraftPlaceholderClass
{
  DraftItemClass parent_class;
} DraftPlaceholderClass;

G_DEFINE_TYPE (DraftPlaceholder, draft_placeholder, DRAFT_TYPE_ITEM)

DraftPlaceholder *
draft_placeholder_new (void)
{
  return g_object_new (DRAFT_TYPE_PLACEHOLDER, NULL);
}

static void
draft_placeholder_start_element (GMarkupParseContext  *context,
                                 const char           *element_name,
                                 const char          **attribute_names,
                                 const char          **attribute_values,
                                 gpointer              user_data,
                                 GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_placeholder_end_element (GMarkupParseContext  *context,
                               const char           *element_name,
                               gpointer              user_data,
                               GError              **error)
{
  DraftXmlParser *state = user_data;

  DRAFT_XML_PARSER_ERROR ();
}

static void
draft_placeholder_load_from_xml (DraftItem            *item,
                                 GMarkupParseContext  *context,
                                 DraftXmlParser       *state,
                                 const char           *element_name,
                                 const char          **attribute_names,
                                 const char          **attribute_values,
                                 GError              **error)
{
  static const GMarkupParser placeholder_parser = {
    draft_placeholder_start_element,
    draft_placeholder_end_element,
  };

  g_markup_parse_context_push (context, &placeholder_parser, state);
}

static void
draft_placeholder_save_to_xml (DraftItem    *item,
                               GString      *string,
                               DraftProject *project,
                               gboolean      for_display)
{
  DraftPlaceholder *self = DRAFT_PLACEHOLDER (item);

  draft_xml_writer_begin_open_element (string, "placeholder");
  draft_xml_writer_end_open_element (string, FALSE);
}

static void
draft_placeholder_class_init (DraftPlaceholderClass *klass)
{
  DraftItemClass *item_class = DRAFT_ITEM_CLASS (klass);

  item_class->load_from_xml = draft_placeholder_load_from_xml;
  item_class->save_to_xml = draft_placeholder_save_to_xml;
}

static void
draft_placeholder_init (DraftPlaceholder *self)
{
}
