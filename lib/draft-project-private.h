/* draft-project-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-ipc-service.h"
#include "draft-project.h"
#include "draft-types-private.h"

G_BEGIN_DECLS

struct _DraftProject
{
  GObject              parent_instance;

  DraftHistory        *history;
  DraftTransaction    *transaction;
  GPtrArray           *documents;
  GPtrArray           *observers;
  DraftItem           *selection;
  DraftTypeCache      *type_cache;
  DraftProjectContext *context;
};

void     _draft_project_select_item     (DraftProject  *self,
                                         DraftItem     *item);
void     _draft_project_clear_selection (DraftProject  *self);
void     _draft_project_add_observer    (DraftProject  *self,
                                         DraftObserver *observer);
void     _draft_project_remove_observer (DraftProject  *self,
                                         DraftObserver *observer);
gboolean _draft_project_can_display     (DraftItem     *item);

#define DRAFT_OBSERVERS_FOREACH(Project, Block)                                    \
  G_STMT_START {                                                                   \
    if ((Project) != NULL && (Project)->observers != NULL)                         \
      {                                                                            \
        for (guint i = 0; i < (Project)->observers->len; i++)                      \
          {                                                                        \
            DraftObserver *observer = g_ptr_array_index ((Project)->observers, i); \
            Block                                                                  \
          }                                                                        \
      }                                                                            \
  } G_STMT_END

G_END_DECLS
