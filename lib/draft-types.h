/* draft-types.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Toplevel project object */
typedef struct _DraftProject           DraftProject;
typedef struct _DraftProjectContext    DraftProjectContext;

/* Data Types */
typedef struct _DraftAccessibility     DraftAccessibility;
typedef struct _DraftBinding           DraftBinding;
typedef struct _DraftChild             DraftChild;
typedef struct _DraftDependency        DraftDependency;
typedef struct _DraftDocument          DraftDocument;
typedef struct _DraftExpression        DraftExpression;
typedef struct _DraftItem              DraftItem;
typedef struct _DraftLayout            DraftLayout;
typedef struct _DraftMenuAttribute     DraftMenuAttribute;
typedef struct _DraftMenu              DraftMenu;
typedef struct _DraftMenuItem          DraftMenuItem;
typedef struct _DraftMenuLink          DraftMenuLink;
typedef struct _DraftObject            DraftObject;
typedef struct _DraftPlaceholder       DraftPlaceholder;
typedef struct _DraftProperty          DraftProperty;
typedef struct _DraftSignal            DraftSignal;
typedef struct _DraftStyle             DraftStyle;
typedef struct _DraftStyleName         DraftStyleName;
typedef struct _DraftTreeColumn        DraftTreeColumn;
typedef struct _DraftTreeColumns       DraftTreeColumns;
typedef struct _DraftXmlItem           DraftXmlItem;

/* Undo/Redo Transactions */
typedef struct _DraftTransaction       DraftTransaction;

/* Panels for embedding in applications */
typedef struct _DraftProjectPanel      DraftProjectPanel;
typedef struct _DraftSignalsPanel      DraftSignalsPanel;

/* Canvas items */
typedef struct _DraftCanvas            DraftCanvas;

/* Enum for expression parsing */
typedef enum _DraftExpressionKind
{
  DRAFT_EXPRESSION_KIND_CONSTANT,
  DRAFT_EXPRESSION_KIND_LOOKUP,
  DRAFT_EXPRESSION_KIND_CLOSURE,
} DraftExpressionKind;

G_END_DECLS
