/* draft-version-macros.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "draft-version.h"

#ifndef _DRAFT_EXTERN
#define _DRAFT_EXTERN extern
#endif

#ifdef DRAFT_DISABLE_DEPRECATION_WARNINGS
# define DRAFT_DEPRECATED _DRAFT_EXTERN
# define DRAFT_DEPRECATED_FOR(f) _DRAFT_EXTERN
# define DRAFT_UNAVAILABLE(maj,min) _DRAFT_EXTERN
#else
# define DRAFT_DEPRECATED G_DEPRECATED _DRAFT_EXTERN
# define DRAFT_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _DRAFT_EXTERN
# define DRAFT_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _DRAFT_EXTERN
#endif

#define DRAFT_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (DRAFT_MINOR_VERSION == 99)
# define DRAFT_VERSION_CUR_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION + 1, 0))
#elif (DRAFT_MINOR_VERSION % 2)
# define DRAFT_VERSION_CUR_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION, DRAFT_MINOR_VERSION + 1))
#else
# define DRAFT_VERSION_CUR_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION, DRAFT_MINOR_VERSION))
#endif

#if (DRAFT_MINOR_VERSION == 99)
# define DRAFT_VERSION_PREV_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION + 1, 0))
#elif (DRAFT_MINOR_VERSION % 2)
# define DRAFT_VERSION_PREV_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION, DRAFT_MINOR_VERSION - 1))
#else
# define DRAFT_VERSION_PREV_STABLE (G_ENCODE_VERSION (DRAFT_MAJOR_VERSION, DRAFT_MINOR_VERSION - 2))
#endif

/**
 * DRAFT_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the drafting.h header.
 *
 * The definition should be one of the predefined Drafting version
 * macros: %DRAFT_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Drafting API to use.
 *
 * If a function has been deprecated in a newer version of Drafting,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef DRAFT_VERSION_MIN_REQUIRED
# define DRAFT_VERSION_MIN_REQUIRED (DRAFT_VERSION_CUR_STABLE)
#endif

/**
 * DRAFT_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the drafting.h header.

 * The definition should be one of the predefined Drafting version
 * macros: %DRAFT_VERSION_1_0, %DRAFT_VERSION_1_2,...
 *
 * This macro defines the upper bound for the Drafting API to use.
 *
 * If a function has been introduced in a newer version of Drafting,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef DRAFT_VERSION_MAX_ALLOWED
# if DRAFT_VERSION_MIN_REQUIRED > DRAFT_VERSION_PREV_STABLE
#  define DRAFT_VERSION_MAX_ALLOWED (DRAFT_VERSION_MIN_REQUIRED)
# else
#  define DRAFT_VERSION_MAX_ALLOWED (DRAFT_VERSION_CUR_STABLE)
# endif
#endif

#if DRAFT_VERSION_MAX_ALLOWED < DRAFT_VERSION_MIN_REQUIRED
#error "DRAFT_VERSION_MAX_ALLOWED must be >= DRAFT_VERSION_MIN_REQUIRED"
#endif
#if DRAFT_VERSION_MIN_REQUIRED < DRAFT_VERSION_1_0
#error "DRAFT_VERSION_MIN_REQUIRED must be >= DRAFT_VERSION_1_0"
#endif

#define DRAFT_AVAILABLE_IN_ALL                  _DRAFT_EXTERN

#if DRAFT_VERSION_MIN_REQUIRED >= DRAFT_VERSION_1_0
# define DRAFT_DEPRECATED_IN_1_0                DRAFT_DEPRECATED
# define DRAFT_DEPRECATED_IN_1_0_FOR(f)         DRAFT_DEPRECATED_FOR(f)
#else
# define DRAFT_DEPRECATED_IN_1_0                _DRAFT_EXTERN
# define DRAFT_DEPRECATED_IN_1_0_FOR(f)         _DRAFT_EXTERN
#endif

#if DRAFT_VERSION_MAX_ALLOWED < DRAFT_VERSION_1_0
# define DRAFT_AVAILABLE_IN_1_0                 DRAFT_UNAVAILABLE(1, 0)
#else
# define DRAFT_AVAILABLE_IN_1_0                 _DRAFT_EXTERN
#endif

