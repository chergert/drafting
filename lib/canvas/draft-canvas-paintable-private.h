/* draft-canvas-paintable-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>

#include "draft-types-private.h"
#include "draft-ipc-paintable.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_CANVAS_PAINTABLE (draft_canvas_paintable_get_type())

G_DECLARE_FINAL_TYPE (DraftCanvasPaintable, draft_canvas_paintable, DRAFT, CANVAS_PAINTABLE, GObject)

DraftCanvasPaintable *draft_canvas_paintable_new            (DraftIpcPaintable    *paintable);
char                 *draft_canvas_paintable_pick           (DraftCanvasPaintable *self,
                                                             double                x,
                                                             double                y,
                                                             GdkRectangle         *area);
double                draft_canvas_paintable_get_zoom       (DraftCanvasPaintable *self);
void                  draft_canvas_paintable_set_zoom       (DraftCanvasPaintable *self,
                                                             double                zoom);
void                  draft_canvas_paintable_get_allocation (DraftCanvasPaintable *self,
                                                             const char           *object_id,
                                                             GtkAllocation        *allocation);

G_END_DECLS
