/* draft-canvas-handle.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "canvas/draft-canvas-handle-private.h"
#include "canvas/draft-canvas-enums.h"

struct _DraftCanvasHandle
{
  GtkWidget parent_instance;
  DraftCanvasHandlePosition position;
};

G_DEFINE_TYPE (DraftCanvasHandle, draft_canvas_handle, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_POSITION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static const char *css_classes[] = {
  "top-start",
  "top-middle",
  "top-end",
  "middle-start",
  "middle-end",
  "bottom-start",
  "bottom-middle",
  "bottom-end",
  NULL
};
static const char *cursor_names[] = {
  "nw-resize",
  "n-resize",
  "ne-resize",
  "w-resize",
  "e-resize",
  "sw-resize",
  "s-resize",
  "se-resize",
  NULL
};

static void
draft_canvas_handle_drag_begin_cb (DraftCanvasHandle *self,
                                   double             x,
                                   double             y,
                                   GtkGesture        *drag)
{
  g_assert (DRAFT_IS_CANVAS_HANDLE (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  gtk_gesture_set_sequence_state (drag,
                                  gtk_gesture_get_last_updated_sequence (drag),
                                  GTK_EVENT_SEQUENCE_CLAIMED);
}

static void
draft_canvas_handle_drag_update_cb (DraftCanvasHandle *self,
                                    double             x,
                                    double             y,
                                    GtkGesture        *drag)
{
  g_assert (DRAFT_IS_CANVAS_HANDLE (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  /* TODO: Update widget sizing */
}

static void
draft_canvas_handle_drag_end_cb (DraftCanvasHandle *self,
                                 double             x,
                                 double             y,
                                 GtkGesture        *drag)
{
  g_assert (DRAFT_IS_CANVAS_HANDLE (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  /* TODO: Complete drag */
}

static void
draft_canvas_handle_dispose (GObject *object)
{
  G_OBJECT_CLASS (draft_canvas_handle_parent_class)->dispose (object);
}

static void
draft_canvas_handle_constructed (GObject *object)
{
  DraftCanvasHandle *self = (DraftCanvasHandle *)object;

  g_assert (DRAFT_IS_CANVAS_HANDLE (self));

  G_OBJECT_CLASS (draft_canvas_handle_parent_class)->constructed (object);

  gtk_widget_add_css_class (GTK_WIDGET (self), css_classes[self->position]);
  gtk_widget_set_cursor_from_name (GTK_WIDGET (self), cursor_names[self->position]);
}

static void
draft_canvas_handle_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftCanvasHandle *self = DRAFT_CANVAS_HANDLE (object);

  switch (prop_id)
    {
    case PROP_POSITION:
      g_value_set_enum (value, self->position);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_handle_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DraftCanvasHandle *self = DRAFT_CANVAS_HANDLE (object);

  switch (prop_id)
    {
    case PROP_POSITION:
      self->position = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_handle_class_init (DraftCanvasHandleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = draft_canvas_handle_constructed;
  object_class->dispose = draft_canvas_handle_dispose;
  object_class->get_property = draft_canvas_handle_get_property;
  object_class->set_property = draft_canvas_handle_set_property;

  properties [PROP_POSITION] =
    g_param_spec_enum ("position",
                       "Position",
                       "Position",
                       DRAFT_TYPE_CANVAS_HANDLE_POSITION,
                       DRAFT_CANVAS_HANDLE_TOP_START,
                       (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "draftcanvashandle");
}

static void
draft_canvas_handle_init (DraftCanvasHandle *self)
{
  GtkEventController *controller;

  controller = GTK_EVENT_CONTROLLER (gtk_gesture_drag_new ());
  g_signal_connect_object (controller,
                           "drag-begin",
                           G_CALLBACK (draft_canvas_handle_drag_begin_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (controller,
                           "drag-update",
                           G_CALLBACK (draft_canvas_handle_drag_update_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (controller,
                           "drag-end",
                           G_CALLBACK (draft_canvas_handle_drag_end_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_event_controller_set_propagation_phase (controller, GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);
}
