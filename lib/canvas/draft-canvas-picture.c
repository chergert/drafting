/* draft-canvas-picture.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "canvas/draft-canvas-paintable-private.h"
#include "canvas/draft-canvas-picture-private.h"
#include "util/draft-signal-group-private.h"

struct _DraftCanvasPicture
{
  GtkWidget         parent_instance;
  GdkPaintable     *paintable;
  DraftSignalGroup *signals;
  int               intrinsic_width;
  int               intrinsic_height;
};

G_DEFINE_TYPE (DraftCanvasPicture, draft_canvas_picture, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_PAINTABLE,
  N_PROPS
};

enum {
  ITEM_SELECTED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

GtkWidget *
draft_canvas_picture_new (void)
{
  return g_object_new (DRAFT_TYPE_CANVAS_PICTURE, NULL);
}

static void
draft_canvas_picture_invalidate_contents_cb (DraftCanvasPicture *self,
                                             GdkPaintable       *paintable)
{
  g_assert (DRAFT_IS_CANVAS_PICTURE (self));
  g_assert (GDK_IS_PAINTABLE (paintable));

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
draft_canvas_picture_invalidate_size_cb (DraftCanvasPicture *self,
                                         GdkPaintable       *paintable)
{
  g_assert (DRAFT_IS_CANVAS_PICTURE (self));
  g_assert (GDK_IS_PAINTABLE (paintable));

  self->intrinsic_width = -1;
  self->intrinsic_height = -1;

  gtk_widget_queue_resize (GTK_WIDGET (self));
}

static void
draft_canvas_picture_measure (GtkWidget      *widget,
                              GtkOrientation  orientation,
                              int             for_size,
                              int            *minimum,
                              int            *natural,
                              int            *minimum_baseline,
                              int            *natural_baseline)
{
  DraftCanvasPicture *self = (DraftCanvasPicture *)widget;

  g_assert (DRAFT_IS_CANVAS_PICTURE (self));
  g_assert (minimum != NULL);
  g_assert (natural != NULL);

  *minimum_baseline = -1;
  *natural_baseline = -1;

  if (self->paintable == NULL)
    {
      *minimum = 0;
      *natural = 0;
      return;
    }

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
      if (self->intrinsic_width < 0)
        self->intrinsic_width = gdk_paintable_get_intrinsic_width (self->paintable);
      *minimum = self->intrinsic_width;
      *natural = self->intrinsic_width;
    }
  else
    {
      if (self->intrinsic_height < 0)
        self->intrinsic_height = gdk_paintable_get_intrinsic_height (self->paintable);
      *minimum = self->intrinsic_height;
      *natural = self->intrinsic_height;
    }
}

static void
draft_canvas_picture_snapshot (GtkWidget   *widget,
                               GtkSnapshot *snapshot)
{
  DraftCanvasPicture *self = (DraftCanvasPicture *)widget;
  GtkAllocation alloc;

  g_assert (DRAFT_IS_CANVAS_PICTURE (self));
  g_assert (GDK_IS_SNAPSHOT (snapshot));

  if (self->paintable == NULL)
    return;

  gtk_widget_get_allocation (widget, &alloc);
  gdk_paintable_snapshot (self->paintable, snapshot, alloc.width, alloc.height);
}

static void
draft_canvas_picture_pressed_cb (DraftCanvasPicture *self,
                                 int                 n_presses,
                                 double              x,
                                 double              y,
                                 GtkGesture         *click)
{
  g_assert (DRAFT_IS_CANVAS_PICTURE (self));
  g_assert (GTK_IS_GESTURE_CLICK (click));

  if (draft_canvas_picture_select_at_coords (self, x, y))
    gtk_gesture_set_sequence_state (click,
                                    gtk_gesture_get_last_updated_sequence (click),
                                    GTK_EVENT_SEQUENCE_CLAIMED);
}

static void
draft_canvas_picture_finalize (GObject *object)
{
  DraftCanvasPicture *self = (DraftCanvasPicture *)object;

  g_clear_object (&self->signals);
  g_clear_object (&self->paintable);

  G_OBJECT_CLASS (draft_canvas_picture_parent_class)->finalize (object);
}

static void
draft_canvas_picture_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  DraftCanvasPicture *self = DRAFT_CANVAS_PICTURE (object);

  switch (prop_id)
    {
    case PROP_PAINTABLE:
      g_value_set_object (value, draft_canvas_picture_get_paintable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_picture_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  DraftCanvasPicture *self = DRAFT_CANVAS_PICTURE (object);

  switch (prop_id)
    {
    case PROP_PAINTABLE:
      draft_canvas_picture_set_paintable (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_picture_class_init (DraftCanvasPictureClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = draft_canvas_picture_finalize;
  object_class->get_property = draft_canvas_picture_get_property;
  object_class->set_property = draft_canvas_picture_set_property;

  widget_class->measure = draft_canvas_picture_measure;
  widget_class->snapshot = draft_canvas_picture_snapshot;

  properties [PROP_PAINTABLE] =
    g_param_spec_object ("paintable",
                         "Paintable",
                         "The paintable to draw",
                         GDK_TYPE_PAINTABLE,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * DraftCanvasPicture:item-selected:
   * @self: a #DraftCanvasPicture
   * @item: the id of the item that was selected
   * @area: a #GdkRectangle containing the widget area
   *
   * This signal is emitted when an item is selected within the
   * picture. The @item corresponds to an internal-id which can
   * be retrieved with _draft_item_get_internal_id().
   */
  signals [ITEM_SELECTED] =
    g_signal_new ("item-selected",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  GDK_TYPE_RECTANGLE | G_SIGNAL_TYPE_STATIC_SCOPE);

  gtk_widget_class_set_css_name (widget_class, "draftcanvaspicture");
}

static void
draft_canvas_picture_init (DraftCanvasPicture *self)
{
  GtkGesture *gesture;

  self->intrinsic_width = -1;
  self->intrinsic_height = -1;

  self->signals = draft_signal_group_new (GDK_TYPE_PAINTABLE);

  draft_signal_group_connect_object (self->signals,
                                     "invalidate-size",
                                     G_CALLBACK (draft_canvas_picture_invalidate_size_cb),
                                     self,
                                     G_CONNECT_SWAPPED);

  draft_signal_group_connect_object (self->signals,
                                     "invalidate-contents",
                                     G_CALLBACK (draft_canvas_picture_invalidate_contents_cb),
                                     self,
                                     G_CONNECT_SWAPPED);

  gesture = gtk_gesture_click_new ();
  g_signal_connect_object (gesture,
                           "pressed",
                           G_CALLBACK (draft_canvas_picture_pressed_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (gesture));
}

GdkPaintable *
draft_canvas_picture_get_paintable (DraftCanvasPicture *self)
{
  return self->paintable;
}

void
draft_canvas_picture_set_paintable (DraftCanvasPicture *self,
                                    GdkPaintable       *paintable)
{
  g_return_if_fail (DRAFT_IS_CANVAS_PICTURE (self));
  g_return_if_fail (!paintable || GDK_IS_PAINTABLE (paintable));

  if (g_set_object (&self->paintable, paintable))
    {
      draft_signal_group_set_target (self->signals, paintable);
      self->intrinsic_width = -1;
      self->intrinsic_height = -1;
      gtk_widget_queue_resize (GTK_WIDGET (self));
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAINTABLE]);
    }
}

gboolean
draft_canvas_picture_select_at_coords (DraftCanvasPicture *self,
                                       double              x,
                                       double              y)
{
  g_autofree char *object_id = NULL;
  GtkAllocation area;

  g_return_val_if_fail (DRAFT_IS_CANVAS_PICTURE (self), FALSE);

  if (self->paintable == NULL)
    return FALSE;

  if (!(object_id = draft_canvas_paintable_pick (DRAFT_CANVAS_PAINTABLE (self->paintable), x, y, &area)))
    return FALSE;

  g_debug ("Picked %s\n", object_id);
  g_signal_emit (self, signals [ITEM_SELECTED], 0, object_id, &area);

  return TRUE;
}
