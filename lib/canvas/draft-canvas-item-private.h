/* draft-canvas-item-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_CANVAS_ITEM (draft_canvas_item_get_type())

G_DECLARE_FINAL_TYPE (DraftCanvasItem, draft_canvas_item, DRAFT, CANVAS_ITEM, GtkWidget)

GtkWidget    *draft_canvas_item_new                  (DraftCanvasPaintable *paintable,
                                                      DraftItem            *item);
DraftItem    *draft_canvas_item_get_item             (DraftCanvasItem      *canvas_item);
GtkWidget    *draft_canvas_item_get_paintable_widget (DraftCanvasItem      *self);
GdkPaintable *draft_canvas_item_get_paintable        (DraftCanvasItem      *self);
void          draft_canvas_item_set_paintable        (DraftCanvasItem      *self,
                                                      DraftCanvasPaintable *paintable);
gboolean      draft_canvas_item_select_at_coords     (DraftCanvasItem      *self,
                                                      double                x,
                                                      double                y);
void          draft_canvas_item_select_item          (DraftCanvasItem      *self,
                                                      DraftItem            *item);
void          draft_canvas_item_resize               (DraftCanvasItem      *self,
                                                      int                   width,
                                                      int                   height);

G_END_DECLS
