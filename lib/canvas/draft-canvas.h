/* draft-canvas.h
 *
 * Copyright 2021 Christian Hergert <unknown@domain.org>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_CANVAS (draft_canvas_get_type())

DRAFT_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DraftCanvas, draft_canvas, DRAFT, CANVAS, GtkWidget)

DRAFT_AVAILABLE_IN_ALL
GtkWidget     *draft_canvas_new          (void);
DRAFT_AVAILABLE_IN_ALL
DraftDocument *draft_canvas_get_document (DraftCanvas   *self);
DRAFT_AVAILABLE_IN_ALL
void           draft_canvas_set_document (DraftCanvas   *self,
                                          DraftDocument *document);

G_END_DECLS
