/* draft-canvas-paintable.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-canvas-paintable-private.h"

struct _DraftCanvasPaintable
{
  GObject            parent_instance;

  DraftIpcPaintable *paintable;
  GskRenderNode     *snapshot;

  double             zoom;

  int                cached_intrinsic_width;
  int                cached_intrinsic_height;
  double             cached_intrinsic_aspect_ratio;

  guint              snapshot_invalid : 1;
};

static void paintable_iface_init (GdkPaintableInterface *iface);

G_DEFINE_TYPE_WITH_CODE (DraftCanvasPaintable, draft_canvas_paintable, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, paintable_iface_init))

static void
draft_canvas_paintable_invalidate_contents (DraftCanvasPaintable *self,
                                            DraftIpcPaintable    *paintable)
{
  g_assert (DRAFT_IS_CANVAS_PAINTABLE (self));
  g_assert (DRAFT_IPC_IS_PAINTABLE (paintable));

  /* Ignore invalidations if we are in a resize that might temporarily
   * clear the sizing (if we had to hide the widget to resize).
   */
  if (self->cached_intrinsic_width == 0 && self->cached_intrinsic_height == 0)
    return;

  self->snapshot_invalid = TRUE;

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
}

static void
draft_canvas_paintable_invalidate_size (DraftCanvasPaintable *self,
                                        DraftIpcPaintable    *paintable)
{
  g_assert (DRAFT_IS_CANVAS_PAINTABLE (self));
  g_assert (DRAFT_IPC_IS_PAINTABLE (paintable));

  /* We might temporarily have to hide a window to resize it, and that could
   * result in a zero size. Instead of invalidating things, just ignore the
   * transient issue as we'll render our old node data until the proper resize
   * completes.
   */
  if (self->paintable != NULL)
    {
      int width = 0, height = 0;
      double aspect_ratio = 0.0;

      draft_ipc_paintable_call_get_intrinsic_height_sync (self->paintable, &height, NULL, NULL);
      draft_ipc_paintable_call_get_intrinsic_width_sync (self->paintable, &width, NULL, NULL);
      draft_ipc_paintable_call_get_intrinsic_aspect_ratio_sync (self->paintable, &aspect_ratio, NULL, NULL);

      if (width == 0 && height == 0)
        return;

      self->cached_intrinsic_width = width;
      self->cached_intrinsic_height = height;
      self->cached_intrinsic_aspect_ratio = aspect_ratio;
    }

  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));
}

DraftCanvasPaintable *
draft_canvas_paintable_new (DraftIpcPaintable *paintable)
{
  DraftCanvasPaintable *self;

  g_return_val_if_fail (DRAFT_IPC_IS_PAINTABLE (paintable), NULL);

  self = g_object_new (DRAFT_TYPE_CANVAS_PAINTABLE, NULL);
  self->paintable = g_object_ref (paintable);

  g_signal_connect_object (self->paintable,
                           "invalidate-contents",
                           G_CALLBACK (draft_canvas_paintable_invalidate_contents),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->paintable,
                           "invalidate-size",
                           G_CALLBACK (draft_canvas_paintable_invalidate_size),
                           self,
                           G_CONNECT_SWAPPED);

  draft_canvas_paintable_invalidate_size (self, paintable);

  return self;
}

static void
draft_canvas_paintable_dispose (GObject *object)
{
  DraftCanvasPaintable *self = (DraftCanvasPaintable *)object;

  g_clear_object (&self->paintable);
  g_clear_pointer (&self->snapshot, gsk_render_node_unref);

  G_OBJECT_CLASS (draft_canvas_paintable_parent_class)->dispose (object);
}

static void
draft_canvas_paintable_class_init (DraftCanvasPaintableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_canvas_paintable_dispose;
}

static void
draft_canvas_paintable_init (DraftCanvasPaintable *self)
{
  self->zoom = 1.0;
}

static void
draft_canvas_paintable_snapshot (GdkPaintable *paintable,
                                 GtkSnapshot  *snapshot,
                                 double        width,
                                 double        height)
{
  DraftCanvasPaintable *self = (DraftCanvasPaintable *)paintable;
  g_autofree char *serialized = NULL;
  g_autoptr(GskRenderNode) root = NULL;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GError) error = NULL;
  gsize length;

  g_assert (GDK_IS_PAINTABLE (paintable));
  g_assert (GTK_IS_SNAPSHOT (snapshot));

  if (self->snapshot == NULL || self->snapshot_invalid)
    {
      if (!draft_ipc_paintable_call_snapshot_sync (self->paintable, width, height, &serialized, NULL, &error))
        {
          g_warning ("%s", error->message);
          return;
        }

      length = strlen (serialized);
      bytes = g_bytes_new_take (g_steal_pointer (&serialized), length);

      if (!(root = gsk_render_node_deserialize (bytes, NULL, &error)))
        {
          g_warning ("%s", error->message);
          return;
        }

      g_clear_pointer (&self->snapshot, gsk_render_node_unref);
      self->snapshot = gsk_render_node_ref (root);
      self->snapshot_invalid = FALSE;
    }

  gtk_snapshot_append_node (snapshot, self->snapshot);
}

static int
draft_canvas_paintable_get_intrinsic_width (GdkPaintable *paintable)
{
  DraftCanvasPaintable *self = (DraftCanvasPaintable *)paintable;

  g_assert (DRAFT_IS_CANVAS_PAINTABLE (self));

  return self->cached_intrinsic_width * self->zoom;
}

static int
draft_canvas_paintable_get_intrinsic_height (GdkPaintable *paintable)
{
  DraftCanvasPaintable *self = (DraftCanvasPaintable *)paintable;

  g_assert (DRAFT_IS_CANVAS_PAINTABLE (self));

  return self->cached_intrinsic_height * self->zoom;
}

static double
draft_canvas_paintable_get_intrinsic_aspect_ratio (GdkPaintable *paintable)
{
  DraftCanvasPaintable *self = (DraftCanvasPaintable *)paintable;

  g_assert (DRAFT_IS_CANVAS_PAINTABLE (self));

  return self->cached_intrinsic_aspect_ratio;
}

static void
paintable_iface_init (GdkPaintableInterface *iface)
{
  iface->snapshot = draft_canvas_paintable_snapshot;
  iface->get_intrinsic_width = draft_canvas_paintable_get_intrinsic_width;
  iface->get_intrinsic_height = draft_canvas_paintable_get_intrinsic_height;
  iface->get_intrinsic_aspect_ratio = draft_canvas_paintable_get_intrinsic_aspect_ratio;
}

char *
draft_canvas_paintable_pick (DraftCanvasPaintable *self,
                             double                x,
                             double                y,
                             GdkRectangle         *area)
{
  g_autofree char *object_id = NULL;
  g_autoptr(GVariant) varea = NULL;

  g_return_val_if_fail (DRAFT_IS_CANVAS_PAINTABLE (self), NULL);

  x /= self->zoom;
  y /= self->zoom;

  if (self->paintable != NULL &&
      draft_ipc_paintable_call_pick_sync (self->paintable, x, y, &object_id, &varea, NULL, NULL))
    {
      if (area != NULL)
        {
          g_variant_get (varea, "(iiii)", &area->x, &area->y, &area->width, &area->height);

          area->x *= self->zoom;
          area->y *= self->zoom;
          area->width *= self->zoom;
          area->height *= self->zoom;
        }

      return g_steal_pointer (&object_id);
    }

  return NULL;
}

void
draft_canvas_paintable_set_zoom (DraftCanvasPaintable *self,
                                 double                zoom)
{
  g_return_if_fail (DRAFT_IS_CANVAS_PAINTABLE (self));

  zoom = CLAMP (zoom, 0.5, 5.0);

  if (zoom != self->zoom)
    {
      self->zoom = zoom;
      gdk_paintable_invalidate_size (GDK_PAINTABLE (self));
    }
}

double
draft_canvas_paintable_get_zoom (DraftCanvasPaintable *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_PAINTABLE (self), 1.0);

  return self->zoom;
}

void
draft_canvas_paintable_get_allocation (DraftCanvasPaintable *self,
                                       const char           *object_id,
                                       GtkAllocation        *allocation)
{
  g_autoptr(GVariant) area = NULL;

  g_return_if_fail (DRAFT_IS_CANVAS_PAINTABLE (self));
  g_return_if_fail (object_id != NULL);
  g_return_if_fail (allocation != NULL);

  if (draft_ipc_paintable_call_get_allocation_sync (self->paintable, object_id, &area, NULL, NULL))
    g_variant_get (area,
                   "(iiii)",
                   &allocation->x,
                   &allocation->y,
                   &allocation->width,
                   &allocation->height);
  else
    memset (allocation, 0, sizeof *allocation);
}
