/* draft-canvas-selection-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_CANVAS_SELECTION (draft_canvas_selection_get_type())

G_DECLARE_FINAL_TYPE (DraftCanvasSelection, draft_canvas_selection, DRAFT, CANVAS_SELECTION, GtkWidget)

typedef enum
{

  DRAFT_CANVAS_SELECTION_ITEM,
  DRAFT_CANVAS_SELECTION_RESIZE,
} DraftCanvasSelectionMode;

DraftCanvasSelection *draft_canvas_selection_new        (void);
void                  draft_canvas_selection_set_mode   (DraftCanvasSelection     *self,
                                                         DraftCanvasSelectionMode  mode);
void                  draft_canvas_selection_get_offset (DraftCanvasSelection     *self,
                                                         int                      *x_offset,
                                                         int                      *y_offset);
DraftItem            *draft_canvas_selection_get_item   (DraftCanvasSelection     *self);
void                  draft_canvas_selection_set_item   (DraftCanvasSelection     *self,
                                                         DraftItem                *item);
void                  draft_canvas_selection_set_size   (DraftCanvasSelection     *self,
                                                         int                       width,
                                                         int                       height);

G_END_DECLS
