/* draft-canvas.c
 *
 * Copyright 2021 Christian Hergert <unknown@domain.org>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-private.h"

#include "canvas/draft-canvas-private.h"
#include "canvas/draft-canvas-item-private.h"
#include "canvas/draft-canvas-paintable-private.h"
#include "canvas/draft-canvas-observer-private.h"
#include "canvas/draft-canvas-selection-private.h"
#include "introspection/draft-type-info-private.h"
#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "items/draft-menu.h"
#include "items/draft-object.h"
#include "undo/draft-transaction.h"
#include "util/draft-signal-group-private.h"

struct _DraftCanvas
{
  GtkWidget             parent_instance;

  GtkOverlay           *overlay;
  GtkScrolledWindow    *scroller;
  GtkBox               *box;
  GtkBox               *widget_views;
  GtkBox               *object_views;
  GtkBox               *menu_views;
  DraftCanvasSelection *selection;

  DraftDocument        *document;
  DraftProject         *project;
  DraftCanvasObserver  *observer;
  DraftCanvasSelection *resizer;

  GHashTable           *resizes;

  struct {
    GdkRectangle        area;
    DraftCanvasItem    *item;
  } selection_info;

  struct {
    DraftCanvasItem    *item;
    double              start_x;
    double              start_y;
    double              offset_x;
    double              offset_y;
    guint               horiz : 1;
    guint               vert : 1;
  } resize_info;
};

typedef struct
{
  int width;
  int height;
  guint waiting_for_reply : 1;
  guint dirty : 1;
} Resize;

static void draft_canvas_set_project (DraftCanvas  *self,
                                      DraftProject *project);

G_DEFINE_TYPE (DraftCanvas, draft_canvas, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_DOCUMENT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
draft_canvas_item_weak_notify_cb (gpointer  data,
                                  GObject  *where_the_item_was)
{
  DraftCanvas *self = data;

  g_assert (DRAFT_IS_CANVAS (self));

  if (self->resizes != NULL)
    g_hash_table_remove (self->resizes, where_the_item_was);
}

static gboolean
draft_canvas_get_child_position_cb (DraftCanvas  *self,
                                    GtkWidget    *child,
                                    GdkRectangle *area,
                                    GtkOverlay   *overlay)
{
  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (GTK_IS_WIDGET (child));
  g_assert (area != NULL);
  g_assert (GTK_IS_OVERLAY (overlay));

  if (child == GTK_WIDGET (self->selection))
    {
      double x, y;
      int x_offset, y_offset;

      g_assert (self->selection_info.item != NULL);

      gtk_widget_translate_coordinates (GTK_WIDGET (self->selection_info.item),
                                        GTK_WIDGET (self->overlay),
                                        0, 0, &x, &y);

      draft_canvas_selection_get_offset (self->selection, &x_offset, &y_offset);

      gtk_widget_measure (child, GTK_ORIENTATION_HORIZONTAL, -1, &area->width, NULL, NULL, NULL);
      gtk_widget_measure (child, GTK_ORIENTATION_VERTICAL, -1, &area->height, NULL, NULL, NULL);

      area->x = x + self->selection_info.area.x - x_offset;
      area->y = y + self->selection_info.area.y - y_offset;

      return TRUE;
    }
  else if (child == GTK_WIDGET (self->resizer))
    {
      GtkWidget *paintable;
      double x, y;

      g_assert (self->resize_info.item != NULL);

      paintable = draft_canvas_item_get_paintable_widget (self->resize_info.item);
      gtk_widget_translate_coordinates (paintable,
                                        GTK_WIDGET (self->overlay),
                                        0, 0, &x, &y);

      gtk_widget_measure (child, GTK_ORIENTATION_HORIZONTAL, -1, &area->width, NULL, NULL, NULL);
      gtk_widget_measure (child, GTK_ORIENTATION_VERTICAL, -1, &area->height, NULL, NULL, NULL);

      area->x = x;
      area->y = y;
      area->width = self->resize_info.start_x + self->resize_info.offset_x;
      area->height = self->resize_info.start_y + self->resize_info.offset_y;

      return TRUE;
    }

  return FALSE;
}

static void
draft_canvas_pressed_cb (DraftCanvas *self,
                         int          n_pressed,
                         double       x,
                         double       y,
                         GtkGesture  *click)
{
  DraftCanvasItem *item;
  GtkWidget *pick;

  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (GTK_IS_GESTURE_CLICK (click));

  if (self->project == NULL)
    return;

  /* The click gesture is used to clear the selection on the canvas
   * or possibly propagate a click to an even more inner widget within
   * the canvas picture. This can happen with DraftCanvasSelection
   * because it cannot pass-through events on the inner box used for
   * drawing borders.
   */

  if (!(pick = gtk_widget_pick (GTK_WIDGET (self), x, y, 0)) || DRAFT_IS_CANVAS (pick))
    goto unselect;

  if (!(pick = gtk_widget_get_ancestor (pick, DRAFT_TYPE_CANVAS_SELECTION)))
    goto unselect;

  item = DRAFT_CANVAS_ITEM (self->selection_info.item);
  gtk_widget_translate_coordinates (GTK_WIDGET (self), GTK_WIDGET (item), x, y, &x, &y);

  if (draft_canvas_item_select_at_coords (item, x, y))
    {
      gtk_gesture_set_sequence_state (click,
                                      gtk_gesture_get_last_updated_sequence (click),
                                      GTK_EVENT_SEQUENCE_CLAIMED);
      return;
    }

unselect:
  gtk_gesture_set_sequence_state (click,
                                  gtk_gesture_get_last_updated_sequence (click),
                                  GTK_EVENT_SEQUENCE_CLAIMED);

  _draft_project_clear_selection (self->project);
}

static void
draft_canvas_action_delete (GtkWidget  *widget,
                            const char *action_name,
                            GVariant   *param)
{
  DraftCanvas *self = (DraftCanvas *)widget;
  DraftTransaction *transaction;
  DraftItem *item;

  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (action_name != NULL);

  if (self->project == NULL)
    return;

  if (!(item = _draft_canvas_observer_get_selected (self->observer)))
    return;

  _draft_project_clear_selection (self->project);

  transaction = draft_project_begin (self->project);
  draft_transaction_remove (transaction, item);
  draft_project_commit (self->project);
}

static void
draft_canvas_set_project (DraftCanvas  *self,
                          DraftProject *project)
{
  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (!project || DRAFT_IS_PROJECT (project));

  if (self->project == project)
    return;

  if (self->observer)
    {
      g_assert (DRAFT_IS_PROJECT (self->project));
      _draft_project_remove_observer (self->project, DRAFT_OBSERVER (self->observer));
    }

  g_set_object (&self->project, project);

  if (self->project != NULL)
    {
      self->observer = _draft_canvas_observer_new (self->project, self);
      _draft_project_add_observer (self->project, DRAFT_OBSERVER (self->observer));
    }
}

void
draft_canvas_set_document (DraftCanvas   *self,
                           DraftDocument *document)
{
  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (DRAFT_IS_DOCUMENT (document));

  if (g_set_object (&self->document, document))
    {
      DraftProject *project = NULL;

      if (document != NULL)
        project = draft_document_get_project (document);

      draft_canvas_set_project (self, project);
    }
}

static void
draft_canvas_resize_begin_cb (DraftCanvas    *self,
                              double          start_x,
                              double          start_y,
                              GtkGestureDrag *drag)
{
  DraftCanvasItem *item;
  GtkWidget *pick;
  double x, y;
  gboolean horiz;
  gboolean vert;
  int width, height;

  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  if (!(pick = gtk_widget_pick (GTK_WIDGET (self), start_x, start_y, GTK_PICK_NON_TARGETABLE)))
    return;

  if (!DRAFT_IS_CANVAS_ITEM (pick))
    pick = gtk_widget_get_ancestor (pick, DRAFT_TYPE_CANVAS_ITEM);

  if (pick == NULL)
    return;

  item = DRAFT_CANVAS_ITEM (pick);
  pick = draft_canvas_item_get_paintable_widget (item);

  gtk_widget_translate_coordinates (GTK_WIDGET (self), pick, start_x, start_y, &x, &y);

  width = gtk_widget_get_width (pick);
  height = gtk_widget_get_height (pick);

  horiz = x > width - 15 && x <= width;
  vert = y > height - 15 && y <= height;

  if (horiz == FALSE && vert == FALSE)
    return;

  _draft_canvas_set_selection_area (self, NULL, NULL);

  self->resize_info.item = item;
  self->resize_info.horiz = horiz;
  self->resize_info.vert = vert;
  self->resize_info.start_x = MIN (width, horiz ? start_x : width);
  self->resize_info.start_y = MIN (height, vert ? start_y : height);
  self->resize_info.offset_x = 0;
  self->resize_info.offset_y = 0;

  gtk_gesture_set_state (GTK_GESTURE (drag), GTK_EVENT_SEQUENCE_CLAIMED);

  gtk_widget_show (GTK_WIDGET (self->resizer));
}

static void
draft_canvas_resize_update_cb (DraftCanvas    *self,
                               double          offset_x,
                               double          offset_y,
                               GtkGestureDrag *drag)
{
  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  if (self->resize_info.horiz)
    self->resize_info.offset_x = offset_x;

  if (self->resize_info.vert)
    self->resize_info.offset_y = offset_y;

  gtk_widget_queue_resize (GTK_WIDGET (self->resizer));
}

static void
draft_canvas_resize_end_cb (DraftCanvas    *self,
                            double          offset_x,
                            double          offset_y,
                            GtkGestureDrag *drag)
{
  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag));

  if (self->resize_info.horiz)
    self->resize_info.offset_x = offset_x;

  if (self->resize_info.vert)
    self->resize_info.offset_y = offset_y;

  if (self->resize_info.item != NULL)
    _draft_canvas_resize (self,
                          draft_canvas_item_get_item (self->resize_info.item),
                          self->resize_info.start_x + self->resize_info.offset_x,
                          self->resize_info.start_y + self->resize_info.offset_y);

  self->resize_info.item = NULL;
  self->resize_info.horiz = FALSE;
  self->resize_info.vert = FALSE;
  self->resize_info.start_x = 0;
  self->resize_info.start_y = 0;
  self->resize_info.offset_x = 0;
  self->resize_info.offset_y = 0;

  gtk_widget_hide (GTK_WIDGET (self->resizer));
}

static void
draft_canvas_dispose (GObject *object)
{
  DraftCanvas *self = (DraftCanvas *)object;

  draft_canvas_set_document (self, NULL);
  g_clear_object (&self->observer);
  g_clear_pointer ((GtkWidget **)&self->scroller, gtk_widget_unparent);

  if (self->resizes != NULL)
    {
      GHashTableIter iter;
      gpointer key, value;

      g_hash_table_iter_init (&iter, self->resizes);

      while (g_hash_table_iter_next (&iter, &key, &value))
        {
          g_object_weak_unref (key, draft_canvas_item_weak_notify_cb, self);
          g_hash_table_iter_remove (&iter);
        }

      g_clear_pointer (&self->resizes, g_hash_table_unref);
    }

  G_OBJECT_CLASS (draft_canvas_parent_class)->dispose (object);
}

static void
draft_canvas_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DraftCanvas *self = DRAFT_CANVAS (object);

  switch (prop_id)
    {
    case PROP_DOCUMENT:
      g_value_set_object (value, draft_canvas_get_document (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DraftCanvas *self = DRAFT_CANVAS (object);

  switch (prop_id)
    {
    case PROP_DOCUMENT:
      draft_canvas_set_document (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_class_init (DraftCanvasClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_canvas_dispose;
  object_class->get_property = draft_canvas_get_property;
  object_class->set_property = draft_canvas_set_property;

  properties [PROP_DOCUMENT] =
    g_param_spec_object ("document",
                         "Document",
                         "The document to display in the canvas",
                         DRAFT_TYPE_DOCUMENT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "draftcanvas");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-canvas.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, box);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, menu_views);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, object_views);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, overlay);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, resizer);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, scroller);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, selection);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvas, widget_views);
  gtk_widget_class_bind_template_callback (widget_class, draft_canvas_get_child_position_cb);

  gtk_widget_class_install_action (widget_class, "canvas.delete", NULL, draft_canvas_action_delete);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Delete, 0, "canvas.delete", NULL);

  g_type_ensure (DRAFT_TYPE_CANVAS_SELECTION);
}

static void
draft_canvas_init (DraftCanvas *self)
{
  GtkEventController *controller;

  self->resizes = g_hash_table_new_full (NULL, NULL, NULL, g_free);

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_set_focusable (GTK_WIDGET (self), TRUE);

  controller = GTK_EVENT_CONTROLLER (gtk_gesture_click_new ());
  g_signal_connect_object (controller,
                           "pressed",
                           G_CALLBACK (draft_canvas_pressed_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);

  controller = GTK_EVENT_CONTROLLER (gtk_gesture_drag_new ());
  g_signal_connect_object (controller,
                           "drag-begin",
                           G_CALLBACK (draft_canvas_resize_begin_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (controller,
                           "drag-update",
                           G_CALLBACK (draft_canvas_resize_update_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (controller,
                           "drag-end",
                           G_CALLBACK (draft_canvas_resize_end_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_event_controller_set_propagation_phase (controller, GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);
}

GtkWidget *
draft_canvas_new (void)
{
  return g_object_new (DRAFT_TYPE_CANVAS, NULL);
}

/**
 * draft_canvas_get_document:
 * @self: a #DraftCanvas
 *
 * Gets the #DraftCanvas:document property.
 *
 * Returns: (transfer none): a #DraftDocument
 */
DraftDocument *
draft_canvas_get_document (DraftCanvas *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS (self), NULL);

  return self->document;
}

void
_draft_canvas_add_widget (DraftCanvas     *self,
                          DraftCanvasItem *item)
{
  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (DRAFT_IS_CANVAS_ITEM (item));

  gtk_box_append (self->widget_views, GTK_WIDGET (item));
}

void
_draft_canvas_set_selection_area (DraftCanvas        *self,
                                  DraftCanvasItem    *item,
                                  const GdkRectangle *area)
{
  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (!item || DRAFT_IS_CANVAS_ITEM (item));

  if (item == NULL || area == NULL)
    {
      memset (&self->selection_info.area, 0, sizeof self->selection_info.area);
      self->selection_info.item = NULL;
      draft_canvas_selection_set_size (self->selection, 0, 0);
      gtk_widget_hide (GTK_WIDGET (self->selection));
    }
  else
    {
      self->selection_info.area = *area;
      self->selection_info.item = item;

      draft_canvas_selection_set_size (self->selection, area->width, area->height);
      gtk_widget_queue_resize (GTK_WIDGET (self->selection));
      gtk_widget_show (GTK_WIDGET (self->selection));
      gtk_widget_grab_focus (GTK_WIDGET (item));
    }
}

DraftCanvasItem *
_draft_canvas_find_item (DraftCanvas *self,
                         DraftItem   *item)
{
  DraftCanvasItem *best_match = NULL;
  int best_depth = G_MAXINT;

  g_return_val_if_fail (DRAFT_IS_CANVAS (self), NULL);
  g_return_val_if_fail (DRAFT_IS_ITEM (item), NULL);

  for (GtkWidget *child = gtk_widget_get_first_child (GTK_WIDGET (self->widget_views));
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      DraftItem *child_item;
      int depth;

      if (!DRAFT_IS_CANVAS_ITEM (child))
        continue;

      child_item = draft_canvas_item_get_item (DRAFT_CANVAS_ITEM (child));
      if (child_item == item)
        return DRAFT_CANVAS_ITEM (child);

      if ((depth = draft_item_is_ancestor (item, child_item)))
        {
          if (depth < best_depth)
            {
              best_depth = depth;
              best_match = DRAFT_CANVAS_ITEM (child);
            }
        }
    }

  return best_match;
}


void
_draft_canvas_set_paintable (DraftCanvas          *self,
                             DraftItem            *item,
                             DraftCanvasPaintable *paintable)
{
  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (DRAFT_IS_CANVAS_PAINTABLE (paintable));

  for (GtkWidget *child = gtk_widget_get_first_child (GTK_WIDGET (self->widget_views));
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      if (DRAFT_IS_CANVAS_ITEM (child))
        {
          DraftItem *child_item = draft_canvas_item_get_item (DRAFT_CANVAS_ITEM (child));

          if (child_item == item)
            {
              draft_canvas_item_set_paintable (DRAFT_CANVAS_ITEM (child), paintable);
              return;
            }
        }
    }

  _draft_canvas_add_widget (self, DRAFT_CANVAS_ITEM (draft_canvas_item_new (paintable, item)));
}

static void
draft_canvas_resize_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  DraftIpcBuilder *builder = (DraftIpcBuilder *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  DraftCanvas *self;
  const char *object_id;
  DraftItem *item;
  Resize *resize = NULL;

  g_assert (DRAFT_IPC_IS_BUILDER (builder));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  item = g_task_get_task_data (task);
  object_id = _draft_item_get_internal_id (item);

  g_assert (DRAFT_IS_CANVAS (self));
  g_assert (DRAFT_IS_ITEM (item));

  /* We might be post-dispose() if the window was in destruction */
  if (self->resizes != NULL &&
      (resize = g_hash_table_lookup (self->resizes, item)))
    resize->waiting_for_reply = FALSE;

  if (!draft_ipc_builder_call_resize_finish (builder, result, &error))
    {
      g_warning ("Failed to resize widget: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (resize == NULL || resize->dirty == FALSE || object_id == NULL)
    goto finish;

  resize->dirty = FALSE;
  resize->waiting_for_reply = TRUE;

  draft_ipc_builder_call_resize (builder,
                                 object_id,
                                 resize->width,
                                 resize->height,
                                 NULL,
                                 draft_canvas_resize_cb,
                                 g_steal_pointer (&task));

  return;

finish:
  g_task_return_boolean (task, TRUE);
}

void
_draft_canvas_resize (DraftCanvas *self,
                      DraftItem   *item,
                      int          width,
                      int          height)
{
  g_autoptr(GTask) task = NULL;
  DraftIpcBuilder *builder;
  DraftDocument *document;
  const char *object_id;
  Resize *resize;

  g_return_if_fail (DRAFT_IS_CANVAS (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (self->observer != NULL);

  /* Clear the selection since it will be off */
  _draft_canvas_set_selection_area (self, NULL, NULL);

  if (!(resize = g_hash_table_lookup (self->resizes, item)))
    {
      resize = g_new0 (Resize, 1);
      resize->width = width;
      resize->height = height;
      resize->dirty = FALSE;
      resize->waiting_for_reply = FALSE;
      g_hash_table_insert (self->resizes, item, resize);
      g_object_weak_ref (G_OBJECT (item),
                         draft_canvas_item_weak_notify_cb,
                         self);
    }

  resize->width = width;
  resize->height = height;

  /* If we're waiting for a reply, just queue this and we'll send the next
   * size request after the active one completes to avoid overwhelming the
   * peer with resize requests.
   */
  if (resize->waiting_for_reply)
    {
      resize->dirty = TRUE;
      return;
    }

  /* There is nothing we can do if we can't get an active builder */
  if (!(object_id = _draft_item_get_internal_id (item)) ||
      !(document = draft_item_get_document (item)) ||
      !(builder = _draft_canvas_observer_get_builder (self->observer, document)))
    return;

  /* Submit the resize request, and wait for a reply before we allow
   * another resize request to be applied.
   */
  task = g_task_new (self, NULL, NULL, NULL);
  g_task_set_source_tag (task, _draft_canvas_resize);
  g_task_set_task_data (task, g_object_ref (item), g_object_unref);

  draft_ipc_builder_call_resize (builder,
                                 object_id,
                                 width,
                                 height,
                                 NULL,
                                 draft_canvas_resize_cb,
                                 g_steal_pointer (&task));
}
