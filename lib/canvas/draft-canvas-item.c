/* draft-canvas-item.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-ipc-builder.h"
#include "draft-project-private.h"
#include "canvas/draft-canvas-item-private.h"
#include "canvas/draft-canvas-grip-private.h"
#include "canvas/draft-canvas-paintable-private.h"
#include "canvas/draft-canvas-picture-private.h"
#include "canvas/draft-canvas-private.h"
#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"

struct _DraftCanvasItem
{
  GtkWidget             parent_instance;

  DraftItem            *item;
  DraftCanvasPaintable *paintable;

  GtkBox               *box;
  DraftCanvasPicture   *picture;
  GtkLabel             *label;

  guint                 rehover_source;
  double                rehover_x;
  double                rehover_y;
  GdkRectangle          rehover_area;
};

G_DEFINE_TYPE (DraftCanvasItem, draft_canvas_item, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_ITEM,
  PROP_PAINTABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_canvas_item_new (DraftCanvasPaintable *paintable,
                       DraftItem            *item)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_PAINTABLE (paintable), NULL);
  g_return_val_if_fail (DRAFT_IS_ITEM (item), NULL);

  return g_object_new (DRAFT_TYPE_CANVAS_ITEM,
                       "item", item,
                       "paintable", paintable,
                       NULL);
}

static void
draft_canvas_item_picture_item_selected_cb (DraftCanvasItem    *self,
                                            const char         *object_id,
                                            const GdkRectangle *area,
                                            DraftCanvasPicture *picture)
{
  DraftDocument *document;
  DraftProject *project;
  DraftItem *item;

  g_assert (DRAFT_IS_CANVAS_ITEM (self));
  g_assert (object_id != NULL);
  g_assert (DRAFT_IS_CANVAS_PICTURE (picture));

  if (self->item == NULL || self->paintable == NULL)
    return;

  if (!(document = draft_item_get_document (self->item)))
    return;

  if (!(item = _draft_document_find_by_id (document, object_id)))
    return;

  if (!(project = draft_item_get_project (item)))
    return;

  _draft_project_select_item (project, item);
}

static void
draft_canvas_item_rehover (DraftCanvasItem *self)
{
  g_autofree char *object_id = NULL;

  g_assert (DRAFT_IS_CANVAS_ITEM (self));

  if (self->paintable == NULL)
    return;

  object_id = draft_canvas_paintable_pick (self->paintable,
                                           self->rehover_x,
                                           self->rehover_y,
                                           &self->rehover_area);
  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static gboolean
draft_canvas_item_do_rehover (gpointer data)
{
  DraftCanvasItem *self = data;

  g_assert (DRAFT_IS_CANVAS_ITEM (self));

  self->rehover_source = 0;
  draft_canvas_item_rehover (self);
  return G_SOURCE_REMOVE;
}

static void
draft_canvas_item_queue_rehover (DraftCanvasItem *self)
{
  g_assert (DRAFT_IS_CANVAS_ITEM (self));

  if (self->rehover_source == 0)
    self->rehover_source = g_timeout_add (50, draft_canvas_item_do_rehover, self);
}

static void
draft_canvas_item_motion_cb (DraftCanvasItem          *self,
                             double                    x,
                             double                    y,
                             GtkEventControllerMotion *motion)
{
  g_assert (DRAFT_IS_CANVAS_ITEM (self));
  g_assert (GTK_IS_EVENT_CONTROLLER_MOTION (motion));

  self->rehover_x = x;
  self->rehover_y = y;

  draft_canvas_item_queue_rehover (self);
}

static void
draft_canvas_item_leave_cb (DraftCanvasItem          *self,
                            GtkEventControllerMotion *motion)
{
  g_assert (DRAFT_IS_CANVAS_ITEM (self));
  g_assert (GTK_IS_EVENT_CONTROLLER_MOTION (motion));

  self->rehover_area.width = 0;
  self->rehover_area.height = 0;
  g_clear_handle_id (&self->rehover_source, g_source_remove);
  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
draft_canvas_item_snapshot (GtkWidget   *widget,
                            GtkSnapshot *snapshot)
{
  DraftCanvasItem *self = (DraftCanvasItem *)widget;

  g_assert (DRAFT_IS_CANVAS_ITEM (self));
  g_assert (GTK_IS_SNAPSHOT (snapshot));

  GTK_WIDGET_CLASS (draft_canvas_item_parent_class)->snapshot (widget, snapshot);

  if (self->rehover_area.width || self->rehover_area.height)
    {
      static GdkRGBA color[4] = {
        {0,0,0,.05}, {0,0,0,.05}, {0,0,0,.05}, {0,0,0,.05},
      };
      static float widths[4] = { 3,3,3,3 };
      GskRoundedRect rect = GSK_ROUNDED_RECT_INIT (self->rehover_area.x - 3,
                                                   self->rehover_area.y - 3,
                                                   self->rehover_area.width + 6,
                                                   self->rehover_area.height + 6);
      rect.corner[0] = GRAPHENE_SIZE_INIT (3, 3);
      rect.corner[1] = GRAPHENE_SIZE_INIT (3, 3);
      rect.corner[2] = GRAPHENE_SIZE_INIT (3, 3);
      rect.corner[3] = GRAPHENE_SIZE_INIT (3, 3);
      gtk_snapshot_append_border (snapshot, &rect, widths, color);
    }
}

static void
draft_canvas_item_constructed (GObject *object)
{
  DraftCanvasItem *self = (DraftCanvasItem *)object;

  g_assert (DRAFT_IS_CANVAS_ITEM (self));

  if (DRAFT_IS_OBJECT (self->item) &&
      _draft_item_is_of_type (self->item, "GtkNative"))
    gtk_widget_add_css_class (GTK_WIDGET (self), "GtkNative");
}

static void
draft_canvas_item_dispose (GObject *object)
{
  DraftCanvasItem *self = (DraftCanvasItem *)object;

  g_clear_handle_id (&self->rehover_source, g_source_remove);
  g_clear_object (&self->item);
  g_clear_object (&self->paintable);
  g_clear_pointer ((GtkWidget **)&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_canvas_item_parent_class)->dispose (object);
}

static void
draft_canvas_item_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  DraftCanvasItem *self = DRAFT_CANVAS_ITEM (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, draft_canvas_item_get_item (self));
      break;

    case PROP_PAINTABLE:
      g_value_set_object (value, draft_canvas_item_get_paintable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_item_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  DraftCanvasItem *self = DRAFT_CANVAS_ITEM (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      self->item = g_value_dup_object (value);
      g_object_bind_property (self->item, "display-name", self->label, "label", G_BINDING_SYNC_CREATE);
      break;

    case PROP_PAINTABLE:
      draft_canvas_item_set_paintable (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_item_class_init (DraftCanvasItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = draft_canvas_item_constructed;
  object_class->dispose = draft_canvas_item_dispose;
  object_class->get_property = draft_canvas_item_get_property;
  object_class->set_property = draft_canvas_item_set_property;

  widget_class->snapshot = draft_canvas_item_snapshot;

  properties [PROP_ITEM] =
    g_param_spec_object ("item",
                         "Item",
                         "The item to be canvas_itemed",
                         DRAFT_TYPE_ITEM,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));

  properties [PROP_PAINTABLE] =
    g_param_spec_object ("paintable",
                         "Paintable",
                         "The paintable do display",
                         DRAFT_TYPE_CANVAS_PAINTABLE,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "draftcanvasitem");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-canvas-item.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasItem, box);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasItem, label);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasItem, picture);
  gtk_widget_class_bind_template_callback (widget_class, draft_canvas_item_picture_item_selected_cb);

  g_type_ensure (DRAFT_TYPE_CANVAS_GRIP);
  g_type_ensure (DRAFT_TYPE_CANVAS_PICTURE);
}

static void
draft_canvas_item_init (DraftCanvasItem *self)
{
  GtkEventController *motion;

  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_widget_set_halign (GTK_WIDGET (self), GTK_ALIGN_START);
  gtk_widget_set_valign (GTK_WIDGET (self), GTK_ALIGN_START);
  gtk_widget_set_focusable (GTK_WIDGET (self), TRUE);

  motion = gtk_event_controller_motion_new ();
  g_signal_connect_object (motion,
                           "motion",
                           G_CALLBACK (draft_canvas_item_motion_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (motion,
                           "leave",
                           G_CALLBACK (draft_canvas_item_leave_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (self), motion);
}

/**
 * draft_canvas_item_get_item:
 *
 * Returns: (transfer none): a #DraftItem
 */
DraftItem *
draft_canvas_item_get_item (DraftCanvasItem *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_ITEM (self), NULL);

  return self->item;
}

GdkPaintable *
draft_canvas_item_get_paintable (DraftCanvasItem *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_ITEM (self), NULL);

  return GDK_PAINTABLE (self->paintable);
}

void
draft_canvas_item_set_paintable (DraftCanvasItem      *self,
                                 DraftCanvasPaintable *paintable)
{
  g_return_if_fail (DRAFT_IS_CANVAS_ITEM (self));
  g_return_if_fail (DRAFT_IS_CANVAS_PAINTABLE (paintable));

  if (g_set_object (&self->paintable, paintable))
    {
      draft_canvas_picture_set_paintable (self->picture, GDK_PAINTABLE (paintable));
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAINTABLE]);
    }
}

gboolean
draft_canvas_item_select_at_coords (DraftCanvasItem *self,
                                    double           x,
                                    double           y)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_ITEM (self), FALSE);

  gtk_widget_translate_coordinates (GTK_WIDGET (self), GTK_WIDGET (self->picture),
                                    x, y, &x, &y);
  return draft_canvas_picture_select_at_coords (self->picture, x, y);
}

void
draft_canvas_item_select_item (DraftCanvasItem *self,
                               DraftItem       *item)
{
  GtkAllocation alloc;
  const char *object_id;
  GtkWidget *canvas;

  g_return_if_fail (DRAFT_IS_CANVAS_ITEM (self));
  g_return_if_fail (self->item == item || draft_item_is_ancestor (item, self->item));

  if (self->paintable == NULL ||
      !(canvas = gtk_widget_get_ancestor (GTK_WIDGET (self), DRAFT_TYPE_CANVAS)))
    return;

  object_id = _draft_item_get_internal_id (item);
  draft_canvas_paintable_get_allocation (self->paintable, object_id, &alloc);
  _draft_canvas_set_selection_area (DRAFT_CANVAS (canvas), self, (const GdkRectangle *)&alloc);
}

GtkWidget *
draft_canvas_item_get_paintable_widget (DraftCanvasItem *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_ITEM (self), NULL);

  return GTK_WIDGET (self->picture);
}

void
draft_canvas_item_resize (DraftCanvasItem *self,
                          int              width,
                          int              height)
{
  GtkWidget *canvas;

  g_return_if_fail (DRAFT_IS_CANVAS_ITEM (self));
  g_return_if_fail (self->item != NULL);

  if ((canvas = gtk_widget_get_ancestor (GTK_WIDGET (self), DRAFT_TYPE_CANVAS)))
    _draft_canvas_resize (DRAFT_CANVAS (canvas), self->item, width, height);
}
