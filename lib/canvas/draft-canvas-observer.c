/* draft-canvas-observer.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-context-private.h"

#include "canvas/draft-canvas-item-private.h"
#include "canvas/draft-canvas-observer-private.h"
#include "canvas/draft-canvas-paintable-private.h"
#include "canvas/draft-canvas-private.h"
#include "items/draft-child.h"
#include "items/draft-dependency.h"
#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "undo/draft-transaction-private.h"

#include "draft-ipc-builder.h"
#include "draft-ipc-paintable.h"
#include "draft-ipc-service.h"

struct _DraftCanvasObserver
{
  DraftObserver        parent_instance;

  DraftProjectContext *context;
  DraftProject        *project;
  DraftCanvas         *canvas;
  GHashTable          *builders;
  GCancellable        *cancellable;

  DraftItem           *selected;
};

G_DEFINE_TYPE (DraftCanvasObserver, draft_canvas_observer, DRAFT_TYPE_OBSERVER)

static void
draft_canvas_observer_set_context (DraftObserver       *observer,
                                   DraftProjectContext *context)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));
  g_assert (!context || DRAFT_IS_PROJECT_CONTEXT (context));

  if (g_set_object (&self->context, context)) { }
}

static char *
draft_canvas_observer_serialize (DraftCanvasObserver *self,
                                 DraftDocument       *document)
{
  GString *str;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));
  g_assert (DRAFT_IS_DOCUMENT (document));

  /* We serialize the document to XML for GtkBuilder to load in the
   * copilot process. However, the serialized state is not the same as
   * what is saved to disk because we make modifications so that we can
   * preview related widgets as well as make all objects accessible by
   * a given internal id.
   */

  str = g_string_new ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  _draft_item_save_to_xml (DRAFT_ITEM (document), str, self->project, TRUE);
  return g_string_free (str, FALSE);
}

typedef struct
{
  DraftCanvasObserver *self;
  DraftDocument       *document;
  DraftIpcBuilder     *builder;
  guint                n_active;
} Update;

static void
update_free (Update *u)
{
  g_assert (u->n_active == 0);
  g_clear_object (&u->self);
  g_clear_object (&u->document);
  g_clear_object (&u->builder);
  g_slice_free (Update, u);
}

static DraftItem *
draft_canvas_observer_find_item (DraftCanvasObserver *self,
                                 DraftDocument       *document,
                                 DraftIpcPaintable   *paintable)
{
  const char *object_path;
  const char *object_id;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IPC_IS_PAINTABLE (paintable));

  if (!(object_path = g_dbus_proxy_get_object_path (G_DBUS_PROXY (paintable))))
    return NULL;

  if (!(object_id = strrchr (object_path, '/')))
    return NULL;

  object_id++;

  return _draft_document_find_by_id (document, object_id);
}

static void
draft_canvas_observer_new_paintable_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(DraftIpcPaintable) proxy = NULL;
  DraftCanvasObserver *self;
  Update *state;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  state = g_task_get_task_data (task);

  if ((proxy = draft_ipc_paintable_proxy_new_finish (result, &error)))
    {
      DraftItem *item = draft_canvas_observer_find_item (self, state->document, proxy);

      if (item != NULL)
        {
          g_autoptr(DraftCanvasPaintable) paintable = draft_canvas_paintable_new (proxy);

          if (self->canvas != NULL)
            _draft_canvas_set_paintable (self->canvas, item, paintable);
        }
    }

  state->n_active--;

  if (state->n_active == 0)
    g_task_return_boolean (task, TRUE);
}

static void
draft_canvas_observer_get_paintable_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  DraftIpcBuilder *builder = (DraftIpcBuilder *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree char *object_path = NULL;
  Update *state;

  g_assert (DRAFT_IPC_IS_BUILDER (builder));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (draft_ipc_builder_call_get_paintable_finish (builder, &object_path, result, &error))
    {
      draft_ipc_paintable_proxy_new (g_dbus_proxy_get_connection (G_DBUS_PROXY (builder)),
                                     G_DBUS_PROXY_FLAGS_NONE,
                                     NULL,
                                     object_path,
                                     g_task_get_cancellable (task),
                                     draft_canvas_observer_new_paintable_cb,
                                     g_object_ref (task));
      return;
    }

  g_warning ("Failed to get DraftIpcPaintable object path: %s",
             error->message);

  state = g_task_get_task_data (task);
  state->n_active--;

  if (state->n_active == 0)
    g_task_return_boolean (task, TRUE);
}

static void
draft_canvas_observer_get_display_objects_cb (GObject      *object,
                                              GAsyncResult *result,
                                              gpointer      user_data)
{
  DraftIpcBuilder *builder = (DraftIpcBuilder *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_auto(GStrv) object_ids = NULL;
  Update *state;

  g_assert (DRAFT_IPC_IS_BUILDER (builder));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!draft_ipc_builder_call_get_display_objects_finish (builder, &object_ids, result, &error))
    {
      g_warning ("Failed to list display objects: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  state = g_task_get_task_data (task);

  for (guint i = 0; object_ids[i]; i++)
    {
      const char *object_id = object_ids[i];

      state->n_active++;
      draft_ipc_builder_call_get_paintable (builder,
                                            object_id,
                                            g_task_get_cancellable (task),
                                            draft_canvas_observer_get_paintable_cb,
                                            g_object_ref (task));
    }

  if (state->n_active == 0)
    g_task_return_boolean (task, TRUE);
}

static void
draft_canvas_observer_get_proxy_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  g_autoptr(DraftIpcBuilder) builder = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  Update *state;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(builder = draft_ipc_builder_proxy_new_finish (result, &error)))
    {
      g_warning ("Failed to get DraftIpcBuilder proxy: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  state = g_task_get_task_data (task);

  g_assert (state != NULL);
  g_assert (DRAFT_IS_CANVAS_OBSERVER (state->self));
  g_assert (DRAFT_IS_DOCUMENT (state->document));

  g_set_object (&state->builder, builder);

  g_hash_table_insert (state->self->builders,
                       g_object_ref (state->document),
                       g_object_ref (builder));

  draft_ipc_builder_call_get_display_objects (builder,
                                              g_task_get_cancellable (task),
                                              draft_canvas_observer_get_display_objects_cb,
                                              g_object_ref (task));
}

static void
draft_canvas_observer_get_builder_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  DraftIpcService *service = (DraftIpcService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree char *object_path = NULL;

  g_assert (DRAFT_IPC_IS_SERVICE (service));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!draft_ipc_service_call_get_builder_finish (service, &object_path, result, &error))
    {
      g_warning ("Failed to retrieve GtkBuilder: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  draft_ipc_builder_proxy_new (g_dbus_proxy_get_connection (G_DBUS_PROXY (service)),
                               G_DBUS_PROXY_FLAGS_NONE,
                               NULL,
                               object_path,
                               g_task_get_cancellable (task),
                               draft_canvas_observer_get_proxy_cb,
                               g_object_ref (task));
}

static void
draft_canvas_observer_require_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  DraftIpcService *service = (DraftIpcService *)object;
  g_autoptr(DraftCanvasObserver) self = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (DRAFT_IPC_IS_SERVICE (service));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));

  if (!draft_ipc_service_call_require_finish (service, result, &error))
    g_warning ("Failed to load library: %s", error->message);
  else
    g_debug ("Library loaded");
}

static void
draft_canvas_observer_update_full (DraftCanvasObserver *self,
                                   DraftDocument       *document,
                                   const char          *xml,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;
  DraftIpcService *service;
  DraftIpcBuilder *builder;
  Update *state;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (DRAFT_IS_PROJECT_CONTEXT (self->context));
  g_assert (xml != NULL);

  if (!(service = _draft_project_context_get_service (self->context, &error)))
    {
      g_warning ("Failed to load service: %s", error->message);
      return;
    }

  if ((builder = g_hash_table_lookup (self->builders, document)))
    {
      draft_ipc_builder_call_destroy (builder, NULL, NULL, NULL);
      g_hash_table_remove (self->builders, document);
      builder = NULL;
    }

  state = g_slice_new0 (Update);
  state->self = g_object_ref (self);
  state->document = g_object_ref (document);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, draft_canvas_observer_update_full);
  g_task_set_task_data (task, state, (GDestroyNotify)update_free);

  /* Pipeline a request to load all our dependencies. We don't care
   * if they do or don't load, we'll try anyway.
   */
  for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (document));
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (DRAFT_IS_DEPENDENCY (child))
        {
          const char *library = draft_dependency_get_library (DRAFT_DEPENDENCY (child));
          const char *version = draft_dependency_get_version (DRAFT_DEPENDENCY (child));

          if (library == NULL || version == NULL)
            continue;

          /* We don't need to import GTK, it's already there */
          if (strcasecmp (library, "gtk") == 0)
            {
              if (strcmp (version, "4.0") != 0)
                g_warning ("GTK version %s is not supported by drafting", version);
              continue;
            }

          g_debug ("Requiring %s version %s", library, version);

          draft_ipc_service_call_require (service,
                                          library,
                                          version,
                                          NULL,
                                          draft_canvas_observer_require_cb,
                                          g_object_ref (self));
        }
    }

  draft_ipc_service_call_get_builder (service,
                                      xml,
                                      NULL,
                                      draft_canvas_observer_get_builder_cb,
                                      g_steal_pointer (&task));
}

static void
draft_canvas_observer_apply (DraftObserver    *observer,
                             DraftTransaction *transaction)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;
  g_autofree char *xml = NULL;
  DraftIpcBuilder *builder;
  DraftDocument *document;
  const GSList *documents;

  g_assert (DRAFT_IS_OBSERVER (observer));
  g_assert (DRAFT_IS_TRANSACTION (transaction));

  if (self->context == NULL)
    return;

  documents = _draft_transaction_get_documents (transaction);
  document = draft_canvas_get_document (self->canvas);
  builder = g_hash_table_lookup (self->builders, document);

  g_assert (documents != NULL);
  g_assert (document != NULL);
  g_assert (DRAFT_IS_DOCUMENT (document));
  g_assert (!builder || DRAFT_IPC_IS_BUILDER (builder));

  /* Ignore unless the canvas's document is affected */
  if (g_slist_find ((GSList *)documents, document) == NULL)
    return;

  if (builder != NULL && _draft_transaction_is_simple (transaction))
    {
      g_autoptr(GError) error = NULL;

      if (_draft_transaction_apply_simple (transaction, document, builder, &error))
        return;

      g_warning ("Failed to transmit property change: %s", error->message);
    }

  xml = draft_canvas_observer_serialize (self, document);
  draft_canvas_observer_update_full (self,
                                     document,
                                     xml,
                                     self->cancellable,
                                     NULL,
                                     NULL);
}

static void
draft_canvas_observer_revert (DraftObserver    *observer,
                              DraftTransaction *transaction)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;

  g_assert (DRAFT_IS_OBSERVER (observer));
  g_assert (DRAFT_IS_TRANSACTION (transaction));

  if (self->context == NULL)
    return;

  if (self->cancellable)
    g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  self->cancellable = g_cancellable_new ();

  for (const GSList *iter = _draft_transaction_get_documents (transaction);
       iter != NULL;
       iter = iter->next)
    {
      DraftDocument *document = iter->data;

      if (document == draft_canvas_get_document (self->canvas))
        {
          g_autofree char *xml = draft_canvas_observer_serialize (self, document);
          draft_canvas_observer_update_full (self, document, xml, self->cancellable, NULL, NULL);
        }
    }
}

static void
draft_canvas_observer_document_added (DraftObserver *observer,
                                      DraftDocument *document)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (observer));
  g_assert (DRAFT_IS_DOCUMENT (document));

  if (document == draft_canvas_get_document (self->canvas))
    {
      g_autofree char *xml = draft_canvas_observer_serialize (self, document);
      draft_canvas_observer_update_full (self, document, xml, self->cancellable, NULL, NULL);
    }
}

static void
draft_canvas_observer_document_removed (DraftObserver *observer,
                                        DraftDocument *document)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;
  DraftIpcBuilder *builder;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (observer));
  g_assert (DRAFT_IS_DOCUMENT (document));

  if ((builder = g_hash_table_lookup (self->builders, document)))
    {
      draft_ipc_builder_call_destroy (builder, NULL, NULL, NULL);
      g_hash_table_remove (self->builders, document);
    }
}

static void
draft_canvas_observer_select_item (DraftObserver *observer,
                                   DraftItem     *item)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;
  DraftCanvasItem *canvas_item;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));
  g_assert (DRAFT_IS_ITEM (item));

  /* Special case <child> and select the first object (usually the widget) */
  if (DRAFT_IS_CHILD (item))
    {
      DraftItem *child = draft_item_get_first_child (item);

      if (DRAFT_IS_OBJECT (child))
        item = child;
    }

  while (item && !DRAFT_IS_OBJECT (item))
    item = draft_item_get_parent (item);

  if (!DRAFT_IS_OBJECT (item))
    return;

  self->selected = item;

  if ((canvas_item = _draft_canvas_find_item (self->canvas, item)))
    draft_canvas_item_select_item (canvas_item, item);
}

static void
draft_canvas_observer_clear_selection (DraftObserver *observer)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)observer;

  g_assert (DRAFT_IS_CANVAS_OBSERVER (self));

  self->selected = NULL;
  _draft_canvas_set_selection_area (self->canvas, NULL, NULL);
}

static void
draft_canvas_observer_finalize (GObject *object)
{
  DraftCanvasObserver *self = (DraftCanvasObserver *)object;

  g_clear_object (&self->cancellable);
  g_clear_object (&self->context);
  g_clear_weak_pointer (&self->project);
  g_clear_weak_pointer (&self->canvas);

  G_OBJECT_CLASS (draft_canvas_observer_parent_class)->finalize (object);
}

static void
draft_canvas_observer_class_init (DraftCanvasObserverClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftObserverClass *observer_class = DRAFT_OBSERVER_CLASS (klass);

  object_class->finalize = draft_canvas_observer_finalize;

  observer_class->set_context = draft_canvas_observer_set_context;
  observer_class->apply = draft_canvas_observer_apply;
  observer_class->revert = draft_canvas_observer_revert;
  observer_class->document_added = draft_canvas_observer_document_added;
  observer_class->document_removed = draft_canvas_observer_document_removed;
  observer_class->select_item = draft_canvas_observer_select_item;
  observer_class->clear_selection = draft_canvas_observer_clear_selection;
}

static void
draft_canvas_observer_init (DraftCanvasObserver *self)
{
  self->builders = g_hash_table_new_full (NULL, NULL, g_object_unref, g_object_unref);
}

DraftCanvasObserver *
_draft_canvas_observer_new (DraftProject *project,
                            DraftCanvas  *canvas)
{
  DraftCanvasObserver *self;

  self = g_object_new (DRAFT_TYPE_CANVAS_OBSERVER, NULL);
  g_set_weak_pointer (&self->project, project);
  g_set_weak_pointer (&self->canvas, canvas);

  return self;
}

DraftItem *
_draft_canvas_observer_get_selected (DraftCanvasObserver *self)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_OBSERVER (self), NULL);

  return self->selected;
}

DraftIpcBuilder *
_draft_canvas_observer_get_builder (DraftCanvasObserver *self,
                                    DraftDocument       *document)
{
  g_return_val_if_fail (DRAFT_IS_CANVAS_OBSERVER (self), NULL);
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (document), NULL);

  return g_hash_table_lookup (self->builders, document);
}
