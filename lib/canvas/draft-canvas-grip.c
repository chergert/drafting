/* draft-canvas-grip.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <math.h>

#include "canvas/draft-canvas-item-private.h"
#include "canvas/draft-canvas-grip-private.h"

struct _DraftCanvasGrip
{
  GtkWidget parent_instance;
  GdkSurfaceEdge edge;
};

G_DEFINE_TYPE (DraftCanvasGrip, draft_canvas_grip, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_EDGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
draft_canvas_grip_new (GdkSurfaceEdge edge)
{
  return g_object_new (DRAFT_TYPE_CANVAS_GRIP,
                       "edge", edge,
                       NULL);
}

static void
draft_canvas_grip_set_edge (DraftCanvasGrip *self,
                            GdkSurfaceEdge   edge)
{
  g_assert (DRAFT_IS_CANVAS_GRIP (self));

  self->edge = edge;

  switch ((int)self->edge)
    {
    case GDK_SURFACE_EDGE_EAST:
      gtk_widget_set_cursor_from_name (GTK_WIDGET (self), "e-resize");
      break;

    case GDK_SURFACE_EDGE_SOUTH:
      gtk_widget_set_cursor_from_name (GTK_WIDGET (self), "s-resize");
      break;

    case GDK_SURFACE_EDGE_SOUTH_EAST:
      gtk_widget_set_cursor_from_name (GTK_WIDGET (self), "se-resize");
      break;

    default:
      break;
    }
}

static void
draft_canvas_grip_dispose (GObject *object)
{
  G_OBJECT_CLASS (draft_canvas_grip_parent_class)->dispose (object);
}

static void
draft_canvas_grip_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  DraftCanvasGrip *self = DRAFT_CANVAS_GRIP (object);

  switch (prop_id)
    {
    case PROP_EDGE:
      g_value_set_enum (value, self->edge);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_grip_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  DraftCanvasGrip *self = DRAFT_CANVAS_GRIP (object);

  switch (prop_id)
    {
    case PROP_EDGE:
      draft_canvas_grip_set_edge (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_grip_class_init (DraftCanvasGripClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_canvas_grip_dispose;
  object_class->get_property = draft_canvas_grip_get_property;
  object_class->set_property = draft_canvas_grip_set_property;

  properties [PROP_EDGE] =
    g_param_spec_enum ("edge",
                       "Edge",
                       "The edge to grip",
                       GDK_TYPE_SURFACE_EDGE,
                       GDK_SURFACE_EDGE_NORTH_WEST,
                       (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "draftcanvasgrip");
}

static void
draft_canvas_grip_init (DraftCanvasGrip *self)
{
}
