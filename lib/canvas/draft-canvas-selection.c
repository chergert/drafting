/* draft-canvas-selection.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "canvas/draft-canvas-handle-private.h"
#include "canvas/draft-canvas-selection-private.h"
#include "items/draft-item.h"

struct _DraftCanvasSelection
{
  GtkWidget  parent_instance;

  DraftItem *item;

  GtkWidget *grid;
  GtkWidget *box;
  GtkWidget *top_start;
  GtkWidget *top_middle;
  GtkWidget *top_end;
  GtkWidget *middle_start;
  GtkWidget *middle_end;
  GtkWidget *bottom_start;
  GtkWidget *bottom_middle;
  GtkWidget *bottom_end;
};

G_DEFINE_TYPE (DraftCanvasSelection, draft_canvas_selection, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_ITEM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
draft_canvas_selection_dispose (GObject *object)
{
  DraftCanvasSelection *self = (DraftCanvasSelection *)object;

  g_clear_object (&self->item);
  g_clear_pointer (&self->grid, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_canvas_selection_parent_class)->dispose (object);
}

static void
draft_canvas_selection_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  DraftCanvasSelection *self = DRAFT_CANVAS_SELECTION (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, draft_canvas_selection_get_item (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_selection_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  DraftCanvasSelection *self = DRAFT_CANVAS_SELECTION (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      draft_canvas_selection_set_item (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_canvas_selection_class_init (DraftCanvasSelectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_canvas_selection_dispose;
  object_class->get_property = draft_canvas_selection_get_property;
  object_class->set_property = draft_canvas_selection_set_property;

  properties [PROP_ITEM] =
    g_param_spec_object ("item",
                         "Item",
                         "The selected DraftItem",
                         DRAFT_TYPE_ITEM,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-canvas-selection.ui");
  gtk_widget_class_set_css_name (widget_class, "draftcanvasselection");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, grid);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, box);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, top_start);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, top_middle);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, top_end);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, middle_start);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, middle_end);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, bottom_start);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, bottom_middle);
  gtk_widget_class_bind_template_child (widget_class, DraftCanvasSelection, bottom_end);

  g_type_ensure (DRAFT_TYPE_CANVAS_HANDLE);
}

static void
draft_canvas_selection_init (DraftCanvasSelection *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  draft_canvas_selection_set_mode (self, DRAFT_CANVAS_SELECTION_ITEM);
}

DraftCanvasSelection *
draft_canvas_selection_new (void)
{
  return g_object_new (DRAFT_TYPE_CANVAS_SELECTION, NULL);
}

/**
 * draft_canvas_selection_get_item:
 * @self: a #DraftCanvasSelection
 *
 * Gets the selected item>
 *
 * Returns: (transfer none) (nullable): a #DraftItem or %NULL
 */
DraftItem *
draft_canvas_selection_get_item (DraftCanvasSelection *self)
{
  return self->item;
}

void
draft_canvas_selection_set_item (DraftCanvasSelection *self,
                                 DraftItem            *item)
{
  g_return_if_fail (DRAFT_IS_CANVAS_SELECTION (self));
  g_return_if_fail (!item || DRAFT_IS_ITEM (item));

  if (g_set_object (&self->item, item))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ITEM]);
}

void
draft_canvas_selection_set_size (DraftCanvasSelection *self,
                                 int                   width,
                                 int                   height)
{
  g_return_if_fail (DRAFT_IS_CANVAS_SELECTION (self));

  gtk_widget_set_size_request (GTK_WIDGET (self->box), width, height);
}

void
draft_canvas_selection_get_offset (DraftCanvasSelection *self,
                                   int                  *x_offset,
                                   int                  *y_offset)
{
  g_return_if_fail (DRAFT_IS_CANVAS_SELECTION (self));
  g_return_if_fail (x_offset != NULL);
  g_return_if_fail (y_offset != NULL);

  gtk_widget_measure (GTK_WIDGET (self->top_start),
                      GTK_ORIENTATION_HORIZONTAL,
                      -1, x_offset, NULL, NULL, NULL);
  gtk_widget_measure (GTK_WIDGET (self->top_start),
                      GTK_ORIENTATION_VERTICAL,
                      -1, y_offset, NULL, NULL, NULL);
}

void
draft_canvas_selection_set_mode (DraftCanvasSelection     *self,
                                 DraftCanvasSelectionMode  mode)
{
  gboolean show_handles;

  g_return_if_fail (DRAFT_IS_CANVAS_SELECTION (self));

  show_handles = mode == DRAFT_CANVAS_SELECTION_RESIZE;

  gtk_widget_set_visible (self->top_start, show_handles);
  gtk_widget_set_visible (self->top_middle, show_handles);
  gtk_widget_set_visible (self->top_end, show_handles);
  gtk_widget_set_visible (self->middle_start, show_handles);
  gtk_widget_set_visible (self->middle_end, show_handles);
  gtk_widget_set_visible (self->bottom_start, show_handles);
  gtk_widget_set_visible (self->bottom_middle, show_handles);
  gtk_widget_set_visible (self->bottom_end, show_handles);
}
