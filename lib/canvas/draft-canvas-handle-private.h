/* draft-canvas-handle-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_CANVAS_HANDLE (draft_canvas_handle_get_type())

typedef enum
{
  DRAFT_CANVAS_HANDLE_TOP_START,
  DRAFT_CANVAS_HANDLE_TOP_MIDDLE,
  DRAFT_CANVAS_HANDLE_TOP_END,
  DRAFT_CANVAS_HANDLE_MIDDLE_START,
  DRAFT_CANVAS_HANDLE_MIDDLE_END,
  DRAFT_CANVAS_HANDLE_BOTTOM_START,
  DRAFT_CANVAS_HANDLE_BOTTOM_MIDDLE,
  DRAFT_CANVAS_HANDLE_BOTTOM_END,
} DraftCanvasHandlePosition;

G_DECLARE_FINAL_TYPE (DraftCanvasHandle, draft_canvas_handle, DRAFT, CANVAS_HANDLE, GtkWidget)

G_END_DECLS
