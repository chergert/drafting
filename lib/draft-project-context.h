/* draft-project-context.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_PROJECT_CONTEXT (draft_project_context_get_type())

DRAFT_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (DraftProjectContext, draft_project_context, DRAFT, PROJECT_CONTEXT, GObject)

struct _DraftProjectContextClass
{
  GObjectClass parent_class;

  /*< private >*/
  gpointer _reserved[8];
};

DRAFT_AVAILABLE_IN_ALL
DraftProjectContext *draft_project_context_new                    (void);
DRAFT_AVAILABLE_IN_ALL
const char * const  *draft_project_context_get_available_themes   (DraftProjectContext *self);
DRAFT_AVAILABLE_IN_ALL
const char          *draft_project_context_get_theme              (DraftProjectContext *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_context_set_theme              (DraftProjectContext *self,
                                                                   const char          *theme);
DRAFT_AVAILABLE_IN_ALL
const char * const  *draft_project_context_get_typelib_dirs       (DraftProjectContext *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_context_set_typelib_dirs       (DraftProjectContext *self,
                                                                   const char * const  *typelib_dirs);
DRAFT_AVAILABLE_IN_ALL
const char * const  *draft_project_context_get_command_prefix     (DraftProjectContext *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_context_set_command_prefix     (DraftProjectContext *self,
                                                                   const char * const  *command_prefix);
DRAFT_AVAILABLE_IN_ALL
GDBusConnection     *draft_project_context_get_service_connection (DraftProjectContext *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_context_set_service_connection (DraftProjectContext *self,
                                                                   GDBusConnection     *connection);

G_END_DECLS
