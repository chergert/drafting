/* draft-project.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_PROJECT (draft_project_get_type())

DRAFT_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DraftProject, draft_project, DRAFT, PROJECT, GObject)

DRAFT_AVAILABLE_IN_ALL
DraftProject        *draft_project_new              (void);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_load_file_async  (DraftProject         *self,
                                                     GFile                *file,
                                                     GCancellable         *cancellable,
                                                     GAsyncReadyCallback   callback,
                                                     gpointer              user_data);
DRAFT_AVAILABLE_IN_ALL
DraftDocument       *draft_project_load_file_finish (DraftProject         *self,
                                                     GAsyncResult         *result,
                                                     GError              **error);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_unload_file      (DraftProject         *self,
                                                     GFile                *file);
DRAFT_AVAILABLE_IN_ALL
gboolean             draft_project_get_can_undo     (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
gboolean             draft_project_get_can_redo     (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_redo             (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_undo             (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
DraftTransaction    *draft_project_begin            (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_commit           (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_rollback         (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
DraftProjectContext *draft_project_get_context      (DraftProject         *self);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_set_context      (DraftProject         *self,
                                                     DraftProjectContext  *context);
DRAFT_AVAILABLE_IN_ALL
void                 draft_project_clear_selection  (DraftProject         *self);

G_END_DECLS
