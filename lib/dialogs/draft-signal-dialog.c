/* draft-signal-dialog.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-project.h"
#include "dialogs/draft-signal-dialog-private.h"
#include "introspection/draft-type-info-private.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-signal.h"
#include "undo/draft-transaction.h"
#include "util/draft-object-model-private.h"

struct _DraftSignalDialog
{
  GtkWindow     parent_instance;

  DraftSignal  *signal;
  DraftProject *project;
  DraftObject  *object;

  GtkWidget    *box;
  GtkButton    *button_cancel;
  GtkButton    *button_apply;
  GtkDropDown  *object_drop_down;
  GtkDropDown  *signal_drop_down;
  GtkSwitch    *after_switch;
  GtkSwitch    *swap_switch;
  GtkEntry     *handler_entry;
  GtkEntry     *detail_entry;

  bool          changed_handler;
  bool          block_changed_handler;
};

G_DEFINE_TYPE (DraftSignalDialog, draft_signal_dialog, GTK_TYPE_WINDOW)

enum {
  PROP_0,
  PROP_OBJECT,
  PROP_PROJECT,
  PROP_SIGNAL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DraftSignalDialog *
draft_signal_dialog_new (GtkWindow *transient_for)
{
  g_return_val_if_fail (!transient_for || GTK_IS_WINDOW (transient_for), NULL);

  return g_object_new (DRAFT_TYPE_SIGNAL_DIALOG,
                       "transient-for", transient_for,
                       NULL);
}

static void
draft_signal_dialog_apply (GtkWidget  *widget,
                           const char *action_name,
                           GVariant   *param)
{
  DraftSignalDialog *self = (DraftSignalDialog *)widget;
  DraftTransaction *transaction = NULL;
  const char *object_id = NULL;
  DraftObject *object = NULL;
  GtkStringObject *name = NULL;

  g_assert (DRAFT_IS_SIGNAL_DIALOG (self));
  g_assert (DRAFT_IS_PROJECT (self->project));
  g_assert (DRAFT_IS_SIGNAL (self->signal));

  transaction = draft_project_begin (self->project);

  if ((object = gtk_drop_down_get_selected_item (self->object_drop_down)))
    {
      if (draft_object_get_is_template (object))
        object_id = draft_object_get_class_name (object);
      else
        {
          /* If this object doesn't have an object-id yet, we need
           * to dynamically assign one so we can reference it.
           */
          if (!(object_id = draft_item_get_id (DRAFT_ITEM (object))))
            object_id = _draft_item_generate_id (DRAFT_ITEM (object), transaction);
        }
    }

  name = gtk_drop_down_get_selected_item (self->signal_drop_down);

  if (draft_item_get_parent (DRAFT_ITEM (self->signal)) == NULL)
    draft_transaction_insert_after (transaction,
                                    DRAFT_ITEM (self->signal),
                                    DRAFT_ITEM (self->object),
                                    NULL);

  draft_transaction_set_item (transaction, DRAFT_ITEM (self->signal),
                              "name", gtk_string_object_get_string (name),
                              "detail", gtk_editable_get_text (GTK_EDITABLE (self->detail_entry)),
                              "handler", gtk_editable_get_text (GTK_EDITABLE (self->handler_entry)),
                              "object", object_id,
                              "after", gtk_switch_get_active (self->after_switch),
                              "swapped", gtk_switch_get_active (self->swap_switch),
                              NULL);

  draft_project_commit (self->project);

  gtk_window_destroy (GTK_WINDOW (widget));
}

static void
draft_signal_dialog_cancel (GtkWidget  *widget,
                            const char *action_name,
                            GVariant   *param)
{
  g_assert (DRAFT_IS_SIGNAL_DIALOG (widget));

  gtk_window_destroy (GTK_WINDOW (widget));
}

static void
draft_signal_dialog_dispose (GObject *object)
{
  DraftSignalDialog *self = (DraftSignalDialog *)object;

  g_clear_object (&self->project);
  g_clear_object (&self->signal);
  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (draft_signal_dialog_parent_class)->dispose (object);
}

static gboolean
validate_handler (const char *word)
{
  if (word == NULL || *word == 0)
    return FALSE;

  /* ascii iteration is fine */
  for (const char *c = word; *c; c++)
    {
      if (g_ascii_isalnum (*c) || *c == '_')
        continue;

      /* We might need to support things like . and : for
       * some languages beyond C callbacks at some point.
       */
      return FALSE;
    }

  return TRUE;
}

static void
make_valid (char *word)
{
  if (word == NULL || *word == 0)
    return;

  /* ascii iteration is fine */
  for (char *c = word; *c; c++)
    {
      if (g_ascii_isalnum (*c) || *c == '_')
        continue;

      *c = '_';
    }
}

static gboolean
validate_signal (const char *name,
                 const char *detail)
{
  g_autofree char *full = NULL;

  if (!g_signal_is_valid_name (name))
    return FALSE;

  if (detail == NULL || *detail == 0)
    return TRUE;

  for (const char *c = detail; *c; c++)
    {
      if (g_ascii_isalnum (*c) || *c == '_' || *c == '-')
        continue;

      return FALSE;
    }

  return TRUE;
}

static void
draft_signal_dialog_update (DraftSignalDialog *self)
{
  GtkStringObject *string;
  const char *handler;
  const char *detail;
  gboolean valid;

  g_assert (DRAFT_IS_SIGNAL_DIALOG (self));

  if(!self->block_changed_handler)
    self->changed_handler = (gtk_entry_get_text_length(self->handler_entry) != 0);

  handler = gtk_editable_get_text (GTK_EDITABLE (self->handler_entry));
  detail = gtk_editable_get_text (GTK_EDITABLE (self->detail_entry));
  string = gtk_drop_down_get_selected_item (self->signal_drop_down);
  valid = self->project != NULL &&
          self->signal != NULL &&
          string != NULL;

  if (!validate_handler (handler))
    {
      gtk_widget_add_css_class (GTK_WIDGET (self->handler_entry), "error");
      valid = FALSE;
    }
  else
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self->handler_entry), "error");
    }

  if (string == NULL ||
      !validate_signal (gtk_string_object_get_string (string), detail))
    {
      gtk_widget_add_css_class (GTK_WIDGET (self->detail_entry), "error");
      gtk_widget_add_css_class (GTK_WIDGET (self->signal_drop_down), "error");
      valid = FALSE;
    }
  else
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self->detail_entry), "error");
      gtk_widget_remove_css_class (GTK_WIDGET (self->signal_drop_down), "error");
    }

  gtk_widget_action_set_enabled (GTK_WIDGET (self), "dialog.apply", valid);
}

static void
draft_signal_dialog_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DraftSignalDialog *self = DRAFT_SIGNAL_DIALOG (object);

  switch (prop_id)
    {
    case PROP_OBJECT:
      g_value_set_object (value, draft_signal_dialog_get_object (self));
      break;

    case PROP_PROJECT:
      g_value_set_object (value, draft_signal_dialog_get_project (self));
      break;

    case PROP_SIGNAL:
      g_value_set_object (value, draft_signal_dialog_get_signal (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
signal_drop_down_changed_cb (GtkDropDown *signal_drop_down,
                             GParamSpec  *pspec,
                             gpointer     user_data)
{
  DraftSignalDialog *self = DRAFT_SIGNAL_DIALOG (user_data);
  GtkStringObject *name = NULL;
  g_autofree char *suggested_name = NULL;
  const char *sender_name;
  const char *object_name;

  g_assert (GTK_IS_DROP_DOWN (signal_drop_down));
  g_assert (DRAFT_IS_SIGNAL_DIALOG (self));
  g_assert (DRAFT_IS_OBJECT (self->object));

  if (self->changed_handler)
    return;

  name = gtk_drop_down_get_selected_item (self->signal_drop_down);
  sender_name = draft_item_get_id (DRAFT_ITEM (self->object));
  object_name =  gtk_string_object_get_string (name);

  /* If sender name is empty, choose a simpler suggestion */
  /* Else, we'll use a more complete suggestion */
  if (sender_name == NULL)
      suggested_name = g_strdup_printf ("on_%s_cb", object_name);
  else
      suggested_name = g_strdup_printf ("on_%s_%s_cb", sender_name, object_name);

  if (suggested_name)
    {
      make_valid(suggested_name);
      self->block_changed_handler = TRUE;
      gtk_editable_set_text (GTK_EDITABLE (self->handler_entry), suggested_name);
      self->block_changed_handler = FALSE;
    }
}

static void
draft_signal_dialog_class_init (DraftSignalDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = draft_signal_dialog_dispose;
  object_class->get_property = draft_signal_dialog_get_property;

  properties [PROP_OBJECT] =
    g_param_spec_object ("object",
                         "Object",
                         "Object",
                         DRAFT_TYPE_OBJECT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_PROJECT] =
    g_param_spec_object ("project",
                         "Project",
                         "Project",
                         DRAFT_TYPE_PROJECT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_SIGNAL] =
    g_param_spec_object ("signal",
                         "Signal",
                         "The signal to be edited",
                         DRAFT_TYPE_SIGNAL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/drafting/ui/draft-signal-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, box);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, button_apply);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, button_cancel);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, detail_entry);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, signal_drop_down);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, object_drop_down);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, after_switch);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, swap_switch);
  gtk_widget_class_bind_template_child (widget_class, DraftSignalDialog, handler_entry);
  gtk_widget_class_bind_template_callback (widget_class, draft_signal_dialog_update);

  gtk_widget_class_install_action (widget_class, "dialog.apply", NULL, draft_signal_dialog_apply);
  gtk_widget_class_install_action (widget_class, "dialog.cancel", NULL, draft_signal_dialog_cancel);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Escape, 0, "dialog.cancel", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Return, GDK_CONTROL_MASK, "dialog.apply", NULL);
}

static void
draft_signal_dialog_init (DraftSignalDialog *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->changed_handler = FALSE;
  self->block_changed_handler = FALSE;

  gtk_widget_action_set_enabled (GTK_WIDGET (self), "dialog.apply", FALSE);
}

static void
draft_signal_dialog_set_class_info (DraftSignalDialog *self,
                                    DraftTypeInfo     *type_info)
{
  g_autoptr(GtkStringList) list = NULL;
  DraftClassInfo *class_info = (DraftClassInfo *)type_info;

  g_return_if_fail (DRAFT_IS_SIGNAL_DIALOG (self));
  g_return_if_fail (class_info != NULL);
  g_return_if_fail (DRAFT_TYPE_INFO_IS_CLASS (type_info));

  if (class_info->signals == NULL || class_info->signals->len == 0)
    return;

  list = gtk_string_list_new (NULL);

  for (guint i = 0; i < class_info->signals->len; i++)
    {
      DraftSignalInfo *info = g_ptr_array_index (class_info->signals, i);

      gtk_string_list_append (list, info->name);
    }

  gtk_drop_down_set_model (self->signal_drop_down, G_LIST_MODEL (list));
}

void
draft_signal_dialog_configure (DraftSignalDialog *self,
                               DraftProject      *project,
                               DraftObject       *object,
                               DraftSignal       *signal)
{
  g_autoptr(DraftObjectModel) objects = NULL;
  DraftDocument *document;
  const char *handler;
  const char *detail;
  const char *name;
  GListModel *model;
  guint n_items;
  int object_position;

  g_return_if_fail (DRAFT_IS_SIGNAL_DIALOG (self));
  g_return_if_fail (DRAFT_IS_PROJECT (project));
  g_return_if_fail (DRAFT_IS_OBJECT (object));
  g_return_if_fail (DRAFT_IS_SIGNAL (signal));

  g_set_object (&self->project, project);
  g_set_object (&self->object, object);
  g_set_object (&self->signal, signal);

  draft_signal_dialog_set_class_info (self, DRAFT_ITEM (object)->type_info);

  name = draft_signal_get_name (signal);
  model = gtk_drop_down_get_model (self->signal_drop_down);
  n_items = g_list_model_get_n_items (model);
  document = draft_item_get_document (DRAFT_ITEM (object));
  objects = draft_object_model_new (DRAFT_ITEM (document));
  object_position = draft_object_model_find (objects, draft_signal_get_object (signal));
  handler = draft_signal_get_handler (signal);
  detail = draft_signal_get_detail (signal);

  if (handler != NULL && handler[0] != '\0')
    self->changed_handler = TRUE;

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(GtkStringObject) item = g_list_model_get_item (model, i);

      if (g_strcmp0 (name, gtk_string_object_get_string (item)) == 0)
        {
          gtk_drop_down_set_selected (self->signal_drop_down, i);
          break;
        }
    }

  gtk_switch_set_active (self->after_switch, draft_signal_get_after (signal));
  gtk_switch_set_active (self->swap_switch, draft_signal_get_swapped (signal));
  gtk_editable_set_text (GTK_EDITABLE (self->handler_entry), handler ? handler : "");
  gtk_editable_set_text (GTK_EDITABLE (self->detail_entry), detail ? detail : "");
  gtk_drop_down_set_model (self->object_drop_down, G_LIST_MODEL (objects));

  if (object_position >= 0)
    gtk_drop_down_set_selected (self->object_drop_down, object_position);
  else
    gtk_drop_down_set_selected (self->object_drop_down, GTK_INVALID_LIST_POSITION);

  draft_signal_dialog_update (self);

  g_signal_connect_object (self->signal_drop_down,
                           "notify::selected",
                           G_CALLBACK (signal_drop_down_changed_cb),
                           self,
                           G_CONNECT_AFTER);
}

DraftSignal *
draft_signal_dialog_get_signal (DraftSignalDialog *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL_DIALOG (self), NULL);

  return self->signal;
}

DraftProject *
draft_signal_dialog_get_project (DraftSignalDialog *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL_DIALOG (self), NULL);
  return self->project;
}

DraftObject *
draft_signal_dialog_get_object (DraftSignalDialog *self)
{
  g_return_val_if_fail (DRAFT_IS_SIGNAL_DIALOG (self), NULL);

  return self->object;
}
