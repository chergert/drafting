/* draft-signal-dialog-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_SIGNAL_DIALOG (draft_signal_dialog_get_type())

G_DECLARE_FINAL_TYPE (DraftSignalDialog, draft_signal_dialog, DRAFT, SIGNAL_DIALOG, GtkWindow)

DraftSignalDialog *draft_signal_dialog_new            (GtkWindow         *transient_for);
void               draft_signal_dialog_configure      (DraftSignalDialog *self,
                                                       DraftProject      *project,
                                                       DraftObject       *object,
                                                       DraftSignal       *signal);
DraftProject      *draft_signal_dialog_get_project    (DraftSignalDialog *self);
DraftObject       *draft_signal_dialog_get_object     (DraftSignalDialog *self);
DraftSignal       *draft_signal_dialog_get_signal     (DraftSignalDialog *self);

G_END_DECLS
