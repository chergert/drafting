/* draft-history.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-private.h"
#include "undo/draft-history-private.h"
#include "undo/draft-transaction-private.h"

struct _DraftHistory
{
  GObject parent_instance;

  DraftProject *project;

  GQueue undo;
  GQueue redo;
};

G_DEFINE_TYPE (DraftHistory, draft_history, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CAN_UNDO,
  PROP_CAN_REDO,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
draft_history_finalize (GObject *object)
{
  DraftHistory *self = (DraftHistory *)object;
  DraftTransaction *transaction;

  while ((transaction = g_queue_pop_head (&self->redo)))
    g_object_unref (transaction);

  while ((transaction = g_queue_pop_tail (&self->undo)))
    g_object_unref (transaction);

  G_OBJECT_CLASS (draft_history_parent_class)->finalize (object);
}

static void
draft_history_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  DraftHistory *self = DRAFT_HISTORY (object);

  switch (prop_id)
    {
    case PROP_CAN_UNDO:
      g_value_set_boolean (value, _draft_history_get_can_undo (self));
      break;

    case PROP_CAN_REDO:
      g_value_set_boolean (value, _draft_history_get_can_redo (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_history_class_init (DraftHistoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_history_finalize;
  object_class->get_property = draft_history_get_property;

  properties [PROP_CAN_REDO] =
    g_param_spec_boolean ("can-redo",
                          "Can Redo",
                          "Can Redo",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_CAN_UNDO] =
    g_param_spec_boolean ("can-undo",
                          "Can Undo",
                          "Can Undo",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
draft_history_init (DraftHistory *self)
{
}

DraftHistory *
_draft_history_new (DraftProject *project)
{
  DraftHistory *self;

  g_return_val_if_fail (DRAFT_IS_PROJECT (project), NULL);

  self = g_object_new (DRAFT_TYPE_HISTORY, NULL);
  self->project = project;

  return self;
}

gboolean
_draft_history_get_can_undo (DraftHistory *self)
{
  g_return_val_if_fail (DRAFT_IS_HISTORY (self), FALSE);

  return self->undo.length > 0;
}

gboolean
_draft_history_get_can_redo (DraftHistory *self)
{
  g_return_val_if_fail (DRAFT_IS_HISTORY (self), FALSE);

  return self->redo.length > 0;
}

void
_draft_history_undo (DraftHistory *self)
{
  DraftTransaction *transaction;
  gboolean can_undo;
  gboolean can_redo;

  g_return_if_fail (DRAFT_IS_HISTORY (self));
  g_return_if_fail (_draft_history_get_can_undo (self));

  can_undo = _draft_history_get_can_undo (self);
  can_redo = _draft_history_get_can_redo (self);

  if (!(transaction = g_queue_pop_tail (&self->undo)))
    g_return_if_reached ();

  g_queue_push_head (&self->redo, transaction);

  _draft_transaction_revert (transaction);

  DRAFT_OBSERVERS_FOREACH (self->project, {
    _draft_observer_revert (observer, transaction);
  });

  if (can_undo != _draft_history_get_can_undo (self))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_UNDO]);

  if (can_redo != _draft_history_get_can_redo (self))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_REDO]);
}

void
_draft_history_redo (DraftHistory *self)
{
  DraftTransaction *transaction;
  gboolean can_undo;
  gboolean can_redo;

  g_return_if_fail (DRAFT_IS_HISTORY (self));
  g_return_if_fail (_draft_history_get_can_redo (self));

  can_undo = _draft_history_get_can_undo (self);
  can_redo = _draft_history_get_can_redo (self);

  if (!(transaction = g_queue_pop_head (&self->redo)))
    g_return_if_reached ();

  g_queue_push_tail (&self->undo, transaction);

  _draft_transaction_apply (transaction);

  DRAFT_OBSERVERS_FOREACH (self->project, {
    _draft_observer_apply (observer, transaction);
  });

  if (can_undo != _draft_history_get_can_undo (self))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_UNDO]);

  if (can_redo != _draft_history_get_can_redo (self))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_REDO]);
}

void
_draft_history_push (DraftHistory  *self,
                     DraftTransaction *transaction)
{
  DraftTransaction *iter;
  gboolean alter_undo;
  gboolean alter_redo;

  g_return_if_fail (DRAFT_IS_HISTORY (self));
  g_return_if_fail (DRAFT_IS_TRANSACTION (transaction));

  alter_undo = self->undo.length == 0;
  alter_redo = self->redo.length > 0;

  while ((iter = g_queue_pop_head (&self->redo)))
    g_object_unref (iter);

  _draft_transaction_freeze (transaction);
  g_queue_push_tail (&self->undo, g_object_ref (transaction));

  DRAFT_OBSERVERS_FOREACH (self->project, {
    _draft_observer_clear_redo_history (observer);
    _draft_observer_apply (observer, transaction);
  });

  if (alter_undo)
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_UNDO]);

  if (alter_redo)
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_REDO]);
}
