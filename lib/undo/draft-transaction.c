/* draft-transaction.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <glib-object.h>
#include <gobject/gvaluecollector.h>
#include <glib/gi18n.h>

#include "items/draft-document.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-property-private.h"
#include "undo/draft-transaction-private.h"

struct _DraftTransaction
{
  GObject parent_instance;
  GArray *ops;
  GSList *documents;
  guint frozen : 1;
};

typedef enum
{
  OP_INSERT_AFTER = 1,
  OP_INSERT_BEFORE,
  OP_REMOVE,
  OP_SET_PROPERTY,
} OpKind;

typedef struct
{
  int kind : 8;
  int document : 8;
  int padding : 16;
  union {
    struct {
      DraftItem *item;
      DraftItem *parent;
      DraftItem *sibling;
    } tree;
    struct {
      DraftItem  *item;
      const char *name;
      GValue      old_value;
      GValue      new_value;
    } property;
  };
} Op;

G_DEFINE_TYPE (DraftTransaction, draft_transaction, G_TYPE_OBJECT)

static int
draft_transaction_add_document (DraftTransaction *self,
                                DraftItem        *item)
{
  DraftDocument *document;
  int pos = 0;

  g_assert (DRAFT_IS_TRANSACTION (self));
  g_assert (DRAFT_IS_ITEM (item));

  if (!(document = draft_item_get_document (item)))
    return -1;

  for (const GSList *iter = self->documents; iter; iter = iter->next)
    {
      if (iter->data == (gpointer)document)
        return pos;
      pos++;
    }

  self->documents = g_slist_append (self->documents, document);

  return pos;
}

static void
op_clear (Op *op)
{
  switch (op->kind)
    {
    case OP_INSERT_AFTER:
    case OP_INSERT_BEFORE:
    case OP_REMOVE:
      g_clear_object (&op->tree.item);
      g_clear_object (&op->tree.parent);
      g_clear_object (&op->tree.sibling);
      break;

    case OP_SET_PROPERTY:
      g_clear_object (&op->property.item);
      op->property.name = NULL;
      g_value_unset (&op->property.old_value);
      g_value_unset (&op->property.new_value);
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
items_changed_in_parent (DraftItem *item)
{
  DraftItem *parent = draft_item_get_parent (item);
  guint position = 0;

  if (parent == NULL)
    return;

  for (DraftItem *child = draft_item_get_first_child (parent);
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (child == item)
        break;

      position++;
    }

  g_list_model_items_changed (G_LIST_MODEL (parent), position, 1, 1);
}

static void
op_apply (const Op *op)
{
  switch (op->kind)
    {
    case OP_INSERT_AFTER:
      _draft_item_insert_after (g_object_ref (op->tree.item),
                                op->tree.parent,
                                op->tree.sibling);
      break;

    case OP_INSERT_BEFORE:
      _draft_item_insert_before (g_object_ref (op->tree.item),
                                 op->tree.parent,
                                 op->tree.sibling);
      break;

    case OP_REMOVE:
      _draft_item_unparent (op->tree.item);
      break;

    case OP_SET_PROPERTY:
      g_object_set_property (G_OBJECT (op->property.item),
                             op->property.name,
                             &op->property.new_value);
      items_changed_in_parent (op->property.item);
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
op_revert (const Op *op)
{
  switch (op->kind)
    {
    case OP_INSERT_AFTER:
    case OP_INSERT_BEFORE:
      _draft_item_unparent (op->tree.item);
      break;

    case OP_REMOVE:
      if (op->tree.sibling == NULL)
        _draft_item_insert_after (g_object_ref (op->tree.item),
                                  op->tree.parent,
                                  NULL);
      else
        _draft_item_insert_before (g_object_ref (op->tree.item),
                                   op->tree.parent,
                                   op->tree.sibling);
      break;

    case OP_SET_PROPERTY:
      g_object_set_property (G_OBJECT (op->property.item),
                             op->property.name,
                             &op->property.old_value);
      items_changed_in_parent (op->property.item);
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
draft_transaction_finalize (GObject *object)
{
  DraftTransaction *self = (DraftTransaction *)object;

  g_clear_pointer (&self->ops, g_array_unref);
  g_clear_pointer (&self->documents, g_slist_free);

  G_OBJECT_CLASS (draft_transaction_parent_class)->finalize (object);
}

static void
draft_transaction_class_init (DraftTransactionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_transaction_finalize;
}

static void
draft_transaction_init (DraftTransaction *self)
{
  self->ops = g_array_new (FALSE, TRUE, sizeof (Op));
  g_array_set_clear_func (self->ops, (GDestroyNotify)op_clear);
}

DraftTransaction *
_draft_transaction_new (void)
{
  return g_object_new (DRAFT_TYPE_TRANSACTION, NULL);
}

void
_draft_transaction_apply (DraftTransaction *self)
{
  g_return_if_fail (DRAFT_IS_TRANSACTION (self));

  for (guint i = 0; i < self->ops->len; i++)
    {
      const Op *op = &g_array_index (self->ops, Op, i);
      op_apply (op);
    }
}

void
_draft_transaction_revert (DraftTransaction *self)
{
  g_return_if_fail (DRAFT_IS_TRANSACTION (self));

  for (guint i = self->ops->len; i > 0; i--)
    {
      const Op *op = &g_array_index (self->ops, Op, i-1);
      op_revert (op);
    }
}

static inline Op *
draft_transaction_append (DraftTransaction *self)
{
  g_array_set_size (self->ops, self->ops->len + 1);
  return &g_array_index (self->ops, Op, self->ops->len - 1);
}

void
draft_transaction_insert_before (DraftTransaction *self,
                                 DraftItem        *item,
                                 DraftItem        *parent,
                                 DraftItem        *next_sibling)
{
  Op *op;

  g_return_if_fail (DRAFT_IS_TRANSACTION (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (!next_sibling || DRAFT_IS_ITEM (next_sibling));
  g_return_if_fail (self->frozen == FALSE);

  op = draft_transaction_append (self);
  op->document = draft_transaction_add_document (self, parent);
  op->kind = OP_INSERT_BEFORE;
  g_set_object (&op->tree.item, item);
  g_set_object (&op->tree.parent, parent);
  g_set_object (&op->tree.sibling, next_sibling);

  op_apply (op);
}

void
draft_transaction_insert_after (DraftTransaction *self,
                                DraftItem        *item,
                                DraftItem        *parent,
                                DraftItem        *previous_sibling)
{
  Op *op;

  g_return_if_fail (DRAFT_IS_TRANSACTION (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (!previous_sibling || DRAFT_IS_ITEM (previous_sibling));
  g_return_if_fail (self->frozen == FALSE);

  op = draft_transaction_append (self);
  op->document = draft_transaction_add_document (self, parent);
  op->kind = OP_INSERT_AFTER;
  g_set_object (&op->tree.item, item);
  g_set_object (&op->tree.parent, parent);
  g_set_object (&op->tree.sibling, previous_sibling);

  op_apply (op);
}

void
draft_transaction_remove (DraftTransaction *self,
                          DraftItem        *item)
{
  Op *op;

  g_return_if_fail (DRAFT_IS_TRANSACTION (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (self->frozen == FALSE);

  op = draft_transaction_append (self);
  op->document = draft_transaction_add_document (self, item);
  op->kind = OP_REMOVE;
  g_set_object (&op->tree.item, item);
  g_set_object (&op->tree.parent, draft_item_get_parent (item));
  g_set_object (&op->tree.sibling, draft_item_get_next_sibling (item));

  op_apply (op);
}

static gboolean
value_equal (const GValue *src_value,
             const GValue *dst_value)
{
  if (G_VALUE_TYPE (src_value) != G_VALUE_TYPE (dst_value))
    return FALSE;

  if (G_VALUE_HOLDS_STRING (src_value))
    return g_strcmp0 (g_value_get_string (src_value),
                      g_value_get_string (dst_value)) == 0;

  if (G_VALUE_HOLDS_BOOLEAN (src_value))
    return g_value_get_boolean (src_value) == g_value_get_boolean (dst_value);

  if (G_VALUE_HOLDS_INT (src_value))
    return g_value_get_int (src_value) == g_value_get_int (dst_value);

  if (G_VALUE_HOLDS_UINT (src_value))
    return g_value_get_uint (src_value) == g_value_get_uint (dst_value);

  if (G_VALUE_HOLDS_INT64 (src_value))
    return g_value_get_int64 (src_value) == g_value_get_int64 (dst_value);

  if (G_VALUE_HOLDS_UINT64 (src_value))
    return g_value_get_uint64 (src_value) == g_value_get_uint64 (dst_value);

  if (G_VALUE_HOLDS_DOUBLE (src_value))
    return g_value_get_double (src_value) == g_value_get_double (dst_value);

  if (G_VALUE_HOLDS_FLOAT (src_value))
    return g_value_get_float (src_value) == g_value_get_float (dst_value);

  return FALSE;
}

static void
draft_transaction_set_item_with_pspec (DraftTransaction *self,
                                       DraftItem        *item,
                                       GParamSpec       *pspec,
                                       const GValue     *value)
{
  Op *op;

  g_assert (DRAFT_IS_TRANSACTION (self));
  g_assert (DRAFT_IS_ITEM (item));
  g_assert (pspec != NULL);
  g_assert (value != NULL);
  g_assert (self->frozen == FALSE);

  op = draft_transaction_append (self);
  op->document = draft_transaction_add_document (self, item);
  op->kind = OP_SET_PROPERTY;
  op->property.name = g_intern_string (pspec->name);
  g_set_object (&op->property.item, item);

  g_value_init (&op->property.old_value, pspec->value_type);
  g_value_init (&op->property.new_value, pspec->value_type);

  g_object_get_property (G_OBJECT (item), pspec->name, &op->property.old_value);
  g_value_copy (value, &op->property.new_value);

  /* If there is no change, avoid extra operations and just silently
   * drop the modification. We'll detect this later with a
   * draft_transaction_is_empty() call when committing the transaction.
   */
  if (value_equal (&op->property.new_value, &op->property.old_value))
    {
      g_value_unset (&op->property.new_value);
      g_value_unset (&op->property.old_value);
      g_clear_object (&op->property.item);
      self->ops->len--;
      return;
    }

  op_apply (op);
}

void
draft_transaction_set_item_property (DraftTransaction *self,
                                     DraftItem        *item,
                                     const char       *property_name,
                                     const GValue     *value)
{
  GParamSpec *pspec;

  g_return_if_fail (DRAFT_IS_TRANSACTION (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (value != NULL);
  g_return_if_fail (self->frozen == FALSE);

  if (!(pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (item), property_name)))
    g_warning ("Failed to locate property \"%s\" of type \"%s\"",
               property_name, G_OBJECT_TYPE_NAME (item));
  else
    draft_transaction_set_item_with_pspec (self, item, pspec, value);
}

void
draft_transaction_set_item (DraftTransaction *self,
                            DraftItem     *item,
                            const char    *first_property,
                            ...)
{
  const char *property_name = first_property;
  GObjectClass *object_class;
  va_list args;

  g_return_if_fail (DRAFT_IS_TRANSACTION (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));
  g_return_if_fail (first_property != NULL);
  g_return_if_fail (self->frozen == FALSE);

  object_class = G_OBJECT_GET_CLASS (item);

  va_start (args, first_property);

  while (property_name != NULL)
    {
      GParamSpec *pspec;
      GValue v = G_VALUE_INIT;
      char *errmsg = NULL;

      if (!(pspec = g_object_class_find_property (object_class, property_name)))
        {
          g_warning ("Failed to locate property \"%s\" of type \"%s\". "
                     "Ignoring all other properties too.",
                     property_name, G_OBJECT_TYPE_NAME (item));
          break;
        }

      g_value_init (&v, pspec->value_type);
      G_VALUE_COLLECT (&v, args, 0, &errmsg);

      if (errmsg != NULL)
        {
          g_warning ("Failed to collect value from va_args: %s. "
                     "Ignoring all other properties too.",
                     errmsg);
          g_free (errmsg);
          break;
        }

      draft_transaction_set_item_with_pspec (self, item, pspec, &v);
      g_value_unset (&v);

      property_name = va_arg (args, const char *);
    }

  va_end (args);
}

void
_draft_transaction_freeze (DraftTransaction *self)
{
  g_return_if_fail (DRAFT_IS_TRANSACTION (self));

  self->frozen = TRUE;
}

/*
 * _draft_transaction_is_simple:
 *
 * A transaction is simple if it only affects changing
 * properties on an existing object. This is useful to
 * determine if a short-circuit can be made to adjust an
 * existing org.gnome.Drafting.Builder service instead of
 * reloading the entire builder XML.
 */
gboolean
_draft_transaction_is_simple (DraftTransaction *self)
{
  for (guint i = 0; i < self->ops->len; i++)
    {
      const Op *op = &g_array_index (self->ops, Op, i);

      if (op->kind == OP_SET_PROPERTY)
        {
          DraftItem *parent;

          if (g_strcmp0 (op->property.name, "value") == 0 &&
              DRAFT_IS_PROPERTY (op->property.item) &&
              (parent = draft_item_get_parent (op->property.item)) &&
              DRAFT_IS_OBJECT (parent))
            continue;
        }
      else if (op->kind == OP_INSERT_AFTER || op->kind == OP_INSERT_BEFORE)
        {
          if (DRAFT_IS_OBJECT (op->tree.parent) &&
              DRAFT_IS_PROPERTY (op->tree.item) &&
              draft_item_get_first_child (op->tree.item) == NULL)
            continue;
        }

      return FALSE;
    }

  return TRUE;
}

gboolean
_draft_transaction_apply_simple (DraftTransaction  *self,
                                 DraftDocument     *document,
                                 DraftIpcBuilder   *builder,
                                 GError           **error)
{
  int docpos = 0;

  g_return_val_if_fail (DRAFT_IS_TRANSACTION (self), FALSE);
  g_return_val_if_fail (DRAFT_IS_DOCUMENT (document), FALSE);
  g_return_val_if_fail (DRAFT_IPC_IS_BUILDER (builder), FALSE);

  /* Find the document position for quick comparisons */
  for (const GSList *iter = self->documents; iter; iter = iter->next)
    {
      if (iter->data == (gpointer)document)
        break;
      docpos++;
    }

  for (guint i = 0; i < self->ops->len; i++)
    {
      const Op *op = &g_array_index (self->ops, Op, i);
      g_autoptr(GVariant) value = NULL;
      g_autoptr(GVariant) wrapped = NULL;
      const char *object_id = NULL;
      const char *name = NULL;

      if (op->document != docpos)
        continue;

      if (op->kind == OP_SET_PROPERTY)
        {
          DraftItem *parent;

          g_assert (g_strcmp0 (op->property.name, "value") == 0);
          g_assert (DRAFT_IS_PROPERTY (op->property.item));
          g_assert (DRAFT_IS_OBJECT (draft_item_get_parent (op->property.item)));

          parent = draft_item_get_parent (op->property.item);
          object_id = _draft_item_get_internal_id (parent);
          name = draft_property_get_name (DRAFT_PROPERTY (op->property.item));
          value = _draft_property_to_variant (DRAFT_PROPERTY (op->property.item));
        }
      else if (op->kind == OP_INSERT_AFTER || op->kind == OP_INSERT_BEFORE)
        {
          g_assert (DRAFT_IS_OBJECT (op->tree.parent));
          g_assert (DRAFT_IS_PROPERTY (op->tree.item));
          g_assert (draft_item_get_first_child (op->tree.item) == NULL);

          object_id = _draft_item_get_internal_id (op->tree.parent);
          name = draft_property_get_name (DRAFT_PROPERTY (op->tree.item));
          value = _draft_property_to_variant (DRAFT_PROPERTY (op->tree.item));
        }

      if (object_id == NULL || name == NULL || value == NULL)
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_NOT_SUPPORTED,
                       "Cannot serialize operation to service worker");
          return FALSE;
        }

      wrapped = g_variant_take_ref (g_variant_new_variant (value));

      if (!draft_ipc_builder_call_set_item_property_sync (builder, object_id, name, wrapped, NULL, error))
        return FALSE;
    }

  return TRUE;
}

GSList *
_draft_transaction_get_documents (DraftTransaction *self)
{
  g_return_val_if_fail (DRAFT_IS_TRANSACTION (self), NULL);

  return self->documents;
}

gboolean
draft_transaction_is_empty (DraftTransaction *self)
{
  g_return_val_if_fail (DRAFT_IS_TRANSACTION (self), FALSE);

  return self->ops->len == 0;
}

char *
_draft_transaction_get_description (DraftTransaction *self)
{
  GString *str;

  g_return_val_if_fail (DRAFT_IS_TRANSACTION (self), NULL);

  str = g_string_new (NULL);

  for (guint i = 0; i < self->ops->len; i++)
    {
      const Op *op = &g_array_index (self->ops, Op, i);

      if (op->kind == OP_SET_PROPERTY)
        {
          const char *id = draft_item_get_id (op->property.item);

          if (id != NULL)
            g_string_append_printf (str,
                                    _("Set property %s of %s\n"),
                                    op->property.name, id);
          else
            g_string_append_printf (str,
                                    _("Set property %s\n"),
                                    op->property.name);
        }
      else if (op->kind == OP_REMOVE)
        {
          const char *id = draft_item_get_id (op->tree.item);
          const char *parent_id = draft_item_get_id (op->tree.parent);

          if (id != NULL && parent_id != NULL)
            g_string_append_printf (str,
                                    _("Remove %s from %s\n"),
                                    id, parent_id);
          else if (id != NULL)
            g_string_append_printf (str,
                                    _("Remove %s from %s\n"),
                                    id, G_OBJECT_TYPE_NAME (op->tree.parent));
          else
            g_string_append_printf (str,
                                    _("Remove %s from %s\n"),
                                    G_OBJECT_TYPE_NAME (op->tree.item),
                                    G_OBJECT_TYPE_NAME (op->tree.parent));
        }
      else if (op->kind == OP_INSERT_AFTER ||
               op->kind == OP_INSERT_BEFORE)
        {
          const char *id = draft_item_get_id (op->tree.item);
          const char *parent_id = draft_item_get_id (op->tree.parent);

          if (id != NULL && parent_id != NULL)
            g_string_append_printf (str,
                                    _("Insert %s on %s\n"),
                                    id, parent_id);
          else if (id != NULL)
            g_string_append_printf (str,
                                    _("Insert %s on %s\n"),
                                    id, G_OBJECT_TYPE_NAME (op->tree.parent));
          else
            g_string_append_printf (str,
                                    _("Insert %s on %s\n"),
                                    G_OBJECT_TYPE_NAME (op->tree.item),
                                    G_OBJECT_TYPE_NAME (op->tree.parent));
        }
    }

  /* Remove trailing \n */
  if (str->len > 0 && str->str[str->len-1] == '\n')
    g_string_truncate (str, str->len-1);

  return g_string_free (str, FALSE);
}
