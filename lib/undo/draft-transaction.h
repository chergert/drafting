/* draft-transaction.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types.h"
#include "draft-version-macros.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_TRANSACTION (draft_transaction_get_type())

DRAFT_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DraftTransaction, draft_transaction, DRAFT, TRANSACTION, GObject)

DRAFT_AVAILABLE_IN_ALL
void     draft_transaction_insert_before     (DraftTransaction *self,
                                              DraftItem        *item,
                                              DraftItem        *parent,
                                              DraftItem        *next_sibling);
DRAFT_AVAILABLE_IN_ALL
void     draft_transaction_insert_after      (DraftTransaction *self,
                                              DraftItem        *item,
                                              DraftItem        *parent,
                                              DraftItem        *previous_sibling);
DRAFT_AVAILABLE_IN_ALL
void     draft_transaction_remove            (DraftTransaction *self,
                                              DraftItem        *item);
DRAFT_AVAILABLE_IN_ALL
void     draft_transaction_set_item_property (DraftTransaction *self,
                                              DraftItem        *item,
                                              const char       *property,
                                              const GValue     *value);
DRAFT_AVAILABLE_IN_ALL
void     draft_transaction_set_item          (DraftTransaction *self,
                                              DraftItem        *item,
                                              const char       *first_property,
                                              ...) G_GNUC_NULL_TERMINATED;
DRAFT_AVAILABLE_IN_ALL
gboolean draft_transaction_is_empty          (DraftTransaction *self);

G_END_DECLS
