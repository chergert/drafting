/* draft-transaction-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-ipc-builder.h"

#include "undo/draft-transaction.h"

G_BEGIN_DECLS

DraftTransaction *_draft_transaction_new             (void);
void              _draft_transaction_apply           (DraftTransaction  *self);
void              _draft_transaction_revert          (DraftTransaction  *self);
void              _draft_transaction_freeze          (DraftTransaction  *self);
gboolean          _draft_transaction_is_simple       (DraftTransaction  *self);
GSList           *_draft_transaction_get_documents   (DraftTransaction  *self);
char             *_draft_transaction_get_description (DraftTransaction  *self);
gboolean          _draft_transaction_apply_simple    (DraftTransaction  *self,
                                                      DraftDocument     *document,
                                                      DraftIpcBuilder   *buider,
                                                      GError           **error);

G_END_DECLS
