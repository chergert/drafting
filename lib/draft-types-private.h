/* draft-types-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types.h"

G_BEGIN_DECLS

/* Document loading and saving */
typedef struct _DraftDocumentFormat    DraftDocumentFormat;
typedef struct _DraftDocumentFormatXml DraftDocumentFormatXml;

/* Undo/Redo Support */
typedef struct _DraftHistory           DraftHistory;

/* Observing changes to documents */
typedef struct _DraftObserver          DraftObserver;
typedef struct _DraftCanvasObserver    DraftCanvasObserver;
typedef struct _DraftCanvasPaintable   DraftCanvasPaintable;
typedef struct _DraftCanvasItem        DraftCanvasItem;
typedef struct _DraftCanvasGrip        DraftCanvasGrip;

/* Rendering */
typedef struct _DraftRenderer          DraftRenderer;

/* Introspection */
typedef struct _DraftTypeCache    DraftTypeCache;
typedef struct _DraftTypeInfo     DraftTypeInfo;
typedef struct _DraftPropertyInfo DraftPropertyInfo;
typedef struct _DraftClassInfo    DraftClassInfo;
typedef struct _DraftSignalInfo   DraftSignalInfo;
typedef struct _DraftFlagsInfo    DraftFlagsInfo;
typedef struct _DraftEnumInfo     DraftEnumInfo;

typedef enum _DraftTypeInfoKind
{
  DRAFT_TYPE_INFO_PROPERTY,
  DRAFT_TYPE_INFO_SIGNAL,
  DRAFT_TYPE_INFO_CLASS,
  DRAFT_TYPE_INFO_FLAGS,
  DRAFT_TYPE_INFO_ENUM,
} DraftTypeInfoKind;

G_END_DECLS
