/* drafting.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#define DRAFTING_INSIDE
# include "draft-enums.h"
# include "draft-init.h"
# include "draft-project.h"
# include "draft-project-context.h"
# include "draft-version.h"
# include "draft-version-macros.h"
# include "canvas/draft-canvas.h"
# include "items/draft-accessibility.h"
# include "items/draft-binding.h"
# include "items/draft-child.h"
# include "items/draft-dependency.h"
# include "items/draft-document.h"
# include "items/draft-expression.h"
# include "items/draft-item.h"
# include "items/draft-layout.h"
# include "items/draft-menu-attribute.h"
# include "items/draft-menu.h"
# include "items/draft-menu-item.h"
# include "items/draft-menu-link.h"
# include "items/draft-object.h"
# include "items/draft-placeholder.h"
# include "items/draft-property.h"
# include "items/draft-signal.h"
# include "items/draft-style.h"
# include "items/draft-style-name.h"
# include "items/draft-tree-column.h"
# include "items/draft-tree-columns.h"
# include "items/draft-xml-item.h"
# include "pages/draft-history-page.h"
# include "pages/draft-object-page.h"
# include "pages/draft-project-page.h"
# include "pages/draft-accessibility-page.h"
# include "pages/draft-behavior-page.h"
# include "pages/draft-layout-page.h"
# include "pages/draft-signals-page.h"
# include "undo/draft-transaction.h"
#undef DRAFTING_INSIDE
