/* draft-observer-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_OBSERVER (draft_observer_get_type())

G_DECLARE_DERIVABLE_TYPE (DraftObserver, draft_observer, DRAFT, OBSERVER, GObject)

struct _DraftObserverClass
{
  GObjectClass parent_class;

  void (*set_context)        (DraftObserver       *self,
                              DraftProjectContext *context);
  void (*document_added)     (DraftObserver       *self,
                              DraftDocument       *document);
  void (*document_removed)   (DraftObserver       *self,
                              DraftDocument       *document);
  void (*add_display)        (DraftObserver       *self,
                              DraftItem           *item);
  void (*remove_display)     (DraftObserver       *self,
                              DraftItem           *item);
  void (*clear_selection)    (DraftObserver       *self);
  void (*select_item)        (DraftObserver       *self,
                              DraftItem           *item);
  void (*apply)              (DraftObserver       *self,
                              DraftTransaction    *transaction);
  void (*revert)             (DraftObserver       *self,
                              DraftTransaction    *transaction);
  void (*clear_redo_history) (DraftObserver       *self);
};

DraftObserver *_draft_observer_new                (void);
void           _draft_observer_set_context        (DraftObserver       *self,
                                                   DraftProjectContext *context);
void           _draft_observer_document_added     (DraftObserver       *self,
                                                   DraftDocument       *document);
void           _draft_observer_document_removed   (DraftObserver       *self,
                                                   DraftDocument       *document);
void           _draft_observer_add_display        (DraftObserver       *self,
                                                   DraftItem           *item);
void           _draft_observer_remove_display     (DraftObserver       *self,
                                                   DraftItem           *item);
void           _draft_observer_select_item        (DraftObserver       *self,
                                                   DraftItem           *item);
void           _draft_observer_clear_selection    (DraftObserver       *self);
void           _draft_observer_apply              (DraftObserver       *self,
                                                   DraftTransaction    *transaction);
void           _draft_observer_revert             (DraftObserver       *self,
                                                   DraftTransaction    *transaction);
void           _draft_observer_clear_redo_history (DraftObserver       *self);

G_END_DECLS
