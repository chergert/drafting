/* draft-type-cache.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-type-cache-private.h"
#include "introspection/draft-type-info-private.h"

struct _DraftTypeCache
{
  GMutex           mutex;
  DraftIpcService *service;
  GHashTable      *ht;
};

DraftTypeCache *
draft_type_cache_new (DraftIpcService *service)
{
  DraftTypeCache *ret;

  g_return_val_if_fail (!service || DRAFT_IPC_IS_SERVICE (service), NULL);

  ret = g_atomic_rc_box_new0 (DraftTypeCache);
  ret->service = service ? g_object_ref (service) : NULL;
  ret->ht = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                   (GDestroyNotify)draft_type_info_unref);
  g_mutex_init (&ret->mutex);

  return ret;
}

static void
draft_type_cache_finalize (gpointer data)
{
  DraftTypeCache *self = data;

  g_mutex_clear (&self->mutex);
  g_clear_object (&self->service);
  g_clear_pointer (&self->ht, g_hash_table_unref);
}

DraftTypeCache *
draft_type_cache_ref (DraftTypeCache *self)
{
  return g_atomic_rc_box_acquire (self);
}

void
draft_type_cache_unref (DraftTypeCache *self)
{
  g_atomic_rc_box_release_full (self, draft_type_cache_finalize);
}

void
draft_type_cache_drop (DraftTypeCache *self)
{
  g_return_if_fail (self != NULL);

  g_hash_table_remove_all (self->ht);
}

DraftTypeInfo *
draft_type_cache_lookup (DraftTypeCache *self,
                         const char     *type_name)
{
  g_autoptr(GMutexLocker) lock = NULL;
  g_autoptr(GVariant) info = NULL;
  DraftTypeInfo *ret;
  GType type;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->ht != NULL, NULL);
  g_return_val_if_fail (type_name != NULL, NULL);

  lock = g_mutex_locker_new (&self->mutex);

  if ((ret = g_hash_table_lookup (self->ht, type_name)))
    return draft_type_info_ref (ret);

  if (self->service != NULL)
    {
      if (draft_ipc_service_call_resolve_sync (self->service, type_name, &info, NULL, NULL))
        ret = draft_type_info_deserialize (info);
    }

  if (ret == NULL && (type = g_type_from_name (type_name)))
    {
      if (G_TYPE_IS_OBJECT (type))
        ret = (DraftTypeInfo *)draft_class_info_new (type);
      else if (G_TYPE_IS_ENUM (type))
        ret = (DraftTypeInfo *)draft_enum_info_new (type);
      else if (G_TYPE_IS_FLAGS (type))
        ret = (DraftTypeInfo *)draft_flags_info_new (type);
      else
        ret = NULL;
    }

  if (ret != NULL)
    {
      ret->cache = draft_type_cache_ref (self);
      g_hash_table_insert (self->ht,
                           (gpointer)g_intern_string (type_name),
                           draft_type_info_ref (ret));
    }

  return ret;
}

gboolean
draft_type_cache_require (DraftTypeCache  *self,
                          const char      *library,
                          const char      *version,
                          GError         **error)
{
  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (library != NULL, FALSE);
  g_return_val_if_fail (version != NULL, FALSE);

  if (library != NULL && version != NULL)
    {
      g_autoptr(GMutexLocker) locker = g_mutex_locker_new (&self->mutex);

      if (self->service != NULL)
        {
          if (draft_ipc_service_call_require_sync (self->service, library, version, NULL, error))
            return TRUE;
        }
    }

  return FALSE;
}
