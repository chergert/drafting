/* draft-resolver-builtin.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "items/draft-accessibility.h"
#include "items/draft-child.h"
#include "items/draft-item-private.h"
#include "items/draft-layout.h"
#include "items/draft-xml-item.h"
#include "items/draft-style.h"
#include "items/draft-tree-columns.h"
#include "introspection/draft-resolver-private.h"

static const struct {
  const char *element_name;
  const char *parent_type;
  const char *item_type;
} overrides[] = {
  { "child",         "GtkWidget",    "DraftChild" },
  { "style",         "GtkWidget",    "DraftStyle" },
  { "layout",        "GtkWidget",    "DraftLayout" },
  { "columns",       "GtkListStore", "DraftTreeColumns" },
  { "columns",       "GtkTreeStore", "DraftTreeColumns" },
  { "accessibility", "GtkWidget",    "DraftAccessibility" },
};
static GHashTable *known_elements;

void
draft_resolver_builtins_init (void)
{
  known_elements = g_hash_table_new (g_str_hash, g_str_equal);

  for (guint i = 0; i < G_N_ELEMENTS (overrides); i++)
    g_hash_table_add (known_elements, (gpointer)overrides[i].element_name);

  g_type_ensure (DRAFT_TYPE_ACCESSIBILITY);
  g_type_ensure (DRAFT_TYPE_CHILD);
  g_type_ensure (DRAFT_TYPE_LAYOUT);
  g_type_ensure (DRAFT_TYPE_STYLE);
  g_type_ensure (DRAFT_TYPE_TREE_COLUMNS);
}

static void
root_start_element (GMarkupParseContext  *context,
                    const char           *element_name,
                    const char          **attribute_names,
                    const char          **attribute_values,
                    gpointer              user_data,
                    GError              **error)
{
  DraftXmlParser *state = user_data;

  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (DRAFT_IS_DOCUMENT (state->document));

  DRAFT_XML_PARSER_PUSH (g_object_ref (state->replacement));
}

static void
root_end_element (GMarkupParseContext  *context,
                  const char           *element_name,
                  gpointer              user_data,
                  GError              **error)
{
  DraftXmlParser *state = user_data;

  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (DRAFT_IS_DOCUMENT (state->document));

  DRAFT_XML_PARSER_POP (G_TYPE_FROM_INSTANCE (state->replacement));
}

static const GMarkupParser root_parser = {
  root_start_element,
  root_end_element,
};

DraftItem *
draft_resolver_resolve_builtins (DraftResolver *self,
                                 DraftItem     *item)
{
  DraftItem *parent;

  g_return_val_if_fail (DRAFT_IS_RESOLVER (self), NULL);
  g_return_val_if_fail (DRAFT_IS_ITEM (item), NULL);

  if (!(parent = draft_item_get_parent (item)))
    return item;

  if (DRAFT_IS_XML_ITEM (item))
    {
      DraftXmlItem *xml = DRAFT_XML_ITEM (item);
      const char *element_name = draft_xml_item_get_element_name (xml);

      /* This is just a little bit tricky, so let me break down what
       * is going on here for you.
       *
       * When parsing the document, we very much avoid trying to do
       * type resolution. That is not done until after the object has
       * been parsed (and here we are).
       *
       * This is primarily because we might not have type information
       * ready at that point, but also we might need to scan across
       * the document in some fashion.
       *
       * Furthermore, that type information may come from another
       * process entirely (even one running in a container with a
       * different CPU arch via Qemu).
       *
       * Because of that, and because we can't parse all possible
       * custom XML of unknown subclasses, we leave those nodes
       * lazy as DraftXmlItem so we can resolve them here.
       *
       * To minimize the ways we load data, this will find out if
       * the XML item is one we care to replace with a "typed node"
       * and then serialize back to XML and parse it normally.
       *
       * That way, we always go through the same flow to load a
       * document from XML and reduce chances for odd errors
       * between loading functions.
       */

      if (!g_hash_table_contains (known_elements, element_name))
        return item;

      for (guint i = 0; i < G_N_ELEMENTS (overrides); i++)
        {
          if (g_strcmp0 (element_name, overrides[i].element_name) == 0 &&
              _draft_item_is_of_type (parent, overrides[i].parent_type))
            {
              GType type = g_type_from_name (overrides[i].item_type);
              g_autoptr(GString) str = g_string_new (NULL);
              g_autoptr(GMarkupParseContext) context = NULL;
              g_autoptr(DraftItem) replace = NULL;
              DraftXmlParser state = {0};

              g_assert (g_type_is_a (type, DRAFT_TYPE_ITEM));

              _draft_item_save_to_xml (item, str, NULL, FALSE);

              replace = g_object_new (type, NULL);

              state.document = draft_item_get_document (item);
              state.format = NULL;
              state.current = parent;
              state.replacement = replace;

              context = g_markup_parse_context_new (&root_parser,
                                                    G_MARKUP_PREFIX_ERROR_POSITION | G_MARKUP_IGNORE_QUALIFIED,
                                                    &state, NULL);

              if (g_markup_parse_context_parse (context, str->str, str->len, NULL))
                {
                  _draft_item_unparent (item);
                  return replace;
                }

              _draft_item_unparent (replace);

              break;
            }
        }
    }

  return item;
}
