/* draft-type-info-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-types-private.h"

G_BEGIN_DECLS

struct _DraftTypeInfo
{
  DraftTypeInfoKind kind;
  DraftTypeCache *cache;
};

struct _DraftEnumInfo
{
  DraftTypeInfo info;
  const char *name;
  guint n_values;
  struct {
    const char *name;
    const char *nick;
    guint value;
  } values[0];
};

struct _DraftFlagsInfo
{
  DraftTypeInfo info;
  const char *name;
  guint n_values;
  struct {
    const char *name;
    const char *nick;
    guint value;
  } values[0];
};

struct _DraftSignalInfo
{
  DraftTypeInfo info;
  const char *name;
};

struct _DraftPropertyInfo
{
  DraftTypeInfo info;
  const char *name;
  const char *type_name;
  const char *class_name;
  const char *description;
  GParamFlags flags;
  GValue min_value;
  GValue max_value;
  GValue default_value;
};

struct _DraftClassInfo
{
  DraftTypeInfo info;
  const char *name;
  const char *parent_name;
  const char *layout_manager_type;
  GPtrArray *properties;
  GPtrArray *signals;
  GArray    *interfaces;
};

static inline gboolean
DRAFT_TYPE_INFO_IS_PROPERTY (DraftTypeInfo *info)
{
  return info && info->kind == DRAFT_TYPE_INFO_PROPERTY;
}

static inline gboolean
DRAFT_TYPE_INFO_IS_SIGNAL (DraftTypeInfo *info)
{
  return info && info->kind == DRAFT_TYPE_INFO_SIGNAL;
}

static inline gboolean
DRAFT_TYPE_INFO_IS_CLASS (DraftTypeInfo *info)
{
  return info && info->kind == DRAFT_TYPE_INFO_CLASS;
}

static inline gboolean
DRAFT_TYPE_INFO_IS_ENUM (DraftTypeInfo *info)
{
  return info && info->kind == DRAFT_TYPE_INFO_ENUM;
}

static inline gboolean
DRAFT_TYPE_INFO_IS_FLAGS (DraftTypeInfo *info)
{
  return info && info->kind == DRAFT_TYPE_INFO_FLAGS;
}

static inline gpointer
draft_type_info_ref (gpointer info)
{
  if (info == NULL)
    return NULL;
  return g_atomic_rc_box_acquire (info);
}

void               draft_type_info_unref           (gpointer           info);
gpointer           draft_type_info_deserialize     (GVariant          *variant);
void               draft_type_info_serialize       (DraftTypeInfo     *self,
                                                    GVariantBuilder   *builder);
gboolean           draft_type_info_is_of_type      (DraftTypeInfo     *self,
                                                    const char        *type_name);
DraftPropertyInfo *draft_property_info_new         (GParamSpec        *pspec);
DraftClassInfo    *draft_class_info_new            (GType              object_type);
DraftClassInfo    *draft_class_info_new_simple     (const char        *type_name,
                                                    const char        *parent_type_name);
DraftSignalInfo   *draft_signal_info_new           (guint              signal_id);
DraftFlagsInfo    *draft_flags_info_new            (GType              type);
DraftEnumInfo     *draft_enum_info_new             (GType              type);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftTypeInfo, draft_type_info_unref)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftClassInfo, draft_type_info_unref)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftPropertyInfo, draft_type_info_unref)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftEnumInfo, draft_type_info_unref)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftFlagsInfo, draft_type_info_unref)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (DraftSignalInfo, draft_type_info_unref)

G_END_DECLS
