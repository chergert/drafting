/* draft-resolver.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-resolver-private.h"
#include "items/draft-document.h"
#include "items/draft-dependency.h"
#include "items/draft-item-private.h"
#include "items/draft-object.h"
#include "items/draft-xml-item.h"

typedef struct
{
  char *library;
  char *version;
  guint requested : 1;
} Dependency;

struct _DraftResolver
{
  GObject parent_instance;
  GArray *dependencies;
  DraftTypeCache *cache;
};

G_DEFINE_TYPE (DraftResolver, draft_resolver, G_TYPE_OBJECT)

static void
dependency_clear (gpointer data)
{
  Dependency *dep = data;

  g_clear_pointer (&dep->library, g_free);
  g_clear_pointer (&dep->version, g_free);
}

DraftResolver *
draft_resolver_new (DraftTypeCache *cache)
{
  DraftResolver *self;

  g_return_val_if_fail (cache != NULL, NULL);

  self = g_object_new (DRAFT_TYPE_RESOLVER, NULL);
  self->cache = draft_type_cache_ref (cache);

  return self;
}

static void
draft_resolver_finalize (GObject *object)
{
  DraftResolver *self = (DraftResolver *)object;

  g_clear_pointer (&self->dependencies, g_array_unref);

  G_OBJECT_CLASS (draft_resolver_parent_class)->finalize (object);
}

static void
draft_resolver_class_init (DraftResolverClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = draft_resolver_finalize;

  draft_resolver_builtins_init ();
}

static void
draft_resolver_init (DraftResolver *self)
{
  self->dependencies = g_array_new (FALSE, FALSE, sizeof (Dependency));
  g_array_set_clear_func (self->dependencies, dependency_clear);
}

void
draft_resolver_add_dependency (DraftResolver *self,
                               const char    *library,
                               const char    *version)
{
  Dependency dep;

  g_return_if_fail (DRAFT_IS_RESOLVER (self));
  g_return_if_fail (library != NULL);

  for (guint i = 0; i < self->dependencies->len; i++)
    {
      const Dependency *d = &g_array_index (self->dependencies, Dependency, i);

      if (g_strcmp0 (d->library, library) == 0 &&
          g_strcmp0 (d->version, version) == 0)
        return;
    }

  dep.library = g_strdup (library);
  dep.version = g_strdup (version);
  dep.requested = FALSE;

  g_array_append_val (self->dependencies, dep);
}

DraftTypeInfo *
draft_resolver_resolve_type_name (DraftResolver *self,
                                  const char    *type_name,
                                  const char    *parent_type_name)
{
  DraftTypeInfo *info;

  g_return_val_if_fail (DRAFT_IS_RESOLVER (self), NULL);

  info = draft_type_cache_lookup (self->cache, type_name);

  if (info == NULL)
    {
      if (parent_type_name)
        info = draft_type_cache_lookup (self->cache, parent_type_name);

      if (!info && type_name && parent_type_name)
        {
          info = (DraftTypeInfo *)draft_class_info_new_simple (type_name, parent_type_name);
          info->cache = draft_type_cache_ref (self->cache);
        }
    }

  return info;
}

DraftTypeInfo *
draft_resolver_resolve_type_func (DraftResolver *self,
                                  const char    *type_func)
{
  g_return_val_if_fail (DRAFT_IS_RESOLVER (self), NULL);

  /* TODO: Resolve types from copilot process */

  return NULL;
}

static void
draft_resolver_resolve_item (DraftResolver *self,
                             DraftItem     *item)
{
  DraftItem *child;

  g_assert (DRAFT_IS_RESOLVER (self));
  g_assert (DRAFT_IS_ITEM (item));

  if (DRAFT_IS_OBJECT (item))
    {
      DraftObject *obj = DRAFT_OBJECT (item);
      const char *class_name = draft_object_get_class_name (obj);
      const char *parent_class = draft_object_get_parent_name (obj);
      const char *type_func = draft_object_get_type_func (obj);
      g_autoptr(DraftTypeInfo) info = NULL;

      if (class_name != NULL)
        info = draft_resolver_resolve_type_name (self, class_name, parent_class);

      if (info == NULL && type_func != NULL)
        info = draft_resolver_resolve_type_func (self, type_func);

      if (info == NULL)
        return;

      /* Annotate the type information on the object */
      _draft_item_annotate (item, info);
    }

  if (!(item = draft_resolver_resolve_builtins (self, item)))
    return;

  child = draft_item_get_first_child (item);

  while (child != NULL)
    {
      DraftItem *next = draft_item_get_next_sibling (child);
      draft_resolver_resolve_item (self, child);
      child = next;
    }
}

static void
draft_resolver_load_dependencies (DraftResolver *self,
                                  DraftDocument *document)
{
  g_assert (DRAFT_IS_RESOLVER (self));
  g_assert (DRAFT_IS_DOCUMENT (document));

  for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (document));
       child != NULL;
       child = draft_item_get_next_sibling (child))
    {
      if (DRAFT_IS_DEPENDENCY (child))
        {
          const char *library = draft_dependency_get_library (DRAFT_DEPENDENCY (child));
          const char *version = draft_dependency_get_version (DRAFT_DEPENDENCY (child));

          if (library != NULL && version != NULL)
            draft_type_cache_require (self->cache, library, version, NULL);
        }
    }
}

void
draft_resolver_resolve (DraftResolver *self,
                        DraftDocument *document)
{
  DraftItem *child;

  g_return_if_fail (DRAFT_IS_RESOLVER (self));
  g_return_if_fail (DRAFT_IS_DOCUMENT (document));

  draft_resolver_load_dependencies (self, document);

  child = draft_item_get_first_child (DRAFT_ITEM (document));

  while (child != NULL)
    {
      DraftItem *next = draft_item_get_next_sibling (child);
      draft_resolver_resolve_item (self, child);
      child = next;
    }
}
