/* draft-type-info.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "introspection/draft-type-info-private.h"
#include "introspection/draft-type-cache-private.h"
#include "util/draft-utils-private.h"

static inline DraftEnumInfo *
draft_enum_info_alloc (guint n_values)
{
  DraftEnumInfo *ret;

  ret = g_atomic_rc_box_alloc0 (sizeof *ret + sizeof ret->values[0] * n_values);
  ret->info.kind = DRAFT_TYPE_INFO_ENUM;
  ret->n_values = n_values;

  return ret;
}

DraftEnumInfo *
draft_enum_info_new (GType type)
{
  DraftEnumInfo *info;
  GEnumClass *klass;

  g_return_val_if_fail (G_TYPE_IS_ENUM (type), NULL);

  klass = g_type_class_ref (type);

  info = draft_enum_info_alloc (klass->n_values);
  info->name = g_intern_string (g_type_name (type));

  for (guint i = 0; i < klass->n_values; i++)
    {
      info->values[i].name = g_intern_string (klass->values[i].value_name);
      info->values[i].nick = g_intern_string (klass->values[i].value_nick);
      info->values[i].value = klass->values[i].value;
    }

  g_type_class_unref (klass);

  return info;
}

static void
draft_enum_info_serialize (DraftEnumInfo   *self,
                           GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add_parsed (builder, "{'kind', <%u>}", self->info.kind);
  g_variant_builder_add_parsed (builder, "{'name', <%s>}", self->name);
  g_variant_builder_open (builder, G_VARIANT_TYPE ("{sv}"));
  g_variant_builder_add (builder, "s", "values");
  g_variant_builder_open (builder, G_VARIANT_TYPE ("v"));
  g_variant_builder_open (builder, G_VARIANT_TYPE ("a(uss)"));
  for (guint i = 0; i < self->n_values; i++)
    {
      g_variant_builder_open (builder, G_VARIANT_TYPE ("(uss)"));
      g_variant_builder_add (builder, "u", self->values[i].value);
      g_variant_builder_add (builder, "s", self->values[i].name);
      g_variant_builder_add (builder, "s", self->values[i].nick);
      g_variant_builder_close (builder);
    }
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
}

static DraftEnumInfo *
draft_enum_info_deserialize (GVariant *variant)
{
  DraftEnumInfo *ret = NULL;
  GVariantIter iter;
  const char *key;
  GVariant *value;
  const char *name;

  g_return_val_if_fail (variant != NULL, NULL);

  g_variant_iter_init (&iter, variant);
  while (g_variant_iter_next (&iter, "{&sv}", &key, &value))
    {
      if (g_strcmp0 (key, "name") == 0)
        {
          name = g_intern_string (g_variant_get_string (value, NULL));
        }
      else if (g_strcmp0 (key, "kind") == 0) { }
      else
        {
          GVariantIter viter;
          const char *vname;
          const char *vnick;
          guint v;
          guint i = 0;

          ret = draft_enum_info_alloc (g_variant_n_children (value));

          g_variant_iter_init (&viter, value);
          while (g_variant_iter_loop (&viter, "(u&s&s)", &v, &vname, &vnick))
            {
              ret->values[i].value = v;
              ret->values[i].name = g_intern_string (vname);
              ret->values[i].nick = g_intern_string (vnick);
              i++;
            }
        }

      g_variant_unref (value);
    }

  if (ret != NULL)
    ret->name = g_intern_string (name);

  return ret;
}

static inline DraftFlagsInfo *
draft_flags_info_alloc (guint n_values)
{
  DraftFlagsInfo *ret;

  ret = g_atomic_rc_box_alloc0 (sizeof *ret + sizeof ret->values[0] * n_values);
  ret->info.kind = DRAFT_TYPE_INFO_FLAGS;
  ret->n_values = n_values;

  return ret;
}

DraftFlagsInfo *
draft_flags_info_new (GType type)
{
  DraftFlagsInfo *info;
  GFlagsClass *klass;

  g_return_val_if_fail (G_TYPE_IS_FLAGS (type), NULL);

  klass = g_type_class_ref (type);

  info = draft_flags_info_alloc (klass->n_values);
  info->name = g_intern_string (g_type_name (type));

  for (guint i = 0; i < klass->n_values; i++)
    {
      info->values[i].name = g_intern_string (klass->values[i].value_name);
      info->values[i].nick = g_intern_string (klass->values[i].value_nick);
      info->values[i].value = klass->values[i].value;
    }

  g_type_class_unref (klass);

  return info;
}

static void
draft_flags_info_serialize (DraftFlagsInfo   *self,
                           GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add_parsed (builder, "{'kind', <%u>}", self->info.kind);
  g_variant_builder_add_parsed (builder, "{'name', <%s>}", self->name);
  g_variant_builder_open (builder, G_VARIANT_TYPE ("{sv}"));
  g_variant_builder_add (builder, "s", "values");
  g_variant_builder_open (builder, G_VARIANT_TYPE ("v"));
  g_variant_builder_open (builder, G_VARIANT_TYPE ("a(uss)"));
  for (guint i = 0; i < self->n_values; i++)
    {
      g_variant_builder_open (builder, G_VARIANT_TYPE ("(uss)"));
      g_variant_builder_add (builder, "u", self->values[i].value);
      g_variant_builder_add (builder, "s", self->values[i].name);
      g_variant_builder_add (builder, "s", self->values[i].nick);
      g_variant_builder_close (builder);
    }
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
}

static DraftFlagsInfo *
draft_flags_info_deserialize (GVariant *variant)
{
  DraftFlagsInfo *ret = NULL;
  GVariantIter iter;
  const char *key;
  GVariant *value;
  const char *name;

  g_return_val_if_fail (variant != NULL, NULL);

  g_variant_iter_init (&iter, variant);
  while (g_variant_iter_next (&iter, "{&sv}", &key, &value))
    {
      if (g_strcmp0 (key, "name") == 0)
        {
          name = g_intern_string (g_variant_get_string (value, NULL));
        }
      else if (g_strcmp0 (key, "kind") == 0) { }
      else
        {
          GVariantIter viter;
          const char *vname;
          const char *vnick;
          guint v;
          guint i = 0;

          ret = draft_flags_info_alloc (g_variant_n_children (value));

          g_variant_iter_init (&viter, value);
          while (g_variant_iter_loop (&viter, "(u&s&s)", &v, &vname, &vnick))
            {
              ret->values[i].value = v;
              ret->values[i].name = g_intern_string (vname);
              ret->values[i].nick = g_intern_string (vnick);
              i++;
            }
        }

      g_variant_unref (value);
    }

  if (ret != NULL)
    ret->name = g_intern_string (name);

  return ret;
}

static inline DraftSignalInfo *
draft_signal_info_alloc (void)
{
  DraftSignalInfo *ret;

  ret = g_atomic_rc_box_alloc0 (sizeof (DraftSignalInfo));
  ret->info.kind = DRAFT_TYPE_INFO_SIGNAL;

  return ret;
}

DraftSignalInfo *
draft_signal_info_new (guint signal_id)
{
  DraftSignalInfo *info;
  const char *name;

  g_return_val_if_fail (signal_id != 0, NULL);
  name = g_signal_name (signal_id);
  g_return_val_if_fail (name != NULL, NULL);

  info = draft_signal_info_alloc ();
  info->name = g_intern_string (name);

  return info;
}

static void
draft_signal_info_serialize (DraftSignalInfo *self,
                             GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add_parsed (builder, "{'kind', <%u>}", self->info.kind);
  g_variant_builder_add_parsed (builder, "{'name', <%s>}", self->name);
  g_variant_builder_close (builder);
}

static DraftSignalInfo *
draft_signal_info_deserialize (GVariant *variant)
{
  DraftSignalInfo *ret;
  const char *name;

  g_return_val_if_fail (variant != NULL, NULL);

  if (!g_variant_lookup (variant, "name", "&s", &name))
    return NULL;

  ret = draft_signal_info_alloc ();
  ret->name = g_intern_string (name);

  return ret;
}

static inline DraftPropertyInfo *
draft_property_info_alloc (void)
{
  DraftPropertyInfo *ret;

  ret = g_atomic_rc_box_alloc0 (sizeof (DraftPropertyInfo));
  ret->info.kind = DRAFT_TYPE_INFO_PROPERTY;

  return ret;
}

DraftPropertyInfo *
draft_property_info_new (GParamSpec *pspec)
{
  DraftPropertyInfo *info;

  g_return_val_if_fail (pspec != NULL, NULL);

  info = draft_property_info_alloc ();
  info->name = g_intern_string (pspec->name);
  info->type_name = g_intern_string (g_type_name (pspec->value_type));
  info->description = g_intern_string (g_param_spec_get_blurb (pspec));
  info->class_name = g_intern_string (g_type_name (pspec->owner_type));
  info->flags = pspec->flags;

#define ELIF_PARAM_DEFAULT_ONLY(TYPE, Type, type)                      \
  else if (G_IS_PARAM_SPEC_##TYPE (pspec))                             \
    {                                                                  \
      g_value_init (&info->default_value, G_TYPE_##TYPE);              \
      g_value_set_##type (&info->default_value,                        \
                          ((GParamSpec##Type *)pspec)->default_value); \
    }
#define ELIF_PARAM_WITH_RANGE(TYPE, Type, type)                        \
  else if (G_IS_PARAM_SPEC_##TYPE (pspec))                             \
    {                                                                  \
      g_value_init (&info->default_value, G_TYPE_##TYPE);              \
      g_value_init (&info->min_value, G_TYPE_##TYPE);                  \
      g_value_init (&info->max_value, G_TYPE_##TYPE);                  \
      g_value_set_##type (&info->default_value,                        \
                          ((GParamSpec##Type *)pspec)->default_value); \
      g_value_set_##type (&info->min_value,                            \
                          ((GParamSpec##Type *)pspec)->minimum);       \
      g_value_set_##type (&info->max_value,                            \
                          ((GParamSpec##Type *)pspec)->maximum);       \
    }

  if (G_IS_PARAM_SPEC_OBJECT (pspec)) {}
  ELIF_PARAM_DEFAULT_ONLY (BOOLEAN, Boolean, boolean)
  ELIF_PARAM_DEFAULT_ONLY (STRING, String, string)
  ELIF_PARAM_WITH_RANGE (DOUBLE, Double, double)
  ELIF_PARAM_WITH_RANGE (FLOAT, Float, float)
  ELIF_PARAM_WITH_RANGE (CHAR, Char, schar)
  ELIF_PARAM_WITH_RANGE (UCHAR, UChar, uchar)
  ELIF_PARAM_WITH_RANGE (INT, Int, int)
  ELIF_PARAM_WITH_RANGE (UINT, UInt, uint)
  ELIF_PARAM_WITH_RANGE (INT64, Int64, int64)
  ELIF_PARAM_WITH_RANGE (UINT64, UInt64, uint64)
  ELIF_PARAM_WITH_RANGE (LONG, Long, long)
  ELIF_PARAM_WITH_RANGE (ULONG, ULong, ulong)
  else
    {
      draft_type_info_unref (info);
      return NULL;
    }

#undef ELIF_PARAM_DEFAULT_ONLY
#undef ELIF_PARAM_WITH_RANGE

  return info;
}

static void
draft_property_info_finalize (gpointer data)
{
  DraftPropertyInfo *self = data;

  if (G_VALUE_TYPE (&self->min_value))
    g_value_unset (&self->min_value);

  if (G_VALUE_TYPE (&self->max_value))
    g_value_unset (&self->max_value);

  if (G_VALUE_TYPE (&self->default_value))
    g_value_unset (&self->default_value);
}

static void
serialize_value (GVariantBuilder *builder,
                 const char      *key,
                 const GValue    *value)
{
  g_autoptr(GVariant) encoded = NULL;

  g_assert (builder != NULL);
  g_assert (key != NULL);
  g_assert (G_VALUE_TYPE (value));

  if ((encoded = _draft_value_encode (value)))
    {
      g_variant_take_ref (encoded);
      g_variant_builder_add_parsed (builder, "{%s, <%v>}", key, encoded);
    }
}

static void
draft_property_info_serialize (DraftPropertyInfo *self,
                               GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add_parsed (builder, "{'kind', <%u>}", self->info.kind);
  g_variant_builder_add_parsed (builder, "{'name', <%s>}", self->name);
  g_variant_builder_add_parsed (builder, "{'type-name', <%s>}", self->type_name);
  g_variant_builder_add_parsed (builder, "{'class-name', <%s>}", self->class_name);
  if (self->description != NULL)
    g_variant_builder_add_parsed (builder, "{'description', <%s>}", self->description);
  g_variant_builder_add_parsed (builder, "{'flags', <%u>}", self->flags);
  if (G_VALUE_TYPE (&self->default_value))
    serialize_value (builder, "default-value", &self->default_value);
  if (G_VALUE_TYPE (&self->min_value))
    serialize_value (builder, "min-value", &self->min_value);
  if (G_VALUE_TYPE (&self->max_value))
    serialize_value (builder, "max-value", &self->max_value);
  g_variant_builder_close (builder);
}

static DraftPropertyInfo *
draft_property_info_deserialize (GVariant *variant)
{
  g_autoptr(GVariant) default_value = NULL;
  g_autoptr(GVariant) min_value = NULL;
  g_autoptr(GVariant) max_value = NULL;
  DraftPropertyInfo *ret;
  const char *name;
  const char *type_name;
  const char *description;
  const char *class_name;

  g_return_val_if_fail (variant != NULL, NULL);

  if (!g_variant_lookup (variant, "name", "&s", &name) ||
      !g_variant_lookup (variant, "type-name", "&s", &type_name))
    return NULL;

  if (!g_variant_lookup (variant, "description", "&s", &description))
    description = NULL;

  if (!g_variant_lookup (variant, "class-name", "&s", &class_name))
    class_name = NULL;

  if (!g_variant_lookup (variant, "default-value", "v", &default_value))
    default_value = NULL;

  if (!g_variant_lookup (variant, "min-value", "v", &min_value))
    min_value = NULL;

  if (!g_variant_lookup (variant, "max-value", "v", &max_value))
    max_value = NULL;

  ret = draft_property_info_alloc ();
  ret->name = g_intern_string (name);
  ret->type_name = g_intern_string (type_name);
  ret->description = g_intern_string (description);
  ret->class_name = g_intern_string (class_name);

  if (!g_variant_lookup (variant, "flags", "u", &ret->flags))
    ret->flags = 0;

  if (default_value)
    _draft_value_from_variant (&ret->default_value, default_value, type_name);

  if (min_value)
    _draft_value_from_variant (&ret->min_value, min_value, type_name);

  if (max_value)
    _draft_value_from_variant (&ret->max_value, max_value, type_name);

  return ret;
}

static inline DraftClassInfo *
draft_class_info_alloc (void)
{
  DraftClassInfo *ret;

  ret = g_atomic_rc_box_alloc0 (sizeof (DraftClassInfo));
  ret->info.kind = DRAFT_TYPE_INFO_CLASS;

  return ret;
}

static void
clear_string (gpointer data)
{
  g_clear_pointer ((gpointer *)data, g_free);
}

DraftClassInfo *
draft_class_info_new (GType type)
{
  DraftClassInfo *info;
  GObjectClass *klass;
  GObjectClass *parent;
  g_autofree GType *ifaces = NULL;
  g_autofree GParamSpec **pspecs = NULL;
  guint n_props;
  guint n_ifaces;

  g_return_val_if_fail (G_TYPE_IS_OBJECT (type), NULL);

  klass = g_type_class_ref (type);
  parent = g_type_class_peek_parent (klass);

  info = draft_class_info_alloc ();
  info->name = g_intern_string (G_OBJECT_CLASS_NAME (klass));
  info->parent_name = parent ? g_intern_string (G_OBJECT_CLASS_NAME (parent)) : "";

  if (GTK_IS_WIDGET_CLASS (klass))
    {
      GType layout_type = gtk_widget_class_get_layout_manager_type (GTK_WIDGET_CLASS (klass));

      if (layout_type != G_TYPE_INVALID)
        info->layout_manager_type = g_intern_string (g_type_name (layout_type));
    }

  pspecs = g_object_class_list_properties (klass, &n_props);

  if (n_props > 0)
    {
      info->properties = g_ptr_array_new_with_free_func ((GDestroyNotify)draft_type_info_unref);

      for (guint i = 0; i < n_props; i++)
        {
          DraftPropertyInfo *prop = draft_property_info_new (pspecs[i]);

          if (prop != NULL)
            g_ptr_array_add (info->properties, prop);
        }
    }

  for (GType parent_type = type;
       parent_type != G_TYPE_INVALID;
       parent_type = g_type_parent (parent_type))
    {
      guint n_signals;
      g_autofree guint *signals = g_signal_list_ids (parent_type, &n_signals);

      if (n_signals > 0)
        {
          if (info->signals == NULL)
            info->signals = g_ptr_array_new_with_free_func ((GDestroyNotify)draft_type_info_unref);

          for (guint i = 0; i < n_signals; i++)
            {
              DraftSignalInfo *sig = draft_signal_info_new (signals[i]);

              if (sig != NULL)
                g_ptr_array_add (info->signals, sig);
            }
        }
    }

  ifaces = g_type_interfaces (type, &n_ifaces);
  if (n_ifaces > 0)
    {
      info->interfaces = g_array_new (TRUE, FALSE, sizeof (char *));
      g_array_set_clear_func (info->interfaces, clear_string);
    }
  for (guint i = 0; i < n_ifaces; i++)
    {
      char *name = g_strdup (g_type_name (ifaces[i]));
      g_array_append_val (info->interfaces, name);
    }

  g_type_class_unref (klass);

  return info;
}

DraftClassInfo *
draft_class_info_new_simple (const char *type_name,
                             const char *parent_type_name)
{
  DraftClassInfo *info;

  g_return_val_if_fail (type_name != NULL, NULL);

  info = draft_class_info_alloc ();
  info->name = g_intern_string (type_name);
  info->parent_name = g_intern_string (parent_type_name);

  return info;
}

static void
draft_class_info_finalize (gpointer data)
{
  DraftClassInfo *self = data;

  g_clear_pointer (&self->properties, g_ptr_array_unref);
  g_clear_pointer (&self->signals, g_ptr_array_unref);
}

static void
draft_class_info_serialize (DraftClassInfo  *self,
                            GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add_parsed (builder, "{'kind', <%u>}", self->info.kind);
  g_variant_builder_add_parsed (builder, "{'name', <%s>}", self->name);

  if (self->parent_name != NULL)
    g_variant_builder_add_parsed (builder,
                                  "{'parent-name', <%s>}",
                                  self->parent_name);

  if (self->layout_manager_type)
    g_variant_builder_add_parsed (builder,
                                  "{'layout-manager-type', <%s>}",
                                  self->layout_manager_type);

  if (self->signals != NULL && self->signals->len > 0)
    {
      g_variant_builder_open (builder, G_VARIANT_TYPE ("{sv}"));
      g_variant_builder_add (builder, "s", "signals");
      g_variant_builder_open (builder, G_VARIANT_TYPE ("v"));
      g_variant_builder_open (builder, G_VARIANT_TYPE ("aa{sv}"));
      for (guint i = 0; i < self->signals->len; i++)
        draft_signal_info_serialize (g_ptr_array_index (self->signals, i), builder);
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
    }

  if (self->properties != NULL && self->properties->len > 0)
    {
      g_variant_builder_open (builder, G_VARIANT_TYPE ("{sv}"));
      g_variant_builder_add (builder, "s", "properties");
      g_variant_builder_open (builder, G_VARIANT_TYPE ("v"));
      g_variant_builder_open (builder, G_VARIANT_TYPE ("aa{sv}"));
      for (guint i = 0; i < self->properties->len; i++)
        draft_property_info_serialize (g_ptr_array_index (self->properties, i), builder);
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
    }

  if (self->interfaces != NULL && self->interfaces->len > 0)
    {
      g_variant_builder_open (builder, G_VARIANT_TYPE ("{sv}"));
      g_variant_builder_add (builder, "s", "interfaces");
      g_variant_builder_open (builder, G_VARIANT_TYPE ("v"));
      g_variant_builder_open (builder, G_VARIANT_TYPE ("as"));
      for (guint i = 0; i < self->interfaces->len; i++)
        {
          const char * str = g_array_index (self->interfaces, char *, i);
          g_variant_builder_add (builder, "s", str);
        }
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
      g_variant_builder_close (builder);
    }

  g_variant_builder_close (builder);
}

static DraftClassInfo *
draft_class_info_deserialize (GVariant *variant)
{
  g_autoptr(GPtrArray) signals = NULL;
  g_autoptr(GPtrArray) properties = NULL;
  g_autoptr(GVariant) vsignals = NULL;
  g_autoptr(GVariant) vproperties = NULL;
  g_auto(GStrv) interfaces = NULL;
  DraftClassInfo *ret;
  const char *name;
  const char *parent_name;
  const char *layout_manager_type;

  g_return_val_if_fail (variant != NULL, NULL);

  if (!g_variant_lookup (variant, "name", "&s", &name))
    return NULL;

  if (!g_variant_lookup (variant, "parent-name", "&s", &parent_name))
    parent_name = NULL;

  if (!g_variant_lookup (variant, "layout-manager-type", "&s", &layout_manager_type))
    layout_manager_type = NULL;

  if (!g_variant_lookup (variant, "interfaces", "^as", &interfaces))
    interfaces = NULL;

  vsignals = g_variant_lookup_value (variant, "signals", NULL);
  vproperties = g_variant_lookup_value (variant, "properties", NULL);

  ret = draft_class_info_alloc ();
  ret->name = g_intern_string (name);
  ret->parent_name = g_intern_string (parent_name);
  ret->layout_manager_type = g_intern_string (layout_manager_type);

  if (vproperties != NULL)
    {
      GVariantIter iter;
      GVariant *v;

      properties = g_ptr_array_new_with_free_func ((GDestroyNotify)draft_type_info_unref);

      g_variant_iter_init (&iter, vproperties);
      while ((v = g_variant_iter_next_value (&iter)))
        {
          DraftPropertyInfo *prop = draft_property_info_deserialize (v);

          if (prop)
            g_ptr_array_add (properties, prop);

          g_variant_unref (v);
        }

      if (properties->len > 0)
        ret->properties = g_steal_pointer (&properties);
    }

  if (vsignals != NULL)
    {
      GVariantIter iter;
      GVariant *v;

      signals = g_ptr_array_new_with_free_func ((GDestroyNotify)draft_type_info_unref);

      g_variant_iter_init (&iter, vsignals);
      while ((v = g_variant_iter_next_value (&iter)))
        {
          DraftSignalInfo *sig = draft_signal_info_deserialize (v);

          if (sig)
            g_ptr_array_add (signals, sig);

          g_variant_unref (v);
        }

      if (signals->len > 0)
        ret->signals = g_steal_pointer (&signals);
    }

  if (interfaces != NULL && interfaces[0] != NULL)
    {
      ret->interfaces = g_array_new (TRUE, FALSE, sizeof (char **));
      for (guint i = 0; interfaces[i]; i++)
        {
          char *str = g_steal_pointer (&interfaces[i]);
          g_array_append_val (ret->interfaces, str);
        }
    }

  return ret;
}

static void
draft_type_info_finalize (gpointer data)
{
  DraftTypeInfo *any = data;

  g_clear_pointer (&any->cache, draft_type_cache_unref);

  if (any->kind == DRAFT_TYPE_INFO_CLASS)
    draft_class_info_finalize (data);
  else if (any->kind == DRAFT_TYPE_INFO_PROPERTY)
    draft_property_info_finalize (data);
}

void
draft_type_info_unref (gpointer data)
{
  if (data != NULL)
    g_atomic_rc_box_release_full (data, draft_type_info_finalize);
}

gpointer
draft_type_info_deserialize (GVariant *variant)
{
  g_autoptr(GVariant) child = NULL;
  gpointer ret;
  guint kind;

  g_return_val_if_fail (variant != NULL, NULL);

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("aa{sv}")))
    {
      if (g_variant_n_children (variant) == 0)
        return NULL;
      variant = child = g_variant_get_child_value (variant, 0);
    }

  if (!g_variant_lookup (variant, "kind", "u", &kind))
    return NULL;

  if (kind == DRAFT_TYPE_INFO_CLASS)
    ret = draft_class_info_deserialize (variant);
  else if (kind == DRAFT_TYPE_INFO_PROPERTY)
    ret = draft_property_info_deserialize (variant);
  else if (kind == DRAFT_TYPE_INFO_SIGNAL)
    ret = draft_signal_info_deserialize (variant);
  else if (kind == DRAFT_TYPE_INFO_ENUM)
    ret = draft_enum_info_deserialize (variant);
  else if (kind == DRAFT_TYPE_INFO_FLAGS)
    ret = draft_flags_info_deserialize (variant);
  else
    ret = NULL;

  return ret;
}

void
draft_type_info_serialize (DraftTypeInfo   *self,
                           GVariantBuilder *builder)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (builder != NULL);

  if (DRAFT_TYPE_INFO_IS_ENUM (self))
    return draft_enum_info_serialize ((DraftEnumInfo *)self, builder);
  else if (DRAFT_TYPE_INFO_IS_FLAGS (self))
    return draft_flags_info_serialize ((DraftFlagsInfo *)self, builder);
  else if (DRAFT_TYPE_INFO_IS_PROPERTY (self))
    return draft_property_info_serialize ((DraftPropertyInfo *)self, builder);
  else if (DRAFT_TYPE_INFO_IS_SIGNAL (self))
    return draft_signal_info_serialize ((DraftSignalInfo *)self, builder);
  else if (DRAFT_TYPE_INFO_IS_CLASS (self))
    return draft_class_info_serialize ((DraftClassInfo *)self, builder);
  else
    g_warn_if_reached ();
}

static gboolean
draft_class_info_is_of_type (DraftClassInfo *self,
                             const char     *type_name,
                             GHashTable     *seen)
{
  if (g_strcmp0 (type_name, self->name) == 0 ||
      g_strcmp0 (type_name, self->parent_name) == 0)
    return TRUE;

  if (self->interfaces != NULL)
    {
      for (guint i = 0; i < self->interfaces->len; i++)
        {
          const char *iface = g_array_index (self->interfaces, char *, i);

          if (g_strcmp0 (type_name, iface) == 0)
            return TRUE;
        }
    }

  if (self->parent_name != NULL &&
      self->info.cache != NULL &&
      (seen == NULL || !g_hash_table_contains (seen, self->parent_name)))
    {
      g_autoptr(DraftTypeInfo) parent = draft_type_cache_lookup (self->info.cache, self->parent_name);

      if (parent != NULL && parent->kind == DRAFT_TYPE_INFO_CLASS)
        {
          g_autoptr(GHashTable) ht = NULL;

          if (seen == NULL)
            seen = ht = g_hash_table_new (g_str_hash, g_str_equal);

          /* Avoid cycles as we walk upwards */
          g_hash_table_insert (seen, (gpointer)self->parent_name, NULL);

          return draft_class_info_is_of_type ((DraftClassInfo *)parent, type_name, seen);
        }
    }

  return FALSE;
}

gboolean
draft_type_info_is_of_type (DraftTypeInfo *self,
                            const char    *type_name)
{
  if (self == NULL || type_name == NULL)
    return FALSE;

  if (self->kind == DRAFT_TYPE_INFO_CLASS)
    return draft_class_info_is_of_type ((DraftClassInfo *)self, type_name, NULL);

  return FALSE;
}
