/* draft-type-cache-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "draft-ipc-service.h"
#include "draft-types-private.h"

G_BEGIN_DECLS

DraftTypeCache *draft_type_cache_new     (DraftIpcService  *service);
DraftTypeCache *draft_type_cache_ref     (DraftTypeCache   *self);
void            draft_type_cache_unref   (DraftTypeCache   *self);
DraftTypeInfo  *draft_type_cache_lookup  (DraftTypeCache   *self,
                                          const char       *type_name);
void            draft_type_cache_drop    (DraftTypeCache   *self);
gboolean        draft_type_cache_require (DraftTypeCache   *self,
                                          const char       *library,
                                          const char       *version,
                                          GError          **error);

G_END_DECLS
