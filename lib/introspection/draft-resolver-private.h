/* draft-type-resolver-private.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "items/draft-document.h"
#include "introspection/draft-type-cache-private.h"
#include "introspection/draft-type-info-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_RESOLVER (draft_resolver_get_type())

G_DECLARE_FINAL_TYPE (DraftResolver, draft_resolver, DRAFT, RESOLVER, GObject)

void           draft_resolver_builtins_init     (void);
DraftResolver *draft_resolver_new               (DraftTypeCache *cache);
void           draft_resolver_add_dependency    (DraftResolver  *self,
                                                 const char     *library,
                                                 const char     *version);
DraftTypeInfo *draft_resolver_resolve_type_name (DraftResolver  *self,
                                                 const char     *type_name,
                                                 const char     *parent_type_name);
DraftTypeInfo *draft_resolver_resolve_type_func (DraftResolver  *self,
                                                 const char     *type_func);
void           draft_resolver_resolve           (DraftResolver  *self,
                                                 DraftDocument  *document);
DraftItem     *draft_resolver_resolve_builtins  (DraftResolver  *self,
                                                 DraftItem      *item);

G_END_DECLS
