/* draft-document-format.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "formats/draft-document-format-private.h"

G_DEFINE_ABSTRACT_TYPE (DraftDocumentFormat, draft_document_format, G_TYPE_OBJECT)

static void
draft_document_format_class_init (DraftDocumentFormatClass *klass)
{
}

static void
draft_document_format_init (DraftDocumentFormat *self)
{
}

/*
 * draft_document_format_load_async:
 * @self: an #DraftDocumentFormat
 * @document: a #DraftDocument
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously load @document.
 *
 * The format should look at #DraftDocument:file to get the file to
 * be loaded.
 */
void
_draft_document_format_load_async (DraftDocumentFormat *self,
                                   DraftDocument       *document,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_return_if_fail (DRAFT_IS_DOCUMENT_FORMAT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DRAFT_DOCUMENT_FORMAT_GET_CLASS (self)->load_async (self, document, cancellable, callback, user_data);
}

/*
 * draft_document_format_load_finish:
 * @self: an #DraftDocumentFormat
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to load the file.
 *
 * Returns: %TRUE on success; otherwise %FALSE and @error is set.
 */
gboolean
_draft_document_format_load_finish (DraftDocumentFormat  *self,
                                    GAsyncResult         *result,
                                    GError              **error)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT_FORMAT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return DRAFT_DOCUMENT_FORMAT_GET_CLASS (self)->load_finish (self, result, error);
}

/*
 * draft_document_format_save_async:
 * @self: an #DraftDocumentFormat
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously requests the document to be saved back to disk.
 */
void
_draft_document_format_save_async (DraftDocumentFormat *self,
                                   DraftDocument       *document,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_return_if_fail (DRAFT_IS_DOCUMENT_FORMAT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DRAFT_DOCUMENT_FORMAT_GET_CLASS (self)->save_async (self, document, cancellable, callback, user_data);
}

/*
 * draft_document_format_save_finish:
 * @self: an #DraftDocumentFormat
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns: %TRUE if the document was saved successfully; otherwise %FALSE
 *   and @error is set.
 */
gboolean
_draft_document_format_save_finish (DraftDocumentFormat  *self,
                                    GAsyncResult         *result,
                                    GError              **error)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT_FORMAT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return DRAFT_DOCUMENT_FORMAT_GET_CLASS (self)->save_finish (self, result, error);
}
