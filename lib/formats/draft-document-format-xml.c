/* draft-document-format-xml.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "formats/draft-document-format-xml-private.h"
#include "items/draft-document.h"
#include "items/draft-item-private.h"
#include "items/draft-menu.h"
#include "items/draft-menu-item.h"

struct _DraftDocumentFormatXml
{
  DraftDocumentFormat parent_instance;
};

G_DEFINE_TYPE (DraftDocumentFormatXml, draft_document_format_xml, DRAFT_TYPE_DOCUMENT_FORMAT)

static void root_end_element        (GMarkupParseContext  *context,
                                     const char           *element_name,
                                     gpointer              user_data,
                                     GError              **error);
static void root_start_element      (GMarkupParseContext  *context,
                                     const char           *element_name,
                                     const char          **attribute_names,
                                     const char          **attribute_values,
                                     gpointer              user_data,
                                     GError              **error);

static const GMarkupParser root_parser = {
  root_start_element,
  root_end_element,
};

static void
root_start_element (GMarkupParseContext  *context,
                    const char           *element_name,
                    const char          **attribute_names,
                    const char          **attribute_values,
                    gpointer              user_data,
                    GError              **error)
{
  DraftXmlParser *state = user_data;

  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (DRAFT_IS_DOCUMENT_FORMAT_XML (state->format));
  g_assert (DRAFT_IS_DOCUMENT (state->document));

  if (g_strcmp0 (element_name, "interface") == 0)
    DRAFT_XML_PARSER_PUSH (g_object_ref (state->document));
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
root_end_element (GMarkupParseContext  *context,
                  const char           *element_name,
                  gpointer              user_data,
                  GError              **error)
{
  DraftXmlParser *state = user_data;

  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (DRAFT_IS_DOCUMENT_FORMAT_XML (state->format));
  g_assert (DRAFT_IS_DOCUMENT (state->document));

  if (g_strcmp0 (element_name, "interface") == 0)
    DRAFT_XML_PARSER_POP (DRAFT_TYPE_DOCUMENT);
  else
    DRAFT_XML_PARSER_ERROR ();
}

static void
draft_document_format_xml_load_bytes_cb (GObject      *object,
                                         GAsyncResult *result,
                                         gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GMarkupParseContext) context = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GBytes) bytes = NULL;
  const guint8 *data;
  DraftXmlParser state = {0};
  gsize len;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(bytes = g_file_load_bytes_finish (file, result, NULL, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  data = g_bytes_get_data (bytes, &len);

  if (!g_utf8_validate ((char *)data, len, NULL))
    {
      g_task_return_new_error (task,
                               G_MARKUP_ERROR,
                               G_MARKUP_ERROR_BAD_UTF8,
                               "Document is not valid UTF-8");
      return;
    }

  state.format = g_task_get_source_object (task);
  state.document = g_task_get_task_data (task);

  g_assert (DRAFT_IS_DOCUMENT_FORMAT_XML (state.format));
  g_assert (DRAFT_IS_DOCUMENT (state.document));

  context = g_markup_parse_context_new (&root_parser,
                                        G_MARKUP_PREFIX_ERROR_POSITION | G_MARKUP_IGNORE_QUALIFIED,
                                        &state, NULL);

  if (!g_markup_parse_context_parse (context, (gchar *)data, len, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_assert (state.current == NULL);

  g_task_return_boolean (task, TRUE);
}

static void
draft_document_format_xml_load_async (DraftDocumentFormat *self,
                                      DraftDocument       *document,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  GFile *file;

  g_return_if_fail (DRAFT_IS_DOCUMENT_FORMAT_XML (self));
  g_return_if_fail (DRAFT_IS_DOCUMENT (document));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, draft_document_format_xml_load_async);
  g_task_set_task_data (task, g_object_ref (document), g_object_unref);

  file = draft_document_get_file (document);

  if (file == NULL)
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_INVALID_FILENAME,
                             "No file set to load");
  else
    g_file_load_bytes_async (file,
                             cancellable,
                             draft_document_format_xml_load_bytes_cb,
                             g_steal_pointer (&task));
}

static gboolean
draft_document_format_xml_load_finish (DraftDocumentFormat  *self,
                                       GAsyncResult         *result,
                                       GError              **error)
{
  g_return_val_if_fail (DRAFT_IS_DOCUMENT_FORMAT_XML (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
draft_document_format_xml_finalize (GObject *object)
{
  G_OBJECT_CLASS (draft_document_format_xml_parent_class)->finalize (object);
}

static void
draft_document_format_xml_class_init (DraftDocumentFormatXmlClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DraftDocumentFormatClass *format_class = DRAFT_DOCUMENT_FORMAT_CLASS (klass);

  object_class->finalize = draft_document_format_xml_finalize;

  format_class->load_async = draft_document_format_xml_load_async;
  format_class->load_finish = draft_document_format_xml_load_finish;
}

static void
draft_document_format_xml_init (DraftDocumentFormatXml *self)
{
}

/*
 * draft_document_format_xml_new:
 *
 * Create a new #DraftDocumentFormatXml.
 *
 * Returns: (transfer full): a newly created #DraftDocumentFormat
 */
DraftDocumentFormat *
_draft_document_format_xml_new (void)
{
  return g_object_new (DRAFT_TYPE_DOCUMENT_FORMAT_XML, NULL);
}
