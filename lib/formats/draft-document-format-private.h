/* draft-document-format.h
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "draft-types-private.h"

G_BEGIN_DECLS

#define DRAFT_TYPE_DOCUMENT_FORMAT (draft_document_format_get_type())

G_DECLARE_DERIVABLE_TYPE (DraftDocumentFormat, draft_document_format, DRAFT, DOCUMENT_FORMAT, GObject)

struct _DraftDocumentFormatClass
{
  GObjectClass parent_class;

  void     (*load_async)  (DraftDocumentFormat  *format,
                           DraftDocument        *document,
                           GCancellable         *cancellable,
                           GAsyncReadyCallback   callback,
                           gpointer              user_data);
  gboolean (*load_finish) (DraftDocumentFormat  *format,
                           GAsyncResult         *result,
                           GError              **error);
  void     (*save_async)  (DraftDocumentFormat  *format,
                           DraftDocument        *document,
                           GCancellable         *cancellable,
                           GAsyncReadyCallback   callback,
                           gpointer              user_data);
  gboolean (*save_finish) (DraftDocumentFormat  *format,
                           GAsyncResult         *result,
                           GError              **error);
};

void     _draft_document_format_load_async  (DraftDocumentFormat  *self,
                                             DraftDocument        *document,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean _draft_document_format_load_finish (DraftDocumentFormat  *self,
                                             GAsyncResult         *result,
                                             GError              **error);
void     _draft_document_format_save_async  (DraftDocumentFormat  *self,
                                             DraftDocument        *document,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean _draft_document_format_save_finish (DraftDocumentFormat  *self,
                                             GAsyncResult         *result,
                                             GError              **error);

G_END_DECLS
