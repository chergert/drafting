/* libdraft-service.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <gtk/gtk.h>
#ifdef GDK_WINDOWING_BROADWAY
# include <gdk/broadway/gdkbroadway.h>
#endif

#include <glib-unix.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/prctl.h>
#endif

#ifdef __FreeBSD__
#include <sys/procctl.h>
#endif

#include "service/draft-ipc-service-impl.h"
#include "service/draft-gtk-widget-private.h"
#include "service/draft-gtk-window-private.h"

static GPid broadwayd_pid;

static void
log_func (const gchar    *log_domain,
          GLogLevelFlags  flags,
          const gchar    *message,
          gpointer        user_data)
{
  g_printerr ("libdrafting-service: %s\n", message);
}

static GDBusConnection *
create_connection (GIOStream  *stream,
                   GMainLoop  *main_loop,
                   GError    **error)
{
  GDBusConnection *ret;

  g_assert (G_IS_IO_STREAM (stream));
  g_assert (main_loop != NULL);
  g_assert (error != NULL);

  if ((ret = g_dbus_connection_new_sync (stream, NULL,
                                         G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING,
                                         NULL, NULL, error)))
    {
      g_dbus_connection_set_exit_on_close (ret, FALSE);
      g_signal_connect_swapped (ret, "closed", G_CALLBACK (g_main_loop_quit), main_loop);
    }

  return ret;
}

static int read_fileno = STDIN_FILENO;
static int write_fileno = STDOUT_FILENO;
static int gdk_scale = 1;
static GOptionEntry main_entries[] = {
  { "read-fd", 0, G_OPTION_FLAG_HIDDEN, G_OPTION_ARG_INT, &read_fileno },
  { "write-fd", 0, G_OPTION_FLAG_HIDDEN, G_OPTION_ARG_INT, &write_fileno },
  { "gdk-scale", 0, G_OPTION_FLAG_HIDDEN, G_OPTION_ARG_INT, &gdk_scale },
  { 0 }
};

static void
spawn_broadway (void)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GPtrArray) argv = g_ptr_array_new_with_free_func (g_free);
  g_auto(GStrv) env = g_get_environ ();
  guint port = g_random_int_range (32000, 65535);
  g_autofree char *display_name = g_strdup_printf (":%u", port);
  GPid pid;

  env = g_environ_unsetenv (env, "BROADWAY_DISPLAY");
  env = g_environ_unsetenv (env, "DISPLAY");
  env = g_environ_unsetenv (env, "GDK_BACKEND");
  env = g_environ_unsetenv (env, "WAYLAND_DISPLAY");

  g_ptr_array_add (argv, g_strdup ("gtk4-broadwayd"));
  g_ptr_array_add (argv, g_strdup (display_name));
  g_ptr_array_add (argv, g_strdup ("--address=127.0.0.1"));
  g_ptr_array_add (argv, g_strdup_printf ("--port=%u", port+1));
  g_ptr_array_add (argv, NULL);

  if (!g_spawn_async (g_get_current_dir (),
                      (char **)(gpointer)argv->pdata,
                      env,
                      (G_SPAWN_SEARCH_PATH |
                       G_SPAWN_STDERR_TO_DEV_NULL |
                       G_SPAWN_STDOUT_TO_DEV_NULL),
                      NULL,
                      NULL,
                      &pid,
                      &error))
    g_error ("Failed to spawn broadway: %s", error->message);

  broadwayd_pid = pid;

  /* TODO: Would be nice to synchronize here somehow */
  g_usleep (G_USEC_PER_SEC / 100);

  g_setenv ("GDK_BACKEND", "broadway", TRUE);
  g_setenv ("BROADWAY_DISPLAY", display_name, TRUE);
  g_unsetenv ("DISPLAY");
  g_unsetenv ("WAYLAND_DISPLAY");
}

#ifdef G_OS_UNIX
G_GNUC_NORETURN
static void
sig_term_handler (int signum)
{
  if (broadwayd_pid != 0)
    kill (broadwayd_pid, SIGTERM);
  exit (EXIT_FAILURE);
}
#endif

int
main (int argc,
      char *argv[])
{
  g_autoptr(GDBusConnection) connection = NULL;
  g_autoptr(DraftIpcService) service = NULL;
  g_autoptr(GOutputStream) stdout_stream = NULL;
  g_autoptr(GInputStream) stdin_stream = NULL;
  g_autoptr(GIOStream) stream = NULL;
  g_autoptr(GMainLoop) main_loop = NULL;
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;

  g_set_prgname ("libdrafting-service");
  g_set_application_name ("libdrafting-service");

#ifdef __linux__
  prctl (PR_SET_PDEATHSIG, SIGTERM);
#elif defined(__FreeBSD__)
  procctl (P_PID, 0, PROC_PDEATHSIG_CTL, &(int){ SIGTERM });
#else
# warning "Please submit a patch to support parent-death signal on your OS"
#endif

#ifdef G_OS_UNIX
  signal (SIGPIPE, SIG_IGN);
  signal (SIGTERM, sig_term_handler);
#endif

  g_log_set_handler (NULL, G_LOG_LEVEL_MASK, log_func, NULL);

  context = g_option_context_new ("");
  g_option_context_add_main_entries (context, main_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    goto error;

  if (!g_unix_set_fd_nonblocking (read_fileno, TRUE, &error) ||
      !g_unix_set_fd_nonblocking (write_fileno, TRUE, &error))
    goto error;

  /* We use a throw-away gtk4-broadwayd so that we have something to drive
   * the frame clock even though we don't use the results of the renderings.
   */
  spawn_broadway ();

  gtk_init ();

  /* Register internal types so the UI host can rely on them */
  gtk_test_register_all_types ();
  g_type_ensure (DRAFT_TYPE_GTK_WIDGET);
  g_type_ensure (DRAFT_TYPE_GTK_WINDOW);

#ifdef GDK_WINDOWING_BROADWAY
  {
    GdkDisplay *display = gdk_display_get_default ();
    if (GDK_IS_BROADWAY_DISPLAY (display))
      gdk_broadway_display_set_surface_scale (GDK_BROADWAY_DISPLAY (display), gdk_scale);
  }
#endif

  main_loop = g_main_loop_new (NULL, FALSE);
  stdin_stream = g_unix_input_stream_new (read_fileno, FALSE);
  stdout_stream = g_unix_output_stream_new (write_fileno, FALSE);
  stream = g_simple_io_stream_new (stdin_stream, stdout_stream);

  if (!(connection = create_connection (stream, main_loop, &error)))
    goto error;

  service = draft_ipc_service_impl_new ();

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (service),
                                         connection,
                                         "/org/gnome/Drafting/Service",
                                         &error))
    goto error;

  /* Always start with "Default" theme as expected by UI */
  g_object_set (gtk_settings_get_default (),
                "gtk-theme-name", "Default",
                NULL);

  g_dbus_connection_start_message_processing (connection);
  g_main_loop_run (main_loop);

  return EXIT_SUCCESS;

error:
  if (error != NULL)
    g_printerr ("%s\n", error->message);
  else
    g_printerr ("An unknown error occurred.\n");

  return EXIT_FAILURE;
}
