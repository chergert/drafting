/* draft-project.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "draft-observer-private.h"
#include "draft-project-context-private.h"
#include "draft-project-private.h"

#include "formats/draft-document-format-xml-private.h"
#include "introspection/draft-resolver-private.h"
#include "items/draft-item.h"
#include "items/draft-document-private.h"
#include "items/draft-menu.h"
#include "items/draft-object.h"
#include "undo/draft-history-private.h"
#include "undo/draft-transaction-private.h"

typedef struct
{
  DraftDocument *document;
  DraftResolver *resolver;
} Resolve;

G_DEFINE_TYPE (DraftProject, draft_project, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CAN_UNDO,
  PROP_CAN_REDO,
  PROP_CONTEXT,
  N_PROPS
};

enum {
  DOCUMENT_ADDED,
  DOCUMENT_REMOVED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
resolve_free (gpointer data)
{
  Resolve *r = data;

  g_clear_object (&r->document);
  g_clear_object (&r->resolver);
  g_free (r);
}

DraftProject *
draft_project_new (void)
{
  return g_object_new (DRAFT_TYPE_PROJECT, NULL);
}

static void
notify_can_undo_cb (DraftProject *self,
                    GParamSpec   *pspec,
                    DraftHistory *history)
{
  g_assert (DRAFT_IS_PROJECT (self));
  g_assert (DRAFT_IS_HISTORY (history));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_UNDO]);
}

static void
notify_can_redo_cb (DraftProject *self,
                    GParamSpec   *pspec,
                    DraftHistory *history)
{
  g_assert (DRAFT_IS_PROJECT (self));
  g_assert (DRAFT_IS_HISTORY (history));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_REDO]);
}

static void
draft_project_dispose (GObject *object)
{
  DraftProject *self = (DraftProject *)object;

  if (self->type_cache != NULL)
    {
      draft_type_cache_drop (self->type_cache);
      g_clear_pointer (&self->type_cache, draft_type_cache_unref);
    }

  g_clear_object (&self->context);
  g_clear_object (&self->history);
  g_clear_pointer (&self->observers, g_ptr_array_unref);
  g_clear_pointer (&self->documents, g_ptr_array_unref);

  G_OBJECT_CLASS (draft_project_parent_class)->dispose (object);
}

static void
draft_project_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  DraftProject *self = DRAFT_PROJECT (object);

  switch (prop_id)
    {
    case PROP_CAN_UNDO:
      g_value_set_boolean (value, draft_project_get_can_undo (self));
      break;

    case PROP_CAN_REDO:
      g_value_set_boolean (value, draft_project_get_can_redo (self));
      break;

    case PROP_CONTEXT:
      g_value_set_object (value, draft_project_get_context (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  DraftProject *self = DRAFT_PROJECT (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      draft_project_set_context (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
draft_project_class_init (DraftProjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = draft_project_dispose;
  object_class->get_property = draft_project_get_property;
  object_class->set_property = draft_project_set_property;

  properties [PROP_CAN_REDO] =
    g_param_spec_boolean ("can-redo",
                          "Can Redo",
                          "If the project can redo the last set of actions",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_CAN_UNDO] =
    g_param_spec_boolean ("can-undo",
                          "Can Undo",
                          "If the project can undo the last set of actions",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONTEXT] =
    g_param_spec_object ("context",
                         "Context",
                         "The context for the project",
                         DRAFT_TYPE_PROJECT_CONTEXT,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [DOCUMENT_ADDED] =
    g_signal_new ("document-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_DOCUMENT);

  signals [DOCUMENT_REMOVED] =
    g_signal_new ("document-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRAFT_TYPE_DOCUMENT);
}

static void
draft_project_init (DraftProject *self)
{
  self->documents = g_ptr_array_new_with_free_func (g_object_unref);
  self->observers = g_ptr_array_new_with_free_func (g_object_unref);
  self->type_cache = draft_type_cache_new (NULL);

  self->history = _draft_history_new (self);
  g_signal_connect_object (self->history,
                           "notify::can-undo",
                           G_CALLBACK (notify_can_undo_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (self->history,
                           "notify::can-redo",
                           G_CALLBACK (notify_can_redo_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

static void
draft_project_resolve_worker (GTask        *task,
                              gpointer      source_object,
                              gpointer      task_data,
                              GCancellable *cancellable)
{
  Resolve *resolve = task_data;

  g_assert (G_IS_TASK (task));
  g_assert (DRAFT_IS_PROJECT (source_object));
  g_assert (resolve != NULL);
  g_assert (DRAFT_IS_DOCUMENT (resolve->document));
  g_assert (DRAFT_IS_RESOLVER (resolve->resolver));

  draft_resolver_resolve (resolve->resolver, resolve->document);
  g_task_return_boolean (task, TRUE);
}

static void
draft_project_resolve_async (DraftProject        *self,
                             DraftDocument       *document,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  Resolve *resolve;

  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  /* TODO: We could copy a shared resolver and then merge the
   *       result set back in so that we get the benefit of reuse
   *       when loading multiple documents. Furthermore, it could
   *       allow us to resolve information between documents.
   */

  resolve = g_new0 (Resolve, 1);
  resolve->document = g_object_ref (document);
  resolve->resolver = draft_resolver_new (self->type_cache);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, draft_project_resolve_async);
  g_task_set_task_data (task, resolve, resolve_free);
  g_task_run_in_thread (task, draft_project_resolve_worker);
}

static gboolean
draft_project_resolve_finish (DraftProject  *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);

  /* TODO: See information about merging resolutions above */
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
draft_project_load_file_resolve_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  DraftProject *self = (DraftProject *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  DraftDocument *document;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!draft_project_resolve_finish (self, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  document = g_task_get_task_data (task);

  g_assert (DRAFT_IS_DOCUMENT (document));

  g_ptr_array_add (self->documents, g_object_ref (document));

  for (guint i = 0; i < self->observers->len; i++)
    {
      DraftObserver *observer = g_ptr_array_index (self->observers, i);

      _draft_observer_document_added (observer, document);

      for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (document));
           child != NULL;
           child = draft_item_get_next_sibling (child))
        {
          if (_draft_project_can_display (child))
            _draft_observer_add_display (observer, child);
        }
    }

  g_signal_emit (self, signals [DOCUMENT_ADDED], 0, document);

  g_task_return_pointer (task,
                         g_object_ref (document),
                         g_object_unref);
}

static void
draft_project_load_file_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  DraftDocumentFormat *format = (DraftDocumentFormat *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  DraftDocument *document;
  DraftProject *self;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  document = g_task_get_task_data (task);

  g_assert (DRAFT_IS_PROJECT (self));
  g_assert (DRAFT_IS_DOCUMENT (document));

  if (!_draft_document_format_load_finish (format, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    draft_project_resolve_async (self,
                                 document,
                                 g_task_get_cancellable (task),
                                 draft_project_load_file_resolve_cb,
                                 g_object_ref (task));
}

/**
 * draft_project_load_file_async:
 * @self: an #DraftProject
 * @file: a #GFile
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously loads a file into the project.
 */
void
draft_project_load_file_async (DraftProject        *self,
                               GFile               *file,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(DraftDocument) document = NULL;
  g_autoptr(DraftDocumentFormat) format = NULL;

  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (G_IS_FILE (file));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, draft_project_load_file_async);

  g_debug ("Loading %s into project %p",
           g_file_peek_path (file),
           self);

  /* Ignore the request if we've already loaded the document. We might
   * at some point want to instead refresh the document, but that will
   * take additional passes to resolve things between documents possibly.
   *
   * So for now just avoid it altogether.
   */
  for (guint i = 0; i < self->documents->len; i++)
    {
      DraftDocument *iter = g_ptr_array_index (self->documents, i);
      GFile *iter_file = draft_document_get_file (iter);

      if (iter_file != NULL && g_file_equal (iter_file, file))
        {
          g_task_return_boolean (task, TRUE);
          return;
        }
    }

  format = _draft_document_format_xml_new ();
  document = _draft_document_new (self);
  _draft_document_set_file (document, file);
  g_task_set_task_data (task, g_object_ref (document), g_object_unref);

  _draft_document_format_load_async (format,
                                     document,
                                     cancellable,
                                     draft_project_load_file_cb,
                                     g_steal_pointer (&task));
}

/**
 * draft_project_load_file_finish:
 * @self: an #DraftProject
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns: (transfer full): a #DraftDocument if successful; otherwise %NULL
 *   and @error is set.
 */
DraftDocument *
draft_project_load_file_finish (DraftProject  *self,
                                GAsyncResult  *result,
                                GError       **error)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
draft_project_unload_document (DraftProject  *self,
                               DraftDocument *document)
{
  g_autoptr(DraftDocument) hold = NULL;

  g_assert (DRAFT_IS_PROJECT (self));
  g_assert (DRAFT_IS_DOCUMENT (document));

  hold = g_object_ref (document);

  if (g_ptr_array_remove (self->documents, document))
    {
      for (guint i = 0; i < self->observers->len; i++)
        {
          DraftObserver *observer = g_ptr_array_index (self->observers, i);

          _draft_observer_document_removed (observer, document);
        }
    }

  g_signal_emit (self, signals [DOCUMENT_REMOVED], 0, document);
}

void
draft_project_unload_file (DraftProject *self,
                           GFile        *file)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (G_IS_FILE (file));

  for (guint i = 0; i < self->documents->len; i++)
    {
      DraftDocument *document = g_ptr_array_index (self->documents, i);
      GFile *docfile = draft_document_get_file (document);

      if (docfile && g_file_equal (docfile, file))
        {
          g_object_ref (document);
          g_ptr_array_remove_index (self->documents, i);
          g_list_model_items_changed (G_LIST_MODEL (self), i, 1, 0);
          draft_project_unload_document (self, document);
          g_object_unref (document);
          break;
        }
    }
}

/**
 * draft_project_get_can_undo:
 * @self: a #DraftProject
 *
 * Gets the #DraftProject:can-undo property.
 *
 * This is %TRUE when there is a command in the undo stack
 * that can be undone.
 *
 * Call draft_project_undo() to undo that command.
 *
 * You may connect to the notify::can-undo signal to track
 * changes to this property.
 *
 * You cannot undo while a transaction created with draft_project_begin()
 * has not been committed or rolled back.
 *
 * Returns: %TRUE if it is safe to call draft_project_undo().
 */
gboolean
draft_project_get_can_undo (DraftProject *self)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), FALSE);

  return self->transaction == NULL &&
         _draft_history_get_can_undo (self->history);
}

/**
 * draft_project_get_can_redo:
 * @self: a #DraftProject
 *
 * Gets the #DraftProject:can-redo property.
 *
 * This is %TRUE when there is a command in the redo stack
 * that can be re-applied.
 *
 * Call draft_project_redo() to redo that command.
 *
 * You may connect to the notify::can-redo signal to track
 * changes to this property.
 *
 * You cannot redo while a transaction created with draft_project_begin()
 * has not been committed or rolled back.
 *
 * Returns: %TRUE if it is safe to call draft_project_redo()
 */
gboolean
draft_project_get_can_redo (DraftProject *self)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), FALSE);

  return self->transaction == NULL &&
         _draft_history_get_can_redo (self->history);
}

/**
 * draft_project_undo:
 * @self: a #DraftProject
 *
 * Undoes the last command committed with draft_project_commit().
 *
 * See draft_project_get_can_undo() to check when it is safe to
 * call this function.
 */
void
draft_project_undo (DraftProject *self)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (draft_project_get_can_undo (self));

  _draft_history_undo (self->history);
}

/**
 * draft_project_redo:
 * @self: a #DraftProject
 *
 * Redoes the next command in the redo stack. The redo stack is
 * empty until a command has bene undone with draft_project_undo().
 *
 * See draft_project_get_can_redo() to check when it is safe to
 * call this function.
 */
void
draft_project_redo (DraftProject *self)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (draft_project_get_can_redo (self));

  _draft_history_redo (self->history);
}

/**
 * draft_project_begin:
 *
 * Begins a new transaction to alter the state of the project.
 * Use the #DraftTransaction to make mutations to the tree or
 * properties of items within the tree.
 *
 * Then either commit or rollback the the transaction using
 * draft_project_commit() or draft_project_rollback().
 *
 * Note that changes created through #DraftTransaction are applied
 * immediately and therefore can be read back through the current
 * state of the tree as they are made.
 *
 * Returns: (transfer none): a #DraftTransaction
 */
DraftTransaction *
draft_project_begin (DraftProject *self)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), NULL);
  g_return_val_if_fail (self->transaction == NULL, NULL);

  self->transaction = _draft_transaction_new ();

  return self->transaction;
}

/**
 * draft_project_rollback:
 * @self: a #DraftProject
 *
 * Reverts an in-progress transaction created with draft_project_begin().
 */
void
draft_project_rollback (DraftProject  *self)
{
  g_autoptr(DraftTransaction) stolen = NULL;

  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (DRAFT_IS_TRANSACTION (self->transaction));

  stolen = g_steal_pointer (&self->transaction);

  _draft_transaction_revert (stolen);
}

/**
 * draft_project_commit:
 * @self: a #DraftProject
 *
 * Commits the in-progress transaction created with draft_project_begin().
 */
void
draft_project_commit (DraftProject  *self)
{
  g_autoptr(DraftTransaction) stolen = NULL;

  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (DRAFT_IS_TRANSACTION (self->transaction));

  stolen = g_steal_pointer (&self->transaction);

  if (draft_transaction_is_empty (stolen))
    return;

  _draft_history_push (self->history, stolen);
}

void
_draft_project_select_item (DraftProject *self,
                            DraftItem    *item)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (DRAFT_IS_ITEM (item));

  g_debug ("Selecting %s", G_OBJECT_TYPE_NAME (item));

  if (g_set_object (&self->selection, item))
    {
      for (guint i = 0; i < self->observers->len; i++)
        {
          DraftObserver *observer = g_ptr_array_index (self->observers, i);

          _draft_observer_select_item (observer, item);
        }
    }
}

void
_draft_project_clear_selection (DraftProject *self)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));

  g_debug ("Clearing selection");

  if (g_set_object (&self->selection, NULL))
    {
      for (guint i = 0; i < self->observers->len; i++)
        {
          DraftObserver *observer = g_ptr_array_index (self->observers, i);

          _draft_observer_clear_selection (observer);
        }
    }
}

void
draft_project_clear_selection (DraftProject *self)
{
  _draft_project_clear_selection (self);
}

gboolean
_draft_project_can_display (DraftItem *item)
{
  return DRAFT_IS_MENU (item) || DRAFT_IS_OBJECT (item);
}

void
_draft_project_add_observer (DraftProject  *self,
                             DraftObserver *observer)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (DRAFT_IS_OBSERVER (observer));

  g_ptr_array_add (self->observers, g_object_ref (observer));

  _draft_observer_set_context (observer, self->context);

  /* Notify the observer of all the documents */
  for (guint i = 0; i < self->documents->len; i++)
    {
      DraftDocument *document = g_ptr_array_index (self->documents, i);
      _draft_observer_document_added (observer, document);
    }

  /* Now notify of all the display items in the documents */
  for (guint i = 0; i < self->documents->len; i++)
    {
      DraftDocument *document = g_ptr_array_index (self->documents, i);

      for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (document));
           child != NULL;
           child = draft_item_get_next_sibling (child))
        {
          if (_draft_project_can_display (child))
            _draft_observer_add_display (observer, child);
        }
    }

  /* Now propagate selection to the observer */
  if (self->selection)
    _draft_observer_select_item (observer, self->selection);
  else
    _draft_observer_clear_selection (observer);
}

void
_draft_project_remove_observer (DraftProject  *self,
                                DraftObserver *observer)
{
  g_autoptr(DraftObserver) hold = NULL;

  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (DRAFT_IS_OBSERVER (observer));

  hold = g_object_ref (observer);

  if (g_ptr_array_remove (self->observers, observer))
    {
      /* First clear the selection */
      _draft_observer_clear_selection (observer);

      /* Now remove all of the display items from the documents */
      for (guint i = 0; i < self->documents->len; i++)
        {
          DraftDocument *document = g_ptr_array_index (self->documents, i);

          for (DraftItem *child = draft_item_get_first_child (DRAFT_ITEM (document));
               child != NULL;
               child = draft_item_get_next_sibling (child))
            {
              if (_draft_project_can_display (child))
                _draft_observer_remove_display (observer, child);
            }
        }

      /* Now remove all the documents for graceful cleanup */
      for (guint i = 0; i < self->documents->len; i++)
        {
          DraftDocument *document = g_ptr_array_index (self->documents, i);

          _draft_observer_document_removed (observer, document);
        }
    }

  _draft_observer_set_context (observer, NULL);
}

/**
 * draft_project_get_context:
 * @self: a #DraftProject
 *
 * Gets the #DraftProject:context property.
 *
 * The context can be used to customize how the `libdrafting-service` is
 * executed within the host system or development container.
 *
 * Returns: (transfer none): a #DraftProjectContext
 */
DraftProjectContext *
draft_project_get_context (DraftProject *self)
{
  g_return_val_if_fail (DRAFT_IS_PROJECT (self), NULL);

  return self->context;
}

void
draft_project_set_context (DraftProject        *self,
                           DraftProjectContext *context)
{
  g_return_if_fail (DRAFT_IS_PROJECT (self));
  g_return_if_fail (!context || DRAFT_IS_PROJECT_CONTEXT (context));

  if (g_set_object (&self->context, context))
    {
      g_autoptr(GError) error = NULL;
      DraftIpcService *service;

      if (self->context == NULL)
        self->context = draft_project_context_new ();

      if (self->type_cache != NULL)
        {
          draft_type_cache_drop (self->type_cache);
          g_clear_pointer (&self->type_cache, draft_type_cache_unref);
        }

      if (!(service = _draft_project_context_get_service (self->context, &error)))
        g_warning ("Failed to execute libdrafting-service: %s",
                   error->message);

      self->type_cache = draft_type_cache_new (service);

      DRAFT_OBSERVERS_FOREACH (self, {
        _draft_observer_set_context (observer, context);
      });

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONTEXT]);
    }
}
