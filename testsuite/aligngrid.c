#include <drafting.h>

#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "widgets/draft-alignment-grid-private.h"

int
main (int argc,
      char *argv[])
{
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(DraftProject) project = draft_project_new ();
  g_autoptr(DraftDocument) document = _draft_document_new (project);
  DraftObject *object;
  GtkWindow *window;
  DraftAlignmentGrid *grid;

  gtk_init ();
  draft_init ();

  object = draft_object_new (FALSE);
  draft_object_set_class_name (object, "GtkLabel");
  _draft_item_insert_after (DRAFT_ITEM (object), DRAFT_ITEM (document), NULL);

  window = g_object_new (GTK_TYPE_WINDOW,
                         "title", "Test Alignment Grid",
                         NULL);
  grid = g_object_new (DRAFT_TYPE_ALIGNMENT_GRID,
                       "margin-start", 24,
                       "margin-end", 24,
                       "margin-top", 24,
                       "margin-bottom", 24,
                       NULL);
  gtk_window_set_child (window, GTK_WIDGET (grid));
  draft_alignment_grid_set_object (grid, object);
  g_signal_connect_swapped (window,
                            "close-request",
                            G_CALLBACK (g_main_loop_quit),
                            main_loop);
  gtk_window_present (window);

  g_main_loop_run (main_loop);

  return 0;
}
