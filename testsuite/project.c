#include <drafting.h>

static DraftDocument *document;

static void
test_basic_load_cb (GObject      *object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  DraftProject *project = DRAFT_PROJECT (object);
  g_autoptr(GError) error = NULL;
  GMainLoop *main_loop = user_data;

  g_assert (DRAFT_IS_PROJECT (project));
  g_assert (G_IS_ASYNC_RESULT (result));

  document = draft_project_load_file_finish (project, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (document);

  g_main_loop_quit (main_loop);
}

static inline gpointer
ASSERT_NTH_TYPE (DraftItem *item,
                 guint      nth,
                 GType      type)
{
  DraftItem *child;

  g_assert (DRAFT_IS_ITEM (item));

  child = draft_item_get_first_child (item);
  g_assert_nonnull (child);
  g_assert_true (item == draft_item_get_parent (child));

  for (; nth > 0; nth--)
    {
      child = draft_item_get_next_sibling (child);
      g_assert_nonnull (child);
      g_assert_true (item == draft_item_get_parent (child));
    }

  g_assert_true (G_TYPE_CHECK_INSTANCE_TYPE (child, type));

  return child;
}

static void
test_basic (void)
{
  g_autoptr(DraftProject) project = draft_project_new ();
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(GFile) file = g_file_new_build_filename (g_getenv ("G_TEST_SRCDIR"), "test-document1.ui", NULL);
  DraftMenu *menu;
  DraftMenuLink *link;
  DraftMenuItem *item;
  DraftMenuAttribute *attr;
  DraftObject *tmpl;
  DraftObject *obj;
  DraftProperty *prop;
  DraftSignal *sig;
  DraftBinding *bind;
  DraftExpression *expr;
  DraftDependency *dep;
  DraftTreeColumns *cols;
  DraftTreeColumn *col;
  DraftStyle *style;
  DraftChild *child;
  DraftLayout *layout;
  DraftAccessibility *a11y;

#if 0
  /* Requires DISPLAY connection */
  g_autoptr(DraftProjectContext) context = draft_project_context_new ();
  draft_project_set_context (project, context);
#endif

  draft_project_load_file_async (project, file, NULL, test_basic_load_cb, main_loop);
  g_main_loop_run (main_loop);

  g_assert_cmpstr ("iface-domain", ==, draft_document_get_translation_domain (document));

  dep = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 0, DRAFT_TYPE_DEPENDENCY);
  g_assert_cmpstr ("gtk", ==, draft_dependency_get_library (dep));
  g_assert_cmpstr ("4.0", ==, draft_dependency_get_version (dep));

  tmpl = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 1, DRAFT_TYPE_OBJECT);
  g_assert_cmpstr ("MyWidget", ==, draft_object_get_class_name (tmpl));
  g_assert_cmpstr ("GtkWidget", ==, draft_object_get_parent_name (tmpl));
  g_assert_true (draft_object_get_is_template (tmpl));
  prop = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 0, DRAFT_TYPE_PROPERTY);
  g_assert_cmpstr ("key", ==, draft_property_get_name (prop));
  g_assert_cmpstr ("value", ==, draft_property_get_value (prop));
  prop = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 1, DRAFT_TYPE_PROPERTY);
  g_assert_cmpstr ("object", ==, draft_property_get_name (prop));
  g_assert_cmpstr (NULL, ==, draft_property_get_value (prop));
  obj = ASSERT_NTH_TYPE (DRAFT_ITEM (prop), 0, DRAFT_TYPE_OBJECT);
  sig = ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 0, DRAFT_TYPE_SIGNAL);
  g_assert_cmpstr ("notify", ==, draft_signal_get_name (sig));
  g_assert_cmpstr ("on_notify_cb", ==, draft_signal_get_handler (sig));
  g_assert_cmpstr ("MyWidget", ==, draft_signal_get_object (sig));
  g_assert_true (draft_signal_get_after (sig));
  g_assert_true (draft_signal_get_swapped (sig));
  prop = ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 1, DRAFT_TYPE_PROPERTY);
  g_assert_cmpstr ("subkey", ==, draft_property_get_name (prop));
  g_assert_cmpstr ("subvalue", ==, draft_property_get_value (prop));
  bind = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 2, DRAFT_TYPE_BINDING);
  g_assert_cmpstr ("other", ==, draft_binding_get_name (bind));
  expr = ASSERT_NTH_TYPE (DRAFT_ITEM (bind), 0, DRAFT_TYPE_EXPRESSION);
  g_assert_cmpstr ("key1", ==, draft_expression_get_name (expr));
  g_assert_cmpstr ("SomeType", ==, draft_expression_get_type_name (expr));
  g_assert_cmpstr (NULL, ==, draft_expression_get_value (expr));
  g_assert_cmpstr (NULL, ==, draft_expression_get_function (expr));
  expr = ASSERT_NTH_TYPE (DRAFT_ITEM (expr), 0, DRAFT_TYPE_EXPRESSION);
  g_assert_cmpstr ("subkey1", ==, draft_expression_get_name (expr));
  g_assert_cmpstr (NULL, ==, draft_expression_get_type_name (expr));
  g_assert_cmpstr (NULL, ==, draft_expression_get_value (expr));
  g_assert_cmpstr (NULL, ==, draft_expression_get_function (expr));
  style = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 3, DRAFT_TYPE_STYLE);
  g_assert_true (draft_style_has_css_class (style, "dim-label"));
  g_assert_true (draft_style_has_css_class (style, "title"));
  g_assert_false (draft_style_has_css_class (style, "subtitle"));
  child = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 4, DRAFT_TYPE_CHILD);
  obj = ASSERT_NTH_TYPE (DRAFT_ITEM (child), 0, DRAFT_TYPE_OBJECT);
  g_assert_cmpstr ("SomeWidget", ==, draft_object_get_class_name (obj));
  /* We can't resolve SomeWidget to a GtkWidget */
  ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 0, DRAFT_TYPE_XML_ITEM);
  /* But we can inside of the template as we know its a widget */
  layout = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 5, DRAFT_TYPE_LAYOUT);
  ASSERT_NTH_TYPE (DRAFT_ITEM (layout), 0, DRAFT_TYPE_PROPERTY);
  /* Make sure we swap out a11y items */
  a11y = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 6, DRAFT_TYPE_ACCESSIBILITY);
  ASSERT_NTH_TYPE (DRAFT_ITEM (a11y), 0, DRAFT_TYPE_PROPERTY);
  /* Make sure we parse <child><placeholder/></child> */
  child = ASSERT_NTH_TYPE (DRAFT_ITEM (tmpl), 7, DRAFT_TYPE_CHILD);
  ASSERT_NTH_TYPE (DRAFT_ITEM (child), 0, DRAFT_TYPE_PLACEHOLDER);

  menu = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 2, DRAFT_TYPE_MENU);
  g_assert_cmpstr ("my-domain", ==, draft_menu_get_translation_domain (menu));
  g_assert_cmpstr ("test-menu", ==, draft_item_get_id (DRAFT_ITEM (menu)));
  link = ASSERT_NTH_TYPE (DRAFT_ITEM (menu), 0, DRAFT_TYPE_MENU_LINK);
  g_assert_cmpstr ("section", ==, draft_menu_link_get_name (link));
  item = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 0, DRAFT_TYPE_MENU_ITEM);
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 0, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("label", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("the label", ==, draft_menu_attribute_get_value (attr));
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 1, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("action", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("the.action", ==, draft_menu_attribute_get_value (attr));
  link = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 1, DRAFT_TYPE_MENU_LINK);
  g_assert_cmpstr ("submenu", ==, draft_menu_link_get_name (link));
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 0, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("label", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("submenu 1", ==, draft_menu_attribute_get_value (attr));
  item = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 1, DRAFT_TYPE_MENU_ITEM);
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 0, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("label", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("submenu 1 item 1", ==, draft_menu_attribute_get_value (attr));
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 1, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("action", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("the.action2", ==, draft_menu_attribute_get_value (attr));
  link = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 2, DRAFT_TYPE_MENU_LINK);
  g_assert_cmpstr ("section", ==, draft_menu_link_get_name (link));
  item = ASSERT_NTH_TYPE (DRAFT_ITEM (link), 0, DRAFT_TYPE_MENU_ITEM);
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 0, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("label", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("submenu 1 section 1 item 1", ==, draft_menu_attribute_get_value (attr));
  attr = ASSERT_NTH_TYPE (DRAFT_ITEM (item), 1, DRAFT_TYPE_MENU_ATTRIBUTE);
  g_assert_cmpstr ("action", ==, draft_menu_attribute_get_name (attr));
  g_assert_cmpstr ("the.action3", ==, draft_menu_attribute_get_value (attr));

  obj = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 3, DRAFT_TYPE_OBJECT);
  g_assert_cmpstr ("treestore1", ==, draft_item_get_id (DRAFT_ITEM (obj)));
  cols = ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 0, DRAFT_TYPE_TREE_COLUMNS);
  col = ASSERT_NTH_TYPE (DRAFT_ITEM (cols), 0, DRAFT_TYPE_TREE_COLUMN);
  g_assert_cmpstr ("gchararray", ==, draft_tree_column_get_type_name (col));
  col = ASSERT_NTH_TYPE (DRAFT_ITEM (cols), 1, DRAFT_TYPE_TREE_COLUMN);
  g_assert_cmpstr ("GObject", ==, draft_tree_column_get_type_name (col));

  obj = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 4, DRAFT_TYPE_OBJECT);
  g_assert_cmpstr ("foo_get_type", ==, draft_object_get_type_func (obj));
  prop = ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 0, DRAFT_TYPE_PROPERTY);
  g_assert_cmpstr ("setting", ==, draft_property_get_name (prop));
  g_assert_cmpstr ("MyWidget", ==, draft_property_get_bind_source (prop));
  g_assert_cmpstr ("something", ==, draft_property_get_bind_property (prop));
  g_assert_cmpint (G_BINDING_INVERT_BOOLEAN|G_BINDING_SYNC_CREATE, ==, draft_property_get_bind_flags (prop));

  obj = ASSERT_NTH_TYPE (DRAFT_ITEM (document), 5, DRAFT_TYPE_OBJECT);
  ASSERT_NTH_TYPE (DRAFT_ITEM (obj), 0, DRAFT_TYPE_STYLE);

  g_assert_finalize_object (g_steal_pointer (&project));
  g_assert_finalize_object (g_steal_pointer (&document));
}

int
main (int argc,
      char *argv[])
{
  gtk_test_register_all_types ();
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Drafting/DocumentFormat/XML/basic", test_basic);
  return g_test_run ();
}
