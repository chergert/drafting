#include <drafting.h>

#include "items/draft-item-private.h"

typedef struct
{
  DraftItem *item;
  gulong handler;
  int position;
  int removed;
  int added;
} ItemsChanged;

#define ITEMS_CHANGED_INIT {NULL,0,-1,-1,-1}

static void
items_changed_cb (DraftItem    *item,
                  guint         position,
                  guint         removed,
                  guint         added,
                  ItemsChanged *ic)
{
  ic->position = position;
  ic->removed = removed;
  ic->added = added;
}

static void
items_changed_connect (ItemsChanged *ic,
                       DraftItem    *item)
{
  ic->item = item;
  ic->handler = g_signal_connect (item, "items-changed", G_CALLBACK (items_changed_cb), ic);
}

static void
items_changed_assert (ItemsChanged *ic,
                      int           position,
                      int           removed,
                      int           added)
{
  g_assert_cmpint (ic->position, ==, position);
  g_assert_cmpint (ic->removed, ==, removed);
  g_assert_cmpint (ic->added, ==, added);
}

static void
test_item_list_model (void)
{
  g_autoptr(DraftObject) tmpl = draft_object_new (TRUE);
  g_autoptr(DraftObject) obj = draft_object_new (FALSE);
  g_autoptr(DraftObject) obj2 = draft_object_new (FALSE);
  g_autoptr(DraftObject) obj3 = draft_object_new (FALSE);
  ItemsChanged ic = ITEMS_CHANGED_INIT;

  items_changed_connect (&ic, DRAFT_ITEM (tmpl));

  _draft_item_insert_after (g_object_ref (DRAFT_ITEM (obj)), DRAFT_ITEM (tmpl), NULL);
  items_changed_assert (&ic, 0, 0, 1);

  _draft_item_insert_after (g_object_ref (DRAFT_ITEM (obj2)), DRAFT_ITEM (tmpl), DRAFT_ITEM (obj));
  items_changed_assert (&ic, 1, 0, 1);

  _draft_item_insert_before (g_object_ref (DRAFT_ITEM (obj3)), DRAFT_ITEM (tmpl), DRAFT_ITEM (obj2));
  items_changed_assert (&ic, 1, 0, 1);

  _draft_item_unparent(DRAFT_ITEM (obj3));
  items_changed_assert (&ic, 1, 1, 0);

  _draft_item_unparent(DRAFT_ITEM (obj));
  items_changed_assert (&ic, 0, 1, 0);

  _draft_item_unparent(DRAFT_ITEM (obj2));
  items_changed_assert (&ic, 0, 1, 0);

  g_assert_finalize_object (g_steal_pointer (&tmpl));
  g_assert_finalize_object (g_steal_pointer (&obj));
  g_assert_finalize_object (g_steal_pointer (&obj2));
  g_assert_finalize_object (g_steal_pointer (&obj3));
}

int
main (int argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Draft/Item/list_model", test_item_list_model);
  return g_test_run ();
}
