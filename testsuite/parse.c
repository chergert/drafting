#include <drafting.h>

#include "items/draft-item-private.h"

static DraftDocument *document;

static void
parse_cb (GObject      *object,
          GAsyncResult *result,
          gpointer      user_data)
{
  DraftProject *project = DRAFT_PROJECT (object);
  g_autoptr(GError) error = NULL;
  GMainLoop *main_loop = user_data;
  gboolean r;

  g_assert (DRAFT_IS_PROJECT (project));
  g_assert (G_IS_ASYNC_RESULT (result));

  document = draft_project_load_file_finish (project, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (document);

  g_main_loop_quit (main_loop);
}

static gboolean cmdline_preview;
static const GOptionEntry entries[] = {
  { "preview", 'p', 0, G_OPTION_ARG_NONE, &cmdline_preview, "Generate XML suitable for preview" },
  { 0 }
};

int
main (int argc,
      char *argv[])
{
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(GOptionContext) context = g_option_context_new ("- parse documents");
  g_autoptr(DraftProject) project = draft_project_new ();
  g_autoptr(DraftProjectContext) pcontext = draft_project_context_new ();
  g_autoptr(GPtrArray) documents = g_ptr_array_new_with_free_func (g_object_unref);
  g_autoptr(GError) error = NULL;

  gtk_test_register_all_types ();

  g_option_context_add_main_entries (context, entries, NULL);
  draft_project_set_context (project, pcontext);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  for (guint i = 1; i < argc; i++)
    {
      g_autoptr(GFile) file = g_file_new_for_commandline_arg (argv[i]);

      g_message ("Parsing %s\n", g_file_peek_path (file));
      draft_project_load_file_async (project, file, NULL, parse_cb, main_loop);
      g_main_loop_run (main_loop);

      if (document != NULL)
        g_ptr_array_add (documents, g_steal_pointer (&document));
    }

  for (guint i = 0; i < documents->len; i++)
    {
      DraftDocument *d = g_ptr_array_index (documents, i);
      g_autoptr(GString) string = g_string_new (NULL);

      _draft_item_save_to_xml (DRAFT_ITEM (d), string, project, cmdline_preview);

      g_print ("%s\n", string->str);
    }

  g_clear_pointer (&documents, g_ptr_array_unref);
  g_assert_finalize_object (g_steal_pointer (&project));

  return 0;
}
