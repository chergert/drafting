#include <drafting.h>

#include "introspection/draft-type-info-private.h"

static void
test_enum_info (void)
{
  DraftEnumInfo *e = draft_enum_info_new (G_TYPE_DATA_STREAM_NEWLINE_TYPE);

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);

  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftEnumInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_cmpstr (e->name, ==, d->name);
  g_assert_cmpint (e->n_values, ==, d->n_values);

  for (guint i = 0; i < e->n_values; i++)
    {
      g_assert_cmpint (e->values[i].value, ==, d->values[i].value);
      g_assert_cmpstr (e->values[i].nick, ==, d->values[i].nick);
      g_assert_cmpstr (e->values[i].name, ==, d->values[i].name);
    }

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_variant_unref (v);
}

static void
test_flags_info (void)
{
  DraftFlagsInfo *e = draft_flags_info_new (G_TYPE_BINDING_FLAGS);

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);

  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftFlagsInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_cmpstr (e->name, ==, d->name);
  g_assert_cmpint (e->n_values, ==, d->n_values);

  for (guint i = 0; i < e->n_values; i++)
    {
      g_assert_cmpint (e->values[i].value, ==, d->values[i].value);
      g_assert_cmpstr (e->values[i].nick, ==, d->values[i].nick);
      g_assert_cmpstr (e->values[i].name, ==, d->values[i].name);
    }

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_variant_unref (v);
}

static void
test_signal_info (void)
{
  GTypeClass *klass = g_type_class_ref (G_TYPE_LIST_STORE);
  guint signal_id = g_signal_lookup ("items-changed", G_TYPE_LIST_STORE);
  DraftSignalInfo *e = draft_signal_info_new (signal_id);

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);
  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftSignalInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_cmpstr (e->name, ==, d->name);

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_variant_unref (v);
  g_type_class_unref (klass);
}

static void
test_property_info (void)
{
  GObjectClass *klass = g_type_class_ref (G_TYPE_BINDING);
  GParamSpec *pspec = g_object_class_find_property (klass, "source");
  DraftPropertyInfo *e = draft_property_info_new (pspec);

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);
  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftPropertyInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_nonnull (d);
  g_assert_cmpstr (e->name, ==, d->name);
  g_assert_cmpstr (e->type_name, ==, d->type_name);
  g_assert_cmpstr (e->type_name, ==, "GObject");

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_type_class_unref (klass);
  g_variant_unref (v);
}

static void
test_class_info (void)
{
  DraftClassInfo *e = draft_class_info_new (G_TYPE_BINDING);
  g_assert_nonnull (e);

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);
  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftClassInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_nonnull (d);
  g_assert_cmpstr (e->name, ==, d->name);
  g_assert_cmpstr (e->parent_name, ==, d->parent_name);
  g_assert_cmpstr (e->name, ==, "GBinding");
  g_assert_cmpstr (e->parent_name, ==, "GObject");

  g_assert_nonnull (e->properties);
  g_assert_nonnull (d->properties);
  g_assert_cmpint (e->properties->len, ==, d->properties->len);

  for (guint i = 0; i < e->properties->len; i++)
    {
      DraftPropertyInfo *p1 = g_ptr_array_index (e->properties, 0);
      DraftPropertyInfo *p2 = g_ptr_array_index (d->properties, 0);

      g_assert_cmpstr (p1->name, ==, p2->name);
      g_assert_cmpstr (p1->type_name, ==, p2->type_name);
      g_assert_cmpint (p1->flags, ==, p2->flags);
    }

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_variant_unref (v);
}

static void
test_class_interfaces (void)
{
  DraftClassInfo *e = draft_class_info_new (GTK_TYPE_WINDOW);
  g_assert_nonnull (e);

  g_assert_true (draft_type_info_is_of_type ((DraftTypeInfo *)e, "GtkNative"));

  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));
  draft_type_info_serialize ((DraftTypeInfo *)e, &builder);
  GVariant *v = g_variant_builder_end (&builder);

  GVariant *child = g_variant_get_child_value (v, 0);
  DraftClassInfo *d = draft_type_info_deserialize (child);
  g_variant_unref (child);

  g_assert_nonnull (d);
  g_assert_cmpstr (e->name, ==, d->name);
  g_assert_cmpstr (e->parent_name, ==, d->parent_name);
  g_assert_cmpstr (e->name, ==, "GtkWindow");
  g_assert_cmpstr (e->parent_name, ==, "GtkWidget");

  g_assert_true (draft_type_info_is_of_type ((DraftTypeInfo *)d, "GtkNative"));

  draft_type_info_unref (e);
  draft_type_info_unref (d);
  g_variant_unref (v);
}

int
main (int argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Draft/EnumInfo/basic", test_enum_info);
  g_test_add_func ("/Draft/FlagsInfo/basic", test_flags_info);
  g_test_add_func ("/Draft/SignalInfo/basic", test_signal_info);
  g_test_add_func ("/Draft/PropertyInfo/basic", test_property_info);
  g_test_add_func ("/Draft/ClassInfo/basic", test_class_info);
  g_test_add_func ("/Draft/ClassInfo/interfaces", test_class_interfaces);
  return g_test_run ();
}
