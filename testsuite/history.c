#include <drafting.h>

#include "items/draft-document-private.h"
#include "items/draft-item-private.h"
#include "undo/draft-history-private.h"
#include "undo/draft-transaction-private.h"

static void
test_history_basic (void)
{
  g_autoptr(DraftDocument) document = _draft_document_new (NULL);
  g_autoptr(DraftProject) project = draft_project_new ();
  g_autoptr(DraftHistory) history = _draft_history_new (project);
  g_autoptr(DraftTransaction) transaction1 = _draft_transaction_new ();
  g_autoptr(DraftTransaction) transaction2 = _draft_transaction_new ();
  g_autoptr(DraftObject) object = draft_object_new (TRUE);
  g_autoptr(DraftObject) object2 = draft_object_new (FALSE);

  g_assert_false (_draft_history_get_can_undo (history));
  g_assert_false (_draft_history_get_can_redo (history));

  draft_transaction_insert_before (transaction1, DRAFT_ITEM (object), DRAFT_ITEM (document), NULL);
  draft_transaction_insert_after (transaction1, DRAFT_ITEM (object2), DRAFT_ITEM (document), DRAFT_ITEM (object));
  _draft_history_push (history, transaction1);

  g_assert_true (_draft_history_get_can_undo (history));
  g_assert_false (_draft_history_get_can_redo (history));

  draft_transaction_remove (transaction2, DRAFT_ITEM (object));
  _draft_history_push (history, transaction2);

  g_assert_true (_draft_history_get_can_undo (history));
  g_assert_false (_draft_history_get_can_redo (history));

  g_assert_null (draft_item_get_parent (DRAFT_ITEM (object)));
  g_assert_null (draft_item_get_previous_sibling (DRAFT_ITEM (object)));
  g_assert_null (draft_item_get_next_sibling (DRAFT_ITEM (object)));
  g_assert_null (draft_item_get_previous_sibling (DRAFT_ITEM (object2)));
  g_assert_null (draft_item_get_next_sibling (DRAFT_ITEM (object2)));

  _draft_history_undo (history);

  g_assert_true (_draft_history_get_can_undo (history));
  g_assert_true (_draft_history_get_can_redo (history));

  g_assert_nonnull (draft_item_get_parent (DRAFT_ITEM (object)));
  g_assert_null (draft_item_get_previous_sibling (DRAFT_ITEM (object)));
  g_assert_nonnull (draft_item_get_next_sibling (DRAFT_ITEM (object)));
  g_assert_nonnull (draft_item_get_previous_sibling (DRAFT_ITEM (object2)));
  g_assert_null (draft_item_get_next_sibling (DRAFT_ITEM (object2)));

  g_assert_finalize_object (g_steal_pointer (&history));
  g_assert_finalize_object (g_steal_pointer (&transaction1));
  g_assert_finalize_object (g_steal_pointer (&transaction2));
  g_assert_finalize_object (g_steal_pointer (&document));
  g_assert_finalize_object (g_steal_pointer (&object));
  g_assert_finalize_object (g_steal_pointer (&object2));
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Drafting/History/basic", test_history_basic);
  return g_test_run ();
}
