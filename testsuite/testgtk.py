#!/usr/bin/env python3

import sys
import os

for filename in sys.argv[1:]:
    if not filename.endswith('.ui'):
        print('Ignoring file', filename, 'because it does not end in .ui')
        continue

    # Ignore tests that are expected to fail. We are lazier than
    # GTK so we can be non-destructive in cases where it cannot.
    expected = filename[:filename.rindex('.')] + '.expected'
    if os.path.exists(expected):
        contents = open(expected).read()
        if contents.strip() != 'SUCCESS':
            continue

    # Ignore the old merge ui files, which are not supported
    if 'merge-' in filename:
        continue

    if os.system('./parse "%s"' % filename) != 0:
        break
