#include <drafting.h>

#include "items/draft-document-private.h"
#include "undo/draft-transaction-private.h"

static void
test_transaction_basic (void)
{
  g_autoptr(DraftDocument) document = _draft_document_new (NULL);
  g_autoptr(DraftTransaction) transaction = _draft_transaction_new ();
  g_autoptr(DraftItem) item1 = DRAFT_ITEM (draft_object_new (FALSE));
  g_autoptr(DraftItem) item2 = DRAFT_ITEM (draft_object_new (FALSE));

  draft_transaction_insert_before (transaction, item1, DRAFT_ITEM (document), NULL);
  g_assert_true (draft_item_get_first_child (DRAFT_ITEM (document)) == item1);

  draft_transaction_insert_after (transaction, item2, DRAFT_ITEM (document), item1);
  g_assert_true (draft_item_get_first_child (DRAFT_ITEM (document)) == item1);
  g_assert_true (draft_item_get_last_child (DRAFT_ITEM (document)) == item2);

  draft_transaction_set_item (transaction, item1, "id", "abc", NULL);
  g_assert_cmpstr ("abc", ==, draft_item_get_id (item1));

  draft_transaction_set_item (transaction, item2, "id", "def", NULL);
  g_assert_cmpstr ("def", ==, draft_item_get_id (item2));

  draft_transaction_remove (transaction, item2);
  g_assert_true (draft_item_get_first_child (DRAFT_ITEM (document)) == item1);
  g_assert_true (draft_item_get_last_child (DRAFT_ITEM (document)) == item1);

  draft_transaction_remove (transaction, item1);
  g_assert_null (draft_item_get_first_child (DRAFT_ITEM (document)));
  g_assert_null (draft_item_get_last_child (DRAFT_ITEM (document)));

  _draft_transaction_revert (transaction);

  g_assert_cmpstr (NULL, ==, draft_item_get_id (item1));
  g_assert_cmpstr (NULL, ==, draft_item_get_id (item2));

  _draft_transaction_apply (transaction);
}

int
main (int argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  gtk_test_register_all_types ();
  g_test_add_func ("/Draft/Transaction/basic", test_transaction_basic);
  return g_test_run ();
}
